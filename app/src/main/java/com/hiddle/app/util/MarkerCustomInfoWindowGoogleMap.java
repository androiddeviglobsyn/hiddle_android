package com.hiddle.app.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.View;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.hiddle.app.R;
import com.hiddle.app.activity.MapActivity;
import com.hiddle.app.model.InfoWindowData;

import de.hdodenhof.circleimageview.CircleImageView;

public class MarkerCustomInfoWindowGoogleMap implements GoogleMap.InfoWindowAdapter {

    private Context context;
    @SuppressLint("StaticFieldLeak")

    MapActivity mapActivity = new MapActivity();
    ;

    public MarkerCustomInfoWindowGoogleMap(Context ctx) {
        context = ctx;
    }

    @Override
    public View getInfoWindow(Marker marker) {

        View view = ((Activity) context).getLayoutInflater()
                .inflate(R.layout.custom_map_circle_marker, null);
        CircleImageView user_dp = view.findViewById(R.id.user_dp);
        /*AppCompatTextView tvAddress = view.findViewById(R.id.tvAddress);
        LinearLayout  llsetLocation = view.findViewById(R.id.llsetLocation);*/

        Glide.with(context).load(marker.getSnippet()).placeholder(R.drawable.ic_placeholder_icn).into(user_dp);


       /* tvName.setText(marker.getTitle());
        tvAddress.setText(marker.getSnippet());*/

      /* llsetLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Log.e("map","map");
            }
        });*/

        InfoWindowData infoWindowData = (InfoWindowData) marker.getTag();

      /*  int imageId = context.getResources().getIdentifier(infoWindowData.getImage().toLowerCase(),
                "drawable", context.getPackageName());
        img.setImageResource(imageId);

        hotel_tv.setText(infoWindowData.getHotel());
        food_tv.setText(infoWindowData.getFood());
        transport_tv.setText(infoWindowData.getTransport());*/

        return view;
    }

    @Override
    public View getInfoContents(Marker marker) {

      /*  View view = ((Activity) context).getLayoutInflater()
                .inflate(R.layout.custom_map_window, null);
        AppCompatTextView tvName = view.findViewById(R.id.tvName);
        AppCompatTextView tvAddress = view.findViewById(R.id.tvAddress);
        llsetLocation = view.findViewById(R.id.llsetLocation);


        tvName.setText(marker.getTitle());
        tvAddress.setText(marker.getSnippet());

        *//*llsetLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mapActivity.addLocationi_FilterLocation();
            }
        });*//*

        InfoWindowData infoWindowData = (InfoWindowData) marker.getTag();

      *//*  int imageId = context.getResources().getIdentifier(infoWindowData.getImage().toLowerCase(),
                "drawable", context.getPackageName());
        img.setImageResource(imageId);

        hotel_tv.setText(infoWindowData.getHotel());
        food_tv.setText(infoWindowData.getFood());
        transport_tv.setText(infoWindowData.getTransport());*//*

        return view;*/

        return null;
    }
}
 