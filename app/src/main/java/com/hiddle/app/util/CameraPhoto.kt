package com.hiddle.app.util

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import androidx.core.content.FileProvider
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class CameraPhoto(private val context: Context) {
    val TAG = this.javaClass.simpleName
    var photoPath: String? = null
        private set

    @Throws(IOException::class)
    fun takePhotoIntent(): Intent {
        val `in` = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        // Ensure that there's a camera activity to handle the intent
        if (`in`.resolveActivity(context.packageManager) != null) { // Create the File where the photo should go
            val photoFile = createImageFile()
            // Continue only if the File was successfully created
            if (photoFile != null) {
                val contentUri: Uri
                contentUri = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    FileProvider.getUriForFile(context, context.packageName + ".provider", photoFile)
                } else {
                    Uri.fromFile(photoFile)
                }
                `in`.putExtra(MediaStore.EXTRA_OUTPUT, contentUri)
            }
        }
        return `in`
    }

    @Throws(IOException::class)
    private fun createImageFile(): File { // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",  /* suffix */
                storageDir /* directory */
        )
        // Save a file: path for use with ACTION_VIEW intents
        photoPath = image.absolutePath
        return image
    }

    fun addToGallery() {
        val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
        val f = File(photoPath)
        val contentUri = Uri.fromFile(f)
        mediaScanIntent.data = contentUri
        context.sendBroadcast(mediaScanIntent)
    }

}