package com.hiddle.app.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import androidx.appcompat.widget.AppCompatTextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.hiddle.app.R;
import com.hiddle.app.activity.MapActivity;
import com.hiddle.app.model.InfoWindowData;

public class CustomInfoWindowGoogleMap implements GoogleMap.InfoWindowAdapter  {

    private Context context;
    @SuppressLint("StaticFieldLeak")

    MapActivity mapActivity = new MapActivity();
   ;

    public CustomInfoWindowGoogleMap(Context ctx) {
        context = ctx;
    }

    @Override
    public View getInfoWindow(Marker marker) {

        View view = ((Activity) context).getLayoutInflater()
                .inflate(R.layout.custom_map_window, null);
        AppCompatTextView tvName = view.findViewById(R.id.tvName);
        AppCompatTextView tvAddress = view.findViewById(R.id.tvAddress);
        LinearLayout  llsetLocation = view.findViewById(R.id.llsetLocation);

        tvName.setText(marker.getTitle());
        tvAddress.setText(marker.getSnippet());

       llsetLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Log.e("map","map");
            }
        });

        InfoWindowData infoWindowData = (InfoWindowData) marker.getTag();

      /*  int imageId = context.getResources().getIdentifier(infoWindowData.getImage().toLowerCase(),
                "drawable", context.getPackageName());
        img.setImageResource(imageId);

        hotel_tv.setText(infoWindowData.getHotel());
        food_tv.setText(infoWindowData.getFood());
        transport_tv.setText(infoWindowData.getTransport());*/

        return view;
    }

    @Override
    public View getInfoContents(Marker marker) {

      /*  View view = ((Activity) context).getLayoutInflater()
                .inflate(R.layout.custom_map_window, null);
        AppCompatTextView tvName = view.findViewById(R.id.tvName);
        AppCompatTextView tvAddress = view.findViewById(R.id.tvAddress);
        llsetLocation = view.findViewById(R.id.llsetLocation);


        tvName.setText(marker.getTitle());
        tvAddress.setText(marker.getSnippet());

        *//*llsetLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mapActivity.addLocationi_FilterLocation();
            }
        });*//*

        InfoWindowData infoWindowData = (InfoWindowData) marker.getTag();

      *//*  int imageId = context.getResources().getIdentifier(infoWindowData.getImage().toLowerCase(),
                "drawable", context.getPackageName());
        img.setImageResource(imageId);

        hotel_tv.setText(infoWindowData.getHotel());
        food_tv.setText(infoWindowData.getFood());
        transport_tv.setText(infoWindowData.getTransport());*//*

        return view;*/

        return null;
    }
}
 