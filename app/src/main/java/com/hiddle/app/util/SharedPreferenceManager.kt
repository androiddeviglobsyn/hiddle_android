package com.hiddle.app.util

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.hiddle.app.Hiddleapp
import com.hiddle.app.retrofit.RestConstant

class SharedPreferenceManager(context: Context?, gson: Gson) {
    private val SP_NAME = "Hiddle_Prefs"
    private var sharedPreferences: SharedPreferences
    private var editor: SharedPreferences.Editor
    private var gson: Gson


    fun clearPreferences() {
        editor.clear().apply()
    }

    fun <T> putObject(key: String?, value: T) {
        val editor = sharedPreferences.edit()
        editor.putString(key, gson.toJson(value))
        editor.apply()
    }

    fun <T> getObject(key: String?, clazz: Class<T>?): T {
        return gson.fromJson(getString(key, null), clazz)
    }

    fun <T> putList(key: String?, list: List<T>?) {
        val editor = sharedPreferences.edit()
        editor.putString(key, gson.toJson(list))
        editor.apply()
    }

    fun <T> getList(key: String?, clazz: Class<T>?): List<T> {
        val typeOfT = TypeToken.getParameterized(MutableList::class.java, clazz).type
        return gson.fromJson(getString(key, null), typeOfT)
    }

    fun <T> putArray(key: String?, arrays: Array<T>?) {
        val editor = sharedPreferences.edit()
        editor.putString(key, gson.toJson(arrays))
        editor.apply()
    }

    fun <T> getArray(key: String?, clazz: Class<Array<T>?>?): Array<T> {
        return gson.fromJson(getString(key, null), clazz)!!
    }

    fun removeKey(key: String?) {
        val editor = sharedPreferences.edit()
        if (editor != null) {
            editor.remove(key)
            editor.apply()
        }
    }

    fun getString(key: String?, defaultValue: String?): String? {
        return sharedPreferences.getString(key, defaultValue)
    }

    companion object {
        private var mySharedPreferences: SharedPreferenceManager? = null
        fun getMySharedPreferences(): SharedPreferenceManager? {
            if (mySharedPreferences == null) {
                mySharedPreferences = SharedPreferenceManager(Hiddleapp.getHiddleApplication(), Gson())
            }
            return mySharedPreferences
        }
    }

    fun SharedPreferenceManager(context: Context) {
        sharedPreferences = context.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
    }


    init {
        sharedPreferences = context!!.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
        this.gson = gson
    }



    fun getMySharedPreferences(): SharedPreferenceManager? {
        if (mySharedPreferences == null) {
            mySharedPreferences = SharedPreferenceManager(Hiddleapp.getHiddleApplication(), Gson())
        }
        return mySharedPreferences
    }

    fun isLogin(): Boolean {
        return sharedPreferences.getBoolean(RestConstant.IS_LOGIN, false)
    }

    fun setLogin(isLogin: Boolean) {
        editor.putBoolean(RestConstant.IS_LOGIN, isLogin).apply()
    }

    fun getDeviceId(): String? {
        return mySharedPreferences?.getString(RestConstant.DEVICE_ID, "")
    }

    fun setDeviceId(deviceid: String) {
        editor.putString(RestConstant.DEVICE_ID, deviceid).apply()
    }

    fun getUserId(): String? {
        return mySharedPreferences?.getString(RestConstant.USER_ID, "")
    }

    fun setUserId(userid: String) {
        editor.putString(RestConstant.USER_ID, userid).apply()
    }

    fun getFcmToken(): String? {
        return mySharedPreferences?.getString(RestConstant.FCM_TOKEN, "")
    }

    fun setFcmToken(fcm_token: String) {
        editor.putString(RestConstant.FCM_TOKEN, fcm_token).apply()
    }



    fun getUserName(): String? {
        return mySharedPreferences?.getString(RestConstant.USER_NAME, "")
    }

    fun setUserName(username: String) {
        editor.putString(RestConstant.USER_NAME, username).apply()
    }


    fun getCountryCode(): String? {
        return mySharedPreferences?.getString(RestConstant.COUNTRY_CODE, "")
    }

    fun setCountryCode(country_code: String) {
        editor.putString(RestConstant.COUNTRY_CODE,country_code).apply()
    }

    fun getMobileNumber(): String? {
        return mySharedPreferences?.getString(RestConstant.MOBILE_NUMBER, "")
    }

    fun setMobileNumber(mobile_number: String) {
        editor.putString(RestConstant.MOBILE_NUMBER,mobile_number).apply()
    }

    fun getAuthorizationToken(): String? {
        return mySharedPreferences?.getString(RestConstant.AUTHORIZATION_TOKEN, "")
    }

    fun setAuthorizationToken(authorization_token: String) {
        editor.putString(RestConstant.AUTHORIZATION_TOKEN,authorization_token).apply()
    }

    fun getEmail(): String? {
        return mySharedPreferences?.getString(RestConstant.EMAIL, "")
    }

    fun setEmail(email: String) {
        editor.putString(RestConstant.EMAIL,email).apply()
    }

    fun getProfileImage(): String? {
        return mySharedPreferences?.getString(RestConstant.PROFILE_IMAGE, "")
    }

    fun setProfileImage(profileImage: String) {
        editor.putString(RestConstant.PROFILE_IMAGE,profileImage).apply()
    }

    fun getLanguage(): String? {
        return mySharedPreferences?.getString(RestConstant.LANGUAGE, "")
    }

    fun setLanguahe(language: String) {
        editor.putString(RestConstant.LANGUAGE,language).apply()
    }

    fun getNotificationUser(): String? {
        return mySharedPreferences?.getString(RestConstant.NOTIFICATON_USER, "")
    }

    fun setNotificationUser(notificationUser: String) {
        editor.putString(RestConstant.NOTIFICATON_USER,notificationUser).apply()
    }

    fun getSound(): String? {
        return mySharedPreferences?.getString(RestConstant.SOUND, "1")
    }

    fun setSound(sound: String) {
        editor.putString(RestConstant.SOUND,sound).apply()
    }

    fun getVibrate(): String? {
        return mySharedPreferences?.getString(RestConstant.VIBRATE, "")
    }

    fun setVibrate(vibrate: String) {
        editor.putString(RestConstant.VIBRATE,vibrate).apply()
    }

    fun getDistance(): String? {
        return mySharedPreferences?.getString(RestConstant.DISTANCE, "")
    }

    fun setDistance(distance: String) {
        editor.putString(RestConstant.DISTANCE,distance).apply()
    }
}