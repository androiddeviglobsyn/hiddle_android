package com.hiddle.app.util;

import android.content.Context;

import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.viewpager.widget.ViewPager;

public class CustomViewPager extends ViewPager {

    public CustomViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return false;
    }


    //set badge count
    //tabs!!.getTabAt(1)!!.setCustomView(R.layout.item_notification_badge_main) // for set badge
    //val txtBadge = tabs!!.getTabAt(1)!!.customView!!.findViewById<View>(R.id.notificationsBadge) as AppCompatTextView
    //txtBadge.text = "4"
}