package com.hiddle.app.util
object Constants {
  val KEY_ANIM_TYPE = "anim_type"
  val KEY_TITLE = "anim_title"
  enum class TransitionType {
    ExplodeJava, ExplodeXML, SlideJava, SlideXML, FadeJava, FadeXML
  }
}