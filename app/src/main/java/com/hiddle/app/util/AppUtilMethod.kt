package com.hiddle.app.util

import android.content.Context
import android.location.Address
import android.location.Geocoder
import android.util.Log
import com.hiddle.app.R
import kotlinx.android.synthetic.main.activity_map.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

object AppUtilMethod {
    var geocoder: Geocoder? = null
    var addresses: List<Address> = ArrayList()
    public fun getDifferenecOfTwoDate(context : Context, dateFromServer: String): String {

        //convert date one format to another format
        var originalFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH)
        var targetFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        var date = originalFormat.parse(dateFromServer)
        var formattedServerDate = targetFormat.format(date) // 20120821
        Log.e("format", formattedServerDate)

        //get current date in GMT
        val dateFormatGmt = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        dateFormatGmt.setTimeZone(TimeZone.getTimeZone("GMT"))
        var currentDate :String= dateFormatGmt.format(Date())
        Log.e("current_date", currentDate)

        //Convert string into date
        var dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        var start_server_date = dateFormat.parse(formattedServerDate)
        var end_current_date = dateFormat.parse(currentDate)

        //add 6 hours to server start date
        start_server_date.setTime(start_server_date.getTime() + TimeUnit.HOURS.toMillis(6));
        //convert date format to another after adding 6 hours
        var originalFormat1: DateFormat = SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy", Locale.ENGLISH)
        var targetFormat1: DateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        var date1 = originalFormat1.parse(start_server_date.toString())
        var formattedServerDate1: String = targetFormat1.format(date1) // 20120821

        //convert string into date
        var dateFormat1 = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        start_server_date = dateFormat1.parse(formattedServerDate1)
        Log.e("format", start_server_date.toString())

        //get difference between current and start date
        var difference_date = printDifference(context,end_current_date,start_server_date)
        Log.e("diff", difference_date)
        return difference_date
    }

    fun printDifference(context : Context,startDate: Date, endDate: Date): String {
        //milliseconds
        var different = endDate.getTime() - startDate.getTime()
        val secondsInMilli: Long = 1000
        val minutesInMilli = secondsInMilli * 60
        val hoursInMilli = minutesInMilli * 60
        val daysInMilli = hoursInMilli * 24

        val elapsedHours = different / hoursInMilli
        different = different % hoursInMilli
        val elapsedMinutes = different / minutesInMilli
        different = different % minutesInMilli
        val elapsedSeconds = different / secondsInMilli

        var diff: String? = null
        var hrs: String? = null
        var mn: String? = null
        if (elapsedHours.toString().length == 1) {
            hrs = "0" + elapsedHours.toString()
        } else {
            hrs = elapsedHours.toString()
        }
        if (elapsedMinutes.toString().length == 1) {
            mn = "0" + elapsedMinutes.toString()
        } else {
            mn = elapsedMinutes.toString()
        }
        if (elapsedHours.toString() == "0") {
            diff = " " + mn + " " + context.getString(R.string.min)/*+ ":" + elapsedSeconds*/
        } else {
            diff = "" + hrs + " " + context.getString(R.string.hour) + " " + mn + " " + context.getString(R.string.min)/*+ ":" + elapsedSeconds*/
        }
        return diff
    }

    fun getAddress(LATITUDE: Double, LONGITUDE: Double,context : Context?): String {
        var address = ""
        try {
            geocoder = Geocoder(context, Locale.ENGLISH)
            addresses = geocoder!!.getFromLocation(LATITUDE, LONGITUDE, 1) // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            Log.e("Address", addresses[0].toString())
            if (addresses.isNotEmpty()) {

                if (addresses[0].locality != null) {
                    if (addresses[0].locality == addresses[0].adminArea) {
                        address = addresses[0].adminArea + ", " + addresses[0].countryName
                    } else {
                        address = addresses[0].locality + ", " + addresses[0].adminArea + ", " + addresses[0].countryName
                    }
                } else {
                    address = addresses[0].adminArea + ", " + addresses[0].countryName
                }
                Log.e("Name", address)

            }

            Log.e("Address", addresses[0].toString())


            if (address == "") {
                /*if (addresses[0].getAddressLine(0) != null) {
                    address = addresses[0].getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                }*/
            }

        } catch (e: Exception) {

        }
        return address

    }
}