package com.hiddle.app.util
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.ViewPager;

class VerticalViewPager:ViewPager {
    constructor(context:Context) : super(context) {
        init()
    }
    constructor(context:Context, attrs:AttributeSet) : super(context, attrs) {
        init()
    }
    private fun init() {
        // The majority of the magic happens here
        setPageTransformer(true, VerticalPageTransformer())
        // The easiest way to get rid of the overscroll drawing that happens on the left and right
        setOverScrollMode(OVER_SCROLL_NEVER)
    }
    private inner class VerticalPageTransformer:ViewPager.PageTransformer {
        override fun transformPage(@NonNull view:View, position:Float) {
            if (position < -1)
            { // [-Infinity,-1)
                // This page is way off-screen to the left.
                view.setAlpha(0F)
            }
            else if (position <= 1)
            { // [-1,1]
                view.setAlpha(1F)
                // Counteract the default slide transition
                view.setTranslationX(view.getWidth() * -position)
                //set Y position to swipe in from top
                val yPosition = position * view.getHeight()
                view.setTranslationY(yPosition)
            }
            else
            { // (1,+Infinity]
                // This page is way off-screen to the right.
                view.setAlpha(0F)
            }
        }
    }
    /**
     * Swaps the X and Y coordinates of your touch event.
     */
    private fun swapXY(ev:MotionEvent):MotionEvent {
        val width = getWidth()
        val height = getHeight()
        val newX = (ev.getY() / height) * width
        val newY = (ev.getX() / width) * height
        ev.setLocation(newX, newY)
        return ev
    }
    override fun onInterceptTouchEvent(ev:MotionEvent):Boolean {
        val intercepted = super.onInterceptTouchEvent(swapXY(ev))
        swapXY(ev) // return touch coordinates to original reference frame for any child views
        return intercepted
    }
    override fun onTouchEvent(ev:MotionEvent):Boolean {
        return super.onTouchEvent(swapXY(ev))
    }
}