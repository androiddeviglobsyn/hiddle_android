package com.hiddle.app.activity

import android.accounts.AuthenticatorException
import android.accounts.NetworkErrorException
import android.content.Context
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Editable
import android.text.Html
import android.text.Html.FROM_HTML_MODE_LEGACY
import android.text.InputFilter
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.animation.Animation
import android.view.animation.TranslateAnimation
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat
import androidx.core.text.HtmlCompat
import com.hiddle.app.R
import com.hiddle.app.model.SendOTPModel
import com.hiddle.app.model.VerifyCodeModel.VerifyModelResponse

import com.hiddle.app.retrofit.RetrofitRestClient
import com.hiddle.app.util.AppUtils
import com.hiddle.app.util.SharedPreferenceManager
import kotlinx.android.synthetic.main.activity_login_using_mobile.*
import kotlinx.android.synthetic.main.activity_verification_code.*
import kotlinx.android.synthetic.main.activity_verification_code.ivBack
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketTimeoutException
import java.util.*


class VerificationCodeActivity : BaseMainActivity(), View.OnClickListener {
    private var paramMobile: String = ""
    private var paramCountryCode: String = ""
    private var isFromLogin: String = ""
    private var socialId: String = ""
    private var paramAPIOTP: String = ""
    var paramUserName: String = ""
    var paramEmail: String = ""
    private var profile_url: String = ""
    var paramIsVerify: String = "0"
    var otpLimit: String = ""

    companion object {
        @JvmStatic
        var authorizationToken: String = ""
        var mainauthorizationToken: String = ""
    }

    //timer
    private var mTimeLeftInMillis: Long = 30000  //half minute 30 second
    private var mTimerRunning: Boolean = false
    private val START_TIME_IN_MILLIS: Long = 30000 //half minute 30 second
    private var mCountDownTimer: CountDownTimer? = null
    private var isVerify: String = "no"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verification_code)

        getIntentData()
        EdittextChangeMethod()
        allClicklistener()
        startTimerFor30Second()

    }

    private fun allClicklistener() {
        btnReSend.setOnClickListener(this)
        ivBack.setOnClickListener(this)
        ivClose.setOnClickListener(this)
        btnContinue.setOnClickListener(this)
        llBackVerification.setOnClickListener(this)
    }

    private fun getIntentData() {
        val i = intent
        if (i != null) {
            paramCountryCode = i.getStringExtra("countryCode")!!
            paramMobile = i.getStringExtra("mobileNumber")!!
            paramUserName = i.getStringExtra("paramUserName")!!
            paramEmail = i.getStringExtra("paramEmail")!!
            isFromLogin = i.getStringExtra("isFromLogin")!!
            socialId = i.getStringExtra("socialId")!!
            paramAPIOTP = i.getStringExtra("paramAPIOTP")!!
            profile_url = i.getStringExtra("profile_url")!!
            otpLimit = i.getStringExtra("otpLimit")!!

            /*etOtp.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(otpLimit.toInt()))
            var s = "<b>" + paramCountryCode + paramMobile + "</b>"*/
            tvMobileCode.text = " " + paramCountryCode + paramMobile //set mobile number in text
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.ivBack -> {
                ivBack.isEnabled = false
                finish()
                AppUtils.overridePendingTransitionExit(activity)
            }
            R.id.llBackVerification -> {
                llBackVerification.isEnabled = false
                finish()
                AppUtils.overridePendingTransitionExit(activity)
            }
            R.id.btnReSend -> {
                callSendOTPAPI()
            }
            R.id.ivClose -> {
                etOtp.background = applicationContext?.let { ContextCompat.getDrawable(it, R.drawable.bg_grey_rounded_shape) }
                etOtp.text?.clear()
                ivClose.visibility = View.GONE
            }
            R.id.btnContinue -> {
                btnContinue.isEnabled = false
                redirectToRecoveryActivity()

            }
        }
    }

    override fun onResume() {
        super.onResume()
        ivBack.isEnabled = true
        btnContinue.isEnabled = true
        llBackVerification.isEnabled = true
    }

    private fun callSendOTPAPI() {
        if (AppUtils.isConnectedToInternet(activity)) {
            AppUtils.showProgressDialog(activity)
            val params = HashMap<String, String?>()
            params["cc"] = paramCountryCode
            params["number"] = paramMobile
            params["device_type"] = "android"
            if (SharedPreferenceManager.getMySharedPreferences()!!.getFcmToken() != null) {
                params["device_token"] = SharedPreferenceManager.getMySharedPreferences()!!.getFcmToken()
            } else {
                params["device_token"] = ""
            }
            Log.e("firebaseToke", SharedPreferenceManager.getMySharedPreferences()!!.getFcmToken()!!)

            val call: Call<SendOTPModel>
            call = RetrofitRestClient.instance!!.sendOTPApi(params)
            call.enqueue(object : Callback<SendOTPModel> {
                override fun onResponse(call: Call<SendOTPModel>?, response: Response<SendOTPModel>?) {
                    AppUtils.hideProgressDialog()
                    val basicModel: SendOTPModel
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {
                            paramAPIOTP = basicModel.data!!.otp!!
                            otpLimit = basicModel.data.otpLimit!!
                            etOtp.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(otpLimit.toInt()))

                            Log.e("OTP", paramAPIOTP)
                            hideResendButton()
                        } else if (Objects.requireNonNull(basicModel).code == "-1") {
                            callLogoutFunction()
                        } else {
                            showSnackBar(activity, basicModel.message)
                        }
                    } else {
                        showSnackBar(activity, response.message())
                    }
                }

                override fun onFailure(call: Call<SendOTPModel>?, t: Throwable?) {
                    AppUtils.hideProgressDialog()
                    if (t is SocketTimeoutException) {
                        showSnackBar(activity, getString(R.string.connection_timeout))
                    } else if (t is NetworkErrorException) {
                        showSnackBar(activity, getString(R.string.network_error))
                    } else if (t is AuthenticatorException) {
                        showSnackBar(activity, getString(R.string.authentiation_error))
                    } else {
                        showSnackBar(activity, getString(R.string.something_went_wrong))
                    }
                }
            })
        } else {
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }


    }

    private fun hideResendButton() {
        btnReSend.visibility = View.GONE
        llRetry.visibility = View.VISIBLE
        startTimerFor30Second()
    }

    private fun getCallVerificationCodeAPI() {
        if (AppUtils.isConnectedToInternet(activity)) {
            AppUtils.showProgressDialog(activity)
            val params = HashMap<String, String?>()
            params["cc"] = paramCountryCode
            params["number"] = paramMobile
            params["device_type"] = "android"
            if (SharedPreferenceManager.getMySharedPreferences()!!.getFcmToken() != null) {
                params["device_token"] = SharedPreferenceManager.getMySharedPreferences()!!.getFcmToken()
            } else {
                params["device_token"] = ""
            }
            if (isFromLogin == "facebook") {
                params["social_id"] = socialId
            }
            if (isFromLogin == "instagram") {
                params["insta_id"] = socialId
            }
            if (isFromLogin == "apple") {
                params["apple_id"] = socialId
            }
            Log.e("params",params.toString())
            val call: Call<VerifyModelResponse>
            call = RetrofitRestClient.instance!!.verifyCodeApi(params)
            call.enqueue(object : Callback<VerifyModelResponse> {
                override fun onResponse(call: Call<VerifyModelResponse>?, response: Response<VerifyModelResponse>?) {
                    AppUtils.hideProgressDialog()
                    val basicModel: VerifyModelResponse
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {
                            // Toast.makeText(activity, basicModel.message, Toast.LENGTH_SHORT).show()
                            authorizationToken = basicModel.data!!.tokens!![0].token!!
                            mainauthorizationToken = basicModel.data!!.tokens!![0].token!!
                            Log.e("verify_tokem", authorizationToken)
                            paramIsVerify = basicModel.data.is_verify!!
                            if (paramIsVerify == "1") {
                                SharedPreferenceManager.getMySharedPreferences()!!.setLogin(true)
                                SharedPreferenceManager.getMySharedPreferences()!!.setUserId(basicModel.data!!.id!!)
                                SharedPreferenceManager.getMySharedPreferences()!!.setUserName(basicModel.data.username!!)
                                SharedPreferenceManager.getMySharedPreferences()!!.setCountryCode(basicModel.data!!.cc!!)
                                SharedPreferenceManager.getMySharedPreferences()!!.setMobileNumber(basicModel.data!!.number!!)
                                SharedPreferenceManager.getMySharedPreferences()!!.setAuthorizationToken(authorizationToken)
                                SharedPreferenceManager.getMySharedPreferences()!!.setSound(basicModel.data.notification!!.sounds!!)
                                SharedPreferenceManager.getMySharedPreferences()!!.setVibrate(basicModel.data.notification!!.vibration!!)
                                SharedPreferenceManager.getMySharedPreferences()!!.setLanguahe(basicModel.data!!.preferences!!.language!!)
                                SharedPreferenceManager.getMySharedPreferences()!!.setDistance(basicModel.data!!.preferences!!.distance_in!!)

                                if (basicModel.data.images!!.isNotEmpty()) {
                                    SharedPreferenceManager.getMySharedPreferences()!!.setProfileImage(basicModel.data.base_url + basicModel.data.images[0].image)
                                }
                                if (basicModel.data.email != null) {
                                    SharedPreferenceManager.getMySharedPreferences()!!.setEmail(basicModel.data.email!!)
                                }
                            } else if (Objects.requireNonNull(basicModel).code == "-1") {
                                callLogoutFunction()
                            } else {
                                SharedPreferenceManager.getMySharedPreferences()!!.setAuthorizationToken(authorizationToken)
                            }
                            openSuccessViewForVerify()
                        } else {
                            showSnackBar(activity, basicModel.message)
                        }
                    } else {
                        showSnackBar(activity, response.message())
                    }
                }

                override fun onFailure(call: Call<VerifyModelResponse>?, t: Throwable?) {
                    AppUtils.hideProgressDialog()
                    /* getCallVerificationCodeAPI()  // call verify otp api*/
                    if (t is SocketTimeoutException) {
                        showSnackBar(activity, getString(R.string.connection_timeout))
                    } else if (t is NetworkErrorException) {
                        showSnackBar(activity, getString(R.string.network_error))
                    } else if (t is AuthenticatorException) {
                        showSnackBar(activity, getString(R.string.authentiation_error))
                    } else {
                        showSnackBar(activity, getString(R.string.something_went_wrong))
                    }
                }
            })
        } else {
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }
    }

    private fun openSuccessViewForVerify() {
        etOtp.background = applicationContext?.let { ContextCompat.getDrawable(it, R.drawable.bg_gradient_green_line_background) }
        isVerify = "yes"
        llVerificationCode.visibility = View.GONE

        llAccountVerified.visibility = View.VISIBLE
        setAnimationForVerifyButton()
    }

    private fun redirectToRecoveryActivity() {
        if (paramIsVerify == "1") {
            val i = Intent(activity, HomeActivity::class.java)
            i.putExtra("type", "home")
            i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or FLAG_ACTIVITY_NEW_TASK
            startActivity(i)
            /*finish()*/
            AppUtils.overridePendingTransitionEnter(activity)

        } else {    //for verify is 0
            val i = Intent(activity, RecoveryEmailActivity::class.java)
            i.putExtra("paramUserName", paramUserName)
            i.putExtra("paramEmail", paramEmail)
            i.putExtra("isFromLogin", isFromLogin)
            i.putExtra("socialId", socialId)
            i.putExtra("authorizationToken", authorizationToken)
            i.putExtra("profile_url", profile_url)
            startActivity(i)
            finish()
            AppUtils.overridePendingTransitionEnter(activity)
        }
    }


    private fun updateCountDownText() {
        val minutes = (mTimeLeftInMillis / 1000).toInt() / 60
        val seconds = (mTimeLeftInMillis / 1000).toInt() % 60
        val timeLeftFormatted: String = java.lang.String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds)
        tvRetryTime.text = timeLeftFormatted
    }

    private fun resetTimer() {
        mTimeLeftInMillis = START_TIME_IN_MILLIS
        updateCountDownText()
    }

    private fun startTimerFor30Second() {
        mCountDownTimer = object : CountDownTimer(mTimeLeftInMillis, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                mTimeLeftInMillis = millisUntilFinished
                updateCountDownText()
            }

            override fun onFinish() {
                mTimerRunning = false
                llRetry.visibility = View.GONE
                btnReSend.visibility = View.VISIBLE
                resetTimer()
            }
        }.start()
        mTimerRunning = true

    }

    private fun EdittextChangeMethod() {
        etOtp.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s.toString().isNotEmpty()) {
                    ivClose.visibility = View.VISIBLE

                    if (s.toString().length < otpLimit.toInt()) {
                        etOtp.background = applicationContext?.let { ContextCompat.getDrawable(it, R.drawable.bg_grey_rounded_shape) }
                    }
                    if (s.toString().length == otpLimit.toInt()) {
                        if (s.toString() == paramAPIOTP) {
                            AppUtils.hideSoftKeyboard(activity)
                            // hideKeyboard()
                            getCallVerificationCodeAPI()  // call verify otp api
                        } else {
                            etOtp.background = applicationContext?.let { ContextCompat.getDrawable(it, R.drawable.bg_gradient_red_line_background) }
                            showSnackBar(activity, getString(R.string.please_enter_valid_otp))
                        }
                    }
                }

                if (s.toString().isEmpty()) {
                    ivClose.visibility = View.GONE
                }
            }
        })
    }


    fun setAnimationForVerifyButton() {
        val ani: Animation = TranslateAnimation(Animation.ABSOLUTE.toFloat(), Animation.ABSOLUTE.toFloat(), 150F, Animation.ABSOLUTE.toFloat())
        ani.duration = 900
        ani.fillAfter = true
        llAccountVerified.startAnimation(ani)
    }


    override fun onDestroy() {
        super.onDestroy()
        mCountDownTimer?.cancel()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (isVerify == "yes") {
            redirectToRecoveryActivity()
        } else {
            finish()
            AppUtils.overridePendingTransitionExit(activity)
        }
    }


}
