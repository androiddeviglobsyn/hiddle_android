package com.hiddle.app.activity


import androidx.appcompat.app.AppCompatActivity
import com.hiddle.app.R
import com.hiddle.app.util.Constants
import com.hiddle.app.util.Constants.TransitionType
import android.os.Bundle

import android.transition.Explode
import android.transition.Slide
import android.transition.TransitionInflater
import android.view.Gravity
import android.view.Window
import android.view.animation.AnticipateOvershootInterpolator


class TransitionActivity: AppCompatActivity() {
  internal lateinit var type: Constants.TransitionType
  internal lateinit var toolbarTitle:String

  override fun onCreate(savedInstanceState: Bundle?) {
    getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS)
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_transition)


    initPage()
    initAnimation()
    // For overlap between Exiting MainActivity.java and Entering TransitionActivity.java
    getWindow().setAllowEnterTransitionOverlap(false)


  }

  private fun initPage() {
    type = getIntent().getSerializableExtra(Constants.KEY_ANIM_TYPE) as Constants.TransitionType
    toolbarTitle = getIntent().getExtras()!!.getString(Constants.KEY_TITLE)!!

  }



  private fun initAnimation() {
    when (type) {
      TransitionType.ExplodeJava -> { // For Explode By Code
        val enterTransition = Explode()
        enterTransition.setDuration(500)
        getWindow().setEnterTransition(enterTransition)
      }
     TransitionType.ExplodeXML -> { // For Explode By XML
        val enterTansition = TransitionInflater.from(this).inflateTransition(R.transition.explode)
        getWindow().setEnterTransition(enterTansition)
      }
      TransitionType.SlideJava -> { // For Slide By Code
        val enterTransition = Slide()
        enterTransition.setSlideEdge(Gravity.TOP)
        enterTransition.setDuration(800)
        enterTransition.setInterpolator(AnticipateOvershootInterpolator())
        getWindow().setEnterTransition(enterTransition)
      }
     /* SlideXML -> { // For Slide by XML
        val enterTansition = TransitionInflater.from(this).inflateTransition(R.transition.slide)
        getWindow().setEnterTransition(enterTansition)
      }
      FadeJava -> { // For Fade By Code
        val enterTransition = Fade()
        enterTransition.setDuration(getResources().getInteger(R.integer.anim_duration_medium))
        getWindow().setEnterTransition(enterTransition)
      }
      FadeXML -> { // For Fade by XML
        val enterTansition = TransitionInflater.from(this).inflateTransition(R.transition.fade)
        getWindow().setEnterTransition(enterTansition)
      }*/
    }
  }

}
/*
finishAfterTransition()*/
