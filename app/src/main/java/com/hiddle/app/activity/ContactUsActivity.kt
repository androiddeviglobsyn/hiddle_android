package com.hiddle.app.activity

import android.accounts.AuthenticatorException
import android.accounts.NetworkErrorException
import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.hiddle.app.R
import com.hiddle.app.model.GeneralModel
import com.hiddle.app.retrofit.RetrofitRestClient
import com.hiddle.app.util.AppUtils
import com.hiddle.app.util.SharedPreferenceManager
import kotlinx.android.synthetic.main.activity_contact_us.*
import kotlinx.android.synthetic.main.activity_notification.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketTimeoutException
import java.util.*

class
ContactUsActivity : BaseMainActivity(), View.OnClickListener {
    var paramUserName: String = ""
    var paramEmail: String = ""
    var paramMessage: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_us)

        setDataNameAndEmail()
        allClicklistener()
    }

    private fun setDataNameAndEmail() {
        etusername.setText(SharedPreferenceManager.getMySharedPreferences()!!.getUserName())
        etEmailContact.setText(SharedPreferenceManager.getMySharedPreferences()!!.getEmail())
    }

    private fun allClicklistener() {
        btnSendContact.setOnClickListener(this)
        ivBackContact.setOnClickListener(this)
        llBackContact.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.ivBackContact -> {
                ivBackContact.isEnabled = false
                llBackContact.isEnabled = false
                finish()
                AppUtils.overridePendingTransitionExit(activity)
            }
            R.id.llBackContact -> {
                ivBackContact.isEnabled = false
                llBackContact.isEnabled = false
                finish()
                AppUtils.overridePendingTransitionExit(activity)
            }
            R.id.btnSendContact -> {
                paramUserName = etusername.text.toString()
                paramEmail = etEmailContact.text.toString()
                paramMessage = etMessage.text.toString()

                if (validateContact()) {
                    callAPIContactUs()
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        ivBackContact.isEnabled = true
        llBackContact.isEnabled = true
    }

    private fun callAPIContactUs() {
        if (AppUtils.isConnectedToInternet(activity)) {
            AppUtils.showProgressDialog(activity)
            val params = HashMap<String, String?>()
            params["info"] = paramUserName
            params["email"] = paramEmail
            params["message"] = paramMessage

            val call: Call<GeneralModel>
            call = RetrofitRestClient.instance!!.contactUsApi(params)
            Log.e("token", SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
            call.enqueue(object : Callback<GeneralModel> {
                @SuppressLint("SetTextI18n")
                override fun onResponse(call: Call<GeneralModel>?, response: Response<GeneralModel>?) {
                    AppUtils.hideProgressDialog()
                    val basicModel: GeneralModel
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {
                            Toast.makeText(activity, basicModel.message, Toast.LENGTH_SHORT).show()
                            finish()
                            AppUtils.overridePendingTransitionExit(activity)
                        } else if (Objects.requireNonNull(basicModel).code == "-1") {
                            callLogoutFunction()
                        } else {

                            showSnackBar(activity, basicModel.message)
                        }
                    } else {

                        showSnackBar(activity, response.message())
                    }
                }

                override fun onFailure(call: Call<GeneralModel>?, t: Throwable?) {
                    AppUtils.hideProgressDialog()

                    if (t is SocketTimeoutException) {
                        showSnackBar(activity, getString(R.string.connection_timeout))
                    } else if (t is NetworkErrorException) {
                        showSnackBar(activity, getString(R.string.network_error))
                    } else if (t is AuthenticatorException) {
                        showSnackBar(activity, getString(R.string.authentiation_error))
                    } else {
                        showSnackBar(activity, getString(R.string.something_went_wrong))
                    }
                }
            })
        } else {
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }
    }

    private fun validateContact(): Boolean {
        if (paramUserName.isEmpty()) {
            showSnackBar(activity, getString(R.string.please_enter_user_name_and_phone))
            return false
        }
        if (paramEmail.isEmpty()) {
            showSnackBar(activity, getString(R.string.please_enter_email))
            return false
        }
        if (!AppUtils.isEmailValid(paramEmail)) {
            showSnackBar(activity, getString(R.string.please_enter_valid_email))
            return false
        }
        if (paramMessage.isEmpty()) {
            showSnackBar(activity, getString(R.string.please_enter_your_message))
            return false
        }
        return true
    }
}
