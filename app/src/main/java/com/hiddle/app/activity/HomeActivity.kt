package com.hiddle.app.activity

import android.Manifest
import android.accounts.AuthenticatorException
import android.accounts.NetworkErrorException
import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.*
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.renderscript.Allocation
import android.renderscript.Element
import android.renderscript.RenderScript
import android.renderscript.ScriptIntrinsicBlur
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonArray
import com.hiddle.app.R
import com.hiddle.app.`interface`.DeleteLocationClick
import com.hiddle.app.`interface`.FilterButtonClick
import com.hiddle.app.`interface`.FragmentClick
import com.hiddle.app.adapter.LocationAdapter
import com.hiddle.app.adapter.LocationAdapter.Companion.selectedLocationList
import com.hiddle.app.fragment.*
import com.hiddle.app.model.FilterModel.FLocation
import com.hiddle.app.model.FilterModel.FilterResponse
import com.hiddle.app.model.GeneralModel
import com.hiddle.app.model.HomeFilterModel
import com.hiddle.app.model.LocationJsonModel
import com.hiddle.app.model.LocationModel
import com.hiddle.app.model.MapFilterModel.MapFilterResponse
import com.hiddle.app.retrofit.RetrofitRestClient
import com.hiddle.app.service.ForegroundService
import com.hiddle.app.service.LocationTrack
import com.hiddle.app.service.OfflineOnlineService
import com.hiddle.app.util.AppUtils

import com.hiddle.app.util.SharedPreferenceManager
import com.warkiz.widget.IndicatorSeekBar
import com.warkiz.widget.OnSeekChangeListener
import com.warkiz.widget.SeekParams

import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.drawer_header.*
import kotlinx.android.synthetic.main.navigation_drawer_menu.*
import okhttp3.internal.trimSubstring
import org.json.JSONObject
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions
import pub.devrel.easypermissions.PermissionRequest
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketTimeoutException
import java.util.*
import kotlin.collections.ArrayList

class HomeActivity : BaseMainActivity(), View.OnClickListener, NavigationView.OnNavigationItemSelectedListener, DeleteLocationClick, DrawerLayout.DrawerListener, com.hiddle.app.common.DrawerLayout.DrawerListener {
    var locationTrack: LocationTrack? = null
    var paramLongitude: String = ""
    var paramLatitude: String = ""
    var fragment: Fragment? = null
    var selectedTabPosition: Int = 0
    var locationAdapter: LocationAdapter? = null
    var doubleBackToExitPressedOnce: Boolean = false
    var originSelection: String? = null
    var geocoder: Geocoder? = null
    var addresses: List<Address> = ArrayList()
    var address: String = ""
    var paramMinAge: String = "18"
    var paramMaxAge: String = "100"
    var paramMinMapAge: String = "18"
    var paramMaxMapAge: String = "100"
    var showMeMap: String = "3"
    var paramMinHeight: String = "1.10"
    var paramMaxHeight: String = "2.20"
    var paramDistance: String = "0"
    var paramFilterOrigin: String = ""
    var paramShowMe: String = "3"
    var clickEventNoti: String = "home"
    var showMe: String = "3"
    var fromIdChatNotificaton: String? = null
    var locationJsonFilterlist: ArrayList<LocationJsonModel> = ArrayList()
    var loclist: ArrayList<FLocation> = ArrayList()

    companion object {
        @JvmStatic
        var homeOriginlist: ArrayList<String> = ArrayList()

        @JvmStatic
        var locationFilterlist: ArrayList<LocationModel> = ArrayList()

        const val RC_ACCESSS_LOCATION: Int = 126
        const val RC_FINE_LOCATION: Int = 127
    }

    var fragmentClick: FragmentClick? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity.window.requestFeature(Window.FEATURE_CONTENT_TRANSITIONS)
        setContentView(R.layout.activity_home)

        Log.e("fcmToken", SharedPreferenceManager.getMySharedPreferences()!!.getFcmToken()!!)

        locationFilterlist.clear()
        checkLoginState()  //if user is not login
        setBottomNavigation()  //set bottom navigation drawer
        setFilterAllData()  // set all data of filter dialog
        nav_view.setNavigationItemSelectedListener(this)
        getIntentData()  //set tab according click using intent data
        callAPIGetFilter("success")
        callAPIMapFilter("success")
        allClicklistener()
        callFineLocation() //for start service and permission for location
        try {
            //for start online offline service
            startService(Intent(baseContext, OfflineOnlineService::class.java)) //for online and offline
        } catch (e: Exception) {
        }

        // getNotificationIntentDataAndDefaultTab()  //for set notification and default as homefragment
    }

    private fun checkLoginState() {
        if (!SharedPreferenceManager.getMySharedPreferences()!!.isLogin()) {
            callLogoutFunction()
        }
    }

    private fun getIntentData() {
        //when we call home activity for app open and redirect from other activity it is called
        try {
            var i = intent
            if (i != null) {
                clickEventNoti = i.getStringExtra("type")!!
                Log.e("step2", clickEventNoti)
                //NotificationMessage
                try {
                    if (clickEventNoti == "4") {
                        fromIdChatNotificaton = i.getStringExtra("from_id")
                    }
                } catch (e: Exception) {

                }
                if (clickEventNoti == "home") {
                    bottom_navigation.menu.getItem(0).isChecked = true
                    openHomeFragment()
                }
                if (clickEventNoti == "profile") {
                    bottom_navigation.menu.getItem(4).isChecked = true
                    openProfileFragment()
                }
                /*when app in foreground and notification come and we not open app and then after kill
                   we click on notification then this method call and also useful when redirect from activity*/
                Log.e("tab_click_method_call", "create")
                setTabAccordingClick()

            }
        } catch (e: Exception) {

        }

    }

    private fun callAPIMapFilter(isFrom: String) {
        if (AppUtils.isConnectedToInternet(activity)) {
            // AppUtils.showProgressDialog(activity)
            val call: Call<MapFilterResponse>
            call = RetrofitRestClient.instance!!.getFilterMapAPI(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
            //Log.e("token", SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
            call.enqueue(object : Callback<MapFilterResponse> {
                @SuppressLint("SetTextI18n")
                override fun onResponse(call: Call<MapFilterResponse>?, response: Response<MapFilterResponse>?) {
                    AppUtils.hideProgressDialog()
                    val basicModel: MapFilterResponse
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {
                            paramMinMapAge = basicModel.data!!.minAge!!
                            paramMaxMapAge = basicModel.data.maxAge!!
                            showMeMap = basicModel.data.mShowMe!!
                            Log.e("ShowMe", showMeMap)
                        } else if (Objects.requireNonNull(basicModel).code == "-1") {
                            callLogoutFunction()
                        } else {
                            showSnackBar(activity, basicModel.message)
                        }
                    } else {
                        showSnackBar(activity, response.message())
                    }
                }

                override fun onFailure(call: Call<MapFilterResponse>?, t: Throwable?) {
                    AppUtils.hideProgressDialog()

                    callAPIMapFilter("fail")
                    if (isFrom == "success") {
                        /* if (t is SocketTimeoutException) {
                             showSnackBar(activity, getString(R.string.connection_timeout))
                         } else if (t is NetworkErrorException) {
                             showSnackBar(activity, getString(R.string.network_error))
                         } else if (t is AuthenticatorException) {
                             showSnackBar(activity, getString(R.string.authentiation_error))
                         } else {
                             showSnackBar(activity, getString(R.string.something_went_wrong))
                         }*/
                    }

                }
            })
        } else {
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }
    }


    private fun getNotificationIntentDataAndDefaultTab() {
        //call this method in background because when application in background then not call so
        //for background and application is killed
        Log.e("log_call", "log_call")

        //clickEventNoti = ""
        //set home fragment as default and also set notification click data
        try {
            val extras: Bundle = intent.extras!!
            // extract the extra-data in the Notification
            clickEventNoti = intent.extras!!.getString("type")!!  //get notification data in killed and background
            Log.e("click_action_main_call", clickEventNoti)

            if (clickEventNoti == "4") {
                fromIdChatNotificaton = intent.extras!!.getString("from_id")!!  //get notification data in killed and background
                Log.e("step5", fromIdChatNotificaton + "name" + clickEventNoti)
            }
            Log.e("tab_click_method_call", "onResume")
            setTabAccordingClick()
        } catch (e: Exception) {

        }


    }

    private fun setTabAccordingClick() {
        Log.e("tab_click_method_call", "mainClick")

        //type: '1' for like user,'2' for superlike user,'3' for strike,'4' for sent message
        if (clickEventNoti == "1" || clickEventNoti == "2" || clickEventNoti == "3" || clickEventNoti == "4") {
            //'1' for like user,'2' for superlike user,'3' for strike
            if (clickEventNoti == "1" || clickEventNoti == "2") {
                bottom_navigation.menu.getItem(3).isChecked = true
                openFavoriteFragment("like")
            }
            if (clickEventNoti == "4") {
                bottom_navigation.menu.getItem(0).isChecked = true
                openHomeFragment()
                callChatActivity()
            }
            if (clickEventNoti == "3") {
                bottom_navigation.menu.getItem(3).isChecked = true
                openFavoriteFragment("strike")
            }
        } /*else if (clickEventNoti == "profile") {
            bottom_navigation.menu.getItem(4).isChecked = true
            openProfileFragment("N")
        }*/ /*else if (clickEventNoti == "home") {
            bottom_navigation.menu.getItem(0).isChecked = true
            openHomeFragment()
        }*/ else {
            //please check then test
            if (clickEventNoti == "home") {
                if (selectedTabPosition == 0) {
                    bottom_navigation.menu.getItem(0).isChecked = true
                }

                /* bottom_navigation.menu.getItem(0).isChecked = true
                 openHomeFragment()*/
            }


            //for default home
            /*bottom_navigation.menu.getItem(0).isChecked = true
            openHomeFragment()*/
        }
    }

    private fun callChatActivity() {
        val myactivity = Intent(activity, ChatActivity::class.java)
        myactivity.putExtra("to_user_name", "")
        myactivity.putExtra("to_user_id", fromIdChatNotificaton)
        myactivity.putExtra("user_image", "")
        myactivity.putExtra("isFrom", "notification")
        startActivity(myactivity)
        AppUtils.overridePendingTransitionEnter(activity)
        //finish()
    }

    @SuppressLint("NewApi")
    fun BlurImage(input: Bitmap): Bitmap {
        try {
            val rsScript = RenderScript.create(applicationContext)
            val alloc = Allocation.createFromBitmap(rsScript, input)
            val blur = ScriptIntrinsicBlur.create(rsScript, Element.U8_4(rsScript))
            blur.setRadius(21F)
            blur.setInput(alloc)
            val result = Bitmap.createBitmap(input.width, input.height, Bitmap.Config.ARGB_8888)
            val outAlloc = Allocation.createFromBitmap(rsScript, result)
            blur.forEach(outAlloc)
            outAlloc.copyTo(result)
            rsScript.destroy()
            return result
        } catch (e: Exception) {
            // TODO: handle exception
            return input
        }
    }

    fun getBitmapFromView(view: View): Bitmap {
        var returnedBitmap: Bitmap? = null
        try {
            returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888)
            val canvas = Canvas(returnedBitmap)
            val bgDrawable = view.getBackground()
            if (bgDrawable != null)
                bgDrawable.draw(canvas)
            else
                canvas.drawColor(Color.WHITE)
            view.draw(canvas)
        } catch (e: Exception) {
        }
        return returnedBitmap!!
    }


    private fun callAPIGetFilter(isFrom: String) {
        swMen.isChecked = false
        swWomen.isChecked = false
        swBoth.isChecked = false

        if (AppUtils.isConnectedToInternet(activity)) {
            // AppUtils.showProgressDialog(activity)

            val params = HashMap<String, String?>()
            params["user_id"] = SharedPreferenceManager.getMySharedPreferences()!!.getUserId()
            Log.e("user_id_get_filter", params.toString())
            val call: Call<FilterResponse>
            call = RetrofitRestClient.instance!!.getFilterAPI(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, params)
            //Log.e("token", SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
            call.enqueue(object : Callback<FilterResponse> {
                @SuppressLint("SetTextI18n")
                override fun onResponse(call: Call<FilterResponse>?, response: Response<FilterResponse>?) {
                    AppUtils.hideProgressDialog()
                    val basicModel: FilterResponse
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {


                            try {
                                if (basicModel.data!!.filter!!.fDistance != null) {
                                    sbDistance.setProgress((basicModel.data!!.filter!!.fDistance)!!.toFloat())

                                }

                                paramMinAge = basicModel.data.filter!!.minAge!!
                                paramMaxAge = basicModel.data.filter.maxAge!!

                                showMe = basicModel.data.filter.fShowMe!!
                                Log.e("ShowMe", showMe)
                                Log.e("Response", basicModel.data.toString());


                                setAgeAndShowMeForHomeFilter() //set data which we get in get Filter API

                                sbHeight.selectedMinValue = basicModel.data.filter.minHeight!!.toFloat()
                                sbHeight.selectedMaxValue = basicModel.data.filter.maxHeight!!.toFloat()

                                paramMinHeight = (sbHeight.selectedMinValue).toString()
                                paramMaxHeight = (sbHeight.selectedMaxValue).toString()

                                if (paramMinHeight.length == 3) {
                                    paramMinHeight = paramMinHeight + "0"
                                }
                                if (paramMaxHeight.length == 3) {
                                    paramMaxHeight = paramMaxHeight + "0"
                                }
                                tvHeightValue.text = paramMinHeight + "-" + paramMaxHeight

                                //for origin set
                                homeOriginlist.clear()

                                if (basicModel.data!!.filter!!.fOrigin!! != null) {
                                    if (basicModel.data!!.filter!!.fOrigin!!.isNotEmpty()) {
                                        if (basicModel.data!!.filter!!.fOrigin!! != null) {
                                            originSelection = basicModel.data.filter!!.fOrigin
                                        }
                                    }
                                }


                                if (originSelection != null) {
                                    homeOriginlist.clear()
                                    originSelection!!.replace("[", "", true)
                                    originSelection!!.replace("]", "", true)

                                    if (originSelection != "null") {
                                        val lstValues: List<String> = originSelection!!.split(",").map { it -> it.trim() }
                                        lstValues.forEach { it ->
                                            Log.i("Values", "value=$it")
                                            //Do Something
                                        }
                                        homeOriginlist = lstValues as ArrayList<String>
                                    }

                                    Log.e("origin", "filter_call" + homeOriginlist.toString() + "selection" + originSelection)
                                    setOriginlist()
                                }
                                Log.e("origin", "filter_call_null" + homeOriginlist.toString() + "selection" + originSelection)

                                /*  LocationAdapter.selectedLocationList.clear()*/ ///make selected location  list clear
                                locationFilterlist.clear()

                                if (basicModel.data!!.filter!!.fLocation!! != null) {
                                    if (basicModel.data.filter!!.fLocation!!.size > 0) {
                                        for (p in basicModel.data.filter!!.fLocation!!.indices) {
                                            val locationModel: LocationModel = LocationModel()
                                            locationModel.id = (p + 1).toString()
                                            locationModel.name = getAddressFrmLatlang(basicModel.data.filter.fLocation!![p].latitude!!, basicModel.data.filter.fLocation[p].longitude!!, basicModel.data.filter.fLocation[p].address!!)
                                            locationModel.lat = basicModel.data.filter.fLocation!![p].latitude!!
                                            locationModel.lang = basicModel.data.filter.fLocation[p].longitude!!
                                            locationModel.isSelected = basicModel.data.filter.fLocation[p].isSelected
                                            /*  if (basicModel.data.filter.fLocation[p].isSelected == true) {
                                                  LocationAdapter.selectedLocationList.add((p + 1).toString())  //for make it as selected location list
                                              }*/
                                            locationFilterlist.add(locationModel)
                                        }
                                    }

                                }
                                setAdapterLocation()


                            } catch (e: Exception) {
                            }


                        } else if (Objects.requireNonNull(basicModel).code == "-1") {
                            callLogoutFunction()
                        } else {
                            Log.e("api_get_filter_error", basicModel.message!!)
                        }
                    } else {
                        Log.e("api_get_filter_error", response.message())
                    }
                }

                override fun onFailure(call: Call<FilterResponse>?, t: Throwable?) {
                    AppUtils.hideProgressDialog()

                    callAPIGetFilter("fail")
                    if (isFrom == "success") {
                        if (t is SocketTimeoutException) {
                            Log.e("api_get_filter_error", getString(R.string.connection_timeout))
                        } else if (t is NetworkErrorException) {
                            Log.e("api_get_filter_error", getString(R.string.network_error))
                        } else if (t is AuthenticatorException) {
                            Log.e("api_get_filter_error", getString(R.string.authentiation_error))
                        } else {
                            Log.e("api_get_filter_error", getString(R.string.something_went_wrong))
                        }
                    }

                }
            })
        } else {
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }
    }

    private fun setAgeAndShowMeForHomeFilter() {
        sbAge.selectedMinValue = paramMinAge.toFloat()
        sbAge.selectedMaxValue = paramMaxAge.toFloat()

        tvAgeValue.text = paramMinAge + "-" + paramMaxAge

        if (showMe == "1") {
            swMen.isChecked = true
            swWomen.isChecked = false
            swBoth.isChecked = false
        }
        if (showMe == "2") {
            swMen.isChecked = false
            swWomen.isChecked = true
            swBoth.isChecked = false
        }
        if (showMe == "3") {
            swMen.isChecked = false
            swWomen.isChecked = false
            swBoth.isChecked = true
        }
    }


    private fun setFilterAllData() {
        //Age seekbar
        sbAge.isNotifyWhileDragging = true;
        sbAge.setOnRangeSeekBarChangeListener { bar, number, number2 ->

            if (selectedTabPosition == 0) {  //for home
                paramMinAge = number.toString()
                paramMaxAge = number2.toString()
                tvAgeValue.text = paramMinAge + "-" + paramMaxAge
            }

            if (selectedTabPosition == 1) { //for map
                paramMinMapAge = number.toString()
                paramMaxMapAge = number2.toString()
                tvAgeValue.text = paramMinMapAge + "-" + paramMaxMapAge
            }

        }

        //Height
        sbHeight.isNotifyWhileDragging = true;

        sbHeight.setOnRangeSeekBarChangeListener { bar, number, number2 ->
            paramMinHeight = number.toString()
            paramMaxHeight = number2.toString()
            if (paramMinHeight.length == 3) {
                paramMinHeight = paramMinHeight + "0"
            }
            if (paramMaxHeight.length == 3) {
                paramMaxHeight = paramMaxHeight + "0"
            }

            tvHeightValue.text = paramMinHeight + "-" + paramMaxHeight
        }

        ////Distance seekbar
        sbDistance.setIndicatorTextFormat("\${PROGRESS}" + " " + getString(R.string.km))
        sbDistance.setOnSeekChangeListener(object : OnSeekChangeListener {
            override fun onSeeking(seekParams: SeekParams) {
                paramDistance = seekParams.progress.toString()
                if (seekParams.progress == 250) {
                    sbDistance.setIndicatorTextFormat("\${PROGRESS}" + "+" + " " + getString(R.string.km))
                } else {
                    sbDistance.setIndicatorTextFormat("\${PROGRESS}" + " " + getString(R.string.km))
                }

            }

            override fun onStartTrackingTouch(seekBar: IndicatorSeekBar) {
                if (seekBar.progress == 250) {
                    sbDistance.setIndicatorTextFormat("\${PROGRESS}" + "+" + " " + getString(R.string.km))
                } else {
                    sbDistance.setIndicatorTextFormat("\${PROGRESS}" + " " + getString(R.string.km))
                }
            }

            override fun onStopTrackingTouch(seekBar: IndicatorSeekBar) {
                if (seekBar.progress == 250) {
                    sbDistance.setIndicatorTextFormat("\${PROGRESS}" + "+" + " " + getString(R.string.km))
                } else {
                    sbDistance.setIndicatorTextFormat("\${PROGRESS}" + " " + getString(R.string.km))
                }
            }
        })

        swMen.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                swWomen.isChecked = false
                swBoth.isChecked = false
            } else {
            }
        }
        swWomen.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                swMen.isChecked = false
                swBoth.isChecked = false
            } else {

            }
        }
        swBoth.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                swMen.isChecked = false
                swWomen.isChecked = false
            } else {
            }
        }
    }

    private fun allClicklistener() {
        ivFilterOpen.setOnClickListener(this)
        ivFilterClose.setOnClickListener(this)
        ivAddOrigin.setOnClickListener(this)
        llAddPlace.setOnClickListener(this)
        rlLocationCurrent.setOnClickListener(this)
    }

    private fun setBottomNavigation() {
        bottom_navigation.setOnNavigationItemSelectedListener(navigationItemSelectedListener);
        /*Navigation drawer with transparent background*/
        drawer_layout.setScrimColor(getResources()!!.getColor(android.R.color.transparent));
        /*Remove navigation drawer shadow/fadding*/
        drawer_layout.setDrawerElevation(0F);
        drawer_layout.setDrawerListener(this);//set method
        //  drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);  // for stop to swipe navigation drawer
    }


    private var navigationItemSelectedListener: BottomNavigationView.OnNavigationItemSelectedListener = object : BottomNavigationView.OnNavigationItemSelectedListener {
        override fun onNavigationItemSelected(item: MenuItem): Boolean {
            when (item.itemId) {

                R.id.navigation_home -> {
                    if (selectedTabPosition != 0) {
                        openHomeFragment()
                    }
                    return true
                }
                R.id.navigation_map -> {
                    if (selectedTabPosition != 1) {
                        openMapFragment()
                    }
                    return true
                }
                R.id.navigation_message -> {
                    if (selectedTabPosition != 2) {
                        openMessageFragment()
                    }
                    return true
                }
                R.id.navigation_favorite -> {
                    if (selectedTabPosition != 3) {
                        openFavoriteFragment("strike")
                    }
                    return true
                }
                R.id.navigation_profile -> {
                    if (selectedTabPosition != 4) {
                        openProfileFragment()
                    }
                    return true
                }

            }

            return false
        }
    }

    private fun openProfileFragment() {
        selectedTabPosition = 4
        ivFilterOpen.visibility = View.GONE
        llNavigation.visibility = View.GONE
        openFragment(ProfileFragment.newInstance("", ""), "Y")
    }

    private fun openFavoriteFragment(isClick: String) {
        selectedTabPosition = 3
        ivFilterOpen.visibility = View.GONE
        llNavigation.visibility = View.GONE
        openFragment(FavoriteFragment.newInstance(isClick, ""), "Y")
    }

    private fun openMessageFragment() {
        selectedTabPosition = 2
        ivFilterOpen.visibility = View.GONE
        llNavigation.visibility = View.GONE
        openFragment(MessageFragment.newInstance("", ""), "Y")
    }

    private fun openMapFragment() {

        getMapFilterDataAndSetData()  //set data which we get in get map filter API

        selectedTabPosition = 1
        openFragment(MapFragment.newInstance("", ""), "Y")
        HideLayoutForFilter()   //only show layout show me age for filter
        ivFilterOpen.visibility = View.VISIBLE
        llNavigation.visibility = View.VISIBLE
    }

    private fun getMapFilterDataAndSetData() {
        try {

            sbAge.selectedMinValue = paramMinMapAge.toFloat()
            sbAge.selectedMaxValue = paramMaxMapAge.toFloat()

            tvAgeValue.text = paramMinMapAge + "-" + paramMaxMapAge

            if (showMeMap == "1") {
                swMen.isChecked = true
                swWomen.isChecked = false
                swBoth.isChecked = false
            }
            if (showMeMap == "2") {
                swMen.isChecked = false
                swWomen.isChecked = true
                swBoth.isChecked = false
            }
            if (showMeMap == "3") {
                swMen.isChecked = false
                swWomen.isChecked = false
                swBoth.isChecked = true
            }
        } catch (e: Exception) {

        }
    }

    private fun openHomeFragment() {

        setAgeAndShowMeForHomeFilter() //set data which we get in get Filter API

        selectedTabPosition = 0
        ivFilterOpen.visibility = View.VISIBLE
        llNavigation.visibility = View.VISIBLE
        openFragment(HomeFragment.newInstance("", ""), "Y")
        makeNavigationVisibleForFilter()  //show all layout for home for filter
    }


    private fun HideLayoutForFilter() {
        llLocationFilter.visibility = View.GONE
        llFilterShowMe.visibility = View.VISIBLE
        llHeightFilter.visibility = View.GONE
        llExtraLayout.visibility = View.VISIBLE
    }

    private fun makeNavigationVisibleForFilter() {
        llLocationFilter.visibility = View.VISIBLE
        llFilterShowMe.visibility = View.VISIBLE
        llHeightFilter.visibility = View.VISIBLE
        llExtraLayout.visibility = View.GONE
    }


    fun openFragment(fragment: Fragment, isAnimation: String) {
        val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()
        if (isAnimation == "Y") {
            transaction.setCustomAnimations(R.anim.slide_from_right_fragment, R.anim.slide_to_left_fragment)
        }
        // transaction.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_in_down, R.anim.slide_out_down, R.anim.slide_out_up);
        transaction.replace(R.id.container, fragment)
        //transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.ivFilterOpen -> {
                setBitmapForFilterBackground()
            }
            R.id.ivFilterClose -> {
                drawer_layout.closeDrawer(Gravity.RIGHT);
                /* if (selectedTabPosition == 0) {
                     callAPIAddHomeFilter()
                     ivFilterOpen.visibility = View.VISIBLE
                 }
                 if (selectedTabPosition == 1) {
                     callAPIAddMapFilter()
                     ivFilterOpen.visibility = View.VISIBLE
                 }*/

            }
            R.id.ivAddOrigin -> {
                ivAddOrigin.isEnabled = false
                val i = Intent(activity, MyOriginActivity::class.java)
                i.putExtra("isFrom", "homeFilter")
                startActivity(i)
                AppUtils.overridePendingTransitionEnter(activity)
            }
            R.id.llAddPlace -> {
                Log.e("size", locationFilterlist.size.toString())
                if (locationFilterlist.size >= 5) {
                    Toast.makeText(activity, getString(R.string.you_can_add_max_location), Toast.LENGTH_SHORT).show()
                } else {
                    llAddPlace.isEnabled = false
                    val i = Intent(activity, MapActivity::class.java)
                    i.putExtra("isFrom", "home")
                    startActivity(i)
                    AppUtils.overridePendingTransitionEnter(activity)
                }

            }
            R.id.rlLocationCurrent -> {
                getUserCurrentLocation()
                Log.e("size", locationFilterlist.size.toString())
                if (locationFilterlist.size >= 5) {
                    Toast.makeText(activity, getString(R.string.you_can_add_max_location), Toast.LENGTH_SHORT).show()
                } else {
                    val locationModel: LocationModel = LocationModel()
                    locationModel.id = (locationFilterlist.size + 1).toString()
                    locationModel.name = getAddressFrmLatlangLocation(paramLatitude, paramLongitude)
                    locationModel.name = getAddressFrmLatlangLocation(paramLatitude, paramLongitude)
                    locationModel.lat = paramLatitude
                    locationModel.lang = paramLongitude
                    if (locationFilterlist.size == 0) {
                        locationModel.isSelected = true
                    } else {
                        locationModel.isSelected = false
                    }

                    locationFilterlist.add(locationModel)

                    setAdapterLocation() //for set location for filter

                    /*val i = Intent(activity, MapActivity::class.java)
                     i.putExtra("isFrom", "homeCurrent")
                     startActivity(i)
                     AppUtils.overridePendingTransitionEnter(activity)*/
                }

            }
        }
    }

    private fun callAPIAddMapFilter() {

        if (AppUtils.isConnectedToInternet(activity)) {
            AppUtils.showProgressDialog(activity)
            val params = HashMap<String, String?>()

            if (swMen.isChecked) {
                paramShowMe = "1"
            }
            if (swWomen.isChecked) {
                paramShowMe = "2"
            }
            if (swBoth.isChecked) {
                paramShowMe = "3"
            }
            params["m_show_me"] = paramShowMe
            params["min_age"] = paramMinMapAge
            params["max_age"] = paramMaxMapAge

            Log.e("paramMapp", params.toString())
            val call: Call<GeneralModel>
            call = RetrofitRestClient.instance!!.addMapFiter(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, params)
            //Log.e("token", SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
            call.enqueue(object : Callback<GeneralModel> {
                @SuppressLint("SetTextI18n", "ShowToast")
                override fun onResponse(call: Call<GeneralModel>?, response: Response<GeneralModel>?) {
                    AppUtils.hideProgressDialog()
                    val basicModel: GeneralModel
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {
                            AppUtils.hideProgressDialog()
                            drawer_layout.closeDrawer(Gravity.RIGHT);

                            showMeMap = paramShowMe

                            if (selectedTabPosition == 1) {
                                fragmentClick!!.clickonFragment("MapFilter")
                                //openFragment(MapFragment.newInstance("", ""), "N")
                            }
                        } else if (Objects.requireNonNull(basicModel).code == "-1") {
                            callLogoutFunction()
                        } else {
                            drawer_layout.closeDrawer(Gravity.RIGHT);
                            showSnackBar(activity, basicModel.message)
                        }
                    } else {
                        drawer_layout.closeDrawer(Gravity.RIGHT);
                        showSnackBar(activity, response.message())
                    }

                }

                override fun onFailure(call: Call<GeneralModel>?, t: Throwable?) {
                    AppUtils.hideProgressDialog()
                    drawer_layout.closeDrawer(Gravity.RIGHT);
                    if (t is SocketTimeoutException) {
                        showSnackBar(activity, getString(R.string.connection_timeout))
                    } else if (t is NetworkErrorException) {
                        showSnackBar(activity, getString(R.string.network_error))
                    } else if (t is AuthenticatorException) {
                        showSnackBar(activity, getString(R.string.authentiation_error))
                    } else {
                        showSnackBar(activity, getString(R.string.something_went_wrong))
                    }
                }
            })
        } else {
            drawer_layout.closeDrawer(Gravity.RIGHT);
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }
        drawer_layout.closeDrawer(Gravity.RIGHT);
    }

    private fun setBitmapForFilterBackground() {
        setAdapterLocation()
        drawer_layout.openDrawer(Gravity.RIGHT);
        if (selectedTabPosition == 0 || selectedTabPosition == 1) {
            ivFilterOpen.visibility = View.GONE
            drawer_layout.openDrawer(Gravity.RIGHT);

            val view1: View = findViewById(R.id.rl1)
            var final_Bitmap: Bitmap? = null
            val a: Bitmap = getBitmapFromView(view1)
            final_Bitmap = BlurImage(a);
            ivBlur!!.setImageBitmap(final_Bitmap)

            if (selectedTabPosition == 0) {
                ivBlur.visibility = View.VISIBLE
                ivBlurMap.visibility = View.INVISIBLE
                drawer_layout.openDrawer(Gravity.RIGHT);

            }
            if (selectedTabPosition == 1) {
                ivBlur.visibility = View.INVISIBLE
                //click to take Map screen bitmap
                fragmentClick!!.clickonFragment("MapBlurBitmap")
                if (MapFragment.final_Bitmap_map != null) {
                    ivBlurMap!!.setImageBitmap(MapFragment.final_Bitmap_map)
                    MapFragment.final_Bitmap_map = null
                }
                ivBlurMap.visibility = View.VISIBLE
                drawer_layout.openDrawer(Gravity.RIGHT);
            }
        }

    }


    private fun callAPIAddHomeFilter() {
        addLocationForJsonArray()

        getParameterOriginAndShowMe()

        if (AppUtils.isConnectedToInternet(activity)) {
            AppUtils.showProgressDialog(activity)
            val params = HashMap<String, String?>()

            var paramObject: JSONObject = JSONObject()
            paramObject.put("f_distance", paramDistance);
            paramObject.put("f_show_me", paramShowMe);
            paramObject.put("min_age", paramMinAge);
            paramObject.put("max_age", paramMaxAge);
            paramObject.put("min_height", paramMinHeight.replace("\\s".toRegex(), ""));
            paramObject.put("max_height", paramMaxHeight.replace("\\s".toRegex(), ""));
            paramObject.put("f_origin", paramFilterOrigin);
            paramObject.put("user_id", SharedPreferenceManager.getMySharedPreferences()!!.getUserId());
            val gsonContact: Gson = GsonBuilder().create()
            val myCustomArrayLocation: JsonArray = gsonContact.toJsonTree(locationJsonFilterlist).getAsJsonArray();
            /* params["f_location"] = myCustomArrayLocation.toString();*/
            paramObject.put("f_location", myCustomArrayLocation.toString());
            // paramObject.put("f_location", locationJsonFilterlist.toString());
            Log.e("location_list", myCustomArrayLocation.toString())

            Log.e("paramFilter", paramObject.toString())


            var requestBody: Map<String, String> = HashMap<String, String>();

            var homeFilterModel: HomeFilterModel = HomeFilterModel()
            homeFilterModel.userId = SharedPreferenceManager.getMySharedPreferences()!!.getUserId()
            homeFilterModel.fDistance = paramDistance
            homeFilterModel.fShowMe = paramShowMe
            homeFilterModel.minAge = paramMinAge
            homeFilterModel.maxAge = paramMaxAge
            homeFilterModel.minHeight = paramMinHeight
            homeFilterModel.maxHeight = paramMaxHeight
            homeFilterModel.fOrigin = paramFilterOrigin

            loclist.clear()
            for (i in locationJsonFilterlist.indices) {
                var fLocation: FLocation = FLocation()
                fLocation.latitude = locationJsonFilterlist[i].latitude
                fLocation.longitude = locationJsonFilterlist[i].longitude
                fLocation.address = locationJsonFilterlist[i].address
                /* if (selectedLocationList.contains(locationJsonFilterlist.id))*/
                fLocation.isSelected = locationJsonFilterlist[i].is_selected
                loclist.add(fLocation)

            }

            homeFilterModel.fLocation = loclist

            /* params["f_distance"] = paramDistance
             params["f_show_me"] = paramShowMe
             params["min_age"] = paramMinAge
             params["max_age"] = paramMaxAge
             params["min_height"] = paramMinHeight.replace("\\s".toRegex(), "")
             params["max_height"] = paramMaxHeight.replace("\\s".toRegex(), "")
             params["f_origin"] = paramFilterOrigin
             params["user_id"] = SharedPreferenceManager.getMySharedPreferences()!!.getUserId()
             params["f_location"] = locationJsonFilterlist.toString()*/

            /*val gsonContact: Gson = GsonBuilder().create()
            val myCustomArrayLocation: JsonArray = gsonContact.toJsonTree(locationJsonFilterlist).getAsJsonArray();
            params["f_location"] = myCustomArrayLocation.toString(); //for locationlist*/

            /* Log.e("paramLocation", myCustomArrayLocation.toString())*/
            Log.e("paramHome", homeFilterModel.toString())

            val call: Call<GeneralModel>
            call = RetrofitRestClient.instance!!.addHomeFiter(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, homeFilterModel)
            // Log.e("token", SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
            call.enqueue(object : Callback<GeneralModel> {
                @SuppressLint("SetTextI18n", "ShowToast")
                override fun onResponse(call: Call<GeneralModel>?, response: Response<GeneralModel>?) {
                    AppUtils.hideProgressDialog()
                    val basicModel: GeneralModel
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {
                            AppUtils.hideProgressDialog()
                            drawer_layout.closeDrawer(Gravity.RIGHT);
                            Log.e("Response_add", response.toString());
                            showMe = paramShowMe

                            if (selectedTabPosition == 0) {
                                fragmentClick!!.clickonFragment("HomeFilter")
                            }
                        } else if (Objects.requireNonNull(basicModel).code == "-1") {
                            callLogoutFunction()
                        } else {
                            drawer_layout.closeDrawer(Gravity.RIGHT);
                            showSnackBar(activity, basicModel.message)
                        }
                    } else {
                        drawer_layout.closeDrawer(Gravity.RIGHT);
                        showSnackBar(activity, response.message())
                    }
                }

                override fun onFailure(call: Call<GeneralModel>?, t: Throwable?) {
                    AppUtils.hideProgressDialog()
                    drawer_layout.closeDrawer(Gravity.RIGHT);
                    if (t is SocketTimeoutException) {
                        showSnackBar(activity, getString(R.string.connection_timeout))
                    } else if (t is NetworkErrorException) {
                        showSnackBar(activity, getString(R.string.network_error))
                    } else if (t is AuthenticatorException) {
                        showSnackBar(activity, getString(R.string.authentiation_error))
                    } else {
                        showSnackBar(activity, getString(R.string.something_went_wrong))
                    }
                }
            })
        } else {
            drawer_layout.closeDrawer(Gravity.RIGHT);
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }
        drawer_layout.closeDrawer(Gravity.RIGHT);
    }

    private fun addLocationForJsonArray() {
        locationJsonFilterlist.clear()
        for (i in locationFilterlist.indices) {
            var locationJsonModel: LocationJsonModel = LocationJsonModel()
            locationJsonModel.latitude = locationFilterlist[i].lat
            locationJsonModel.longitude = locationFilterlist[i].lang
            locationJsonModel.address = locationFilterlist[i].name
            locationJsonModel.is_selected = locationFilterlist[i].isSelected
            locationJsonFilterlist.add(locationJsonModel)
        }
    }

    fun updateApi(listener: FragmentClick) {
        fragmentClick = listener
    }

    private fun getParameterOriginAndShowMe() {
        var origin_data: String = ""
        for (k in homeOriginlist.indices) {
            if (k == homeOriginlist.size - 1) {
                origin_data = origin_data + homeOriginlist[k].toString().trimSubstring(0)
            } else {
                origin_data = origin_data + homeOriginlist[k].toString().trimSubstring(0) + ","
            }
        }
        origin_data = origin_data.replace("[", "").replace("]", "").trimSubstring(0)
        paramFilterOrigin = origin_data
        Log.e("paramOrigin", paramFilterOrigin)


        if (swMen.isChecked) {
            paramShowMe = "1"
        }
        if (swWomen.isChecked) {
            paramShowMe = "2"
        }
        if (swBoth.isChecked) {
            paramShowMe = "3"
        }
    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        drawer_layout.closeDrawer(Gravity.RIGHT);
        return true
    }

    @SuppressLint("SetTextI18n")
    override fun onResume() {
        super.onResume()
        SharedPreferenceManager.getMySharedPreferences()!!.setNotificationUser("")
        getNotificationIntentDataAndDefaultTab()
        setOriginlist()  //for set origin in filter
        Log.e("origin", "resume" + homeOriginlist.toString())
        setAdapterLocation() //for set location for filter

        ivAddOrigin.isEnabled = true
        llAddPlace.isEnabled = true
    }

    private fun setOriginlist() {
        Log.e("list1", homeOriginlist.toString())
        Log.e("list", homeOriginlist.size.toString())
        if (homeOriginlist.size > 0) {
            if (homeOriginlist.contains(getString(R.string.select_all))) {
                tvOriginSelection.text = getString(R.string.all_origins_selected)
            } else {
                if (homeOriginlist.size > 1) {
                    tvOriginSelection.text = homeOriginlist.size.toString() + " " + getString(R.string.origins_selected)
                }
                if (homeOriginlist.size == 1) {
                    tvOriginSelection.text = homeOriginlist.size.toString() + " " + getString(R.string.origin_selected)
                }
            }
        } else {
            if (homeOriginlist.size == 0) {
                tvOriginSelection.text = getString(R.string.no_origins_selected)
            }
        }


    }

    public fun setAdapterLocation() {
        rvLocationFilter.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false) //set adapter
        locationAdapter = LocationAdapter(activity, locationFilterlist, this@HomeActivity, this@HomeActivity)
        rvLocationFilter.adapter = locationAdapter
    }

    public fun setAdapterLocationPos(pos: Int) {

        for (i in locationFilterlist.indices) {
            var locationModel: LocationModel = LocationModel()
            locationModel.id = locationFilterlist[i].id
            locationModel.lat = locationFilterlist[i].lat
            locationModel.lang = locationFilterlist[i].lang
            locationModel.name = locationFilterlist[i].name
            if (i == pos) {
                locationModel.isSelected = true
            } else {
                locationModel.isSelected = false
            }

            locationFilterlist[i] = locationModel
        }

        rvLocationFilter.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false) //set adapter
        locationAdapter = LocationAdapter(activity, locationFilterlist, this@HomeActivity, this@HomeActivity)
        rvLocationFilter.adapter = locationAdapter

    }

    override fun onBackPressed() {

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            val intent = Intent(Intent.ACTION_MAIN)
            intent.addCategory(Intent.CATEGORY_HOME)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            intent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
            startActivity(intent)

            return
        }
        this.doubleBackToExitPressedOnce = true
        showSnackBar(activity, getString(R.string.please_press_back_again_exit))

    }

    override fun clickOnLocatioDelete(position: Int) {
        locationFilterlist.removeAt(position)
        if (locationFilterlist.size == 1) {
            setAdapterLocationPos(0)
        } else {
            setAdapterLocation()
        }

    }

    private fun getAddressFrmLatlang(paramLatitude: String, paramLongitude: String, location_address: String): String {
        try {
            geocoder = Geocoder(activity, Locale.ENGLISH)
            addresses = geocoder!!.getFromLocation(paramLatitude.toDouble(), paramLongitude.toDouble(), 1) // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            if (addresses.isNotEmpty()) {
                if (addresses.isNotEmpty()) {
                    if (addresses[0].locality != null) {
                        address = addresses[0].locality
                        Log.e("address1", address)
                    } else {
                        if (addresses[0].adminArea != null) {
                            address = addresses[0].adminArea
                        } else {
                            if (addresses[0].getAddressLine(0) != null) {
                                address = addresses[0].getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                                Log.e("address2", address)
                            }
                        }
                    }
                }
            } else {
                address = location_address
            }
            if (address == null) {
                address = location_address
            }

        } catch (e: Exception) {
        }
        return address
    }

    private fun getAddressFrmLatlangLocation(paramLatitude: String, paramLongitude: String): String {
        try {
            geocoder = Geocoder(activity, Locale.ENGLISH)
            addresses = geocoder!!.getFromLocation(paramLatitude.toDouble(), paramLongitude.toDouble(), 1) // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            if (addresses.isNotEmpty()) {
                if (addresses.isNotEmpty()) {
                    if (addresses[0].locality != null) {
                        address = addresses[0].locality
                        Log.e("address1", address)
                    } else {
                        if (addresses[0].adminArea != null) {
                            address = addresses[0].adminArea
                        } else {
                            if (addresses[0].getAddressLine(0) != null) {
                                address = addresses[0].getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                                Log.e("address2", address)
                            }
                        }
                    }
                }
            }

        } catch (e: Exception) {

        }
        return address
    }


    override fun onDrawerStateChanged(newState: Int) {
    }

    override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
        Log.e("call", "slide")
        setBlurBackgroundBlurWhenSilde()
    }

    private fun setBlurBackgroundBlurWhenSilde() {
        if (selectedTabPosition == 0 || selectedTabPosition == 1) {

            val view1: View = findViewById(R.id.rl1)
            var final_Bitmap: Bitmap? = null
            val a = getBitmapFromView(view1)
            final_Bitmap = BlurImage(a);
            ivBlur!!.setImageBitmap(final_Bitmap)

            if (selectedTabPosition == 0) {
                ivBlur.visibility = View.VISIBLE
                ivBlurMap.visibility = View.INVISIBLE

            }
            if (selectedTabPosition == 1) {
                ivBlur.visibility = View.INVISIBLE
                //click to take Map screen bitmap
                fragmentClick!!.clickonFragment("MapBlurBitmap")
                if (MapFragment.final_Bitmap_map != null) {
                    ivBlurMap!!.setImageBitmap(MapFragment.final_Bitmap_map)
                    MapFragment.final_Bitmap_map = null
                }
                ivBlurMap.visibility = View.VISIBLE
            }
        }
    }

    override fun onDrawerClosed(drawerView: View) {
        callAPIAddFilterMethod()

        if (selectedTabPosition == 0 || selectedTabPosition == 1) {
            ivFilterOpen.visibility = View.VISIBLE
        } else {
            ivFilterOpen.visibility = View.GONE
        }
    }

    private fun callAPIAddFilterMethod() {
        if (selectedTabPosition == 0) {
            callAPIAddHomeFilter()
            ivFilterOpen.visibility = View.VISIBLE
        }
        if (selectedTabPosition == 1) {
            callAPIAddMapFilter()
            ivFilterOpen.visibility = View.VISIBLE
        }
    }

    override fun onDrawerOpened(drawerView: View) {
        setBitmapForFilterBackground()
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)

        clickEventNoti = ""
        //when application in foreground state (means when we interect with app) for notification click this method called
        Log.e("call_new_intent_method", "call2")

        checkLoginState()
        clickEventNoti = ""
        var i = intent
        if (i != null) {
            try {
                clickEventNoti = i.getStringExtra("type").toString()  //click_event_noti
                Log.e("foreground_click1", clickEventNoti)

                if (clickEventNoti == "4") {
                    fromIdChatNotificaton = i.getStringExtra("from_id")
                    Log.e("foreground_click1", clickEventNoti + "ff" + fromIdChatNotificaton)

                    //set home fragment as deefault
                    bottom_navigation.menu.getItem(0).isChecked = true
                    openHomeFragment()

                    val myactivity = Intent(activity, ChatActivity::class.java)
                    myactivity.putExtra("to_user_name", "")
                    myactivity.putExtra("to_user_id", fromIdChatNotificaton)
                    myactivity.putExtra("user_image", "")
                    myactivity.putExtra("isFrom", "notification") //here we set blank
                    startActivity(myactivity)
                    AppUtils.overridePendingTransitionEnter(activity)
                    //finish()
                }
                if (clickEventNoti == "1" || clickEventNoti == "2") {
                    bottom_navigation.menu.getItem(3).isChecked = true
                    openFavoriteFragment("like")
                }
                if (clickEventNoti == "3") {
                    bottom_navigation.menu.getItem(3).isChecked = true
                    openFavoriteFragment("strike")
                }
                /*  setTabAccordingClick()*/
            } catch (e: Exception) {

            }
        }
    }

    @AfterPermissionGranted(RC_FINE_LOCATION)
    fun callFineLocation() {
        if (EasyPermissions.hasPermissions(activity, Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Have permission, do the thing!
            callCorseLocation()
        } else {
            // Request one permission
            EasyPermissions.requestPermissions(
                    PermissionRequest.Builder(this, RC_FINE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
                            .setRationale(R.string.rationale_call)
                            .setPositiveButtonText(getString(R.string.ok))
                            .setNegativeButtonText(getString(R.string.cancel))
                            .build())
        }
    }

    //permisson read storage
    @AfterPermissionGranted(RC_ACCESSS_LOCATION)
    fun callCorseLocation() {
        if (EasyPermissions.hasPermissions(activity, Manifest.permission.ACCESS_COARSE_LOCATION)) {
            // Have permission, do the thing!
            //for start foreground location service
            ForegroundService.startService(this, "Foreground Service is running...")
            getUserCurrentLocation()

        } else {
            // Request one permission
            EasyPermissions.requestPermissions(
                    PermissionRequest.Builder(this, RC_ACCESSS_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
                            .setRationale(R.string.rationale_call)
                            .setPositiveButtonText(getString(R.string.ok))
                            .setNegativeButtonText(getString(R.string.cancel))
                            .build())
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    private fun getUserCurrentLocation() {
        locationTrack = LocationTrack(activity)
        if (locationTrack!!.canGetLocation()) {
            if (locationTrack!!.getLongitude().toString() != "0.0") {
                paramLongitude = locationTrack!!.getLongitude().toString()
                paramLatitude = locationTrack!!.getLatitude().toString()
                Log.e("latlng_home", paramLatitude + "name" + paramLongitude)
            }
        } else {
            // locationTrack.showSettingsAlert()
        }
    }
}







