package com.hiddle.app.activity

import android.accounts.AuthenticatorException
import android.accounts.NetworkErrorException
import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.hiddle.app.R
import com.hiddle.app.model.GeneralModel
import com.hiddle.app.retrofit.RetrofitRestClient
import com.hiddle.app.util.AppUtils
import com.hiddle.app.util.SharedPreferenceManager
import kotlinx.android.synthetic.main.activity_add_recovery_email.*
import kotlinx.android.synthetic.main.activity_change_recovery_email.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketTimeoutException
import java.util.*

class ChangeRecoveryEmailActivity : BaseMainActivity(), View.OnClickListener {
    var paramOldEmail: String = ""
    var paramNewEmail: String = ""
    var paramRepeatEmail: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_recovery_email)

        if (SharedPreferenceManager.getMySharedPreferences()!!.getEmail() != null) {
            etEmailOld.setText(SharedPreferenceManager.getMySharedPreferences()!!.getEmail())
        }
        allClicklistener()
    }

    private fun allClicklistener() {
        btnUpdate.setOnClickListener(this)
        ivBackRecovery.setOnClickListener(this)
        llBackRecovery.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnUpdate -> {
                paramOldEmail = etEmailOld.text.toString()
                paramNewEmail = etEmailNew.text.toString()
                paramRepeatEmail = etEmailRecovery.text.toString()
                if (validateEmail()) {
                    callChangeRecoveryAPI()
                }
            }
            R.id.ivBackRecovery -> {
                ivBackRecovery.isEnabled = false
                llBackRecovery.isEnabled = false
                finish()
                AppUtils.overridePendingTransitionExit(activity)
            }
            R.id.llBackRecovery -> {
                ivBackRecovery.isEnabled = false
                llBackRecovery.isEnabled = false
                finish()
                AppUtils.overridePendingTransitionExit(activity)
            }
        }
    }
    override fun onResume() {
        super.onResume()
        ivBackRecovery.isEnabled = true
        llBackRecovery.isEnabled = true
    }
    private fun callChangeRecoveryAPI() {
        if (AppUtils.isConnectedToInternet(activity)) {
            AppUtils.showProgressDialog(activity)
            val params = HashMap<String, String?>()
            params["old_email"] = paramOldEmail
            params["new_email"] = paramRepeatEmail
            val call: Call<GeneralModel>
            call = RetrofitRestClient.instance!!.changeEmail(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, params)
            Log.e("token", SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
            call.enqueue(object : Callback<GeneralModel> {
                @SuppressLint("SetTextI18n", "ShowToast")
                override fun onResponse(call: Call<GeneralModel>?, response: Response<GeneralModel>?) {
                    AppUtils.hideProgressDialog()
                    val basicModel: GeneralModel
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {
                            SharedPreferenceManager.getMySharedPreferences()!!.setEmail(paramRepeatEmail)

                            Toast.makeText(activity, basicModel.message, Toast.LENGTH_SHORT).show()
                            //Toast.makeText(activity, getString(R.string.recovery_email_change_successfully), Toast.LENGTH_SHORT).show()
                            finish()
                            AppUtils.overridePendingTransitionExit(activity)
                        }  else if(Objects.requireNonNull(basicModel).code == "-1")
                        {
                            callLogoutFunction()
                        } else {
                            showSnackBar(activity, basicModel.message)
                        }
                    } else {

                        showSnackBar(activity, response.message())
                    }
                }

                override fun onFailure(call: Call<GeneralModel>?, t: Throwable?) {
                    AppUtils.hideProgressDialog()

                    if (t is SocketTimeoutException) {
                        showSnackBar(activity, getString(R.string.connection_timeout))
                    } else if (t is NetworkErrorException) {
                        showSnackBar(activity, getString(R.string.network_error))
                    } else if (t is AuthenticatorException) {
                        showSnackBar(activity, getString(R.string.authentiation_error))
                    } else {
                        showSnackBar(activity, getString(R.string.something_went_wrong))
                    }
                }
            })
        } else {
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }
    }

    private fun validateEmail(): Boolean {

        if (paramOldEmail.isEmpty()) {
            showSnackBar(activity, getString(R.string.please_enter_old_email))
            return false
        }
        if (!AppUtils.isEmailValid(paramOldEmail)) {
            showSnackBar(activity, getString(R.string.please_enter_valid_old_email))
            return false
        }
        if (paramNewEmail.isEmpty()) {
            showSnackBar(activity, getString(R.string.please_enter_new_email))
            return false
        }
        if (!AppUtils.isEmailValid(paramNewEmail)) {
            showSnackBar(activity, getString(R.string.please_enter_valid_new_email))
            return false
        }
        if (paramRepeatEmail != paramNewEmail) {
            showSnackBar(activity, getString(R.string.please_enter_repeat_recovery_as_new_recovery_email))
            return false
        }
        return true
    }
}
