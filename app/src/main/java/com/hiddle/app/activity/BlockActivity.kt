package com.hiddle.app.activity

import android.accounts.AuthenticatorException
import android.accounts.NetworkErrorException
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import android.view.ContextThemeWrapper
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.hiddle.app.R
import com.hiddle.app.`interface`.ClickUnblockUser
import com.hiddle.app.adapter.BlockAdapter
import com.hiddle.app.model.BlockModel.BlockDataResponse
import com.hiddle.app.model.BlockModel.BlockResponse
import com.hiddle.app.model.GeneralModel
import com.hiddle.app.retrofit.RetrofitRestClient
import com.hiddle.app.util.AppUtils
import com.hiddle.app.util.SharedPreferenceManager
import kotlinx.android.synthetic.main.activity_block.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketTimeoutException
import java.util.*
import kotlin.collections.ArrayList

class BlockActivity : BaseMainActivity(), View.OnClickListener, ClickUnblockUser {
    var blocklist: ArrayList<BlockDataResponse> = ArrayList()
    var blockAdapter: BlockAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_block)

        callAPIBlockList()
        allClicklistener()
    }

    private fun setAdapter() {
        rvBlock!!.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false) //set adapter
        blockAdapter = BlockAdapter(activity, blocklist, this@BlockActivity)
        rvBlock!!.adapter = blockAdapter
    }

    private fun callAPIBlockList() {
        if (AppUtils.isConnectedToInternet(activity)) {
            AppUtils.showProgressDialog(activity)

            val call: Call<BlockResponse>
            call = RetrofitRestClient.instance!!.blockListAPI(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
            Log.e("token", SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
            call.enqueue(object : Callback<BlockResponse> {
                @SuppressLint("SetTextI18n")
                override fun onResponse(call: Call<BlockResponse>?, response: Response<BlockResponse>?) {
                    AppUtils.hideProgressDialog()
                    val basicModel: BlockResponse
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {

                            blocklist.clear()
                            blocklist = basicModel.data as ArrayList<BlockDataResponse>

                            callNoData()

                            setAdapter()
                        }
                        else if(Objects.requireNonNull(basicModel).code == "-1")
                        {
                            callLogoutFunction()
                        }
                        else {
                            callNoData()
                            showSnackBar(activity, basicModel.message)

                        }
                    } else {
                        callNoData()
                        showSnackBar(activity, response.message())

                    }
                }


                override fun onFailure(call: Call<BlockResponse>?, t: Throwable?) {
                    callNoData()
                    AppUtils.hideProgressDialog()
                    if (t is SocketTimeoutException) {
                        showSnackBar(activity, getString(R.string.connection_timeout))
                    } else if (t is NetworkErrorException) {
                        showSnackBar(activity, getString(R.string.network_error))
                    } else if (t is AuthenticatorException) {
                        showSnackBar(activity, getString(R.string.authentiation_error))
                    } else {
                        showSnackBar(activity, getString(R.string.something_went_wrong))
                    }
                }
            })
        } else {

            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }
    }

    private fun callNoData() {
        if (blocklist.size > 0) {
            rvBlock.visibility = View.VISIBLE
            llNoData.visibility = View.GONE
        } else {
            llNoData.visibility = View.VISIBLE
            rvBlock.visibility = View.GONE
        }

    }

    private fun allClicklistener() {
        ivBackBlock.setOnClickListener(this)
        llBackBlock.setOnClickListener(this)
        tvRetry.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.ivBackBlock -> {
                finish()
                AppUtils.overridePendingTransitionExit(activity)
            }
            R.id.llBackBlock -> {
                finish()
                AppUtils.overridePendingTransitionExit(activity)
            }
            R.id.tvRetry -> {
               callAPIBlockList()
            }

        }
    }

    override fun clickOnUser(position: Int) {
        val builder1 = AlertDialog.Builder(ContextThemeWrapper(activity, R.style.AlertDialogCustom))
        builder1.setMessage(getString(R.string.are_you_sure_unblock))
        builder1.setCancelable(true)
        builder1.setPositiveButton(getString(R.string.unblock)) { dialog, id ->
            callUnBlockAPI(position) //api for log out
            dialog.cancel()
        }
        builder1.setNegativeButton(getString(R.string.cancel)) { dialog, id -> dialog.cancel() }
        val alert11 = builder1.create()
        alert11.show()
    }

    private fun callUnBlockAPI(position: Int) {
        if (AppUtils.isConnectedToInternet(activity)) {
            AppUtils.showProgressDialog(activity)
            val params = java.util.HashMap<String, String?>()
            params["to_id"] = blocklist[position].toId

            Log.e("param", params.toString())

            val call: Call<GeneralModel>
            call = RetrofitRestClient.instance!!.callUnBlockUser(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, params)
            Log.e("token", SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
            call.enqueue(object : Callback<GeneralModel> {
                @SuppressLint("SetTextI18n", "ShowToast")
                override fun onResponse(call: Call<GeneralModel>?, response: Response<GeneralModel>?) {
                    val basicModel: GeneralModel
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {

                            blocklist.removeAt(position)
                            setAdapter()
                            callNoData()

                            Toast.makeText(activity, basicModel.message, Toast.LENGTH_SHORT).show()
                        }  else if(Objects.requireNonNull(basicModel).code == "-1")
                        {
                            callLogoutFunction()
                        } else {
                            showSnackBar(activity, basicModel.message)
                        }
                    } else {
                        showSnackBar(activity, response.message())
                    }
                    AppUtils.hideProgressDialog()
                }

                override fun onFailure(call: Call<GeneralModel>?, t: Throwable?) {
                    AppUtils.hideProgressDialog()
                    Log.e("message3", "fail")
                    if (t is SocketTimeoutException) {
                        showSnackBar(activity, getString(R.string.connection_timeout))
                    } else if (t is NetworkErrorException) {
                        showSnackBar(activity, getString(R.string.network_error))
                    } else if (t is AuthenticatorException) {
                        showSnackBar(activity, getString(R.string.authentiation_error))
                    } else {
                        showSnackBar(activity, getString(R.string.something_went_wrong))
                    }
                }
            })
        } else {
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }
    }
}
