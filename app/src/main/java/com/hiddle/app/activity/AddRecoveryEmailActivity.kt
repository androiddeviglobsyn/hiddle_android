package com.hiddle.app.activity

import android.accounts.AuthenticatorException
import android.accounts.NetworkErrorException
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.hiddle.app.R
import com.hiddle.app.model.VerifyCodeModel.VerifyModelResponse
import com.hiddle.app.retrofit.RetrofitRestClient
import com.hiddle.app.util.AppUtils
import com.hiddle.app.util.SharedPreferenceManager
import kotlinx.android.synthetic.main.activity_add_recovery_email.*
import kotlinx.android.synthetic.main.activity_login_using_mobile.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketTimeoutException
import java.util.*
import kotlin.collections.HashMap

class AddRecoveryEmailActivity : BaseMainActivity(), View.OnClickListener {
    var paramEmail: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_recovery_email)

        allClicklistener()
    }

    private fun allClicklistener() {
        btnContinue.setOnClickListener(this)
        ivBackEmail.setOnClickListener(this)
        llBackEmail.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnContinue -> {
                paramEmail = etEmail.text.toString()
                if (validateEmail()) {
                    callAddRecoveryEmailAPI(paramEmail)
                }
            }
            R.id.ivBackEmail -> {
                ivBackEmail.isEnabled = false
                llBackEmail.isEnabled = false
                finish()
                AppUtils.overridePendingTransitionExit(activity)
            }
            R.id.llBackEmail -> {
                ivBackEmail.isEnabled = false
                llBackEmail.isEnabled = false
                finish()
                AppUtils.overridePendingTransitionExit(activity)
            }
        }
    }
    override fun onResume() {
        super.onResume()
        ivBackEmail.isEnabled = true
        llBackEmail.isEnabled = true
    }
    private fun validateEmail(): Boolean {

        if (paramEmail.isEmpty()) {
            showSnackBar(activity, getString(R.string.please_enter_email))
            return false
        }
        if (!AppUtils.isEmailValid(paramEmail)) {
            showSnackBar(activity, getString(R.string.please_enter_valid_email))
            return false
        }

        return true
    }

 
        private fun callAddRecoveryEmailAPI(paramEmail : String) {
            if (AppUtils.isConnectedToInternet(activity)) {
                AppUtils.showProgressDialog(activity)
                val params = HashMap<String, RequestBody?>()
    
                params["email"] = AppUtils.getRequestBody(paramEmail)
             /*   params["language"] = AppUtils.getRequestBody(getString(R.string.french))*/

                Log.e("parameter", "authorization :" + SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken())
    
                val partbody1 = arrayOfNulls<MultipartBody.Part>(0)
    
    
                val call: Call<VerifyModelResponse>
                call = RetrofitRestClient.instance!!.loginUpdateApi(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, params, partbody1)
                call.enqueue(object : Callback<VerifyModelResponse> {
                    override fun onResponse(call: Call<VerifyModelResponse>?, response: Response<VerifyModelResponse>?) {
                        AppUtils.hideProgressDialog()
                        val basicModel: VerifyModelResponse
                        Log.e("Response", response.toString())
                        if (response!!.isSuccessful) {
                            basicModel = response.body()!!
                            if (Objects.requireNonNull(basicModel).code == "1") {
                                Toast.makeText(activity, getString(R.string.recovery_email_added_successfully), Toast.LENGTH_SHORT).show()

                                SharedPreferenceManager.getMySharedPreferences()!!.setEmail(paramEmail)
                                SharedPreferenceManager.getMySharedPreferences()!!.setUserId(basicModel.data!!.id!!)
                                SharedPreferenceManager.getMySharedPreferences()!!.setCountryCode(basicModel.data.cc!!)
                                SharedPreferenceManager.getMySharedPreferences()!!.setMobileNumber(basicModel.data.number!!)
                                SharedPreferenceManager.getMySharedPreferences()!!.setAuthorizationToken(basicModel.data.tokens!![0].token!!)
    
                                /*if (basicModel.data[0].images!!.isNotEmpty()) {
                                    SharedPreferenceManager.getMySharedPreferences()!!.setProfileImage(basicModel.data[0].images!![0])
                                }*/

                                finish()
                                AppUtils.overridePendingTransitionExit(activity)
                            }
                            else if(Objects.requireNonNull(basicModel).code == "-1")
                            {
                                callLogoutFunction()
                            }else {
                                showSnackBar(activity, basicModel.message)
                            }
                        } else {
                            showSnackBar(activity, response.message())
                        }
                    }
    
                    override fun onFailure(call: Call<VerifyModelResponse>?, t: Throwable?) {
                        Log.e("callError", call.toString())
                        Log.e("Error", t.toString())
                        AppUtils.hideProgressDialog()
                        if (t is SocketTimeoutException) {
                            showSnackBar(activity, getString(R.string.connection_timeout))
                        } else if (t is NetworkErrorException) {
                            showSnackBar(activity, getString(R.string.network_error))
                        } else if (t is AuthenticatorException) {
                            showSnackBar(activity, getString(R.string.authentiation_error))
                        } else {
                            showSnackBar(activity, getString(R.string.something_went_wrong))
                        }
                    }
                })
            } else {
                AppUtils.showToast(activity, getString(R.string.no_internet_connection))
            }
        }
    
  
}
