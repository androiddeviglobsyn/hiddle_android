package com.hiddle.app.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.transition.Explode
import android.transition.TransitionInflater
import android.util.Log
import android.view.View
import android.view.Window
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.bumptech.glide.Glide
import com.hiddle.app.R
import com.hiddle.app.util.AppUtils
import com.hiddle.app.util.Constants
import com.hiddle.app.util.SharedPreferenceManager
import kotlinx.android.synthetic.main.activity_strike.*

class StrikeActivity : BaseMainActivity(), View.OnClickListener {
    internal lateinit var type: Constants.TransitionType
    var image: String = ""
    var to_user_name: String = ""
    var to_user_id: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_strike)


        getIntentData()
        initAnimation()
        // For overlap between Exiting MainActivity.java and Entering TransitionActivity.java
        window.allowEnterTransitionOverlap = false
        allClicklistener()
        setRotationOfFavorite()

    }

    private fun setRotationOfFavorite() {
        val aniRotate: Animation = AnimationUtils.loadAnimation(activity!!.applicationContext, R.anim.rotate_backward_more)
        aniRotate.repeatCount = Animation.INFINITE
        aniRotate.fillAfter = true;
        rlBackground!!.startAnimation(aniRotate)

    }

    private fun allClicklistener() {
        tvSearch.setOnClickListener(this)
        btnWrite.setOnClickListener(this)
    }

    private fun getIntentData() {
        var i = intent
        if (i != null) {
            image = i.getStringExtra("image")!!
            Log.e("strike_image3",image)
            if (image != null) {
                Glide.with(activity).load(image).placeholder(R.drawable.ic_placeholder_icn).into(ivStrike)
            }
            type = getIntent().getSerializableExtra(Constants.KEY_ANIM_TYPE) as Constants.TransitionType
            to_user_name = i.getStringExtra("to_user_name")!!
            to_user_id = i.getStringExtra("to_user_id")!!

            tvUserName.setText(getString(R.string.write_to) + " " + to_user_name)
        }
        /* if(SharedPreferenceManager.getMySharedPreferences()!!.getProfileImage()!=null)
         {*/
        Glide.with(activity).load(SharedPreferenceManager.getMySharedPreferences()!!.getProfileImage()).placeholder(R.drawable.ic_placeholder_icn).into(ivHomeImage)
        /*}*/
    }


    override fun onClick(view: View) {
        when (view.id) {
            R.id.tvSearch -> {
                onBackPressed()
                /*finish()
                AppUtils.overridePendingTransitionExit(activity)*/
            }

            R.id.btnWrite -> {
                val myactivity = Intent(activity, ChatActivity::class.java)
                myactivity.putExtra("to_user_name", to_user_name)
                myactivity.putExtra("to_user_id", to_user_id)
                myactivity.putExtra("user_image", image)
                myactivity.putExtra("isFrom","")
                startActivity(myactivity)
                AppUtils.overridePendingTransitionEnter(activity)
                finish()
            }


        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
        AppUtils.overridePendingTransitionExit(activity)
        /*rlBackgroundAnim.visibility =  View.GONE
        Handler().postDelayed({
            finishAfterTransition()
        }, 900)*/

    }

    private fun initAnimation() {
        when (type) {
            Constants.TransitionType.ExplodeJava -> { // For Explode By Code
                val enterTransition = Explode()
                enterTransition.setDuration(1000)
                getWindow().setEnterTransition(enterTransition)

                //for make background layout visible
                Handler().postDelayed({
                    rlBackgroundAnim.visibility = View.VISIBLE
                    val aniRotate: Animation = AnimationUtils.loadAnimation(activity.applicationContext, R.anim.blink)
                    aniRotate.fillAfter = false;
                    rlBackgroundAnim!!.startAnimation(aniRotate)
                }, 1400)
            }

            Constants.TransitionType.ExplodeXML -> { // For Explode By XML
                val enterTansition = TransitionInflater.from(this).inflateTransition(R.transition.explode)
                getWindow().setEnterTransition(enterTansition)
            }
            /*  SlideJava -> { // For Slide By Code
                val enterTransition = Slide()
                enterTransition.setSlideEdge(Gravity.TOP)
                enterTransition.setDuration(getResources().getInteger(R.integer.anim_duration_very_long))
                enterTransition.setInterpolator(AnticipateOvershootInterpolator())
                getWindow().setEnterTransition(enterTransition)
              }
              SlideXML -> { // For Slide by XML
                val enterTansition = TransitionInflater.from(this).inflateTransition(R.transition.slide)
                getWindow().setEnterTransition(enterTansition)
              }
              FadeJava -> { // For Fade By Code
                val enterTransition = Fade()
                enterTransition.setDuration(getResources().getInteger(R.integer.anim_duration_medium))
                getWindow().setEnterTransition(enterTransition)
              }
              FadeXML -> { // For Fade by XML
                val enterTansition = TransitionInflater.from(this).inflateTransition(R.transition.fade)
                getWindow().setEnterTransition(enterTansition)
              }*/
        }
    }
}
