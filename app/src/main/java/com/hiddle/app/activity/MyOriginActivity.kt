package com.hiddle.app.activity

import android.Manifest
import android.Manifest.permission
import android.accounts.AuthenticatorException
import android.accounts.NetworkErrorException
import android.content.Intent
import android.content.IntentSender.SendIntentException
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.blongho.country_data.World
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener
import com.google.android.gms.location.*
import com.hiddle.app.R
import com.hiddle.app.activity.EditProfileActivity.Companion.originProfilelist
import com.hiddle.app.activity.HomeActivity.Companion.homeOriginlist
import com.hiddle.app.adapter.OriginAdapter
import com.hiddle.app.adapter.OriginAdapter.Companion.selectedOriginList

import com.hiddle.app.model.OriginModel
import com.hiddle.app.model.VerifyCodeModel.VerifyModelResponse
import com.hiddle.app.retrofit.RetrofitRestClient
import com.hiddle.app.service.LocationTrack
import com.hiddle.app.util.AppUtils
import com.hiddle.app.util.SharedPreferenceManager
import kotlinx.android.synthetic.main.activity_my_height.*
import kotlinx.android.synthetic.main.activity_my_origin.*
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.internal.trimSubstring
import org.json.JSONException
import org.json.JSONObject
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions
import pub.devrel.easypermissions.PermissionRequest
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.net.SocketTimeoutException
import java.util.*
import kotlin.collections.ArrayList


import kotlin.collections.HashMap


public class MyOriginActivity : BaseMainActivity(), View.OnClickListener, ConnectionCallbacks, OnConnectionFailedListener, LocationListener {
    ///location
    var locationTrack: LocationTrack? = null
    var mLocation: Location? = null
    var mGoogleApiClient: GoogleApiClient? = null
    private val PLAY_SERVICES_RESOLUTION_REQUEST = 9000
    private var mLocationRequest: LocationRequest? = null
    private val UPDATE_INTERVAL: Long = 15000 /* 15 secs */
    private val FASTEST_INTERVAL: Long = 5000 /* 5 secs */
    var mLastLocation: Location? = null
    var isSearch: Boolean = false;
    var handler = Handler()
    var geocoder: Geocoder? = null
    var addresses: List<Address> = ArrayList()
    var address: String = ""

    companion object {   // for make static
        const val RC_ACCESSS_LOCATION: Int = 126
        const val RC_FINE_LOCATION: Int = 127
    }

    var originlist: ArrayList<OriginModel> = ArrayList()
    var originAdapter: OriginAdapter? = null
    var paramIsFrom: String = "homeFilter"

    //for login success
    var paramHeight: String = ""
    var paramAge: String = ""
    var paramEmail: String = ""
    var paramUserName: String = ""
    private var isFromLogin: String = ""
    private var socialId: String = ""
    private var authorizationToken: String = ""
    private var paramGender: String = ""
    private var paramLatitude: String = ""
    private var paramLongitude: String = ""
    var list: ArrayList<String> = ArrayList()
    var mainlist: ArrayList<String> = ArrayList()
    var paramOrigin: String = ""
    var paramAddress: String = ""
    private var uploadImagelist: ArrayList<String> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_origin)

        getPermissionForLocation()
        getIntentData()
        setAdapterForOrigin()  // set adapter for origin
        setSearchListenerMethod()//search method for it
        allClicklistener()//click listener
    }

    private fun getPermissionForLocation() {
        callFineLocation()
    }

    private fun getIntentData() {
        val i = intent
        if (i != null) {
            paramIsFrom = i.getStringExtra("isFrom")!!

            if (paramIsFrom == "homeFilter") {

                tvTitle.text = getString(R.string.origins)

                selectedOriginList.clear()
                for (p in homeOriginlist.indices) {
                    selectedOriginList.add(homeOriginlist[p])
                }
            }

            if (paramIsFrom == "profile") {
                selectedOriginList.clear()
                for (p in originProfilelist.indices) {
                    selectedOriginList.add(originProfilelist[p])
                }
            }


            if (paramIsFrom == "myHeight") {

                selectedOriginList.clear()
                paramUserName = i.getStringExtra("paramUserName")!!
                paramEmail = i.getStringExtra("paramEmail")!!
                isFromLogin = i.getStringExtra("isFromLogin")!!
                socialId = i.getStringExtra("socialId")!!
                authorizationToken = i.getStringExtra("authorizationToken")!!
                paramGender = i.getStringExtra("paramGender")!!
                paramAge = i.getStringExtra("paramAge")!!
                paramHeight = i.getStringExtra("paramHeight")!!
                uploadImagelist = i.getStringArrayListExtra("uploadImagelist")!!
                Log.e("origin_name", paramUserName)
                Log.e("verify_token1", authorizationToken)
            }

        }
    }


    private fun setSearchListenerMethod() {
        etSearch.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                if (s.toString().isNotEmpty()) {
                    ivCrossOrigin.visibility = View.VISIBLE
                } else {
                    ivCrossOrigin.visibility = View.GONE
                }

                try {
                    if (originlist.size > 0) {
                        isSearch = true
                        filter(s.toString())
                    } else {
                        isSearch = false
                        filter(s.toString())
                    }

                } catch (e: Exception) {
                }
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun afterTextChanged(s: Editable) {}
        }) //for local Search
    }

    private fun setAdapterForOrigin() {
        World.init(applicationContext);   //library for set image flag of country
        var originModel: OriginModel? = null

        getDataFromJsonForOrigin()  //set origin list from jsonfile

        mainlist = ArrayList(list.sortedWith(compareBy { it }))  //sort list by alphabet

        if (paramIsFrom == "homeFilter") {
            mainlist.add(0, getString(R.string.select_all))
        }
        for (i in mainlist.indices) {
            originModel = OriginModel()
            originModel.countryName = mainlist[i]
            originModel.id = (i + 1).toString()
            originlist.add(originModel)
            Log.e("Name", mainlist[i])
        }

        setOriginAdapter()
    }

    private fun getDataFromJsonForOrigin() {
        try {
            val jsonfile: String = applicationContext!!.assets.open("country_name").bufferedReader().use { it.readText() }
            val obj = JSONObject(jsonfile)
            val userArray = obj.getJSONArray("country")
            // Log.e("object",obj.toString())
            // implement for loop for getting users list data
            for (i in 0 until userArray.length()) {
                val userDetail = userArray.getJSONObject(i)
                list.add(userDetail.getString("name"))
            }
            Log.e("countrylist", list.size.toString())
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    fun setOriginAdapter() {
        if (paramIsFrom == "homeFilter") {
            if (selectedOriginList.contains(getString(R.string.select_all))) {
                for (i in originlist.indices) {
                    if (!selectedOriginList.contains(originlist[i].countryName)) {
                        selectedOriginList.add(originlist[i].countryName!!)
                    }
                }
            }
        }

        rvOrigin.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false) //set adapter
        originAdapter = OriginAdapter(activity, originlist, paramIsFrom, this@MyOriginActivity)
        rvOrigin.adapter = originAdapter
    }

    fun removeSelectAllOrigin() {
        originAdapter?.notifyItemChanged(0)
    }

    private fun allClicklistener() {
        ivBackOrigin.setOnClickListener(this)
        llBackOrigin.setOnClickListener(this)
        llDone.setOnClickListener(this)
        ivCrossOrigin.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.ivBackOrigin -> {
                ivBackOrigin.isEnabled = false
                llBackOrigin.isEnabled = false
                finish()
                AppUtils.overridePendingTransitionExit(activity)
            }
            R.id.llBackOrigin -> {
                ivBackOrigin.isEnabled = false
                llBackOrigin.isEnabled = false
                finish()
                AppUtils.overridePendingTransitionExit(activity)
            }
            R.id.ivCrossOrigin -> {
                if (etSearch.text.toString() != "") {
                    etSearch.setText("")
                    filter("")
                    ivCrossOrigin.visibility = View.GONE
                }
            }
            R.id.llDone -> {
                getParameterOrigin()

                if (validate()) {
                    if (paramIsFrom == "homeFilter") {

                        homeOriginlist.clear()

                        //check conditon all selected or not
                        if (selectedOriginList.contains(getString(R.string.select_all))) {
                            homeOriginlist.add(getString(R.string.select_all))
                        }
                        //check conditon only selected country
                        if (!selectedOriginList.contains(getString(R.string.select_all))) {
                            for (k in selectedOriginList.indices) {
                                homeOriginlist.add(selectedOriginList[k])
                            }
                        }
                        llDone.isEnabled = false
                        finish()
                        AppUtils.overridePendingTransitionExit(activity)
                    }
                    if (paramIsFrom == "profile") {
                        originProfilelist.clear()
                        for (k in selectedOriginList.indices) {
                            originProfilelist.add(selectedOriginList[k])
                        }
                        llDone.isEnabled = false
                        finish()
                        AppUtils.overridePendingTransitionExit(activity)
                    }
                    if (paramIsFrom == "myHeight") {

                        getPermissionForLocation() //for get lat_long if not get

                        if (paramLatitude != "") {//for get address from latlng
                            paramAddress = getAddressFrmLatlang(paramLatitude, paramLongitude)
                        }

                        getCallLoginUserUpdateDetailAPI()  //API update detail
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        ivBackOrigin.isEnabled = true
        llBackOrigin.isEnabled = true
        llDone.isEnabled = true
    }

    private fun getParameterOrigin() {
        var origin :String = ""/*selectedOriginList.toString().replace("{","")*/


        for (k in selectedOriginList.indices) {
            if (k == selectedOriginList.size - 1) {
                origin += selectedOriginList[k].toString().trimSubstring(0)
            } else {
                origin = origin + selectedOriginList[k].toString().trimSubstring(0) + ","
            }
        }

        /* origin = origin.toString().replace("}","")
         origin = origin.toString().replace("[","")
         origin = origin.toString().replace("]","")*/
        paramOrigin = origin.trim()
        Log.e("paramOrigin", paramOrigin)
    }


    fun getAddressFrmLatlang(LATITUDE: String, LONGITUDE: String): String {
        try {
            geocoder = Geocoder(activity, Locale.ENGLISH)
            addresses = geocoder!!.getFromLocation(LATITUDE.toDouble(), LONGITUDE.toDouble(), 1) // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            Log.e("Address", addresses[0].toString())
            if (addresses.isNotEmpty()) {

                if (addresses[0].locality != null) {
                    if (addresses[0].locality == addresses[0].adminArea) {
                        address = addresses[0].adminArea + "," + addresses[0].countryName
                    } else {
                        address = addresses[0].locality + "," + addresses[0].adminArea + "," + addresses[0].countryName
                    }
                } else {
                    address = addresses[0].adminArea + "," + addresses[0].countryName
                }


                Log.e("Name", address)

            }

            if (address == "") {
                if (addresses[0].getAddressLine(0) != null) {
                    address = addresses[0].getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                }
            }

        } catch (e: Exception) {

        }
        return address;

    }

    private fun redirectToHomeScreenActivity() {
        val i = Intent(activity, HomeActivity::class.java)
        i.putExtra("type", "home")
        i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(i)
        AppUtils.overridePendingTransitionEnterDown(activity)
    }

    private fun getCallLoginUserUpdateDetailAPI() {

        paramHeight = paramHeight.replace("\\s".toRegex(), "")

        Log.e("name", paramUserName)
        if (AppUtils.isConnectedToInternet(activity)) {
            AppUtils.showProgressDialog(activity)
            val params = HashMap<String, RequestBody?>()
            if (isFromLogin.equals("facebook")) {
                params["social_id"] = AppUtils.getRequestBody(socialId)
                Log.e("facebook_id", socialId)
            }
            if (isFromLogin.equals("instagram")) {
                params["insta_id"] = AppUtils.getRequestBody(socialId)
                Log.e("insta_id", socialId)
            }
            if (isFromLogin.equals("apple")) {
                params["apple_id"] = AppUtils.getRequestBody(socialId)
                Log.e("apple_id", socialId)
            }

            params["email"] = AppUtils.getRequestBody(paramEmail)
            params["username"] = AppUtils.getRequestBody(paramUserName)
            params["gender"] = AppUtils.getRequestBody(paramGender)
            params["age"] = AppUtils.getRequestBody(paramAge)
            params["height"] = AppUtils.getRequestBody(paramHeight)
            params["origins"] = AppUtils.getRequestBody(paramOrigin)
            params["education"] = AppUtils.getRequestBody("")
            params["address"] = AppUtils.getRequestBody(paramAddress)
            params["latitude"] = AppUtils.getRequestBody(paramLatitude)
            params["longitude"] = AppUtils.getRequestBody(paramLongitude)
            params["language"] = AppUtils.getRequestBody(SharedPreferenceManager.getMySharedPreferences()!!.getLanguage())

            Log.e("parameter", SharedPreferenceManager.getMySharedPreferences()!!.getLanguage() + paramLatitude + "long" + paramLongitude)
            Log.e("verify_token", authorizationToken+"99"+paramOrigin)


            val partbody1 = arrayOfNulls<MultipartBody.Part>(uploadImagelist.size)
            if (uploadImagelist.size > 0) {
                for (index in uploadImagelist.indices) {
                    if (uploadImagelist[index] != "") {

                        params["image" + (index + 1).toString() + "_order_id"] = AppUtils.getRequestBody((index + 1).toString())
                        params["image" + (index + 1).toString() + "_edit_id"] = AppUtils.getRequestBody("")

                        val file: File = File(uploadImagelist[index])
                        val reqFile1: RequestBody = file.asRequestBody("image/*".toMediaTypeOrNull())
                        partbody1[index] = MultipartBody.Part.createFormData("image" + (index + 1).toString(), file.name, reqFile1) //for  imagelist of file
                        Log.e("photopath", uploadImagelist[index])
                    }

                }
            }

            val call: Call<VerifyModelResponse>
            call = RetrofitRestClient.instance!!.loginUpdateApi(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, params, partbody1)
            call.enqueue(object : Callback<VerifyModelResponse> {
                override fun onResponse(call: Call<VerifyModelResponse>?, response: Response<VerifyModelResponse>?) {
                    AppUtils.hideProgressDialog()
                    val basicModel: VerifyModelResponse
                    Log.e("Response", response.toString())
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {
                            Toast.makeText(activity, getString(R.string.successfully_login_text), Toast.LENGTH_SHORT).show()
                            SharedPreferenceManager.getMySharedPreferences()!!.setLogin(true)
                            SharedPreferenceManager.getMySharedPreferences()!!.setUserId(basicModel.data!!.id!!)
                            SharedPreferenceManager.getMySharedPreferences()!!.setUserName(basicModel.data.username!!)
                            SharedPreferenceManager.getMySharedPreferences()!!.setCountryCode(basicModel.data!!.cc!!)
                            SharedPreferenceManager.getMySharedPreferences()!!.setMobileNumber(basicModel.data!!.number!!)
                            SharedPreferenceManager.getMySharedPreferences()!!.setAuthorizationToken(basicModel.data!!.tokens!![0].token!!)
                            SharedPreferenceManager.getMySharedPreferences()!!.setLanguahe(basicModel.data!!.preferences!!.language!!)

                            if (basicModel.data.images!!.isNotEmpty()) {
                                SharedPreferenceManager.getMySharedPreferences()!!.setProfileImage(basicModel.data!!.base_url + basicModel.data.images[0].image)
                            }

                            if (basicModel.data.email != null) {
                                SharedPreferenceManager.getMySharedPreferences()!!.setEmail(basicModel.data!!.email!!)
                            }

                            redirectToHomeScreenActivity()  //succesfully login
                        } else if (Objects.requireNonNull(basicModel).code == "-1") {
                            callLogoutFunction()
                        } else {
                            showSnackBar(activity, basicModel.message)
                        }
                    } else {
                        showSnackBar(activity, response.message())
                    }
                }

                override fun onFailure(call: Call<VerifyModelResponse>?, t: Throwable?) {
                    Log.e("callError", call.toString())
                    Log.e("Error", t.toString())
                    AppUtils.hideProgressDialog()
                    if (t is SocketTimeoutException) {
                        showSnackBar(activity, getString(R.string.connection_timeout))
                    } else if (t is NetworkErrorException) {
                        showSnackBar(activity, getString(R.string.network_error))
                    } else if (t is AuthenticatorException) {
                        showSnackBar(activity, getString(R.string.authentiation_error))
                    } else {
                        showSnackBar(activity, getString(R.string.something_went_wrong))
                    }
                }
            })
        } else {
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }
    }

    private fun validate(): Boolean {
        if (selectedOriginList.size == 0) {
            showSnackBar(activity, getString(R.string.select_origin))
            return false
        }
        return true
    }

    fun filter(text: String) {
        val filterdNames: ArrayList<OriginModel> = ArrayList<OriginModel>()
        for (s in originlist) {
            if (s.countryName != null) {
                if (s.countryName.toString().startsWith(text, true)) {
                    /* if (s.countryName!!.toLowerCase().contains(text.toLowerCase())) {*/
                    filterdNames.add(s)
                }
            }
        }
        if (originlist.size > 0 && filterdNames.size > 0) {
            originAdapter?.updateList(filterdNames)
        } else {
            originAdapter?.updateList(filterdNames)
        }
    }

    @AfterPermissionGranted(RC_FINE_LOCATION)
    fun callFineLocation() {
        if (EasyPermissions.hasPermissions(activity, Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Have permission, do the thing!
            callCorseLocation()
        } else {
            // Request one permission
            EasyPermissions.requestPermissions(
                    PermissionRequest.Builder(this, RC_FINE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
                            .setRationale(R.string.rationale_call)
                            .setPositiveButtonText(getString(R.string.ok))
                            .setNegativeButtonText(getString(R.string.cancel))
                            .build())
        }
    }

    //permisson read storage
    @AfterPermissionGranted(RC_ACCESSS_LOCATION)
    fun callCorseLocation() {
        if (EasyPermissions.hasPermissions(activity, Manifest.permission.ACCESS_COARSE_LOCATION)) {
            // Have permission, do the thing!

            getUserCurrentLocation()  //call google api client for get location

            if (paramLatitude == "") {
                handler = Handler()  //handler for get location up to not get location
                handler.postDelayed({
                    if (paramLatitude == "") {
                        callLocationTracker()
                        getUserCurrentLocation()
                        Log.e("handler", "call")
                    } else {
                        handler.removeCallbacksAndMessages(null)
                        handler.removeMessages(0)
                        Log.e("handler", "stop")
                    }
                    //Do something after 20 seconds
                }, 2000) //the time is in miliseconds}*/

            }

        } else {
            // Request one permission
            EasyPermissions.requestPermissions(
                    PermissionRequest.Builder(this, RC_ACCESSS_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
                            .setRationale(R.string.rationale_call)
                            .setPositiveButtonText(getString(R.string.ok))
                            .setNegativeButtonText(getString(R.string.cancel))
                            .build())
        }
    }

    private fun getUserCurrentLocation() {

        mGoogleApiClient = GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build()
        if (mGoogleApiClient != null) {
            mGoogleApiClient!!.connect()
        } else Log.e("Not Connected", "Not Connected")

        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }


    override fun onStart() {
        super.onStart()
        if (mGoogleApiClient != null) {
            mGoogleApiClient!!.connect()
        }
    }

    override fun onLocationChanged(location: Location?) {
        if (location != null)
            paramLatitude = location.latitude.toString()
        paramLongitude = location!!.longitude.toString()
        //Log.e("location", paramLatitude + "kk" + paramLongitude)
    }

    /*Ending the updates for the location service*/
    override fun onStop() {
        stopLocationUpdates()
        super.onStop()

        //for stop handler
        handler.removeCallbacksAndMessages(null);
        handler.removeMessages(0);

    }

    override fun onConnected(bundle: Bundle?) {
        if (paramLatitude == "") {
            settingRequest()
        }
    }

    override fun onConnectionSuspended(i: Int) {
        Log.e("Connection Suspended!", "Connection Suspended!")
        // Toast.makeText(this, "Connection Suspended!", Toast.LENGTH_SHORT).show();
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        Log.e("Connection Failed!", "Connection Failed!")
        //Toast.makeText(this, "Connection Failed!", Toast.LENGTH_SHORT).show();
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, 90000)
            } catch (e: SendIntentException) {
                e.printStackTrace()
            }
        } else {
            Log.i("Current Location", "Location services connection failed with code " + connectionResult.errorCode)
        }
    }

    /*Method to get the enable location settings dialog*/
    fun settingRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest!!.interval = 10000 // 10 seconds, in milliseconds
        mLocationRequest!!.fastestInterval = 5000 // 1 second, in milliseconds
        mLocationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        val builder = LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest!!)
        val result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient,
                builder.build())
        result.setResultCallback { result ->
            val status = result.status
            val state = result.locationSettingsStates
            when (status.statusCode) {
                LocationSettingsStatusCodes.SUCCESS -> {
                    // All location settings are satisfied. The client can
                    // initialize location requests here.
                    if (paramLatitude == "") {
                        getLocation()
                    }

                    getPermissionForLocation()
                }

                LocationSettingsStatusCodes.RESOLUTION_REQUIRED ->                         // Location settings are not satisfied, but this can be fixed
                    // by showing the user a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        status.startResolutionForResult(activity, 1000)

                    } catch (e: SendIntentException) {
                        // Ignore the error.
                    }
                LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                }
            }
        }
    }

    /*Get Location*/
    fun getLocation() {
        if (ActivityCompat.checkSelfPermission(this, permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        } else {
            /*Getting the location after aquiring location service*/
            if (mLastLocation != null) {
                paramLatitude = mLastLocation!!.getLatitude().toString()
                paramLongitude = mLastLocation!!.getLongitude().toString()
                Log.e("latitude", paramLatitude + "location" + paramLongitude)

            } else {
                /*if there is no last known location. Which means the device has no data for the loction currently.
                 * So we will get the current location.
                 * For this we'll implement Location Listener and override onLocationChanged*/
                Log.i("Current Location", "No data for location found")
                if (mGoogleApiClient != null) {
                    if (!mGoogleApiClient!!.isConnected) mGoogleApiClient!!.connect()

                    if (mGoogleApiClient!!.isConnected) {
                        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, activity as LocationListener?)
                    } else {
                        mGoogleApiClient!!.disconnect()
                        Log.e("Google_Api_Client:", "It was NOT connected on ");
                        /* if (mGoogleApiClient != null) {
                             mGoogleApiClient!!.connect()
                         }*/
                    }

                }

            }
        }
    }

    private fun callLocationTracker() {
        locationTrack = LocationTrack(activity)
        if (locationTrack!!.canGetLocation()) {
            if (locationTrack!!.getLongitude().toString() != "0.0") {
                paramLongitude = locationTrack!!.getLongitude().toString()
                paramLatitude = locationTrack!!.getLatitude().toString()
                Log.e("location_tracker", paramLatitude)
            }
        } else {
            // locationTrack.showSettingsAlert()
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        try {
            stopLocationUpdates()
            if (locationTrack != null) {
                locationTrack!!.stopListener()
            }
        } catch (e: Exception) {
        }
    }

    private fun stopLocationUpdates() {
        if (mGoogleApiClient != null) {
            if (mGoogleApiClient!!.isConnected) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this)
                mGoogleApiClient!!.disconnect()
            }
        }
    }
}
