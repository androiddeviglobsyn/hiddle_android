package com.hiddle.app.activity

import android.content.Intent
import android.os.Bundle
import android.view.View

import com.hiddle.app.R
import com.hiddle.app.util.AppUtils
import kotlinx.android.synthetic.main.activity_recovery_email.*

class RecoveryEmailActivity : BaseMainActivity(), View.OnClickListener {

    var paramEmail: String = ""
    var paramUserName: String = ""
    private var isFromLogin: String = ""
    private var socialId: String = ""
    private var authorizationToken: String = ""
    private var profile_url : String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recovery_email)

        getIntentData()
        allClicklistener()
    }

    private fun getIntentData() {
        var i = intent
        if (i != null) {
            paramUserName = i.getStringExtra("paramUserName")!!
            paramEmail = i.getStringExtra("paramEmail")!!
            isFromLogin = i.getStringExtra("isFromLogin")!!
            socialId = i.getStringExtra("socialId")!!
            authorizationToken = i.getStringExtra("authorizationToken")!!
            profile_url = i.getStringExtra("profile_url")!!
            etEmail.setText(paramEmail)
        }
    }

    private fun allClicklistener() {
        ivBack.setOnClickListener(this)
        tvSkip.setOnClickListener(this)
        llNext.setOnClickListener(this)
        llBackRecovery.setOnClickListener(this)
    }


    private fun validate(): Boolean {
        if (paramEmail.isEmpty()) {
            showSnackBar(activity, getString(R.string.please_enter_email))
            return false
        }
        if (!AppUtils.isEmailValid(paramEmail)) {
            showSnackBar(activity, getString(R.string.please_enter_valid_email))
            return false
        }
        return true
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.tvSkip -> {
                paramEmail = ""
                reditectToAddPicture()
            }
            R.id.llNext -> {
                paramEmail = etEmail.text.toString()
                if (validate()) {
                    reditectToAddPicture()
                }
            }
            R.id.ivBack -> {
                finish()
                AppUtils.overridePendingTransitionExit(activity)
            }
            R.id.llBackRecovery -> {
                finish()
                AppUtils.overridePendingTransitionExit(activity)
            }
        }
    }

    private fun reditectToAddPicture() {
        val i = Intent(activity, AddPictureScreenActivity::class.java)
        i.putExtra("paramUserName", paramUserName)
        i.putExtra("paramEmail", paramEmail)
        i.putExtra("isFromLogin", isFromLogin)
        i.putExtra("socialId", socialId)
        i.putExtra("authorizationToken",authorizationToken)
        i.putExtra("profile_url",profile_url)
        startActivity(i)
        AppUtils.overridePendingTransitionEnter(activity)
    }
}
