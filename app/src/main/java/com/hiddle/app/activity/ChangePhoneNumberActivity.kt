package com.hiddle.app.activity

import android.accounts.AuthenticatorException
import android.accounts.NetworkErrorException
import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.hiddle.app.R
import com.hiddle.app.model.GeneralModel
import com.hiddle.app.model.SendOTPModel
import com.hiddle.app.retrofit.RetrofitRestClient
import com.hiddle.app.util.AppUtils
import com.hiddle.app.util.SharedPreferenceManager
import kotlinx.android.synthetic.main.activity_change_phone_number.*
import kotlinx.android.synthetic.main.activity_change_phone_number.btnReSend
import kotlinx.android.synthetic.main.activity_change_phone_number.btnUpdate
import kotlinx.android.synthetic.main.activity_change_phone_number.llRetry
import kotlinx.android.synthetic.main.activity_change_phone_number.tvRetryTime
import kotlinx.android.synthetic.main.activity_change_recovery_email.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketTimeoutException
import java.util.*
import kotlin.collections.HashMap

class ChangePhoneNumberActivity : BaseMainActivity(), View.OnClickListener {

    var paramOldPhone: String = ""
    var paramNewPhone: String = ""
    var paramRepeatPhone: String = ""
    var apiGetOTP: String = ""
    var otpLimit: String = ""
    //timer
    //timer
    private var mTimeLeftInMillis: Long = 30000  //half minute 30 second
    private var mTimerRunning: Boolean = false
    private val START_TIME_IN_MILLIS: Long = 30000 //half minute 30 second
    private var mCountDownTimer: CountDownTimer? = null
    private var isVerify: String = "no"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_phone_number)

        etPhoneOld.setText(SharedPreferenceManager.getMySharedPreferences()!!.getMobileNumber())
        EdittextChangeMethod()
        allClicklistener()

    }

    private fun allClicklistener() {
        ivBackphone.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
        btnReSend.setOnClickListener(this)
        ivClosePhone.setOnClickListener(this)
        llBackphone.setOnClickListener(this)
    }

    private fun EdittextChangeMethod() {
        etOtpPhone.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s.toString().isNotEmpty()) {
                    if (s.toString().length < otpLimit.toInt()) {
                        etOtpPhone.background = applicationContext?.let { ContextCompat.getDrawable(it, R.drawable.bg_grey_rounded_shape) }
                    }
                    if (s.toString().length == otpLimit.toInt()) {
                        if (s.toString() == apiGetOTP) {
                            hideKeyboard()
                            callAPIChangephone()  // call API for chnage mobile number
                        } else {
                            ivClosePhone.visibility = View.GONE
                            etOtpPhone.background = applicationContext?.let { ContextCompat.getDrawable(it, R.drawable.bg_gradient_red_line_background) }
                            showSnackBar(activity, getString(R.string.please_enter_valid_otp))
                        }
                    }
                }
                if (s.toString().isEmpty()) {
                    ivClosePhone.visibility = View.GONE
                }
            }
        })
    }

    private fun hideKeyboard() {
        val inputManager: InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.hideSoftInputFromWindow(currentFocus!!.windowToken, InputMethodManager.SHOW_FORCED)
    }
    override fun onResume() {
        super.onResume()
        ivBackphone.isEnabled = true
        llBackphone.isEnabled = true
    }
    override fun onClick(view: View) {
        when (view.id) {
            R.id.ivBackphone -> {
                ivBackphone.isEnabled = false
                llBackphone.isEnabled = false
                onBackPressed()
            }
            R.id.llBackphone -> {
                ivBackphone.isEnabled = false
                llBackphone.isEnabled = false
                onBackPressed()
            }
            R.id.btnUpdate -> {
                paramOldPhone = etPhoneOld.text.toString()
                paramNewPhone = etPhoneNew.text.toString()
                paramRepeatPhone = etPhoneRepeat.text.toString()
                if (validatePhone()) {

                    callSendOTPAPI("send")

                }
            }
            R.id.btnReSend -> {
                callSendOTPAPI("resend")
            }
            R.id.ivClosePhone -> {
                etOtpPhone.background = applicationContext?.let { ContextCompat.getDrawable(it, R.drawable.bg_grey_rounded_shape) }
                etOtpPhone.text?.clear()
                ivClosePhone.visibility = View.GONE
            }
        }
    }

    private fun callSendOTPAPI(paramIsFrom: String) {
        if (AppUtils.isConnectedToInternet(activity)) {
            AppUtils.showProgressDialog(activity)
            val params = java.util.HashMap<String, String?>()
            params["cc"] = SharedPreferenceManager.getMySharedPreferences()!!.getCountryCode()
            params["number"] = paramRepeatPhone
            params["device_type"] = "android"
            if (SharedPreferenceManager.getMySharedPreferences()!!.getFcmToken() != null) {
                params["device_token"] = SharedPreferenceManager.getMySharedPreferences()!!.getFcmToken()
            } else {
                params["device_token"] = ""
            }
            Log.e("firebaseToke", SharedPreferenceManager.getMySharedPreferences()!!.getFcmToken()!!)

            Log.e("param", params.toString())
            val call: Call<SendOTPModel>
            call = RetrofitRestClient.instance!!.sendOTPApi(params)
            call.enqueue(object : Callback<SendOTPModel> {
                override fun onResponse(call: Call<SendOTPModel>?, response: Response<SendOTPModel>?) {
                    AppUtils.hideProgressDialog()
                    val basicModel: SendOTPModel
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {
                            apiGetOTP = basicModel.data!!.otp!!
                            otpLimit = basicModel.data.otpLimit!!
                            etOtpPhone.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(otpLimit.toInt()))

                            Log.e("OTP", apiGetOTP)
                           /* showSnackBar(activity, "OTP :" + apiGetOTP)*/
                            if (paramIsFrom == "send") {
                                MakeVerifyViewVisible()
                            } else {
                                hideResendButton()
                            }
                            startTimerFor30Second()

                        } else {
                            showSnackBar(activity, basicModel.message)
                        }
                    } else {
                        showSnackBar(activity, response.message())
                    }
                }

                override fun onFailure(call: Call<SendOTPModel>?, t: Throwable?) {
                    AppUtils.hideProgressDialog()
                    if (t is SocketTimeoutException) {
                        showSnackBar(activity, getString(R.string.connection_timeout))
                    } else if (t is NetworkErrorException) {
                        showSnackBar(activity, getString(R.string.network_error))
                    } else if (t is AuthenticatorException) {
                        showSnackBar(activity, getString(R.string.authentiation_error))
                    } else {
                        showSnackBar(activity, getString(R.string.something_went_wrong))
                    }
                }
            })
        } else {
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }
    }

    private fun MakeVerifyViewVisible() {
        isVerify = "yes"

        llVerifyOTP.visibility = View.VISIBLE
        llPhone.visibility = View.GONE
    }

    private fun hideResendButton() {
        btnReSend.visibility = View.GONE
        llRetry.visibility = View.VISIBLE
    }

    private fun callAPIChangephone() {
        if (AppUtils.isConnectedToInternet(activity)) {
            AppUtils.showProgressDialog(activity)
            val params = HashMap<String, String?>()
            params["old_number"] = paramOldPhone
            params["new_number"] = paramNewPhone

            Log.e("param", params.toString())
            val call: Call<GeneralModel>
            call = RetrofitRestClient.instance!!.changePhoneNumberAPI(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, params)
            Log.e("token", SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
            call.enqueue(object : Callback<GeneralModel> {
                @SuppressLint("SetTextI18n", "ShowToast")
                override fun onResponse(call: Call<GeneralModel>?, response: Response<GeneralModel>?) {
                    AppUtils.hideProgressDialog()
                    val basicModel: GeneralModel
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {
                            Toast.makeText(activity, basicModel.message, Toast.LENGTH_SHORT).show()
                            callLogoutFunction()
                            /*  finish()
                              AppUtils.overridePendingTransitionExit(activity)*/
                        } else if (Objects.requireNonNull(basicModel).code == "-1") {
                            callLogoutFunction()
                        } else {
                            ivClosePhone.visibility = View.VISIBLE
                            showSnackBar(activity, basicModel.message)
                        }
                    } else {
                        ivClosePhone.visibility = View.VISIBLE
                        showSnackBar(activity, response.message())
                    }
                }

                override fun onFailure(call: Call<GeneralModel>?, t: Throwable?) {
                    AppUtils.hideProgressDialog()
                    ivClosePhone.visibility = View.VISIBLE
                    if (t is SocketTimeoutException) {
                        showSnackBar(activity, getString(R.string.connection_timeout))
                    } else if (t is NetworkErrorException) {
                        showSnackBar(activity, getString(R.string.network_error))
                    } else if (t is AuthenticatorException) {
                        showSnackBar(activity, getString(R.string.authentiation_error))
                    } else {
                        showSnackBar(activity, getString(R.string.something_went_wrong))
                    }
                }
            })
        } else {
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }
    }

    private fun validatePhone(): Boolean {

        if (paramOldPhone.isEmpty()) {
            showSnackBar(activity, getString(R.string.please_enter_old_phone))
            return false
        }
        if (!AppUtils.isValidMobile(paramOldPhone)) {
            showSnackBar(activity, getString(R.string.please_enter_valid_old_phone))
            return false
        }
        if (paramNewPhone.isEmpty()) {
            showSnackBar(activity, getString(R.string.please_enter_new_phone))
            return false
        }
        if (!AppUtils.isValidMobile(paramNewPhone)) {
            showSnackBar(activity, getString(R.string.please_enter_valid_new_phone))
            return false
        }
        if (paramRepeatPhone.isEmpty()) {
            showSnackBar(activity, getString(R.string.please_enter_repeat_new_phone_number))
            return false
        }
        if (paramRepeatPhone != paramNewPhone) {
            showSnackBar(activity, getString(R.string.please_enter_repeat_phone_as_new_phone))
            return false
        }
        return true
    }

    private fun startTimerFor30Second() {
        mCountDownTimer = object : CountDownTimer(mTimeLeftInMillis, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                mTimeLeftInMillis = millisUntilFinished
                updateCountDownText()
            }

            override fun onFinish() {
                mTimerRunning = false
                llRetry.visibility = View.GONE
                btnReSend.visibility = View.VISIBLE
                resetTimer()
            }
        }.start()
        mTimerRunning = true

    }

    private fun updateCountDownText() {
        val minutes = (mTimeLeftInMillis / 1000).toInt() / 60
        val seconds = (mTimeLeftInMillis / 1000).toInt() % 60
        val timeLeftFormatted: String = java.lang.String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds)
        tvRetryTime.text = timeLeftFormatted
    }


    private fun resetTimer() {
        mTimeLeftInMillis = START_TIME_IN_MILLIS
        updateCountDownText()
    }

    override fun onDestroy() {
        super.onDestroy()
        mCountDownTimer?.cancel()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (isVerify == "no") {
            finish()
            AppUtils.overridePendingTransitionExit(activity)
        } else {
            llPhone.visibility = View.VISIBLE
            llVerifyOTP.visibility = View.GONE
            isVerify = "no"
        }
    }
}
