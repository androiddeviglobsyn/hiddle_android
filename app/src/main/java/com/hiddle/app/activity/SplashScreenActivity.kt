package com.hiddle.app.activity


import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.WindowManager
import com.hiddle.app.R
import com.hiddle.app.util.SharedPreferenceManager
import java.util.*


public class SplashScreenActivity : BaseMainActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN) //for create in full screen



        initAnimation() // for animation
        getsplashMethod() // for spalsh time

    }

    private fun getsplashMethod() {
        SharedPreferenceManager.getMySharedPreferences()!!.setNotificationUser("")

        Handler().postDelayed({
            // This method will be executed once the timer is over
            if (SharedPreferenceManager.getMySharedPreferences()!!.isLogin()) {
                val i = Intent(activity, HomeActivity::class.java)
                i.putExtra("type", "home")
                startActivity(i)
                finish()
                /*AppUtils.overridePendingTransitionEnter(activity)*/
            } else {
                setLanguageDefault()
                val i = Intent(activity, LoginScreenActivity::class.java)
                startActivity(i)
                finish()
                //AppUtils.overridePendingTransitionEnter(activity)
            }
        }, 2900)
    }

    private fun setLanguageDefault() {
        var paramLanguage = Locale.getDefault().language
        Log.e("language", paramLanguage)
       /* if (SharedPreferenceManager.getMySharedPreferences()!!.getLanguage() == "") {*/
            callSetLanguage()
        /*} else {

        }*/
        /* SharedPreferenceManager.getMySharedPreferences()!!.setLanguahe(getString(R.string.french))
          setLanguage("fr")*/
    }

    private fun callSetLanguage() {
        var paramLanguage = Locale.getDefault().language

        if (paramLanguage == "en") {
            SharedPreferenceManager.getMySharedPreferences()!!.setLanguahe(getString(R.string.english))
            setLanguage("en")
        } else if (paramLanguage == "tr") {
            SharedPreferenceManager.getMySharedPreferences()!!.setLanguahe(getString(R.string.turkish))
            setLanguage("tr")
        } else if (paramLanguage == "fr") {
            SharedPreferenceManager.getMySharedPreferences()!!.setLanguahe(getString(R.string.french))
            setLanguage("fr")
        } else if (paramLanguage == "de") {
            SharedPreferenceManager.getMySharedPreferences()!!.setLanguahe(getString(R.string.german))
            setLanguage("de")
        } else if (paramLanguage == "gsw") {
            SharedPreferenceManager.getMySharedPreferences()!!.setLanguahe(getString(R.string.spanish))
            setLanguage("gsw")
        } else if (paramLanguage == "it") {
            SharedPreferenceManager.getMySharedPreferences()!!.setLanguahe(getString(R.string.italian))
            setLanguage("it")
        } else if (paramLanguage == "pt") {
            SharedPreferenceManager.getMySharedPreferences()!!.setLanguahe(getString(R.string.Portuguese))
            setLanguage("pt")
        } else if (paramLanguage == "sq") {
            SharedPreferenceManager.getMySharedPreferences()!!.setLanguahe(getString(R.string.Albanian))
            setLanguage("sq")
        } else {
            SharedPreferenceManager.getMySharedPreferences()!!.setLanguahe(getString(R.string.english))
            setLanguage("en")
        }

    }

    private fun initAnimation() {
        /*val  myAnim = AnimationUtils.loadAnimation(activity, R.anim.bounce_jump_animation);
        *//*ivSplash.visibility == View.VISIBLE*//*
        val interpolator = MyBounceInterpolator(0.1, 15.0)
        myAnim.interpolator = interpolator
       // myAnim.repeatCount = 2
        ivSplash.startAnimation(myAnim)*/
    }
}
