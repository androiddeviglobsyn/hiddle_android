package com.hiddle.app.activity

import android.Manifest
import android.accounts.AuthenticatorException
import android.accounts.NetworkErrorException
import android.annotation.SuppressLint
import android.content.ContextWrapper
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import android.os.*
import android.util.Log
import android.view.View
import android.view.animation.Animation
import android.view.animation.TranslateAnimation
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.core.app.ActivityCompat
import com.androidquery.AQuery
import com.facebook.*
import com.facebook.FacebookSdk.getApplicationContext
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.*
import com.hiddle.app.R
import com.hiddle.app.retrofit.ApiInterface
import com.hiddle.app.`interface`.AuthenticationListener
import com.hiddle.app.model.InstagramModel.InstagramAccessToken
import com.hiddle.app.model.InstagramUserDetailModel.InstagramUserDetailModel
import com.hiddle.app.model.VerifyCodeModel.VerifyModelResponse
import com.hiddle.app.retrofit.APIClient
import com.hiddle.app.retrofit.RetrofitRestClient
import com.hiddle.app.service.LocationTrack
import com.hiddle.app.util.AppUtils
import com.hiddle.app.util.AppUtils.hideProgressDialog
import com.hiddle.app.util.SharedPreferenceManager
import kotlinx.android.synthetic.main.activity_login_screen.*
import org.json.JSONException
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions
import pub.devrel.easypermissions.PermissionRequest
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*
import java.net.*
import java.util.*
import android.os.Bundle
import androidx.annotation.NonNull
import androidx.appcompat.widget.AppCompatTextView
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.auth.*
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.hiddle.app.cardstackview.CardStackListener.DEFAULT
import com.hiddle.app.dialog.NameDialog
import kotlinx.android.synthetic.main.custom_dialog_name.*
import java.net.URLEncoder.encode
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.text.DateFormat.DEFAULT

@Suppress("DEPRECATION")
class LoginScreenActivity : BaseMainActivity(), AuthenticationListener, View.OnClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    //location
    var mGoogleApiClient: GoogleApiClient? = null
    private var mLocationRequest: LocationRequest? = null
    private var paramLatitude: String = ""
    private var paramLongitude: String = ""
    var mLastLocation: Location? = null
    var paramProfilePic: String = ""
    var handler = Handler()

    companion object {   // for make static
        const val RC_ACCESSS_LOCATION: Int = 126
        const val RC_FINE_LOCATION: Int = 127
        const val RC_STORAGE_PERMISSION: Int = 128

        @JvmStatic
        var instaUserId: String? = null
    }

    //insta
    var mWebView: WebView? = null
    var height: Int? = null

    //fb login
    var callbackManager: CallbackManager? = null
    private var accessTokenTracker: AccessTokenTracker? = null
    private var profileTracker: ProfileTracker? = null
    private var aQuery: AQuery? = null
    var isWebviewOpen = false
    var doubleBackToExitPressedOnce: Boolean = false
    var geocoder: Geocoder? = null

    //variable
    var paramUserName: String = ""
    var paramEmail: String = ""
    var clickEvent: String = ""
    var country_code: String = ""
    var country_name: String = ""
    //location tracker

    var locationTrack: LocationTrack? = null
    private var mAuth: FirebaseAuth? = null
    private lateinit var auth: FirebaseAuth


    var nameDialog: NameDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        FacebookSdk.sdkInitialize(this);
        setContentView(R.layout.activity_login_screen)

        var name = Locale.getDefault().displayCountry.toString()
        Log.e("Result", name)



        initializeAuth()
        initView()
        initFbInfo()  //init fb
        initAnimation() //amination for man screen
        allClicklistener()
        getPermissionForLocation()
        SharedPreferenceManager.getMySharedPreferences()!!.setNotificationUser("")

    }

    private fun initializeAuth() {
        mAuth = FirebaseAuth.getInstance()
        auth = Firebase.auth
    }


    private fun initView() {
        mWebView = findViewById<WebView>(R.id.webview)   //init webview
        geocoder = Geocoder(this, Locale.getDefault())
    }

    //important  fb call back
    private var callback: FacebookCallback<LoginResult> = object : FacebookCallback<LoginResult> {
        override fun onSuccess(loginResult: LoginResult) {
            val request = GraphRequest.newMeRequest(loginResult.accessToken) { `object`, response ->
                Log.e("LoginActivity", response.toString())


                Log.e("token", loginResult.accessToken.token)
                AppUtils.showProgressDialog(activity)
                // Application code
                try {
                    Log.e("tttttt", `object`.getString("id"))
                    var birthday: String = ""
                    if (`object`.has("birthday")) {
                        birthday = `object`.getString("birthday") // 01/31/1980 format
                        Log.e("birthday", birthday)
                    }
                    if (`object`.has("gender")) {
                        var gender: String = `object`.getString("gender") // 01/31/1980 format

                        Log.e("gender", gender)
                    }
                    // val gender : String = `object`.getString("gender")
                    val fnm: String = `object`.getString("first_name")
                    val lnm: String = `object`.getString("last_name")
                    val mail = `object`.getString("email")
                    val facebookId: String = `object`.getString("id")
                    paramUserName = fnm + " " + lnm
                    if (mail != null) {
                        paramEmail = mail
                    }
                    //Toast.makeText(activity, getString(R.string.successfully_login), Toast.LENGTH_SHORT).show()
                    Log.e("aswwww", "https://graph.facebook.com/$facebookId/picture?type=large")
                    hideProgressDialog()
                    getDownload("https://graph.facebook.com/" + facebookId + "/picture?type=large")
                    paramProfilePic = "/storage/emulated/0/Pictures/fbImage.jpg"

                    LoginManager.getInstance().logOut()  // for facebook logout

                    getCallAPISocialLogin(facebookId, "Facebook")
                    //Toast.makeText(activity, "Name: $fnm $lnm \nEmail:$mail \n" + "ID: $fid ", Toast.LENGTH_SHORT).show()


                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
            val parameters: Bundle = Bundle()
            parameters.putString("fields", "id, first_name, last_name,email,birthday,gender,location")
            request.parameters = parameters
            request.executeAsync()
        }

        override fun onCancel() {

        }

        override fun onError(error: FacebookException) {
            showSnackBar(activity, getString(R.string.something_went_wrong))
            Log.e("main_error", error.toString())
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun getInastagramWebview() {
        isWebviewOpen = true
        android.webkit.CookieManager.getInstance().removeAllCookie();
        // mWebView1 = findViewById<TextView>(R.id.webview)   //init webview
        llWebview.visibility = View.VISIBLE

        //for get user id and accesstoken of instagram
        val REQUEST_URL =
                "https://www.instagram.com/oauth/authorize?app_id=" + getString(R.string.instagram_id) + "&redirect_uri=" + getString(R.string.callback_url) + "&scope=user_profile,user_media&response_type=code"
        // Log.e("URl",REQUEST_URL)
        mWebView?.settings!!.useWideViewPort = true
        mWebView?.settings!!.loadWithOverviewMode = true
        mWebView?.settings!!.setSupportZoom(true)
        mWebView?.settings!!.builtInZoomControls = true
        mWebView?.settings!!.displayZoomControls = true
        mWebView?.setPadding(0, 0, 0, 0)
        mWebView?.clearCache(true);
        mWebView?.clearHistory()
        mWebView?.settings!!.savePassword = false;
        mWebView?.settings!!.saveFormData = false;

        //Log.e("webview", "${Utility.getScale(activity!!)}")
        try {
            mWebView?.setInitialScale(30)
            mWebView?.scrollTo(0, height!! * resources!!.displayMetrics.density.toInt())
        } catch (e: Exception) {
        }
        try {
            mWebView?.settings!!.javaScriptEnabled = true
        } catch (e: Exception) {
        }
        mWebView?.loadUrl(REQUEST_URL)
        mWebView?.webViewClient = mWebViewClient
    }


    private fun initFbInfo() {
        aQuery = AQuery(activity);
        callbackManager = CallbackManager.Factory.create()

        accessTokenTracker = object : AccessTokenTracker() {
            override fun onCurrentAccessTokenChanged(oldToken: AccessToken?, newToken: AccessToken?) {}
        }
        profileTracker = object : ProfileTracker() {
            override fun onCurrentProfileChanged(oldProfile: Profile?, newProfile: Profile?) {}
        }
        accessTokenTracker!!.startTracking()
        profileTracker!!.startTracking()
        login_button.setReadPermissions(Arrays.asList("public_profile", "email"/*, "user_birthday"*//*, "user_friends"*/))
        login_button.registerCallback(callbackManager, callback)
    }

    private fun initAnimation() {
        /* val ani: Animation = TranslateAnimation(Animation.ABSOLUTE.toFloat(), Animation.ABSOLUTE.toFloat(), 150F, Animation.ABSOLUTE.toFloat())
         ani.duration = 1200
         ani.fillAfter = true
         llMain.startAnimation(ani)*/
    }

    private fun allClicklistener() {
        tvByRegister.setOnClickListener(this)
        tvMobile.setOnClickListener(this)
        ivFb.setOnClickListener(this)
        ivInstagram.setOnClickListener(this)
        tvLoginProblem.setOnClickListener(this)
        rlMainView.setOnClickListener(this)
        rlRootview.setOnClickListener(this)
        apple_login_btn.setOnClickListener(this)

    }

    private fun getCallAPISocialLogin(socialId: String, IsFrm: String) {
        if (AppUtils.isConnectedToInternet(activity)) {
            AppUtils.showProgressDialog(activity)
            val params = HashMap<String, String?>()
            params["social_id"] = socialId

            if (IsFrm == "Facebook") {
                params["login_via"] = "Facebook"
            }
            if (IsFrm == "Instagram") {
                params["login_via"] = "Instagram"
            }
            if (IsFrm == "Apple") {
                params["login_via"] = "Apple"
            }
            params["device_type"] = "android"
            if (SharedPreferenceManager.getMySharedPreferences()!!.getFcmToken() != null) {
                params["device_token"] = SharedPreferenceManager.getMySharedPreferences()!!.getFcmToken()
            } else {
                params["device_token"] = ""
            }
            Log.e("param", params.toString())
            val call: Call<VerifyModelResponse>
            call = RetrofitRestClient.instance!!.socialLoginApi(params)
            call.enqueue(object : Callback<VerifyModelResponse> {
                override fun onResponse(call: Call<VerifyModelResponse>?, response: Response<VerifyModelResponse>?) {
                    hideProgressDialog()
                    val basicModel: VerifyModelResponse
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {

                            val paramIsVerify: String = basicModel.data!!.is_verify!!

                            if (paramIsVerify == "0") {

                                if (IsFrm == "Facebook") {
                                    Toast.makeText(activity, getString(R.string.welcome) + " " + paramUserName, Toast.LENGTH_SHORT).show()
                                    redirectToLoginScreen("facebook", socialId)
                                }
                                if (IsFrm == "Instagram") {
                                    Toast.makeText(activity, getString(R.string.welcome) + " " + paramUserName, Toast.LENGTH_SHORT).show()
                                    redirectToLoginScreen("instagram", socialId)
                                }
                                if (IsFrm == "Apple") {

                                    if (paramUserName == "") {
                                        hideProgressDialog()
                                        openPopAppleFirstName(socialId)
                                    } else {
                                        Toast.makeText(activity, getString(R.string.welcome) + " " + paramUserName, Toast.LENGTH_SHORT).show()
                                        redirectToLoginScreen("apple", socialId)
                                    }

                                }


                            } else {
                                SharedPreferenceManager.getMySharedPreferences()!!.setLogin(true)
                                SharedPreferenceManager.getMySharedPreferences()!!.setUserId(basicModel.data!!.id!!)
                                SharedPreferenceManager.getMySharedPreferences()!!.setUserName(basicModel.data.username!!)
                                SharedPreferenceManager.getMySharedPreferences()!!.setCountryCode(basicModel.data.cc!!)
                                SharedPreferenceManager.getMySharedPreferences()!!.setMobileNumber(basicModel.data.number!!)
                                SharedPreferenceManager.getMySharedPreferences()!!.setAuthorizationToken(basicModel.data.tokens!![0].token!!)
                                if (basicModel.data.images!!.isNotEmpty()) {
                                    SharedPreferenceManager.getMySharedPreferences()!!.setProfileImage(basicModel.data.base_url + basicModel.data.images[0].image)
                                }

                                if (basicModel.data.email != null) {
                                    SharedPreferenceManager.getMySharedPreferences()!!.setEmail(basicModel.data.email!!)
                                }

                                redirectToHomeScreen()
                            }
                        } else {
                            showSnackBar(activity, basicModel.message)
                        }
                    } else {
                        showSnackBar(activity, response.message())
                    }
                }

                override fun onFailure(call: Call<VerifyModelResponse>?, t: Throwable?) {
                    hideProgressDialog()
                    if (t is SocketTimeoutException) {
                        showSnackBar(activity, getString(R.string.connection_timeout))
                    } else if (t is NetworkErrorException) {
                        showSnackBar(activity, getString(R.string.network_error))
                    } else if (t is AuthenticatorException) {
                        showSnackBar(activity, getString(R.string.authentiation_error))
                    } else {
                        showSnackBar(activity, getString(R.string.something_went_wrong))
                    }
                }
            })
        } else {
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }
    }

    private fun openPopAppleFirstName(socialId: String) {
        nameDialog = NameDialog(activity)
        nameDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        nameDialog!!.show()


        nameDialog!!.tvSubmitDl.setOnClickListener(View.OnClickListener {
            if (nameDialog!!.etUsername.text.toString().isEmpty()) {
                Toast.makeText(activity, getString(R.string.please_enter_user_name), Toast.LENGTH_SHORT).show()
            } else {
                paramUserName = etUsername.text.toString()
                nameDialog!!.dismiss()
                redirectToLoginScreen("apple", socialId)

            }
        })

    }

    private fun redirectToHomeScreen() {
        val i = Intent(activity, HomeActivity::class.java)
        i.putExtra("type", "home")
        i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(i)
        AppUtils.overridePendingTransitionEnter(activity)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager!!.onActivityResult(requestCode, resultCode, data)
    }


    override fun onResume() {
        super.onResume()
        val profile = Profile.getCurrentProfile()
        llWebview.visibility = View.GONE
        isWebviewOpen = false

        tvMobile.isEnabled = true
        tvLoginProblem.isEnabled = true

    }


    override fun onClick(v: View) {
        when (v.id) {
            R.id.tvByRegister -> {

            }
            R.id.tvMobile -> {
                tvMobile.isEnabled = false
                paramUserName = ""
                paramEmail = ""
                paramProfilePic = ""
                redirectToLoginScreen("normal", "")  //for normal login pass socialid as ""

            }
            R.id.ivFb -> {

                mWebView?.clearCache(true);
                mWebView?.clearHistory()
                paramUserName = ""
                paramEmail = ""
                paramProfilePic = ""
                clickEvent = "FB"
                methodRequiresStoragePermission()  //method for storage permisision
            }
            R.id.ivInstagram -> {
                /*            CookieSyncManager.createInstance(ogout.this);
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.removeAllCookie();*/
                paramUserName = ""
                paramEmail = ""
                paramProfilePic = ""
                clickEvent = "INSTAGRAM"
                methodRequiresStoragePermission() //method for storage
            }
            R.id.tvLoginProblem -> {
                tvLoginProblem.isEnabled = false
                val i1 = Intent(activity, LoginProblemScreenActivity::class.java)
                startActivity(i1)
                AppUtils.overridePendingTransitionEnter(activity)
            }
            R.id.rlMainView -> {
                llWebview.visibility = View.GONE
            }
            R.id.rlRootview -> {
                llWebview.visibility = View.GONE
            }
            R.id.apple_login_btn -> {
                paramUserName = ""
                paramEmail = ""
                paramProfilePic = ""
                LoginWithApple()   //apple sign in
            }
        }

    }


    fun LoginWithApple() {
        val mAuth: FirebaseAuth
        val provider = OAuthProvider.newBuilder("apple.com")

        val scopes = object : ArrayList<String>() {
            init {
                add("email")
                add("name")
            }
        }
        provider.setScopes(scopes)

        mAuth = FirebaseAuth.getInstance()
        mAuth.signOut()

        val pending = mAuth.getPendingAuthResult()
        if (pending != null) {
            pending.addOnSuccessListener(object : OnSuccessListener<AuthResult> {
                override fun onSuccess(authResult: AuthResult) {
                    authResult.credential
                    hideProgressDialog()
                    Log.e("LoginWithApple", "checkPending:onSuccess:" + authResult)
                    // Get the user profile with authResult.getUser() and
                    // authResult.getAdditionalUserInfo(), and the ID
                    // token from Apple with authResult.getCredential().
                }
            }).addOnFailureListener(object : OnFailureListener {
                override fun onFailure(@NonNull e: Exception) {
                    hideProgressDialog()
                    Log.e("LoginWithApple:Failure", e.toString())
                }
            })
        } else {
            AppUtils.showProgressDialog(activity)
            Log.e("LoginWithApple", "pending: null")
        }

        mAuth.startActivityForSignInWithProvider(this, provider.build())
                .addOnSuccessListener(
                        object : OnSuccessListener<AuthResult> {
                            override fun onSuccess(authResult: AuthResult) {
                                hideProgressDialog()

                                Log.e("LoginWithApple", "Name" + authResult.user!!.displayName + "Email" + authResult.user!!.email!!)

                                var apple_user_id = authResult.user!!.uid

                                if (authResult.user!!.displayName != null) {
                                    paramUserName = authResult.user!!.displayName!!
                                } else {
                                    paramUserName = ""
                                }
                                paramEmail = authResult.user!!.email!!

                                getCallAPISocialLogin(apple_user_id, "Apple")

                                // ...
                            }
                        })
                .addOnFailureListener(
                        object : OnFailureListener {
                            override fun onFailure(@NonNull e: Exception) {
                                hideProgressDialog()
                                Log.e("LoginWithApple", "activitySignIn:onFailure", e)
                                hideProgressDialog()
                            }
                        })


    }


    @AfterPermissionGranted(RC_STORAGE_PERMISSION)
    private fun methodRequiresStoragePermission() {
        val perms = arrayOf<String>(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (EasyPermissions.hasPermissions(this, *perms)) {
            // Already have permission, do the thing
            // ...
            if (clickEvent == "FB") {
                callFacebookLogin()  //call FB Implementattion
            }
            if (clickEvent == "INSTAGRAM") {
                callInstagramLogin() //call Instagram Login
            }
        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.rationale_call), RC_STORAGE_PERMISSION, *perms)
            // Do not have permissions, request them now
        }
    }

    private fun callInstagramLogin() {
        paramEmail = ""
        paramUserName = ""
        getInastagramWebview()  //open insta webview
    }

    private fun callFacebookLogin() {
        android.webkit.CookieManager.getInstance().removeAllCookie();
        //permission granted
        LoginManager.getInstance().logOut()  // for facebook logout
        paramEmail = ""
        paramUserName = ""
        login_button.performClick()  //for fb login button click
    }

    private fun redirectToLoginScreen(isFromLogin: String, socialId: String) {

        getAddressFormLat()
        Log.e("code", country_code)
        val i = Intent(activity, LoginUsingMobileActivity::class.java)
        i.putExtra("paramUserName", paramUserName)
        i.putExtra("paramEmail", paramEmail)
        i.putExtra("isFromLogin", isFromLogin)
        i.putExtra("socialId", socialId)
        i.putExtra("profile_url", paramProfilePic)
        i.putExtra("country_code", country_code)
        startActivity(i)
        AppUtils.overridePendingTransitionEnter(activity)
    }

    private fun getAddressFormLat() {
        if (paramLatitude != null) {
            if (paramLatitude != "") {

                var addresses: List<Address> = ArrayList()
                try {
                    addresses = geocoder!!.getFromLocation(paramLatitude.toDouble(), paramLongitude.toDouble(), 1)
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                if (addresses.isNotEmpty()) {

                    if (addresses[0].countryCode != null) {
                        country_code = addresses[0].countryCode
                        country_name = addresses[0].countryName
                        Log.e("Name", country_code + "name" + country_name)
                    }
                }


            }
        }


    }

    private val mWebViewClient = object : WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
            if (url?.startsWith(getString(R.string.callback_url))!!) {
                return true
            }
            return false
        }

        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
            if (URLDecoder.decode(url, "UTF-8")?.contains("code=")!!) {
                val uri = Uri.parse(URLDecoder.decode(url, "UTF-8"))
                var access_token = uri.toString().substring(uri.toString().indexOf("code=") + 5, uri.toString().indexOf("#"))

                onTokenReceived(access_token)  //get insta auth token

                height = view!!.contentHeight
            }
        }
    }

    override fun onTokenReceived(auth_token: String) {
        Log.e("auth_token", auth_token)
        getInstagramUserId_Accesstoken(auth_token)   //API for get insta user_id and access token
        //getAccessToken(auth_token!!)

    }

    private fun getInstagramUserId_Accesstoken(authToken: String) {
        AppUtils.showProgressDialog(activity)

        val apiInterface: ApiInterface = APIClient.getClientInstaToken()!!.create(ApiInterface::class.java)
        val params = HashMap<String, String?>()

        params["client_id"] = getString(R.string.instagram_id)
        params["client_secret"] = getString(R.string.instagram_app_secret)
        params["grant_type"] = "authorization_code"
        params["redirect_uri"] = getString(R.string.callback_url)
        params["code"] = authToken

        Log.e("param", params.toString())

        val call: Call<InstagramAccessToken> = apiInterface.getInstagramAceesToken(params)
        call.enqueue(object : Callback<InstagramAccessToken?> {
            override fun onResponse(call: Call<InstagramAccessToken?>, response: Response<InstagramAccessToken?>) {
                AppUtils.hideProgressDialog()
                var resultDistance: InstagramAccessToken? = response.body()
                if (response.isSuccessful) {
                    resultDistance = response.body()
                    Log.e("id", resultDistance!!.user_id + "acces_token" + resultDistance.access_token)
                    instaUserId = resultDistance.user_id
                    var instaUserAccessToken = resultDistance.access_token

                    getInstagramDetailUser(instaUserAccessToken, instaUserId!!)  //API for get instagram user detail

                } else {
                    if (response.message().equals("")) {
                        llWebview.visibility = View.GONE
                    } else {
                        showSnackBar(activity, getString(R.string.something_went_wrong))
                    }

                }
            }

            override fun onFailure(call: Call<InstagramAccessToken?>, t: Throwable) {
                Log.e("errorGot", t.toString())
                AppUtils.hideProgressDialog()
                showSnackBar(activity, getString(R.string.something_went_wrong))
                call.cancel()
            }
        })
    }

    private fun getInstagramDetailUser(paramInstaAccessToken: String, paramInstaUserId: String) {
        val apiInterface: ApiInterface = APIClient.getClientUser()!!.create(ApiInterface::class.java)
        AppUtils.showProgressDialog(activity)//profile_picture
        val call: Call<InstagramUserDetailModel> = apiInterface.getInstaUserDetailMethod(paramInstaUserId, "id,media_count,media,username,account_type", paramInstaAccessToken, paramInstaUserId)
        Log.e("DetailToken", paramInstaAccessToken)
        Log.e("Detail", paramInstaUserId)
        call.enqueue(object : Callback<InstagramUserDetailModel?> {
            override fun onResponse(call: Call<InstagramUserDetailModel?>, response: Response<InstagramUserDetailModel?>) {
                var resultDistance: InstagramUserDetailModel? = response.body()
                AppUtils.hideProgressDialog()
                if (response.isSuccessful) {

                    resultDistance = response.body()
                    Log.e("GetResponse", resultDistance.toString())

                    var instaUserName = resultDistance!!.username
                    paramUserName = instaUserName.toString()
                    // Toast.makeText(activity, getString(R.string.successfully_login), Toast.LENGTH_SHORT).show()

                    getCallAPISocialLogin(paramInstaUserId, "Instagram")   //get Call instagram login API

                } else {
                    showSnackBar(activity, getString(R.string.something_went_wrong))
                }
            }

            override fun onFailure(call: Call<InstagramUserDetailModel?>, t: Throwable) {
                Log.e("errorGot", t.toString())
                AppUtils.hideProgressDialog()
                showSnackBar(activity, getString(R.string.something_went_wrong))
                call.cancel()
            }
        })
    }


    override fun onBackPressed() {

        if (!isWebviewOpen) {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed()
                val intent = Intent(Intent.ACTION_MAIN)
                intent.addCategory(Intent.CATEGORY_HOME)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                return
            }
            this.doubleBackToExitPressedOnce = true
            showSnackBar(activity, getString(R.string.please_press_back_again_exit))

        } else {

            if (mWebView != null && mWebView!!.canGoBack()) {
                mWebView!!.goBack();// if there is previous page open it
            } else {
                llWebview.visibility = View.GONE
                isWebviewOpen = false
            }


        }


    }


    private fun getPermissionForLocation() {
        callFineLocation()
    }

    @AfterPermissionGranted(RC_FINE_LOCATION)
    fun callFineLocation() {
        if (EasyPermissions.hasPermissions(activity, Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Have permission, do the thing!
            callCorseLocation()
        } else {
            // Request one permission
            EasyPermissions.requestPermissions(
                    PermissionRequest.Builder(this, RC_FINE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
                            .setRationale(R.string.rationale_call)
                            .setPositiveButtonText(getString(R.string.ok))
                            .setNegativeButtonText(getString(R.string.cancel))
                            .build())
        }
    }

    //permisson read storage
    @AfterPermissionGranted(RC_ACCESSS_LOCATION)
    fun callCorseLocation() {
        if (EasyPermissions.hasPermissions(activity, Manifest.permission.ACCESS_COARSE_LOCATION)) {
            // Have permission, do the thing!

            getUserCurrentLocation()  //for get location

            if (paramLatitude == "") {
                handler = Handler()  //handler for get location up to not get location
                handler.postDelayed({
                    if (paramLatitude == "") {
                        callLocationTracker()
                        getUserCurrentLocation()
                        Log.e("handler", "call")
                    } else {
                        handler.removeCallbacksAndMessages(null)
                        handler.removeMessages(0)
                        Log.e("handler", "stop")
                    }
                    //Do something after 20 seconds
                }, 2000) //the time is in miliseconds}*/

            }

            getAddressFormLat()


        } else {
            // Request one permission
            EasyPermissions.requestPermissions(
                    PermissionRequest.Builder(this, RC_ACCESSS_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
                            .setRationale(R.string.rationale_call)
                            .setPositiveButtonText(getString(R.string.ok))
                            .setNegativeButtonText(getString(R.string.cancel))
                            .build())
        }
    }

    private fun callLocationTracker() {
        locationTrack = LocationTrack(activity)
        if (locationTrack!!.canGetLocation()) {
            if (locationTrack!!.getLongitude().toString() != "0.0") {
                paramLongitude = locationTrack!!.getLongitude().toString()
                paramLatitude = locationTrack!!.getLatitude().toString()
                Log.e("location_tracker", paramLatitude)
                getAddressFormLat()
            }
        } else {
            // locationTrack.showSettingsAlert()
        }
    }

    private fun getUserCurrentLocation() {

        mGoogleApiClient = GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build()
        if (mGoogleApiClient != null) {
            mGoogleApiClient!!.connect()
        } else Log.e("Not Connected", "Not Connected")

        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient)
    }


    override fun onStart() {
        super.onStart()
        if (mGoogleApiClient != null) {
            mGoogleApiClient!!.connect()
        }


    }


    override fun onLocationChanged(location: Location?) {
        if (location != null)
            paramLatitude = location!!.latitude.toString()
        paramLongitude = location!!.longitude.toString()
        // Log.e("location", paramLatitude + "kk" + paramLongitude)
    }


    /*Ending the updates for the location service*/
    override fun onStop() {

        accessTokenTracker!!.stopTracking()
        profileTracker!!.stopTracking()

        stopLocationUpdates()
        super.onStop()
        handler.removeCallbacksAndMessages(null);
        handler.removeMessages(0);

        //for stop handle for pass location

    }

    override fun onConnected(bundle: Bundle?) {
        if (paramLatitude == "") {
            settingRequest()
        }

    }

    override fun onConnectionSuspended(i: Int) {
        Log.e("Connection Suspended!", "Connection Suspended!")
        // Toast.makeText(this, "Connection Suspended!", Toast.LENGTH_SHORT).show();
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        Log.e("Connection Failed!", "Connection Failed!")
        //Toast.makeText(this, "Connection Failed!", Toast.LENGTH_SHORT).show();
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, 90000)
            } catch (e: IntentSender.SendIntentException) {
                e.printStackTrace()
            }
        } else {
            Log.e("Current Location", "Location services connection failed with code " + connectionResult.errorCode)
        }
    }

    /*Method to get the enable location settings dialog*/
    fun settingRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest!!.interval = 10000 // 10 seconds, in milliseconds
        mLocationRequest!!.fastestInterval = 1000 // 1 second, in milliseconds
        mLocationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        val builder = LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest!!)
        val result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient,
                builder.build())
        result.setResultCallback { result ->
            val status = result.status
            val state = result.locationSettingsStates
            when (status.statusCode) {
                LocationSettingsStatusCodes.SUCCESS -> {
                    // All location settings are satisfied. The client can
                    // initialize location requests here.
                    if (paramLatitude == "") {
                        getLocation()
                    }


                    getPermissionForLocation()
                }

                LocationSettingsStatusCodes.RESOLUTION_REQUIRED ->                         // Location settings are not satisfied, but this can be fixed
                    // by showing the user a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        status.startResolutionForResult(activity, 1000)

                    } catch (e: IntentSender.SendIntentException) {
                        // Ignore the error.
                    }
                LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                }
            }
        }
    }

    /*Get Location*/
    fun getLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            return
        } else {
            /*Getting the location after aquiring location service*/

            if (mLastLocation != null) {
                paramLatitude = mLastLocation!!.getLatitude().toString()
                paramLongitude = mLastLocation!!.getLongitude().toString()
                Log.e("latitude", paramLatitude + "location" + paramLongitude)

            } else {
                Log.e("Current Location", "No data for location found")
                if (mGoogleApiClient != null) {
                    if (!mGoogleApiClient!!.isConnected) mGoogleApiClient!!.connect()

                    if (mGoogleApiClient!!.isConnected) {
                        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, activity as LocationListener?)
                    } else {
                        Log.e("Google_Api_Client:", "It was NOT connected on ");
                        /* if (mGoogleApiClient != null) {
                             mGoogleApiClient!!.connect()
                         }*/
                    }

                }

            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onDestroy() {
        super.onDestroy()
        stopLocationUpdates()
        if (locationTrack != null) {
            locationTrack!!.stopListener()
        }

    }

    private fun stopLocationUpdates() {
        if (mGoogleApiClient != null) {
            if (mGoogleApiClient!!.isConnected) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this)
                mGoogleApiClient!!.disconnect()

            }
        }

    }

    private fun getDownload(URL: String) { //for download fb and insta image
        val filename = URL.substring(URL.lastIndexOf("/") + 1)
        // Log.e("filename", filename);
        val file = File("/storage/emulated/0/Pictures/fbImage.jpg")
        Log.e("pathname", file.toString())
        if (file.exists()) {
            file.delete()
        }
        DownloadTask().execute(stringToURL(URL))

    }

    // Custom method to convert string to url
    private fun stringToURL(urlString: String): URL? {
        try {
            val url = URL(urlString)
            return url
        } catch (e: MalformedURLException) {
            e.printStackTrace()
        }
        return null
    }

    private class DownloadTask : AsyncTask<URL, Void, Bitmap>() {
        // Before the tasks execution
        override fun onPreExecute() {
            //  mProgressDialog.show()
        }

        // Do the task in background/non UI thread
        protected override fun doInBackground(vararg urls: URL): Bitmap? {
            val url = urls[0]
            var connection: HttpURLConnection? = null
            try {
                // Initialize a new http url connection
                connection = url.openConnection() as HttpURLConnection
                connection.connect()
                val inputStream = connection.getInputStream()
                val bufferedInputStream = BufferedInputStream(inputStream)
                // Convert BufferedInputStream to Bitmap object
                val bmp = BitmapFactory.decodeStream(bufferedInputStream)
                // Return the downloaded bitmap
                return bmp
            } catch (e: IOException) {
                e.printStackTrace()
            } finally {
                // Disconnect the http url connection
                connection!!.disconnect()
            }
            return null
        }

        // When all async task done
        protected override fun onPostExecute(result: Bitmap) {
            //mProgressDialog.dismiss()
            if (result != null) {
                // Display the downloaded image into ImageView
                //mImageView.setImageBitmap(result)
                // Save bitmap to internal storage
                val imageInternalUri = saveImageToInternalStorage(result)
                // Set the ImageView image from internal storage
                //  mImageViewInternal.setImageURI(imageInternalUri)
            } else {
                // Notify user that an error occurred while downloading image
                Log.e("error", "error_check")

            }
        }

        // Custom method to save a bitmap into internal storage
        fun saveImageToInternalStorage(bitmap: Bitmap): Uri {
            // Initialize ContextWrapper
            val wrapper = ContextWrapper(getApplicationContext())
            // Initializing a new file
            // The bellow line return a directory in internal storage
            var file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath() + "/"
            // Create a file to save the image
            file = File(file, "fbImage" + ".jpg").toString()
            try {
                // Initialize a new OutputStream
                var stream: OutputStream? = null
                // If the output file exists, it can be replaced or appended to it
                stream = FileOutputStream(file)
                // Compress the bitmap
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
                // Flushes the stream
                stream.flush()
                // Closes the stream
                stream.close()
            } catch (e: IOException) // Catch the exception
            {
                e.printStackTrace()
            }
            // Parse the gallery image url to uri
            val savedImageURI = Uri.parse(file)
            Log.e("Image_url", savedImageURI.toString())
            // Return the saved image Uri
            return savedImageURI
        }
    }
}







