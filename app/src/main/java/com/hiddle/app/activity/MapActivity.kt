package com.hiddle.app.activity

import android.Manifest.permission
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.hiddle.app.R
import com.hiddle.app.retrofit.ApiInterface
import com.hiddle.app.`interface`.ClickLocation
import com.hiddle.app.activity.EditProfileActivity.Companion.paramProfileLat
import com.hiddle.app.activity.EditProfileActivity.Companion.paramProfileLng
import com.hiddle.app.activity.EditProfileActivity.Companion.paramProfileLocation
import com.hiddle.app.adapter.SearchMapAdapter
import com.hiddle.app.model.LocationModel
import com.hiddle.app.model.PlaceSearchModel.SearchDataResponseModel
import com.hiddle.app.retrofit.APIClient
import com.hiddle.app.util.AppUtils
import com.hiddle.app.util.CustomInfoWindowGoogleMap
import kotlinx.android.synthetic.main.activity_map.*
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions
import pub.devrel.easypermissions.PermissionRequest
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList


public class MapActivity : BaseMainActivity(), OnMapReadyCallback, ClickLocation, View.OnClickListener {
    companion object {   // for make static
        const val RC_ACCESSS_LOCATION: Int = 126
        const val RC_FINE_LOCATION: Int = 127
    }

    private var mMap: GoogleMap? = null
    var isFirstTime: Boolean = false
    var param_latitude: String = ""
    var param_longitude: String = ""
    var address: String = ""
    var isFrom: String = ""
    var mapFragment: SupportMapFragment? = null
    var searchMapAdapter: SearchMapAdapter? = null
    var apiService: ApiInterface? = null
    var searchData: ArrayList<com.hiddle.app.model.PlaceSearchModel.Result> = ArrayList<com.hiddle.app.model.PlaceSearchModel.Result>()
    var LocationList: ArrayList<com.hiddle.app.model.PlaceSearchModel.Result> = ArrayList<com.hiddle.app.model.PlaceSearchModel.Result>()
    private val latLng: LatLng? = null
    private var marker: Marker? = null
    var state: String = ""
    var main_address: String = ""
    var geocoder: Geocoder? = null
    var addresses: List<Address> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)

        getIntentData()
        getPermissionforMapAndInitialization()  // permission

    }

    private fun getIntentData() {
        val i = intent
        if (i != null) {
            isFrom = i.getStringExtra("isFrom")!!
        }
    }

    private fun allClicklistener() {

        ivBackLocation.setOnClickListener(this)
        ivCrossLocation.setOnClickListener(this)
        ivAddLocation.setOnClickListener(this)
        llBackLocation.setOnClickListener(this)

        etSearchLocation.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (s.toString().isEmpty()) {
                    searchData.clear()
                    rvPlaces.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
                    searchMapAdapter = SearchMapAdapter(activity, searchData, this@MapActivity)
                    rvPlaces.adapter = searchMapAdapter
                    rvPlaces.visibility = View.GONE
                } /*else {
                    searchData.clear()
                    rvPlaces.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
                    searchMapAdapter = SearchMapAdapter(activity, searchData, this@MapActivity)
                    rvPlaces.adapter = searchMapAdapter
                    rvPlaces.visibility = View.VISIBLE
                    val enteredValue = s.toString()
                    SearchData(enteredValue)

                }*/
            }

            override fun afterTextChanged(s: Editable) {
                if (s.toString()!="") {
                    searchData.clear()
                    rvPlaces.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
                    searchMapAdapter = SearchMapAdapter(activity, searchData, this@MapActivity)
                    rvPlaces.adapter = searchMapAdapter
                    rvPlaces.visibility = View.VISIBLE
                    val enteredValue = s.toString()
                    SearchData(etSearchLocation.text.toString())
                    SearchData(etSearchLocation.text.toString())
                }
                if (s.toString().isEmpty()) {
                    searchData.clear()
                    rvPlaces.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
                    searchMapAdapter = SearchMapAdapter(activity, searchData, this@MapActivity)
                    rvPlaces.adapter = searchMapAdapter
                    rvPlaces.visibility = View.GONE
                }
                // get entered value (if required)
            }
        })
    }

    private fun getPermissionforMapAndInitialization() {
        callFineLocation()
    }


    @AfterPermissionGranted(RC_FINE_LOCATION)
    fun callFineLocation() {
        if (EasyPermissions.hasPermissions(activity, permission.ACCESS_FINE_LOCATION)) {
            // Have permission, do the thing!
            callCorseLocation()
        } else {
            // Request one permission
            EasyPermissions.requestPermissions(
                    PermissionRequest.Builder(this, RC_FINE_LOCATION, permission.ACCESS_FINE_LOCATION)
                            .setRationale(R.string.rationale_call)
                            .setPositiveButtonText(getString(R.string.ok))
                            .setNegativeButtonText(getString(R.string.cancel))
                            .build())
        }
    }

    //permisson read storage
    @AfterPermissionGranted(RC_ACCESSS_LOCATION)
    fun callCorseLocation() {
        if (EasyPermissions.hasPermissions(activity, permission.ACCESS_COARSE_LOCATION)) {
            // Have permission, do the thing!

            getMapInit() //for initialize map
            allClicklistener()  // all click listener

        } else {
            // Request one permission
            EasyPermissions.requestPermissions(
                    PermissionRequest.Builder(this, RC_ACCESSS_LOCATION, permission.ACCESS_COARSE_LOCATION)
                            .setRationale(R.string.rationale_call)
                            .setPositiveButtonText(getString(R.string.ok))
                            .setNegativeButtonText(getString(R.string.cancel))
                            .build())
        }
    }

    private fun getMapInit() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        Objects.requireNonNull(mapFragment)!!.getMapAsync(this)
        mapFragment!!.getMapAsync(this)
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onMapReady(googleMap: GoogleMap?) {

        geocoder = Geocoder(this, Locale.getDefault())
        mMap = googleMap
        mMap!!.isMyLocationEnabled = true
        mMap!!.uiSettings.isMyLocationButtonEnabled = true
        mMap!!.setMinZoomPreference(11f)

        mapClicklistener()
        val myLocationChangeListener = GoogleMap.OnMyLocationChangeListener { location ->
            val loc = LatLng(location.latitude, location.longitude)
            /*  param_current_latitude = location.latitude.toString() //for get user current laitude and  longitude
              param_current_longitude = location.longitude.toString()*/
            //  Log.e("latitude", param_current_latitude + " longitude" + param_current_longitude)
            if (!isFirstTime) {
                mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(location.latitude, location.longitude), 15f))

                if (isFrom == "homeCurrent") {
                    geocoder = Geocoder(activity, Locale.ENGLISH)
                    if (location.latitude != null) {
                        try {
                            if (geocoder!!.getFromLocation(location.latitude, location.longitude, 1) != null) {
                                addresses = geocoder!!.getFromLocation(location.latitude, location.longitude, 1) // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                                Log.e("Address", addresses[0].toString())
                                if (addresses[0].getAddressLine(0) != null) {
                                    address = addresses[0].getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                                    param_latitude = location.latitude.toString()
                                    param_longitude = location.longitude.toString()
                                    etSearchLocation.setText(address)
                                    rvPlaces.visibility = View.GONE
                                    showMarker(address, param_latitude, param_longitude, getString(R.string.my_current_location))
                                    clearAllLocationList()
                                }
                            }
                        } catch (e: Exception) {

                        }


                    }


                }
            }
            isFirstTime = true
        }
        mMap!!.setOnMyLocationChangeListener(myLocationChangeListener)

    }

    private fun mapClicklistener() {
        /*mMap!!.setOnMapClickListener(GoogleMap.OnMapClickListener { point ->

            state = ""
            main_address = ""

            param_latitude = point.latitude.toString()
            Log.e("name", point.toString())
            param_longitude = point.longitude.toString()

            var addresses: List<Address> = ArrayList()
            try {
                addresses = geocoder!!.getFromLocation(point.latitude, point.longitude, 1)
            } catch (e: IOException) {
                e.printStackTrace()
            }
            if (addresses.isNotEmpty()) {
                val address = addresses[0]
                if (addresses[0].getAddressLine(0) != null) {
                    main_address = addresses[0].getAddressLine(0)
                }

                if (addresses[0].adminArea != null) {
                    state = addresses[0].adminArea
                }

                Log.e("Main", main_address)
                if (address != null) {
                    val sb = StringBuilder()
                    for (i in 0 until address.maxAddressLineIndex) {
                        sb.append(address.getAddressLine(i) + "\n")
                        Log.e("line", address.getAddressLine(i))
                    }
                    Log.e("addreess", sb.toString() + "Address" + address)
                    Log.e("address1", sb.toString())
                }

                if (main_address != "") {
                    etSearchLocation.setText(main_address)
                    rvPlaces.visibility = View.GONE
                    showMarker(state, param_latitude, param_longitude, main_address)
                    clearAllLocationList()

                }
            }


        })*/
    }


    private fun SearchData(search: String) {
        pbMap.visibility =  View.VISIBLE
        apiService = APIClient.getClientMap()!!.create(ApiInterface::class.java)

        val call: Call<SearchDataResponseModel?>? = apiService!!.getData(search, getString(R.string.google_maps_key))
        call!!.enqueue(object : Callback<SearchDataResponseModel?> {
            override fun onResponse(call: Call<SearchDataResponseModel?>, response: Response<SearchDataResponseModel?>) {
                val resultDistance: SearchDataResponseModel? = response.body()
                if ("OK".equals(resultDistance!!.status, ignoreCase = true)) {

                    /* LocationList.clear()
                     LocationList.addAll(resultDistance.results!!)
                     searchData = searchData.map { it.name }*/
                    searchData.clear()
                    searchData.addAll(resultDistance.results!!)

                    Log.e("Response", resultDistance.results.toString())

                    rvPlaces.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
                    searchMapAdapter = SearchMapAdapter(activity, searchData, this@MapActivity)
                    rvPlaces.adapter = searchMapAdapter
                    pbMap.visibility =  View.GONE
                    //       Toast.makeText(MapActivity.this, "Success", Toast.LENGTH_LONG).show();
                }
            }

            override fun onFailure(call: Call<SearchDataResponseModel?>, t: Throwable) {
                call.cancel()
                pbMap.visibility =  View.GONE
                val enteredValue = etSearchLocation.text.toString()
                SearchData(enteredValue)

            }
        })
    }


    private fun showMarker(address: String, lat: String, lang: String, placeName: String) {
        Log.e("name", address)
        mMap!!.clear()

        val latLng = LatLng(lat.toDouble(), lang.toDouble())

        val markerOptions: MarkerOptions = MarkerOptions()
        markerOptions.position(latLng).title(placeName)
                .snippet(address)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET))


        val customInfoWindow: CustomInfoWindowGoogleMap = CustomInfoWindowGoogleMap(activity)
        mMap!!.setInfoWindowAdapter(customInfoWindow)
        val m: Marker = mMap!!.addMarker(markerOptions)
        m.showInfoWindow()

        mMap!!.moveCamera(CameraUpdateFactory.newLatLng(latLng))
        mMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14.0f))

        mMap!!.setOnInfoWindowClickListener(GoogleMap.OnInfoWindowClickListener {
            addLocationi_FilterLocation()
        })
    }

    override fun clickOnLocation(position: Int, name: String, address_location: String, lat: String, lng: String) {
        try {
            param_latitude = lat
            param_longitude = lng
            address = address_location
            etSearchLocation.setText(address)
            showMarker(address, param_latitude, param_longitude, name)

            AppUtils.hideSoftKeyboard(activity)
            clearAllLocationList()
            rvPlaces.visibility = View.GONE
        } catch (e: Exception) {
            Log.e("data_error", "location_error")
        }

    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.ivBackLocation -> {
                finish()
                AppUtils.overridePendingTransitionExit(activity)
            }
            R.id.llBackLocation -> {
                finish()
                AppUtils.overridePendingTransitionExit(activity)
            }
            R.id.ivCrossLocation -> {
                etSearchLocation.setText("")
                param_latitude = ""
                param_longitude = ""
                clearAllLocationList()
                mMap!!.clear()
            }
            R.id.ivAddLocation -> {
                addLocationi_FilterLocation()  //  for add location in filter location list
            }
            /* R.id.llsetLocation -> {
                 addLocationi_FilterLocation() //  for add location in filter location list
             }*/
        }
    }

    fun addLocationi_FilterLocation() {
        if (validate()) {
            if (isFrom == "home" || isFrom == "homeCurrent") {
                Toast.makeText(activity, getString(R.string.add_place_successfully), Toast.LENGTH_SHORT).show()

                val locationModel: LocationModel = LocationModel()
                locationModel.id = (HomeActivity.locationFilterlist.size + 1).toString()
                /* locationModel.name = etSearchLocation.text.toString()*/
                locationModel.name = getAddressFrmLatlangLocation(param_latitude, param_longitude)
                locationModel.lat = param_latitude
                locationModel.lang = param_longitude
                locationModel.isSelected = false
                HomeActivity.locationFilterlist.add(locationModel)
            }

            if (isFrom == "editProfile") {
                //paramProfileLocation = etSearchLocation.text.toString()
                paramProfileLocation = getAddress(param_latitude.toDouble(), param_longitude.toDouble())
                if (paramProfileLocation == "") {
                    paramProfileLocation = etSearchLocation.text.toString()
                }
                paramProfileLat = param_latitude
                paramProfileLng = param_longitude
            }

            finish()
            AppUtils.overridePendingTransitionExit(activity)

        }
    }

    private fun getAddressFrmLatlangLocation(paramLatitude: String, paramLongitude: String): String {
        try {
            geocoder = Geocoder(activity, Locale.ENGLISH)
            addresses = geocoder!!.getFromLocation(paramLatitude.toDouble(), paramLongitude.toDouble(), 1) // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            if (addresses.isNotEmpty()) {
                if (addresses[0].locality != null) {
                    address = addresses[0].locality
                    Log.e("address1", address)
                } else {
                    if (addresses[0].adminArea != null) {
                        address = addresses[0].adminArea
                    } else {
                        if (addresses[0].getAddressLine(0) != null) {
                            address = addresses[0].getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                            Log.e("address2", address)
                        }
                    }
                }
            } else {
                address = etSearchLocation.text.toString()
            }
        } catch (e: Exception) {

        }
        return address
    }

    private fun validate(): Boolean {
        if (etSearchLocation.text.toString().isEmpty()) {
            showSnackBar(activity, getString(R.string.select_location))
            return false
        }
        if (param_latitude == "") {
            showSnackBar(activity, getString(R.string.select_location))
            return false
        }
        return true
    }

    private fun clearAllLocationList() {
        searchData.clear()
        rvPlaces.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        searchMapAdapter = SearchMapAdapter(activity, searchData, this@MapActivity)
        rvPlaces.adapter = searchMapAdapter
    }

    fun getAddress(LATITUDE: Double, LONGITUDE: Double): String {

        try {
            geocoder = Geocoder(activity, Locale.ENGLISH)
            addresses = geocoder!!.getFromLocation(LATITUDE, LONGITUDE, 1) // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            Log.e("Address", addresses[0].toString())
            if (addresses.isNotEmpty()) {

                if (addresses[0].locality != null) {
                    if (addresses[0].locality == addresses[0].adminArea) {
                        address = addresses[0].adminArea + ", " + addresses[0].countryName
                    } else {
                        address = addresses[0].locality + ", " + addresses[0].adminArea + ", " + addresses[0].countryName
                    }
                } else {
                    address = addresses[0].adminArea + ", " + addresses[0].countryName
                }
                Log.e("Name", address)

            }

            Log.e("Address", addresses[0].toString())
            /*   if (addresses.isNotEmpty()) {

                   if (addresses[0].locality != null) {
                       address = addresses[0].locality
                   }
                   if (addresses[0].adminArea != null) {
                       address = address + "," + addresses[0].adminArea
                   }
                   if (addresses[0].countryName != null) {
                       address = address + "," + addresses[0].countryName
                   }
                   Log.e("Name", address)

               }*/

            if (address == "") {
                address = etSearchLocation.text.toString()
                /*if (addresses[0].getAddressLine(0) != null) {
                    address = addresses[0].getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                }*/
            }

        } catch (e: Exception) {

        }
        return address

    }
}
