package com.hiddle.app.activity

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.renderscript.Allocation
import android.renderscript.Element
import android.renderscript.RenderScript
import android.renderscript.ScriptIntrinsicBlur
import android.view.View
import com.hiddle.app.R
import com.hiddle.app.adapter.SlidingImage_Adapter
import com.hiddle.app.model.UserModel.UserImageModel
import com.hiddle.app.util.AppUtils
import kotlinx.android.synthetic.main.activity_profile_image.*
import kotlinx.android.synthetic.main.activity_profile_image.dots_indicator_profile
import kotlinx.android.synthetic.main.activity_profile_image.ivCircleBlur
import kotlinx.android.synthetic.main.activity_profile_image.ivClose
import kotlinx.android.synthetic.main.activity_profile_image.vpProfileUser
import kotlinx.android.synthetic.main.content_event.*

class ProfileImageActivity : BaseMainActivity(), View.OnClickListener {
    var imageProfilelist: ArrayList<UserImageModel> = ArrayList()
    var baseUrl: String = ""
    var position: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_image)

        getIntentData()
        setCrossBlurIcon()
        setAdapter()
        allClicklistener()

    }

    private fun setCrossBlurIcon() {
        var final_Bitmap: Bitmap? = null
        final_Bitmap = BitmapFactory.decodeResource(activity.resources, R.drawable.ic_placeholder_icn);
        final_Bitmap = BlurImage(final_Bitmap);
        ivCircleBlur!!.setImageBitmap(final_Bitmap)
    }

    private fun allClicklistener() {
        ivClose.setOnClickListener(this)
        cvView.setOnClickListener(this)
    }
    @SuppressLint("NewApi")
    fun BlurImage(input: Bitmap): Bitmap {
        try {
            val rsScript = RenderScript.create(applicationContext)
            val alloc = Allocation.createFromBitmap(rsScript, input)
            val blur = ScriptIntrinsicBlur.create(rsScript, Element.U8_4(rsScript))
            blur.setRadius(21F)
            blur.setInput(alloc)
            val result = Bitmap.createBitmap(input.width, input.height, Bitmap.Config.ARGB_8888)
            val outAlloc = Allocation.createFromBitmap(rsScript, result)
            blur.forEach(outAlloc)
            outAlloc.copyTo(result)
            rsScript.destroy()
            return result
        } catch (e: Exception) {
            // TODO: handle exception
            return input
        }
    }

    private fun getIntentData() {

        var i = intent
        if (i != null) {
            baseUrl = i.getStringExtra("baseUrl")!!
            position = i.getStringExtra("position")!!
            imageProfilelist = (i.getSerializableExtra("imagelist") as ArrayList<UserImageModel>?)!!

        }


    }

    private fun setAdapter() {
        vpProfileUser!!.adapter = SlidingImage_Adapter(activity, imageProfilelist, baseUrl)
        dots_indicator_profile!!.setViewPager(vpProfileUser!!)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.ivClose -> {
                finish()
                AppUtils.overridePendingTransitionExit(activity)
            }
            R.id.cvView -> {
                finish()
                AppUtils.overridePendingTransitionExit(activity)
            }


        }
    }
}
