package com.hiddle.app.activity

import android.Manifest
import android.accounts.AuthenticatorException
import android.accounts.NetworkErrorException
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Geocoder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.EditText
import android.widget.HorizontalScrollView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.androidquery.AQuery
import com.bumptech.glide.Glide
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonArray
import com.hiddle.app.R
import com.hiddle.app.`interface`.AddImageClick
import com.hiddle.app.`interface`.AuthenticationListener
import com.hiddle.app.`interface`.StartProfileDragListener
import com.hiddle.app.adapter.EditProfileAdapter
import com.hiddle.app.adapter.EditProfileAdapter.Companion.selectedItem
import com.hiddle.app.adapter.InstaAdapter
import com.hiddle.app.adapter.OriginAdapter
import com.hiddle.app.itemDrag.ItemMoveProfileCallback
import com.hiddle.app.model.FbMediaModel.FbMediaResponse
import com.hiddle.app.model.GeneralModel
import com.hiddle.app.model.ImageModel
import com.hiddle.app.model.InstaMediaModel.InstaMediaResponse
import com.hiddle.app.model.InstaModel
import com.hiddle.app.model.InstagramModel.InstagramAccessToken
import com.hiddle.app.model.UserModel.UserImageModel
import com.hiddle.app.model.VerifyCodeModel.VerifyModelResponse
import com.hiddle.app.retrofit.APIClient
import com.hiddle.app.retrofit.ApiInterface
import com.hiddle.app.retrofit.RetrofitRestClient
import com.hiddle.app.util.*
import droidninja.filepicker.FilePickerBuilder
import droidninja.filepicker.FilePickerConst
import droidninja.filepicker.utils.ContentUriUtils
import kotlinx.android.synthetic.main.activity_edit_profile.*
import kotlinx.android.synthetic.main.activity_login_screen.*
import kotlinx.android.synthetic.main.activity_profile_image.*
import kotlinx.android.synthetic.main.activity_strike.*
import kotlinx.android.synthetic.main.bottom_sheet_picture.view.tvCancel
import kotlinx.android.synthetic.main.bottom_sheet_picture.view.tvGallery
import kotlinx.android.synthetic.main.bottom_sheet_picture.view.tvTakePhoto
import kotlinx.android.synthetic.main.bottom_sheet_picture_profile.view.*
import kotlinx.android.synthetic.main.layout_social_media.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.internal.trimSubstring
import org.json.JSONException
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.IOException
import java.net.SocketTimeoutException
import java.net.URLDecoder
import java.util.*


class EditProfileActivity : BaseMainActivity(), AuthenticationListener, AddImageClick, View.OnClickListener, View.OnTouchListener, ViewTreeObserver.OnScrollChangedListener, StartProfileDragListener {
    private val profileImagelist: ArrayList<ImageModel> = ArrayList(8)

    var userChoosenTask: String = ""
    var clickedPosition: Int = -1
    var picturePath: String = ""
    private val PICK_CAMERA_REQUEST = 2
    val MY_PERMISSIONS_REQUEST_WRITE_STORAGE = 101
    val MY_PERMISSIONS_REQUEST_STORAGE = 100
    val MY_PERMISSIONS_REQUEST_CAMERA = 150
    var mainProfilePicturePath: String = ""
    var mainProfilePicturePath_isNew: String = ""
    var paramRemovedImage: String = ""
    private val PICK_IMAGE_REQUEST = 1
    private var cameraPhoto: CameraPhoto? = null
    var isSelectPic: Boolean = false
    var isFrom: String = ""
    var paramProfileName: String = ""
    var paramProfileHeight: String = ""
    var paramProfileAge: String = ""
    var paramProfileGender: String = ""
    var imageProfilelist: ArrayList<UserImageModel> = ArrayList()
    var imageFilelist: ArrayList<String> = ArrayList()
    var removeImagelist: ArrayList<String> = ArrayList()
    var paramProfileProfession: String = ""
    var paramBaseUrl: String = ""
    val imageModel: ImageModel? = null
    private var MAX_ATTACHMENT_COUNT = 8
    private val photoPaths: ArrayList<Uri> = ArrayList()
    var isAllImageBlank = false
    var clickEvent: String = ""
    var layoutManager: GridLayoutManager? = null

    //insta
    var height: Int? = null

    //insta login
    var callbackManager: CallbackManager? = null
    private var accessTokenTracker: AccessTokenTracker? = null
    private var profileTracker: ProfileTracker? = null
    private var aQuery: AQuery? = null

    var doubleBackToExitPressedOnce: Boolean = false
    var geocoder: Geocoder? = null
    var instaUserId: String? = null
    var afterInstaCursor: String? = null
    var afterFbCursor: String = ""

    var instaMedialist: ArrayList<String> = ArrayList()
    var instalist: ArrayList<InstaModel> = ArrayList()

    var fbMedialist: ArrayList<String> = ArrayList()
    var fblist: ArrayList<InstaModel> = ArrayList()

    var paramFbConnect: String = "0"
    var paramInstaConnect: String = "0"
    var isWebviewOpen: Boolean = false
    var isFromClick: String = ""

    companion object {
        const val RC_STORAGE_PERMISSION: Int = 128

        @JvmStatic
        var originProfilelist: ArrayList<String> = ArrayList()

        @JvmStatic
        var paramProfileLocation = ""

        @JvmStatic
        var paramProfileLat = ""

        @JvmStatic
        var paramProfileLng = ""
    }

    var editProfileAdapter: EditProfileAdapter? = null

    var touchHelper: ItemTouchHelper? = null

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)

        AppUtils.showProgressDialog(activity)
        getIntentData()
        initView()  // init view
        setRecyclerviewForProfile()  // for set recyclerview to add picture
        allClicklistener()
        //draglistnerMethod()
        initFbInfo()  //init fb


        //for scroll horizontal scrollview left side in
        hsView.postDelayed({
            hsView.scrollTo(1, 0)
            hsView.fullScroll(HorizontalScrollView.FOCUS_LEFT)
            AppUtils.hideProgressDialog()
            llMainLayoutProfile.visibility = View.VISIBLE
        }, 100L)
        //try this


    }

    @RequiresApi(Build.VERSION_CODES.N)


    private fun draglistnerMethod() {
        cvProfile.setOnDragListener(View.OnDragListener { v, event ->
            when (event.action) {
                DragEvent.ACTION_DRAG_ENTERED -> ivUserProfile.setBackgroundColor(Color.TRANSPARENT)
                DragEvent.ACTION_DRAG_EXITED -> ivUserProfile.setBackgroundColor(Color.TRANSPARENT)
                DragEvent.ACTION_DRAG_ENDED -> ivUserProfile.setBackgroundColor(Color.TRANSPARENT)
                DragEvent.ACTION_DROP -> setImgIntoContainer(event, event.localState.toString())
                //  final float dropX = dragEvent.getX();
                //  final float dropY = dragEvent.getY();
                /*  var state : DragData = event.getLocalState()*/
            }
            true
        })
    }

    private fun setImgIntoContainer(event: DragEvent, toString: String) {
        //  var state : DragData= event.localState.toString()
        // var state: DragData =  event.localState.
        // Toast.makeText(activity, selectedItem.toString(), Toast.LENGTH_SHORT).show()

        var profile_isnew = mainProfilePicturePath_isNew
        var profile_image = mainProfilePicturePath

        if (profileImagelist[selectedItem].is_new == "N") {
            // setMainProfileinRemovelist()
            mainProfilePicturePath_isNew = "N"
            mainProfilePicturePath = profileImagelist[selectedItem].new_image
            //for remove when drag other  image
            //removeDragImageFromadapter(selectedItem)
        }
        if (profileImagelist[selectedItem].is_new == "Y") {

            //setMainProfileinRemovelist()
            mainProfilePicturePath_isNew = "Y"
            mainProfilePicturePath = profileImagelist[selectedItem].new_image
            //for remove image when drag
            // removeDragImageFromadapter(selectedItem)
        }
        Glide.with(activity).load(mainProfilePicturePath).placeholder(R.drawable.ic_placeholder_icn).into(ivUserProfile)

        val imageModel: ImageModel = ImageModel()
        imageModel.is_new = profile_isnew
        if (profile_isnew == "") {
            imageModel.is_new = "N"
        }
        imageModel.new_image = profile_image
        profileImagelist[selectedItem] = imageModel

        setProfileImageAdapter()

        /*Toast.makeText(activity,)
        imageView.setImageResource(listModal.getDrawable());*/
    }

    private fun removeDragImageFromadapter(position: Int) {
        val imageModel: ImageModel = ImageModel()
        imageModel.is_new = ""
        imageModel.new_image = ""
        imageModel.order_id = ""
        imageModel.is_selected = false
        imageModel._id = ""
        profileImagelist.set(position, imageModel)

        setProfileImageAdapter()
    }


    private fun getIntentData() {
        val i = intent
        if (i != null) {

            paramProfileName = i.getStringExtra("paramProfileName")!!
            paramProfileHeight = i.getStringExtra("paramProfileHeight")!!
            paramProfileAge = i.getStringExtra("paramProfileAge")!!
            paramProfileGender = i.getStringExtra("paramProfileGender")!!
            paramProfileLat = i.getStringExtra("paramProfileLatitude")!!
            paramProfileLng = i.getStringExtra("paramProfileLongitude")!!
            paramProfileLocation = i.getStringExtra("paramProfileLocation")!!
            paramProfileProfession = i.getStringExtra("paramProfileProfession")!!
            paramBaseUrl = i.getStringExtra("paramBaseUrl")!!
            originProfilelist = i.getStringArrayListExtra("originProfilelist")!!
            imageProfilelist = (i.getSerializableExtra("imageProfilelist") as ArrayList<UserImageModel>?)!!
            paramInstaConnect = i.getStringExtra("paramInstaCoonect")!!
            paramFbConnect = i.getStringExtra("paramFbConnect")!!


            if (paramFbConnect == "1") {
                ivFbProfile.visibility = View.GONE
                ivFbDisconnect.visibility = View.VISIBLE
                fbMedialist.clear()
                fbMedialist = i.getStringArrayListExtra("fblist")!!
                showAllFbMedia()

            } else {
                ivFbProfile.visibility = View.VISIBLE
                ivFbDisconnect.visibility = View.GONE
                llFb.visibility = View.GONE

            }

            if (paramInstaConnect == "1") {
                ivInstagramProfile.visibility = View.GONE
                ivInstagramProfileDisconnect.visibility = View.VISIBLE
                instaMedialist.clear()
                instaMedialist = i.getStringArrayListExtra("instalist")!!
                Log.e("instamedialist", instaMedialist.toString())
                showAllInstaMedia()

            } else {
                ivInstagramProfile.visibility = View.VISIBLE
                ivInstagramProfileDisconnect.visibility = View.GONE
                llInsta.visibility = View.GONE
            }


            Log.e("param1", imageProfilelist.toString())

            if (paramProfileName != null) {
                tvAboutprofile!!.text = paramProfileName + ", " + paramProfileHeight + "" +
                        getString(R.string.meter) + ", " + paramProfileAge + " " + getString(R.string.years_old) + " " + paramProfileGender
            }

            if (paramProfileProfession != null) {
                etProfession.setText(paramProfileProfession)
            }

            if (paramProfileLocation != null) {
                if (paramProfileLocation != "") {
                    tvProfileLocation.text = paramProfileLocation
                }
            }

            setOriginData()
        }
    }

    private fun setOriginData() {
        if (originProfilelist.size > 0) {
            var origin: String = originProfilelist.toString().replace("[", "")
            origin = origin.replace("]", "")
            tvProfileOrigin.setText(origin)
        }

        val layoutManager: GridLayoutManager = GridLayoutManager(activity, 3, RecyclerView.HORIZONTAL, false) //set adapter
        layoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                when (position) {
                    else -> return 2
                }
            }
        }

    }

    private fun initView() {
        setProfessionaEnableFalse()
    }

    private fun allClicklistener() {
        ivBackProfile.setOnClickListener(this)
        ivUserProfile.setOnClickListener(this)
        ivRemoveProfile.setOnClickListener(this)
        ivEditLocation.setOnClickListener(this)
        ivEditOrigin.setOnClickListener(this)
        ivEditProfession.setOnClickListener(this)
        ivProfessionSubmit.setOnClickListener(this)
        ivFbProfile.setOnClickListener(this)
        ivInstagramProfile.setOnClickListener(this)
        ivFbDisconnect.setOnClickListener(this)
        ivInstagramProfileDisconnect.setOnClickListener(this)

        tvUpdate.setOnClickListener(this)
        ivBackIcon.setOnClickListener(this)

        setHorizontallscrollview()

        rvEditProfile.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, e: MotionEvent?): Boolean {
                val action: Int = e!!.getAction();
                if (action == MotionEvent.ACTION_MOVE) {
                    rvEditProfile.getParent().requestDisallowInterceptTouchEvent(true);
                }
                return false
            }
        })
    }

    private fun setHorizontallscrollview() {
        hsView.setOnTouchListener(this);
        hsView.viewTreeObserver.addOnScrollChangedListener(this);
        ivDot.setImageResource(R.drawable.ic_dot_1)
    }

    private fun setRecyclerviewForProfile() {
        //set adapter for show list

        for (i in 0..8) {     //add 9 picture random for design
            val imageModel: ImageModel = ImageModel()
            imageModel.is_new = ""
            imageModel.new_image = ""
            imageModel._id = ""
            imageModel.is_selected = false
            imageModel.order_id = ""
            profileImagelist.add(i, imageModel)
        }

        setProfileImageAdapter()

        //set profile image list
        try {
            for (j in imageProfilelist.indices) {

                val imageModel: ImageModel = ImageModel()
                imageModel.is_new = "N"
                imageModel.new_image = paramBaseUrl + imageProfilelist[j].image
                Log.e("url", paramBaseUrl + imageProfilelist[j].image)
                imageModel._id = imageProfilelist[j].id.toString()
                imageModel.is_selected = imageProfilelist[j].isSelected!!
                imageModel.order_id = imageProfilelist[j].orderId.toString()
                profileImagelist[j] = imageModel   //set as index

            }
            setProfileImageAdapter()
        } catch (e: Exception) {

        }

    }


    private fun setProfileImageAdapter() {
        layoutManager = GridLayoutManager(activity, 2, GridLayoutManager.HORIZONTAL, false) //set adapter
        editProfileAdapter = EditProfileAdapter(activity, profileImagelist, this, this)

        //for move view in one to another
        val callback = ItemMoveProfileCallback(editProfileAdapter)
        touchHelper = ItemTouchHelper(callback)
        touchHelper!!.attachToRecyclerView(rvEditProfile)


        layoutManager!!.setSpanSizeLookup(object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return if (position == 0) 2 else 1
            }
        })

        rvEditProfile.layoutManager = layoutManager
        rvEditProfile.adapter = editProfileAdapter


    }

    override fun requestProfileDrag(viewHolder: RecyclerView.ViewHolder?) {
        touchHelper!!.startDrag(viewHolder!!)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.ivBackProfile -> {
                isFromClick = "back"
                onBackPressed()
            }
            R.id.ivBackIcon -> {
                isFromClick = "back"
                onBackPressed()
            }
            R.id.ivUserProfile -> {
                isFrom = "profile"
                selectImage(1000, isFrom)  //for only select mail profile picture
            }
            R.id.ivRemoveProfile -> {
            }
            R.id.ivEditLocation -> {
                // Toast.makeText(activity,getString(R.string.work_in_progress),Toast.LENGTH_SHORT).show()
                ivEditLocation.isEnabled = false
                val i = Intent(activity, MapActivity::class.java)
                i.putExtra("isFrom", "editProfile")
                startActivity(i)
                AppUtils.overridePendingTransitionEnter(activity)
            }
            R.id.ivEditOrigin -> {
                ivEditOrigin.isEnabled = false
                val i = Intent(activity, MyOriginActivity::class.java)
                i.putExtra("isFrom", "profile")
                startActivity(i)
                AppUtils.overridePendingTransitionEnter(activity)
            }
            R.id.ivEditProfession -> {
                ivEditProfession.visibility = View.GONE
                ivProfessionSubmit.visibility = View.VISIBLE
                etProfession.isFocusable = true;
                etProfession.isFocusableInTouchMode = true; // user touches widget on phone with touch screen
                etProfession.isClickable = true;
                etProfession.showKeyboard()
            }
            R.id.ivProfessionSubmit -> {
                AppUtils.hideSoftKeyboard(activity)
                ivProfessionSubmit.visibility = View.GONE
                ivEditProfession.visibility = View.VISIBLE
                setProfessionaEnableFalse()

            }
            R.id.ivFbProfile -> {
                isFromClick = "social"
                afterFbCursor = ""
                fbMedialist.clear()
                clickEvent = "FB"
                methodRequiresStoragePermission() //call FB Implementattion
            }
            R.id.ivInstagramProfile -> {
                isFromClick = "social"
                mWebView?.clearCache(true);
                mWebView?.clearHistory()
                mWebView?.settings!!.savePassword = false;
                mWebView?.settings!!.saveFormData = false;
                afterInstaCursor = ""
                instaMedialist.clear()
                clickEvent = "INSTAGRAM"
                methodRequiresStoragePermission() //method for storage

            }
            R.id.ivFbDisconnect -> {
                isFromClick = "social"
                afterFbCursor = ""
                paramFbConnect = "0"
                fbMedialist.clear()
                ivFbProfile.visibility = View.VISIBLE
                ivFbDisconnect.visibility = View.GONE
                showAllFbMedia()
                // Toast.makeText(activity, getString(R.string.successfully_connected_with_fb), Toast.LENGTH_SHORT).show()
                callAPIUpdateViewForSocialMedia()
            }
            R.id.ivInstagramProfileDisconnect -> {
                isFromClick = "social"
                afterInstaCursor = ""
                paramInstaConnect = "0"
                instaMedialist.clear()
                ivInstagramProfile.visibility = View.VISIBLE
                ivInstagramProfileDisconnect.visibility = View.GONE
                showAllInstaMedia()
                //Toast.makeText(activity, getString(R.string.successfully_connected_with_insta), Toast.LENGTH_SHORT).show()
                callAPIUpdateViewForSocialMedia()
            }
            R.id.tvUpdate -> {
                callUpdateAllView()
            }
        }
    }

    private fun callAPIUpdateViewForSocialMedia() {
        callLoginUpdateForSocialMedia()
    }

    private fun callLoginUpdateForSocialMedia() {
        if (AppUtils.isConnectedToInternet(activity)) {
            //AppUtils.showProgressDialog(activity)
            val params = HashMap<String, RequestBody?>()

            params["is_fb_connect"] = AppUtils.getRequestBody(paramFbConnect)
            params["is_insta_connect"] = AppUtils.getRequestBody(paramInstaConnect)

            val gsonInsta: Gson = GsonBuilder().create()
            val myCustomArrayInsta: JsonArray = gsonInsta.toJsonTree(instaMedialist).getAsJsonArray();
            params["insta_json_array"] = AppUtils.getRequestBody(myCustomArrayInsta.toString())

            val gsonFb: Gson = GsonBuilder().create()
            val myCustomArrayFb: JsonArray = gsonFb.toJsonTree(fbMedialist).getAsJsonArray();
            params["fb_json_array"] = AppUtils.getRequestBody(myCustomArrayFb.toString())

            val partbody1 = arrayOfNulls<MultipartBody.Part>(0)

            val call: Call<VerifyModelResponse>
            call = RetrofitRestClient.instance!!.loginUpdateApi(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, params, partbody1)
            call.enqueue(object : Callback<VerifyModelResponse> {
                override fun onResponse(call: Call<VerifyModelResponse>?, response: Response<VerifyModelResponse>?) {
                    AppUtils.hideProgressDialog()
                    val basicModel: VerifyModelResponse
                    Log.e("Response", response.toString())
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {

                        } else if (Objects.requireNonNull(basicModel).code == "-1") {
                            callLogoutFunction()
                        } else {
                            // showSnackBar(activity, basicModel.message)
                        }
                    } else {
                        //showSnackBar(activity, response.message())
                    }
                }

                override fun onFailure(call: Call<VerifyModelResponse>?, t: Throwable?) {
                    Log.e("callError", call.toString())
                    Log.e("Error", t.toString())
                    AppUtils.hideProgressDialog()
                    /* if (t is SocketTimeoutException) {
                         showSnackBar(activity, getString(R.string.connection_timeout))
                     } else if (t is NetworkErrorException) {
                         showSnackBar(activity, getString(R.string.network_error))
                     } else if (t is AuthenticatorException) {
                         showSnackBar(activity, getString(R.string.authentiation_error))
                     } else {
                         showSnackBar(activity, getString(R.string.something_went_wrong))
                     }*/
                }
            })
        } else {
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }
    }

    private fun setDataAfterRemoving() {
        if (mainProfilePicturePath == "") {
            //first take whole imagelist in previous image
            var previousimagelist: ArrayList<ImageModel> = ArrayList()

            for (i in profileImagelist.indices) {
                if (profileImagelist[i].is_new != "") {
                    val imageModel: ImageModel = ImageModel()
                    imageModel.is_new = profileImagelist[i].is_new
                    /*imageModel.old_image = profileImagelist[i].old_image*/
                    imageModel.new_image = profileImagelist[i].new_image
                    imageModel.order_id = profileImagelist[i].order_id
                    imageModel.is_selected = profileImagelist[i].is_selected
                    imageModel._id = profileImagelist[i]._id
                    previousimagelist.add(imageModel)
                }
            }

            profileImagelist.clear() //clear image list

            //now add previous imagelist in main imagelist
            for (i in previousimagelist.indices) {

                if (i == 0) {
                    if (previousimagelist[0].is_new == "N") {
                        mainProfilePicturePath_isNew = "N"
                        mainProfilePicturePath = previousimagelist[0].new_image
                        Glide.with(activity).load(mainProfilePicturePath).placeholder(R.drawable.ic_placeholder_icn).into(ivUserProfile)

                    } else {
                        mainProfilePicturePath_isNew = "Y"
                        mainProfilePicturePath = previousimagelist[0].new_image
                        Glide.with(activity).load(mainProfilePicturePath).placeholder(R.drawable.ic_placeholder_icn).into(ivUserProfile)

                    }
                } else {
                    val imageModel: ImageModel = ImageModel()
                    imageModel.is_new = previousimagelist[i].is_new
                    imageModel.new_image = previousimagelist[i].new_image
                    imageModel.order_id = previousimagelist[i].order_id
                    imageModel.is_selected = previousimagelist[i].is_selected
                    imageModel._id = previousimagelist[i]._id
                    profileImagelist.add(imageModel)
                }


            }
            //now add another box with blank image
            for (i in profileImagelist.size..8) { //now add left box in list and set adapter
                val imageModel: ImageModel = ImageModel()
                imageModel.is_new = ""
                imageModel.new_image = ""
                imageModel.order_id = ""
                imageModel.is_selected = false
                imageModel._id = ""
                profileImagelist.add(imageModel)
            }
            setProfileImageAdapter()
            previousimagelist.clear()

        } else {
            var previousimagelist: ArrayList<ImageModel> = ArrayList()

            for (i in profileImagelist.indices) {
                if (profileImagelist[i].is_new != "") {
                    val imageModel: ImageModel = ImageModel()
                    imageModel.is_new = profileImagelist[i].is_new
                    imageModel.new_image = profileImagelist[i].new_image
                    imageModel.order_id = profileImagelist[i].order_id
                    imageModel.is_selected = profileImagelist[i].is_selected
                    imageModel._id = profileImagelist[i]._id
                    previousimagelist.add(imageModel)
                }
            }

            profileImagelist.clear() //clear image list

            //now add previous imagelist in main imagelist
            for (i in previousimagelist.indices) {


                val imageModel: ImageModel = ImageModel()
                imageModel.is_new = previousimagelist[i].is_new
                imageModel.new_image = previousimagelist[i].new_image
                imageModel.order_id = previousimagelist[i].order_id
                imageModel.is_selected = previousimagelist[i].is_selected
                imageModel._id = previousimagelist[i]._id
                profileImagelist.add(imageModel)


            }
            //now add another box with blank image
            for (i in profileImagelist.size..8) { //now add left box in list and set adapter
                val imageModel: ImageModel = ImageModel()
                imageModel.is_new = ""
                imageModel.new_image = ""
                imageModel.order_id = ""
                imageModel.is_selected = false
                imageModel._id = ""
                profileImagelist.add(imageModel)
            }
            setProfileImageAdapter()
            previousimagelist.clear()
        }


    }

    private fun callUpdateAllView() {

        if (removeImagelist.size != 0) {
            Log.e("removelist1", removeImagelist.toString())
            var removepath: String = removeImagelist.toString().replace("{", "")
            removepath = removepath.replace("}", "")
            removepath = removepath.replace("[", "")
            removepath = removepath.replace("]", "")
            removepath = removepath.replaceFirst(".$", "");  //for remove last character from string
            removepath = removepath.replace("\\s".toRegex(), "")//for remove space
            Log.e("removelist2", removepath.toString())
            callAPIRemoveImage(removepath)
        }
        if (removeImagelist.size == 0) {
            callAPIUpdateProfile()
        }

    }

    private fun callAPIUpdateProfile() {
        paramProfileHeight = paramProfileHeight.replace("m", "")
        paramProfileHeight = paramProfileHeight.replace("\\s".toRegex(), "")
        getUpdatedImageList()

        if (AppUtils.isConnectedToInternet(activity)) {
            AppUtils.showProgressDialog(activity)
            val params = HashMap<String, RequestBody?>()

            var origin_data: String = ""

            for (k in originProfilelist.indices) {
                if (k == originProfilelist.size - 1) {
                    origin_data = origin_data + originProfilelist[k].toString().trimSubstring(0)
                } else {
                    origin_data = origin_data + originProfilelist[k].toString().trimSubstring(0) + ","
                }
            }


            Log.e("main_origin_data",origin_data)
            origin_data = origin_data.replace("[", "").replace("]", "").trimSubstring(0)
            params["origins"] = AppUtils.getRequestBody(origin_data)
            params["education"] = AppUtils.getRequestBody(AppUtils.getText(etProfession))
            params["address"] = AppUtils.getRequestBody(AppUtils.getText(tvProfileLocation))
            params["height"] = AppUtils.getRequestBody(paramProfileHeight)
            Log.e("value_height", paramProfileHeight)
            params["latitude"] = AppUtils.getRequestBody(paramProfileLat)
            params["longitude"] = AppUtils.getRequestBody(paramProfileLng)
            params["language"] = AppUtils.getRequestBody(SharedPreferenceManager.getMySharedPreferences()!!.getLanguage())
            // params["image1"] = AppUtils.getRequestBody(removeImagelist.toString())

            params["is_fb_connect"] = AppUtils.getRequestBody(paramFbConnect)
            params["is_insta_connect"] = AppUtils.getRequestBody(paramInstaConnect)

            val gsonInsta: Gson = GsonBuilder().create()
            val myCustomArrayInsta: JsonArray = gsonInsta.toJsonTree(instaMedialist).getAsJsonArray();
            params["insta_json_array"] = AppUtils.getRequestBody(myCustomArrayInsta.toString())
            Log.e("insta", myCustomArrayInsta.toString())

            val gsonFb: Gson = GsonBuilder().create()
            val myCustomArrayFb: JsonArray = gsonFb.toJsonTree(fbMedialist).getAsJsonArray();
            params["fb_json_array"] = AppUtils.getRequestBody(myCustomArrayFb.toString())

            Log.e("removelist", removeImagelist.toString() + AppUtils.getText(etProfession) + "origin" + AppUtils.getText(tvProfileOrigin) + "addres" + AppUtils.getText(tvProfileLocation) + "lat" + paramProfileLat + "long" + paramProfileLng)
            Log.e("parameter", params.toString())


            val partbody1 = arrayOfNulls<MultipartBody.Part>(profileImagelist.size)  //put here profileImagelist.size
            if (profileImagelist.size > 0) {
                for (index in profileImagelist.indices) {

                    if (profileImagelist[index].is_new == "Y") {
                        if (profileImagelist[index].new_image != "") {
                            val file: File = File(profileImagelist[index].new_image)
                            val reqFile1: RequestBody = file.asRequestBody("image/*".toMediaTypeOrNull())
                            partbody1[index] = MultipartBody.Part.createFormData("image" + (index + 1).toString(), file.name, reqFile1) //for  imagelist of file
                            Log.e("photopath", profileImagelist[index].new_image)
                        }
                    }

                    if (profileImagelist[index].is_new != "") {
                        params["image" + (index + 1).toString() + "_order_id"] = AppUtils.getRequestBody((index + 1).toString())
                        params["image" + (index + 1).toString() + "_edit_id"] = AppUtils.getRequestBody(profileImagelist[index]._id.toString())
                    }


                }
            }

            val call: Call<VerifyModelResponse>
            call = RetrofitRestClient.instance!!.loginUpdateApi(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, params, partbody1)
            call.enqueue(object : Callback<VerifyModelResponse> {
                override fun onResponse(call: Call<VerifyModelResponse>?, response: Response<VerifyModelResponse>?) {
                    AppUtils.hideProgressDialog()
                    val basicModel: VerifyModelResponse
                    /* Log.e("Response", response.toString())*/
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {

                            imageFilelist.clear()

                            SharedPreferenceManager.getMySharedPreferences()!!.setLogin(true)
                            SharedPreferenceManager.getMySharedPreferences()!!.setUserId(basicModel.data!!.id!!)
                            SharedPreferenceManager.getMySharedPreferences()!!.setUserName(basicModel.data.username!!)
                            SharedPreferenceManager.getMySharedPreferences()!!.setCountryCode(basicModel.data.cc!!)
                            SharedPreferenceManager.getMySharedPreferences()!!.setMobileNumber(basicModel.data.number!!)
                            SharedPreferenceManager.getMySharedPreferences()!!.setAuthorizationToken(basicModel.data.tokens!![0].token!!)
                            SharedPreferenceManager.getMySharedPreferences()!!.setDistance(basicModel.data.preferences!!.distance_in!!)

                            if (basicModel.data.images!!.isNotEmpty()) {
                                SharedPreferenceManager.getMySharedPreferences()!!.setProfileImage(basicModel.data.base_url + basicModel.data.images[0].image)
                            }

                            if (basicModel.data.email != null) {
                                SharedPreferenceManager.getMySharedPreferences()!!.setEmail(basicModel.data.email!!)
                            }
                            finish()
                            AppUtils.overridePendingTransitionExit(activity)
                        } else if (Objects.requireNonNull(basicModel).code == "-1") {
                            callLogoutFunction()
                        } else {
                            showSnackBar(activity, basicModel.message)
                        }
                    } else {
                        showSnackBar(activity, response.message())
                    }
                }

                override fun onFailure(call: Call<VerifyModelResponse>?, t: Throwable?) {
                    Log.e("callError", call.toString())
                    Log.e("Error", t.toString())
                    AppUtils.hideProgressDialog()
                    if (t is SocketTimeoutException) {
                        showSnackBar(activity, getString(R.string.connection_timeout))
                    } else if (t is NetworkErrorException) {
                        showSnackBar(activity, getString(R.string.network_error))
                    } else if (t is AuthenticatorException) {
                        showSnackBar(activity, getString(R.string.authentiation_error))
                    } else {
                        showSnackBar(activity, getString(R.string.something_went_wrong))
                    }
                }
            })
        } else {
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }
    }


    private fun getUpdatedImageList() {

        for (i in profileImagelist.indices) {
            if (profileImagelist[i].is_new == "Y") {
                imageFilelist.add(compressImage(profileImagelist[i].new_image))
            }
        }
    }

    fun EditText.showKeyboard() {
        post {
            requestFocus()
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
        }
    }

    private fun setProfessionaEnableFalse() {
        etProfession.isFocusable = false;
        etProfession.isFocusableInTouchMode = false; // user touches widget on phone with touch screen
        etProfession.isClickable = false;
    }

    override fun addImageSelection(position: Int) {
        isFrom = "multiple"
        selectImage(position, isFrom)
    }

    public fun selectImage(position: Int, paramIsFrom: String) {
        clickedPosition = position;

        //Open bottom sheet dialog
        val dialog = BottomSheetDialog(this)
        val bottomSheet = layoutInflater.inflate(R.layout.bottom_sheet_picture_profile, null)

        if (paramIsFrom == "profile") {
            if (mainProfilePicturePath == "") {
                bottomSheet.tvTakePhoto.visibility = View.VISIBLE
                bottomSheet.tvGallery.visibility = View.VISIBLE
                bottomSheet.tvRemove.visibility = View.GONE
            } else {
                checkImagelistBlank()
                if (isAllImageBlank == false) {
                    bottomSheet.tvTakePhoto.visibility = View.VISIBLE
                    bottomSheet.tvGallery.visibility = View.VISIBLE
                } else {
                    bottomSheet.tvTakePhoto.visibility = View.GONE
                    bottomSheet.tvGallery.visibility = View.GONE
                }
                bottomSheet.tvRemove.visibility = View.VISIBLE
            }
            //bottomSheet.tvRemove.visibility = View.GONE
            bottomSheet.tvProfilePic.visibility = View.GONE
        } else {

            if (profileImagelist[clickedPosition].is_new == "") {
                bottomSheet.tvTakePhoto.visibility = View.VISIBLE
                bottomSheet.tvGallery.visibility = View.VISIBLE
            } else {
                bottomSheet.tvTakePhoto.visibility = View.GONE
                bottomSheet.tvGallery.visibility = View.GONE
            }

            bottomSheet.tvProfilePic.visibility = View.GONE
            if (profileImagelist[clickedPosition].is_new == "") {
                bottomSheet.tvRemove.visibility = View.GONE

            } else {
                if (profileImagelist[clickedPosition].is_new == "") {
                    bottomSheet.tvRemove.visibility = View.GONE
                    bottomSheet.tvProfilePic.visibility = View.GONE
                } else {
                    bottomSheet.tvRemove.visibility = View.VISIBLE
                    bottomSheet.tvProfilePic.visibility = View.VISIBLE
                }
                // bottomSheet.tvRemove.visibility = View.VISIBLE
                //  bottomSheet.tvProfilePic.visibility = View.VISIBLE
            }

            if (position == 0) {
                bottomSheet.tvProfilePic.visibility = View.GONE
            }

        }

        bottomSheet.tvTakePhoto.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                userChoosenTask = getString(R.string.take_photo)
                checkWritePermission()
                dialog.dismiss()

            } else {
                CaptureImage()
                dialog.dismiss()

            }
        }

        bottomSheet.tvGallery.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                userChoosenTask = getString(R.string.choose_library)
                checkWritePermission()
                dialog.dismiss()
            } else {
                PickGalleryImage()
                dialog.dismiss()
            }
            dialog.dismiss()
        }

        bottomSheet.tvCancel.setOnClickListener {
            dialog.dismiss()
        }

        bottomSheet.tvProfilePic.setOnClickListener {
            Collections.swap(profileImagelist, clickedPosition, 0)  //swipe image with profile picture
            setProfileImageAdapter()
            dialog.dismiss()
        }

        bottomSheet.tvRemove.setOnClickListener {

            if (profileImagelist[clickedPosition].is_new == "N") {
                //add it in remove list
                val fileName: String = profileImagelist[clickedPosition].new_image.substring(profileImagelist[clickedPosition].new_image.lastIndexOf("/") + 1)
                removeImagelist.add(profileImagelist[clickedPosition]._id)
                Log.e("path", fileName)
                removeParticularImageAndSetAdapter(clickedPosition)
            } else {
                removeParticularImageAndSetAdapter(clickedPosition)
            }
            setDataAfterRemovingUsingSwipe()   //after data remove set data according it
            dialog.dismiss()


        }

        dialog.setContentView(bottomSheet)
        dialog.show()
    }

    private fun setDataAfterRemovingUsingSwipe() {
        //first take whole imagelist in previous image
        var previousimagelist: ArrayList<ImageModel> = ArrayList()

        for (i in profileImagelist.indices) {
            if (profileImagelist[i].is_new != "") {
                val imageModel: ImageModel = ImageModel()
                imageModel.is_new = profileImagelist[i].is_new
                imageModel.new_image = profileImagelist[i].new_image
                imageModel._id = profileImagelist[i]._id
                imageModel.is_selected = profileImagelist[i].is_selected
                imageModel.order_id = profileImagelist[i].order_id

                previousimagelist.add(imageModel)
            }
        }


        profileImagelist.clear() //clear image list
        //now add previous imagelist in main imagelist
        for (i in previousimagelist.indices) {
            val imageModel: ImageModel = ImageModel()
            imageModel.is_new = previousimagelist[i].is_new
            imageModel.new_image = previousimagelist[i].new_image
            imageModel._id = previousimagelist[i]._id
            imageModel.is_selected = previousimagelist[i].is_selected
            imageModel.order_id = previousimagelist[i].order_id
            profileImagelist.add(imageModel)
        }
        //now add another box with blank image
        for (i in profileImagelist.size..8) { //now add left box in list and set adapter
            val imageModel: ImageModel = ImageModel()
            imageModel.is_new = ""
            imageModel.new_image = ""
            imageModel._id = ""
            imageModel.is_selected = false
            imageModel.order_id = ""
            profileImagelist.add(imageModel)
        }
        setProfileImageAdapter()
        previousimagelist.clear()
    }


    private fun removeParticularImageAndSetAdapter(clickedPosition: Int) {
        val imageModel: ImageModel = ImageModel()
        imageModel.is_new = ""
        imageModel._id = ""
        imageModel.order_id = ""
        imageModel.is_selected = false
        imageModel.new_image = ""
        profileImagelist[clickedPosition] = imageModel
        setProfileImageAdapter()  //set adapter as image
    }


    private fun callAPIRemoveImage(removeImagePath: String) {
        if (AppUtils.isConnectedToInternet(activity)) {
            AppUtils.showProgressDialog(activity)
            val params = HashMap<String, String?>()
            params["image"] = removeImagePath

            Log.e("param", params.toString())
            val call: Call<GeneralModel>
            call = RetrofitRestClient.instance!!.removeImage(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, params)
            Log.e("token", SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
            call.enqueue(object : Callback<GeneralModel> {
                @SuppressLint("SetTextI18n", "ShowToast")
                override fun onResponse(call: Call<GeneralModel>?, response: Response<GeneralModel>?) {
                    AppUtils.hideProgressDialog()
                    val basicModel: GeneralModel
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {
                            removeImagelist.clear()
                            callAPIUpdateProfile()
                        } else if (Objects.requireNonNull(basicModel).code == "-1") {
                            callLogoutFunction()
                        } else {
                            showSnackBar(activity, basicModel.message)
                        }
                    } else {
                        showSnackBar(activity, response.message())
                    }
                }

                override fun onFailure(call: Call<GeneralModel>?, t: Throwable?) {
                    AppUtils.hideProgressDialog()
                    if (t is SocketTimeoutException) {
                        showSnackBar(activity, getString(R.string.connection_timeout))
                    } else if (t is NetworkErrorException) {
                        showSnackBar(activity, getString(R.string.network_error))
                    } else if (t is AuthenticatorException) {

                        showSnackBar(activity, getString(R.string.authentiation_error))
                    } else {
                        showSnackBar(activity, getString(R.string.something_went_wrong))
                    }
                }
            })
        } else {
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }

    }

    fun checkWritePermission(): Boolean {
        return if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity!!,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(activity!!, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        MY_PERMISSIONS_REQUEST_WRITE_STORAGE)
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(activity!!, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        MY_PERMISSIONS_REQUEST_WRITE_STORAGE)
            }
            false
        } else {
            checkStoragePermission()
            true
        }
    }

    fun checkStoragePermission(): Boolean {
        return if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity!!,
                            Manifest.permission.READ_EXTERNAL_STORAGE)) {
                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(activity!!, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                        MY_PERMISSIONS_REQUEST_STORAGE)
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(activity!!, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                        MY_PERMISSIONS_REQUEST_STORAGE)
            }
            false
        } else {
            when (userChoosenTask) {
                getString(R.string.take_photo) -> checkCameraPermission()
                getString(R.string.choose_library) -> PickGalleryImage()
            }
            true
        }
    }

    /**
     * Runtime permission camera
     *
     * @return
     */
    fun checkCameraPermission(): Boolean {
        return if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                            Manifest.permission.CAMERA)) {
                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.CAMERA),
                        MY_PERMISSIONS_REQUEST_CAMERA)
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.CAMERA),
                        MY_PERMISSIONS_REQUEST_CAMERA)
            }
            false
        } else {
            when (userChoosenTask) {
                getString(R.string.take_photo) -> CaptureImage()
            }
            true
        }
    }

    /**
     * capture image from camera
     */
    open fun CaptureImage() {
        try {
            cameraPhoto = CameraPhoto(this)
            startActivityForResult(cameraPhoto!!.takePhotoIntent(), PICK_CAMERA_REQUEST)
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    /**
     * pick image from gallery
     */
    private fun PickGalleryImage() {

        if (isFrom == "profile") {
            val i = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            startActivityForResult(i, PICK_IMAGE_REQUEST)
        }
        if (isFrom == "multiple") {
            var k = 0
            for (i in profileImagelist.indices) {
                if (profileImagelist[i].is_new == "") {
                    k++
                }
                MAX_ATTACHMENT_COUNT = k
            }

            if ((photoPaths.size) == MAX_ATTACHMENT_COUNT) {
                Toast.makeText(this, "Cannot select more than " + MAX_ATTACHMENT_COUNT + " items",
                        Toast.LENGTH_SHORT).show();
            } else {
                photoPaths.clear();

                FilePickerBuilder.instance
                        .setMaxCount(MAX_ATTACHMENT_COUNT)
                        //.setSelectedFiles(photoPaths)
                        .setActivityTheme(R.style.FilePickerTheme)
                        .setActivityTitle("Please select media")
                        .enableVideoPicker(false)
                        .enableCameraSupport(false)
                        .showGifs(false)
                        .showFolderView(true)
                        .enableSelectAll(false)
                        .enableImagePicker(true)
                        .withOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                        .pickPhoto(this, PICK_IMAGE_REQUEST);
            }
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_WRITE_STORAGE -> {
                run {
                    // If request is cancelled, the result arrays are empty.
                    if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        // permission was granted. Do the
                        // contacts-related task you need to do.
                        if (ContextCompat.checkSelfPermission(activity!!, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                checkStoragePermission()
                            }
                        }
                    } else {
                        var i = 0
                        val len = permissions.size
                        while (i < len) {
                            if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                                // user rejected the permission
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    // Permission denied, Disable the functionality that depends on this permission.
                                    Toast.makeText(activity, "Permission Denied", Toast.LENGTH_LONG).show()
                                    AlertDialog.Builder(activity).setMessage("Approve All Permission")
                                            .setPositiveButton("Ok") { dialog, which ->
                                                //checkWritePermission();
                                                dialog.dismiss()
                                            }.setNegativeButton("Setting") { dialog, which ->
                                                val intent = Intent()
                                                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                                val uri = Uri.fromParts("package", packageName, null)
                                                intent.data = uri
                                                startActivity(intent)
                                                dialog.dismiss()
                                            }.show()
                                }
                            }
                            i++
                        }
                    }
                }
                return
            }
            MY_PERMISSIONS_REQUEST_STORAGE -> {
                run {
                    // If request is cancelled, the result arrays are empty.
                    if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        // permission was granted. Do the
                        // contacts-related task you need to do.
                        if (ContextCompat.checkSelfPermission(activity!!, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                when (userChoosenTask) {
                                    getString(R.string.take_photo) -> checkCameraPermission()
                                    getString(R.string.choose_library) -> PickGalleryImage()
                                }
                            }
                        }
                    } else {
                        var i = 0
                        val len = permissions.size
                        while (i < len) {
                            if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                                // user rejected the permission
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    // Permission denied, Disable the functionality that depends on this permission.
                                    Toast.makeText(activity, "Permission Denied", Toast.LENGTH_LONG).show()
                                    AlertDialog.Builder(activity).setMessage("Approve All Permission")
                                            .setPositiveButton("Ok") { dialog, which -> dialog.dismiss() }
                                            .setNegativeButton("Setting") { dialog, which ->
                                                val intent = Intent()
                                                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                                val uri = Uri.fromParts("package", packageName, null)
                                                intent.data = uri
                                                startActivity(intent)
                                                dialog.dismiss()
                                            }.show()
                                }
                            }
                            i++
                        }
                    }
                }
                return
            }
            MY_PERMISSIONS_REQUEST_CAMERA -> {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted. Do the
                    // contacts-related task you need to do.
                    if (ContextCompat.checkSelfPermission(activity!!, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (userChoosenTask == getString(R.string.take_photo)) {
                                CaptureImage()
                            }
                        }
                    }
                } else {
                    var i = 0
                    val len = permissions.size
                    while (i < len) {
                        if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                            // user rejected the permission
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                // Permission denied, Disable the functionality that depends on this permission.
                                Toast.makeText(activity, "Permission Denied", Toast.LENGTH_LONG).show()
                                AlertDialog.Builder(activity).setMessage("Approve All Permission")
                                        .setPositiveButton("Ok") { dialog, which ->
                                            //checkCameraPermission();
                                            dialog.dismiss()
                                        }.setNegativeButton("Setting") { dialog, which ->
                                            val intent = Intent()
                                            intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                            val uri = Uri.fromParts("package", packageName, null)
                                            intent.data = uri
                                            startActivity(intent)
                                            dialog.dismiss()
                                        }.show()
                            }
                        }
                        i++
                    }
                }
                return
            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_CAMERA_REQUEST && resultCode == RESULT_OK) {
            try {
                cameraPhoto?.addToGallery()
                picturePath = cameraPhoto?.photoPath!!

                if (isFrom == "profile") { //  for main profile picture
                    /* if (mainProfilePicturePath_isNew == "N" || mainProfilePicturePath_isNew == "") {
                         if (mainProfilePicturePath != "") {
                             val fileName: String = mainProfilePicturePath.substring(mainProfilePicturePath.lastIndexOf("/") + 1)
                             removeImagelist.add(fileName)
                             Log.e("path", fileName)
                             Log.e("call", "remove4")
                         }
                         // callAPIRemoveImage(fileName)
                     }
                     setMainProfilePicture(picturePath)*/
                } else {

                    if (profileImagelist[clickedPosition].is_new == "N") {
                        val fileName: String = profileImagelist[clickedPosition].new_image.substring(profileImagelist[clickedPosition].new_image.lastIndexOf("/") + 1)
                        removeImagelist.add(profileImagelist[clickedPosition]._id)
                        // callAPIRemoveImage(fileName)
                        Log.e("path", fileName)
                        Log.e("call", "remove5")
                    }

                    val imageModel: ImageModel = ImageModel()
                    imageModel.is_new = "Y"
                    imageModel.new_image = picturePath
                    imageModel.is_selected = false
                    imageModel.order_id = ""
                    imageModel._id = ""
                    profileImagelist[clickedPosition] = imageModel
                    setProfileImageAdapter()
                }

            } catch (e: Exception) {
            }

        } else if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK /*&& data != null && data.data != null*/) {

            if (isFrom == "profile") {
                /* picturePath = getPicturePath(data!!.data!!)
                 //  for main profile picture

                 if (mainProfilePicturePath_isNew == "N" || mainProfilePicturePath_isNew == "") {
                     if (mainProfilePicturePath != "") {
                         val fileName: String = mainProfilePicturePath.substring(mainProfilePicturePath.lastIndexOf("/") + 1)
                         removeImagelist.add(fileName)
                         //callAPIRemoveImage(fileName)
                         Log.e("path", fileName)
                         Log.e("call", "remove6")
                     }
                 }
                 setMainProfilePicture(picturePath)*/
            } else {

                if (clickedPosition != -1) {
                    if (profileImagelist[clickedPosition].is_new == "N") {
                        val fileName: String = profileImagelist[clickedPosition].new_image.substring(profileImagelist[clickedPosition].new_image.lastIndexOf("/") + 1)
                        removeImagelist.add(profileImagelist[clickedPosition]._id)
                        Log.e("path", fileName)
                        Log.e("call", "remove7")
                        //callAPIRemoveImage(fileName)
                    }

                    /* //for select single image
                     val imageModel: ImageModel = ImageModel()
                     imageModel.is_new = "Y"
                     imageModel.old_image = ""
                     imageModel.new_image = picturePath
                     profileImagelist[clickedPosition] = imageModel*/

                    photoPaths.addAll(data!!.getParcelableArrayListExtra<Uri>(FilePickerConst.KEY_SELECTED_MEDIA)!!);

                    /* val path = ContentUriUtils.getFilePath(activity, photoPaths.get(0));*/
                    if (photoPaths.size > 0) {
                        setMultipleImageinAdapter()
                        photoPaths.clear();
                    } else {
                        showSnackBar(activity, getString(R.string.you_have_not_picked_any_pick))
                    }

                } else {
                    showSnackBar(activity, getString(R.string.something_went_wrong))
                }

            }

        } else {
            // showSnackBar(activity, getString(R.string.you_have_not_picked_any_pick))
        }
        if (clickEvent == "FB") {
            callbackManager!!.onActivityResult(requestCode, resultCode, data)
        }

    }

    private fun setMultipleImageinAdapter() {
        //first take whole imagelist in previous image
        var previousimagelist: ArrayList<ImageModel> = ArrayList()

        for (i in profileImagelist.indices) {
            if (profileImagelist[i].is_new != "") {
                val imageModel: ImageModel = ImageModel()
                imageModel.is_new = profileImagelist[i].is_new
                imageModel.new_image = profileImagelist[i].new_image
                imageModel.is_selected = profileImagelist[i].is_selected
                imageModel.order_id = profileImagelist[i].order_id
                imageModel._id = profileImagelist[i]._id


                previousimagelist.add(imageModel)

            }
        }
        profileImagelist.clear() //clear image list

        //now add previous imagelist in main imagelist
        for (i in previousimagelist.indices) {

            val imageModel: ImageModel = ImageModel()
            imageModel.is_new = previousimagelist[i].is_new
            imageModel.new_image = previousimagelist[i].new_image
            imageModel.is_selected = previousimagelist[i].is_selected
            imageModel.order_id = previousimagelist[i].order_id
            imageModel._id = previousimagelist[i]._id

            profileImagelist.add(imageModel)
        }

        //now add photopath in imagelist
        for (i in photoPaths.indices) {
            val imageModel: ImageModel = ImageModel()
            imageModel.is_new = "Y"
            imageModel.new_image = ContentUriUtils.getFilePath(activity, photoPaths.get(i))!!
            imageModel.is_selected = false
            imageModel.order_id = ""
            imageModel._id = ""
            profileImagelist.add(imageModel)

        }

        //now add another box with blank image
        for (i in profileImagelist.size..8) { //now add left box in list and set adapter
            val imageModel: ImageModel = ImageModel()
            imageModel.is_new = ""
            imageModel.new_image = ""
            imageModel.is_selected = false
            imageModel.order_id = ""
            imageModel._id = ""
            profileImagelist.add(imageModel)

        }

        setProfileImageAdapter()
        previousimagelist.clear()
    }

    private fun setMainProfilePicture(path: String) {
        mainProfilePicturePath_isNew = "Y"
        mainProfilePicturePath = path  // set main path
        Glide.with(activity).load(mainProfilePicturePath).placeholder(R.drawable.ic_placeholder_icn).into(ivUserProfile)
    }

    @SuppressLint("SetTextI18n")
    override fun onResume() {
        super.onResume()
        ivEditOrigin.isEnabled = true
        ivEditLocation.isEnabled = true
        tvProfileLocation.text = paramProfileLocation
        setOriginData()
        isWebviewOpen = false
    }

    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        return false
    }

    override fun onScrollChanged() {
        var view: View = hsView.getChildAt(hsView.getChildCount() - 1)
        var topDetector: Int = hsView.scrollX
        var bottomDetector: Int = hsView.scrollY
        if (bottomDetector == 0) {
            ivDot.setImageResource(R.drawable.ic_dot_2)
            // Toast.makeText(baseContext, "Scroll View bottom reached", Toast.LENGTH_SHORT).show()
        }
        if (topDetector <= 0) {
            ivDot.setImageResource(R.drawable.ic_dot_1)
            // Toast.makeText(baseContext, "Scroll View top reached", Toast.LENGTH_SHORT).show()
        }
    }


    override fun onBackPressed() {

        if (!isWebviewOpen) {
            checkImagelistBlank()
            if (/*mainProfilePicturePath == "" && */isAllImageBlank == false) {
                showSnackBar(activity, getString(R.string.please_select_picture))
            } else {
                callUpdateAllView()
            }
        } else {

            /*  if (mWebView != null && mWebView!!.canGoBack()) {
                  mWebView!!.goBack();// if there is previous page open it
              } else {*/
            llWebviewProfile.visibility = View.GONE
            isWebviewOpen = false
            /* }*/


        }


    }

    private fun checkImagelistBlank() {
        isAllImageBlank = false
        for (i in profileImagelist.indices) {
            if (profileImagelist[i].is_new != "") {
                isAllImageBlank = true
            }
        }
    }

    @AfterPermissionGranted(RC_STORAGE_PERMISSION)
    public fun methodRequiresStoragePermission() {
        val perms = arrayOf<String>(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (EasyPermissions.hasPermissions(this, *perms)) {
            // Already have permission, do the thing
            // ...
            if (clickEvent == "FB") {
                callFacebookLogin()  //call FB Implementattion
            }
            if (clickEvent == "INSTAGRAM") {
                getInastagramWebview()
            }
        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.rationale_call), RC_STORAGE_PERMISSION, *perms)
            // Do not have permissions, request them now
        }
    }

    private fun callFacebookLogin() {
        android.webkit.CookieManager.getInstance().removeAllCookie();
        //permission granted
        LoginManager.getInstance().logOut()  // for facebook logout
        login_button_profile_fb.performClick()  //for fb login button click
    }

    private fun initFbInfo() {
        aQuery = AQuery(activity);
        callbackManager = CallbackManager.Factory.create()

        accessTokenTracker = object : AccessTokenTracker() {
            override fun onCurrentAccessTokenChanged(oldToken: AccessToken?, newToken: AccessToken?) {}
        }
        profileTracker = object : ProfileTracker() {
            override fun onCurrentProfileChanged(oldProfile: Profile?, newProfile: Profile?) {}
        }
        accessTokenTracker!!.startTracking()
        profileTracker!!.startTracking()

        //add every permission here for add in fb user account permisision else not get data for test and other user
        login_button_profile_fb.setReadPermissions(Arrays.asList("public_profile", "email"/*, "user_birthday", "user_friends"*/, "user_photos"))
        login_button_profile_fb.registerCallback(callbackManager, callback)
    }

    /* @SuppressLint("SetJavaScriptEnabled")*/
    public fun getInastagramWebview() {
        isWebviewOpen = true
        android.webkit.CookieManager.getInstance().removeAllCookie();

        llWebviewProfile.visibility = View.VISIBLE

        //for get user id and accesstoken of instagram
        val REQUEST_URL =
                "https://www.instagram.com/oauth/authorize?app_id=" + getString(R.string.instagram_id) + "&redirect_uri=" + getString(R.string.callback_url) + "&scope=user_profile,user_media&response_type=code"
        // Log.e("URl",REQUEST_URL)
        mWebView?.settings!!.useWideViewPort = true
        mWebView?.settings!!.loadWithOverviewMode = true
        mWebView?.settings!!.setSupportZoom(true)
        mWebView?.settings!!.builtInZoomControls = true
        mWebView?.settings!!.displayZoomControls = true
        mWebView?.setPadding(0, 0, 0, 0)
        mWebView?.clearCache(true);
        mWebView?.clearHistory()
        mWebView?.settings!!.savePassword = false;
        mWebView?.settings!!.saveFormData = false;

        //Log.e("webview", "${Utility.getScale(activity!!)}")
        try {
            mWebView?.setInitialScale(30)
            mWebView?.scrollTo(0, height!! * resources!!.displayMetrics.density.toInt())
        } catch (e: Exception) {
        }
        try {
            mWebView?.settings!!.javaScriptEnabled = true
        } catch (e: Exception) {
        }
        mWebView?.loadUrl(REQUEST_URL)
        mWebView?.webViewClient = mWebViewClient
    }

    private val mWebViewClient = object : WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
            if (url?.startsWith(getString(R.string.callback_url))!!) {
                return true
            }
            return false
        }

        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
            if (URLDecoder.decode(url, "UTF-8")?.contains("code=")!!) {
                val uri = Uri.parse(URLDecoder.decode(url, "UTF-8"))
                var access_token = uri.toString().substring(uri.toString().indexOf("code=") + 5, uri.toString().indexOf("#"))

                onTokenReceived(access_token)  //get insta auth token

                height = view!!.contentHeight
            }
        }
    }

    override fun onTokenReceived(auth_token: String) {
        Log.e("auth_token", auth_token)
        getInstagramUserId_Accesstoken(auth_token)   //API for get insta user_id and access token
        //getAccessToken(auth_token!!)

    }


    private fun getInstagramUserId_Accesstoken(authToken: String) {
        AppUtils.showProgressDialog(activity)

        val apiInterface: ApiInterface = APIClient.getClientInstaToken()!!.create(ApiInterface::class.java)
        val params = java.util.HashMap<String, String?>()

        params["client_id"] = getString(R.string.instagram_id)
        params["client_secret"] = getString(R.string.instagram_app_secret)
        params["grant_type"] = "authorization_code"
        params["redirect_uri"] = getString(R.string.callback_url)
        params["code"] = authToken

        Log.e("param", params.toString())

        val call: Call<InstagramAccessToken> = apiInterface.getInstagramAceesToken(params)
        call.enqueue(object : Callback<InstagramAccessToken?> {
            override fun onResponse(call: Call<InstagramAccessToken?>, response: Response<InstagramAccessToken?>) {
                AppUtils.hideProgressDialog()
                var resultDistance: InstagramAccessToken? = response.body()
                if (response.isSuccessful) {
                    resultDistance = response.body()
                    Log.e("id", resultDistance!!.user_id + "acces_token" + resultDistance.access_token)
                    instaUserId = resultDistance.user_id
                    var instaUserAccessToken = resultDistance.access_token

                    getInstagramImageDetailUser(instaUserAccessToken, instaUserId!!)  //API for get instagram user detail

                } else {
                    if (response.message().equals("")) {
                        llWebview.visibility = View.GONE
                    } else {
                        showSnackBar(activity, getString(R.string.something_went_wrong))
                    }

                }
            }

            override fun onFailure(call: Call<InstagramAccessToken?>, t: Throwable) {
                Log.e("errorGot", t.toString())
                AppUtils.hideProgressDialog()
                showSnackBar(activity, getString(R.string.something_went_wrong))
                call.cancel()
            }
        })
    }

    private fun getInstagramImageDetailUser(instaUserAccessToken: String, instaUserId: String) {
        val apiInterface: ApiInterface = APIClient.getClientUser()!!.create(ApiInterface::class.java)
        AppUtils.showProgressDialog(activity)//profile_picture
        val call: Call<InstaMediaResponse> = apiInterface.getInstaMedia(instaUserAccessToken, "id,media_type,media_url", afterInstaCursor)
        Log.e("access_token", instaUserAccessToken)
        Log.e("fields", "id,media_type,media_url")
        call.enqueue(object : Callback<InstaMediaResponse?> {
            override fun onResponse(call: Call<InstaMediaResponse?>, response: Response<InstaMediaResponse?>) {
                var resultDistance: InstaMediaResponse? = response.body()
                AppUtils.hideProgressDialog()
                if (response.isSuccessful) {

                    resultDistance = response.body()

                    if (resultDistance!!.data!!.size > 0) {
                        for (i in resultDistance!!.data!!.indices) {
                            if (resultDistance.data!![i].mediaType == "IMAGE") {
                                if (!instaMedialist.contains(resultDistance!!.data!![i].mediaUrl)) {
                                    if (instaMedialist.size < 20) {
                                        instaMedialist.add(resultDistance!!.data!![i].mediaUrl!!)
                                    }

                                }

                            }
                        }
                        Log.e("imageMedia", instaMedialist.size.toString())

                        afterInstaCursor = resultDistance!!.paging!!.cursors!!.after
                        if (instaMedialist.size < 20) {
                            getInstagramImageDetailUser(instaUserAccessToken, instaUserId!!)
                        } else {
                            setInstagramMediaAdapterAndSetView()
                        }


                    }

                    if (resultDistance.data!!.size == 0) {
                        setInstagramMediaAdapterAndSetView()

                    }
                    Log.e("GetResponse", afterInstaCursor + "respose" + resultDistance.toString())
                } else {
                    AppUtils.hideProgressDialog()
                    showSnackBar(activity, getString(R.string.something_went_wrong))
                }
            }

            override fun onFailure(call: Call<InstaMediaResponse?>, t: Throwable) {
                Log.e("errorGot", t.toString())
                AppUtils.hideProgressDialog()
                showSnackBar(activity, getString(R.string.something_went_wrong))
                call.cancel()
                AppUtils.hideProgressDialog()
            }
        })
    }

    private fun setInstagramMediaAdapterAndSetView() {
        paramInstaConnect = "1"
        AppUtils.hideProgressDialog()
        Toast.makeText(activity, getString(R.string.successfully_connected_with_insta), Toast.LENGTH_SHORT).show()
        llWebviewProfile.visibility = View.GONE
        ivInstagramProfileDisconnect.visibility = View.VISIBLE
        ivInstagramProfile.visibility = View.GONE

        showAllInstaMedia()
        Log.e("data", instaMedialist.toString())

        callAPIUpdateViewForSocialMedia()
    }

    private fun showAllInstaMedia() {
        instalist.clear()

        val G: Int = 6;
        val NG: Int = (instaMedialist.size + G - 1) / G;
        val chunks: List<List<String>> = instaMedialist.chunked(6)
        Log.e("chunk", chunks.toString())
        Log.e("chunk", chunks.size.toString())

        for (i in chunks.indices) {
            var instaModel: InstaModel = InstaModel()
            instaModel.path = ""
            instaModel.list = chunks[i]
            instalist.add(instaModel)
            Log.e("main_size", instalist[i].list.toString())
            AppUtils.hideProgressDialog()
        }

        if (instalist.size > 0) {
            llInsta.visibility = View.VISIBLE
        } else {
            llInsta.visibility = View.GONE
        }


        vpInsta!!.adapter = InstaAdapter(activity, instalist)
        dots_indicator_insta.setViewPager(vpInsta)
        AppUtils.hideProgressDialog()
    }

    //important  fb call back
    private var callback: FacebookCallback<LoginResult> = object : FacebookCallback<LoginResult> {
        override fun onSuccess(loginResult: LoginResult) {
            val request = GraphRequest.newMeRequest(loginResult.accessToken) { `object`, response ->
                Log.e("LoginActivity", response.toString())

                Log.e("token", loginResult.accessToken.token)
                // AppUtils.showProgressDialog(activity)
                // Application code
                try {
                    Log.e("tttttt", `object`.getString("id"))
                    val facebookId: String = `object`.getString("id")
                    LoginManager.getInstance().logOut()  // for facebook logout
                    getCallFacebookMedia(facebookId, loginResult.accessToken.token)
                    //Toast.makeText(activity, "Name: $fnm $lnm \nEmail:$mail \n" + "ID: $fid ", Toast.LENGTH_SHORT).show()

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
            val parameters: Bundle = Bundle()
            /*parameters.putString("fields", "id,first_name,last_name,email,birthday,gender,location")*/
            parameters.putString("fields", "id,first_name,last_name,email")
            request.parameters = parameters
            request.executeAsync()
        }

        override fun onCancel() {
        }

        override fun onError(error: FacebookException) {
            showSnackBar(activity, getString(R.string.something_went_wrong))
            Log.e("main_error", error.toString())
        }
    }

    private fun getCallFacebookMedia(facebookId: String, token: String?) {
        AppUtils.showProgressDialog(activity)//profile_picture

        val apiInterface: ApiInterface = APIClient.getClientFbUser()!!.create(ApiInterface::class.java)
        val call: Call<FbMediaResponse> = apiInterface.getFbUserMedia(facebookId, "images", token, "uploaded", afterFbCursor, "")
        Log.e("access_token", token!!)
        Log.e("fields", facebookId + "images")
        call.enqueue(object : Callback<FbMediaResponse?> {
            override fun onResponse(call: Call<FbMediaResponse?>, response: Response<FbMediaResponse?>) {
                var resultDistance: FbMediaResponse? = response.body()
                AppUtils.hideProgressDialog()
                if (response.isSuccessful) {

                    resultDistance = response.body()

                    if (resultDistance!!.data!!.size > 0) {
                        for (i in resultDistance.data!!.indices) {
                            if (!fbMedialist.contains(resultDistance.data!![i].images!![0].source.toString())) {
                                if (fbMedialist.size < 20) {
                                    fbMedialist.add(resultDistance.data!![i].images!![0].source!!)
                                }

                            }
                        }
                        Log.e("imageMedia", fbMedialist.size.toString())

                        if (resultDistance.data!!.isNotEmpty()) {
                            if (resultDistance.paging != null) {
                                afterFbCursor = resultDistance.paging!!.cursors!!.after!!
                                Log.e("size", resultDistance!!.data!!.size.toString() + resultDistance.paging!!.cursors!!.after!!.toString())

                                if (fbMedialist.size < 20) {
                                    getCallFacebookMedia(facebookId, token)
                                } else {
                                    setFbMediaAdapterAndSetView()
                                }
                            }
                        }
                    }

                    if (resultDistance.data!!.size == 0) {
                        setFbMediaAdapterAndSetView()
                    }
                    Log.e("GetResponse", afterInstaCursor + "respose" + resultDistance.toString())
                } else {
                    AppUtils.hideProgressDialog()
                    showSnackBar(activity, getString(R.string.something_went_wrong))
                }
            }

            override fun onFailure(call: Call<FbMediaResponse?>, t: Throwable) {
                Log.e("errorGot", t.toString())
                AppUtils.hideProgressDialog()
                showSnackBar(activity, getString(R.string.something_went_wrong))
                call.cancel()
                AppUtils.hideProgressDialog()
            }
        })
    }

    private fun setFbMediaAdapterAndSetView() {
        AppUtils.hideProgressDialog()
        Toast.makeText(activity, getString(R.string.successfully_connected_with_fb), Toast.LENGTH_SHORT).show()
        AppUtils.hideProgressDialog()

        paramFbConnect = "1"
        ivFbDisconnect.visibility = View.VISIBLE
        ivFbProfile.visibility = View.GONE
        showAllFbMedia()
        Log.e("data", instaMedialist.toString())
        AppUtils.hideProgressDialog()
        callAPIUpdateViewForSocialMedia()
    }

    private fun showAllFbMedia() {
        fblist.clear()
        val G: Int = 6;
        val NG: Int = (fbMedialist.size + G - 1) / G;
        val chunks: List<List<String>> = fbMedialist.chunked(6)
        Log.e("chunk", chunks.toString())
        Log.e("chunk", chunks.size.toString())

        for (i in chunks.indices) {
            var instaModel: InstaModel = InstaModel()
            instaModel.path = ""
            instaModel.list = chunks[i]
            fblist.add(instaModel)
            Log.e("main_size", fblist[i].list.toString())
            AppUtils.hideProgressDialog()
        }

        vpFb!!.adapter = InstaAdapter(activity, fblist)
        dots_indicator_fb.setViewPager(vpFb)
        AppUtils.hideProgressDialog()

        if (fblist.size > 0) {
            llFb.visibility = View.VISIBLE
        } else {
            llFb.visibility = View.GONE
        }
    }
}



