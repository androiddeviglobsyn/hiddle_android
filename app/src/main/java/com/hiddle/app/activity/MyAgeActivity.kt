package com.hiddle.app.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import com.hiddle.app.R
import com.hiddle.app.util.AppUtils
import kotlinx.android.synthetic.main.activity_gender_selection.*
import kotlinx.android.synthetic.main.activity_my_age.*
import kotlinx.android.synthetic.main.activity_my_age.ivBack
import kotlin.collections.ArrayList

class MyAgeActivity : BaseMainActivity(), View.OnClickListener {
    var paramAge: String = "18"
    var paramEmail: String = ""
    var paramUserName: String = ""
    private var isFromLogin: String = ""
    private var socialId: String = ""
    private var authorizationToken: String = ""
    private var paramGender: String = ""
    private var uploadImagelist: ArrayList<String> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_age)

        getIntentData()
        numberPickerAgeMethod()
        allClicklistener()
    }

    private fun getIntentData() {
        var i = intent
        if (i != null) {
            paramUserName = i.getStringExtra("paramUserName")!!
            paramEmail = i.getStringExtra("paramEmail")!!
            isFromLogin = i.getStringExtra("isFromLogin")!!
            socialId = i.getStringExtra("socialId")!!
            authorizationToken = i.getStringExtra("authorizationToken")!!
            paramGender = i.getStringExtra("paramGender")!!
            uploadImagelist = i.getStringArrayListExtra("uploadImagelist")!!
            Log.e("age_image", uploadImagelist.get(0))
            Log.e("age_name", paramUserName)
        }
    }

    private fun allClicklistener() {
        ivBack.setOnClickListener(this)
        llNextAge.setOnClickListener(this)
        llBackAge.setOnClickListener(this)
    }
    private fun numberPickerAgeMethod() {
        // OnValueChangeListener
        number_picker_age.setOnValueChangedListener { picker, oldVal, newVal ->
            paramAge = newVal.toString()
            //Toast.makeText(activity, newVal.toString(), Toast.LENGTH_SHORT).show()
        }
    }
    override fun onClick(view: View) {
        when (view.id) {
            R.id.ivBack -> {
                ivBack.isEnabled = false
                finish()
                AppUtils.overridePendingTransitionExit(activity)
            }
            R.id.llBackAge -> {
                llBackAge.isEnabled = false
                finish()
                AppUtils.overridePendingTransitionExit(activity)
            }
            R.id.llNextAge -> {
                llNextAge.isEnabled = false
                val i = Intent(activity, MyHeightActivity::class.java)
                i.putExtra("paramUserName", paramUserName)
                i.putExtra("paramEmail", paramEmail)
                i.putExtra("isFromLogin", isFromLogin)
                i.putExtra("socialId", socialId)
                i.putExtra("authorizationToken",authorizationToken)
                i.putExtra("paramGender",paramGender)
                i.putExtra("paramAge",paramAge)
                i.putStringArrayListExtra("uploadImagelist",uploadImagelist)
                startActivity(i)
                AppUtils.overridePendingTransitionEnterDown(activity)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        ivBack.isEnabled = true
        llBackAge.isEnabled = true
        llNextAge.isEnabled = true
    }

}
