package com.hiddle.app.activity

import android.accounts.AuthenticatorException
import android.accounts.NetworkErrorException
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View

import com.hiddle.app.R
import com.hiddle.app.countrypicker.CountryCodePicker

import com.hiddle.app.model.SendOTPModel
import com.hiddle.app.retrofit.RetrofitRestClient
import com.hiddle.app.util.AppUtils
import com.hiddle.app.util.SharedPreferenceManager
import kotlinx.android.synthetic.main.activity_login_using_mobile.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketTimeoutException
import java.util.*

class LoginUsingMobileActivity : BaseMainActivity(), CountryCodePicker.OnCountryChangeListener, View.OnClickListener {
    //variable
    var paramUserName: String = ""
    var paramEmail: String = ""
    private var countryCode: String = ""
    private var paramMobile: String = ""
    private var countryName: String? = null
    private var isFromLogin: String = ""
    private var socialId: String = ""
    private var profile_url: String = ""
    private var apiGetOTP = ""
    private var country_name_code = ""
    private var otpLimit: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_using_mobile)

        getIntentData()
        initCountryPicker() //init country picker and its method
        allClicklistener()

    }

    private fun getIntentData() {
        var i = intent
        if (i != null) {
            paramUserName = i.getStringExtra("paramUserName")!!
            paramEmail = i.getStringExtra("paramEmail")!!
            isFromLogin = i.getStringExtra("isFromLogin")!!
            socialId = i.getStringExtra("socialId")!!
            profile_url = i.getStringExtra("profile_url")!!
            country_name_code = i.getStringExtra("country_code")!!
            Log.e("code", country_name_code)
            //ccPicker.setDefaultCountryUsingNameCode(country_name_code)
        }
    }

    private fun allClicklistener() {
        btnSendCode.setOnClickListener(this)
        ivBack.setOnClickListener(this)
        llBackLogin.setOnClickListener(this)
    }

    private fun initCountryPicker() {
        ccPicker.setOnCountryChangeListener(this)
        //to set default country code as India
        //  ccPicker.setDefaultCountryUsingNameCode("CH")  //for set switzerland
        ccPicker.setCountryForNameCode(country_name_code)
    }

    override fun onCountrySelected() {
        countryCode = "+" + ccPicker.selectedCountryCode
        countryName = ccPicker.selectedCountryName
        Log.e("country_code", countryCode)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.ivBack -> {
                ivBack.isEnabled = false
                llBackLogin.isEnabled = false
                finish()
                AppUtils.overridePendingTransitionExit(activity)
            }
            R.id.llBackLogin -> {
                ivBack.isEnabled = false
                llBackLogin.isEnabled = false
                finish()
                AppUtils.overridePendingTransitionExit(activity)
            }
            R.id.btnSendCode -> {
                if (validate()) {
                    callSendOTPAPI()
                }
            }

        }
    }

    private fun callSendOTPAPI() {
        if (AppUtils.isConnectedToInternet(activity)) {
            AppUtils.showProgressDialog(activity)
            val params = HashMap<String, String?>()
            params["cc"] = countryCode
            params["number"] = paramMobile
            params["device_type"] = "android"
            if (SharedPreferenceManager.getMySharedPreferences()!!.getFcmToken() != null) {
                params["device_token"] = SharedPreferenceManager.getMySharedPreferences()!!.getFcmToken()
            } else {
                params["device_token"] = ""
            }
            Log.e("firebaseToke", SharedPreferenceManager.getMySharedPreferences()!!.getFcmToken()!!)

            Log.e("param", params.toString())
            val call: Call<SendOTPModel>
            call = RetrofitRestClient.instance!!.sendOTPApi(params)
            call.enqueue(object : Callback<SendOTPModel> {
                override fun onResponse(call: Call<SendOTPModel>?, response: Response<SendOTPModel>?) {
                    AppUtils.hideProgressDialog()
                    val basicModel: SendOTPModel
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {
                            apiGetOTP = basicModel.data!!.otp!!
                            otpLimit = basicModel.data.otpLimit!!
                            Log.e("OTP", apiGetOTP)
                            /* showSnackBar(activity, "OTP :" + apiGetOTP)*/
                            getRedirectToVerifyMobileScreen()
                        } else {
                            showSnackBar(activity, basicModel.message)
                        }
                    } else {
                        showSnackBar(activity, response.message())
                    }
                }

                override fun onFailure(call: Call<SendOTPModel>?, t: Throwable?) {
                    AppUtils.hideProgressDialog()
                    if (t is SocketTimeoutException) {
                        showSnackBar(activity, getString(R.string.connection_timeout))
                    } else if (t is NetworkErrorException) {
                        showSnackBar(activity, getString(R.string.network_error))
                    } else if (t is AuthenticatorException) {
                        showSnackBar(activity, getString(R.string.authentiation_error))
                    } else {
                        AppUtils.hideProgressDialog()
                        showSnackBar(activity, getString(R.string.something_went_wrong))
                    }
                }
            })
        } else {
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }
    }

    private fun getRedirectToVerifyMobileScreen() {
        val i = Intent(activity, VerificationCodeActivity::class.java)
        i.putExtra("countryCode", countryCode)
        i.putExtra("mobileNumber", paramMobile)
        i.putExtra("paramUserName", paramUserName)
        i.putExtra("paramEmail", paramEmail)
        i.putExtra("isFromLogin", isFromLogin)
        i.putExtra("socialId", socialId)
        i.putExtra("paramAPIOTP", apiGetOTP)
        i.putExtra("profile_url", profile_url)
        i.putExtra("otpLimit", otpLimit)
        startActivity(i)
        AppUtils.overridePendingTransitionEnter(activity)
    }

    private fun validate(): Boolean {
        paramMobile = etMobile.text.toString()
        if (countryCode == "") {
            showSnackBar(activity, getString(R.string.please_select_country_code))
            return false
        }
        if (paramMobile.isEmpty()) {
            showSnackBar(activity, getString(R.string.please_enter_mobile_number))
            return false
        }
        if (!AppUtils.isValidMobile(paramMobile)) {
            showSnackBar(activity, getString(R.string.please_enter_valid_mobile))
            return false
        }
        return true
    }

    override fun onResume() {
        super.onResume()
        ivBack.isEnabled = true
        llBackLogin.isEnabled = true
    }


}
