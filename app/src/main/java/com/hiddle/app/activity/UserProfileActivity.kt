package com.hiddle.app.activity

import android.Manifest
import android.accounts.AuthenticatorException
import android.accounts.NetworkErrorException
import android.annotation.SuppressLint
import android.app.ActivityOptions
import android.app.AlertDialog
import android.content.ContentValues
import android.content.Intent
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.os.Handler
import android.renderscript.Allocation
import android.renderscript.Element
import android.renderscript.RenderScript
import android.renderscript.ScriptIntrinsicBlur
import android.util.Log
import android.view.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.MarkerOptions
import com.hiddle.app.R
import com.hiddle.app.`interface`.ClickOnImage
import com.hiddle.app.adapter.InstaAdapter
import com.hiddle.app.adapter.ProfileOriginAdapter
import com.hiddle.app.adapter.SlidingProfileImage_Adapter
import com.hiddle.app.dialog.OtherMessageDialog
import com.hiddle.app.dialog.ReportDialog
import com.hiddle.app.dialog.UserDialog
import com.hiddle.app.fragment.HomeFragment
import com.hiddle.app.model.GeneralModel
import com.hiddle.app.model.InstaModel
import com.hiddle.app.model.LikeModel.LikeResponse
import com.hiddle.app.model.PersonModel.PersonResponse
import com.hiddle.app.model.UserModel.UserImageModel
import com.hiddle.app.retrofit.RetrofitRestClient
import com.hiddle.app.service.LocationTrack
import com.hiddle.app.util.AppUtilMethod.getDifferenecOfTwoDate
import com.hiddle.app.util.AppUtils
import com.hiddle.app.util.Constants
import com.hiddle.app.util.SharedPreferenceManager
import com.hiddle.app.util.SharedPreferenceManager.Companion.getMySharedPreferences
import kotlinx.android.synthetic.main.activity_edit_profile.*
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_user_profile.*
import kotlinx.android.synthetic.main.content_event.*
import kotlinx.android.synthetic.main.custom_dialog_other.*
import kotlinx.android.synthetic.main.custom_dialog_report.*
import kotlinx.android.synthetic.main.custom_dialog_report.tvOther
import kotlinx.android.synthetic.main.custom_dialog_user.*
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions
import pub.devrel.easypermissions.PermissionRequest
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.Serializable
import java.net.SocketTimeoutException
import java.text.DecimalFormat
import java.util.*
import kotlin.collections.ArrayList

class UserProfileActivity : BaseMainActivity(), View.OnTouchListener, ViewTreeObserver.OnScrollChangedListener, OnMapReadyCallback, View.OnClickListener, ClickOnImage {
    //FOR MAP
    companion object {   // for make static
        const val RC_ACCESSS_LOCATION: Int = 126
        const val RC_FINE_LOCATION: Int = 127

    }

    private var mMap: GoogleMap? = null
    var isFirstTime: Boolean = false
    var mapFragment: SupportMapFragment? = null
    var geocoder: Geocoder? = null
    var addresses: List<Address> = ArrayList()
    public var image_profile: String? = null

    //DIFFerent parameter
    var userId: String = ""
    var address: String = ""
    var paramOrigin: String = ""
    var paramCrossLatitude: String = ""
    var paramCrossLongitude: String = ""
    var paramProfileLatitude: String = ""
    var paramProfileLongitude: String = ""
    var paramName: String = ""
    var paramProfessioon: String = ""
    var paramAge: String = ""
    var paramHeight: String = ""
    var paramGender: String = ""
    var baseUrl: String = ""
    var paramDistance: String = ""
    var paramIsFrom: String = ""
    var userImage: String = ""
    var isFrom: String = ""
    var imageProfilelist: ArrayList<UserImageModel> = ArrayList()
    var originProfilelist: ArrayList<String> = ArrayList()
    var profileOriginAdapter: ProfileOriginAdapter? = null
    var locationTrack: LocationTrack? = null
    var userDialog: UserDialog? = null
    var reportDialog: ReportDialog? = null
    var otherMessageDialog: OtherMessageDialog? = null
    var paramCrossLongitude_current: String = ""
    var paramCrossLatitude_current: String = ""
    var isClickMessage: Boolean = false
    var instaMedialist: ArrayList<String> = ArrayList()
    var fbMedialist: ArrayList<String> = ArrayList()
    var paramFbConnect: String = "0"
    var paramInstaConnect: String = "0"

    var instalist: ArrayList<InstaModel> = ArrayList()
    var fblist: ArrayList<InstaModel> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_profile)

        getIntentData()
        allClicklistener()
        animationForFavorite()
        getPermissionforMapAndInitialization()  // permission

    }

    private fun animationForFavorite() {
        val aniRotate: Animation = AnimationUtils.loadAnimation(activity!!.applicationContext, R.anim.rotate_backward)
        aniRotate.repeatCount = Animation.INFINITE
        aniRotate.fillAfter = true;
        ivFavoriteProfile!!.startAnimation(aniRotate)
    }

    private fun allClicklistener() {
        //for scrollview method
        svProfile.setOnTouchListener(this);
        svProfile.viewTreeObserver.addOnScrollChangedListener(this);

        ivClose.setOnClickListener(this)
        ivCloseNoData.setOnClickListener(this)
        ivFavoriteProfile.setOnClickListener(this)
        ivRightProfile.setOnClickListener(this)
        ivCrossProfile.setOnClickListener(this)
        ivProfileMenu.setOnClickListener(this)
        ivMenu.setOnClickListener(this)

        tvBlockViewUser.setOnClickListener(this)
        tvReportViewUser.setOnClickListener(this)
        llMainLayout.setOnClickListener(this)


        vpProfileUser.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                svProfile.isEnabled = false
                svProfile.isNestedScrollingEnabled = false
                svProfile.isSmoothScrollingEnabled = false
                svProfile.isFocusable = false
                svProfile.isFocusableInTouchMode = false
                svProfile.isVerticalScrollBarEnabled = false

                svProfile.requestDisallowInterceptTouchEvent(true)
                //svProfile.getParent().requestDisallowInterceptTouchEvent(false);
                return v?.onTouchEvent(event) ?: true
            }
        })

        vpProfileUser.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(newState: Int) {
                if (newState == ViewPager.SCROLL_STATE_DRAGGING) {
                    // Prevent the ScrollView from intercepting this event now that the page is changing.
                    // When this drag ends, the ScrollView will start accepting touch events again.
                    svProfile.requestDisallowInterceptTouchEvent(true);
                }
            }

        })
    }

    private fun getIntentData() {
        var i = intent
        if (i != null) {
            userId = i.getStringExtra("userId")!!
            userImage = i.getStringExtra("userImage")!!
            //paramIsFrom =  i.getStringExtra("isFrom")
            isFrom = i.getStringExtra("isFrom").toString()
            if (userImage != null) {
                image_profile = userImage
                setProfileImage(userImage)
            }
        }
    }

    private fun setProfileImage(profile: String) {
        Glide.with(activity).load(profile).placeholder(R.drawable.ic_placeholder_icn).into(ivUserImage)
        Glide.with(activity).load(profile).placeholder(R.drawable.ic_placeholder_icn).into(ivSelectedImage)
    }

    override fun onResume() {
        super.onResume()
        ivClose.isEnabled = true
        ivCloseNoData.isEnabled = true
        isClickMessage = false
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.ivClose -> {
                ivClose.isEnabled = false
                finish()
                AppUtils.overridePendingTransitionExit(activity)
            }
            R.id.ivCloseNoData -> {
                ivCloseNoData.isEnabled = false
                finish()
                AppUtils.overridePendingTransitionExit(activity)
            }
            R.id.ivRightProfile -> {
                callAPILikeUser("like")
            }
            R.id.ivCrossProfile -> {
                callAPIDislikeUser("dislike")
            }
            R.id.ivFavoriteProfile -> {
                callSuperLikeAPI()
            }
            R.id.ivMenu -> {
                rlDialogViewUser.visibility = View.VISIBLE
            }
            R.id.ivProfileMenu -> {
                callDialogBlock()
            }
            R.id.tvBlockViewUser -> {
                callDialogBlockUserAndAPI()
                rlDialogViewUser.visibility = View.GONE
            }
            R.id.tvReportViewUser -> {
                callReportDialog()
                rlDialogViewUser.visibility = View.GONE
            }
            R.id.llMainLayout -> {
                rlDialogViewUser.visibility = View.GONE
            }

        }
    }

    private fun callBlurCrossIcon() {
        var final_Bitmap: Bitmap? = null
        final_Bitmap = BitmapFactory.decodeResource(activity.resources, R.drawable.ic_placeholder_icn);
        final_Bitmap = BlurImage(final_Bitmap);
        ivCircleBlur!!.setImageBitmap(final_Bitmap)

    }

    @SuppressLint("NewApi")
    fun BlurImage(input: Bitmap): Bitmap {
        try {
            val rsScript = RenderScript.create(applicationContext)
            val alloc = Allocation.createFromBitmap(rsScript, input)
            val blur = ScriptIntrinsicBlur.create(rsScript, Element.U8_4(rsScript))
            blur.setRadius(21F)
            blur.setInput(alloc)
            val result = Bitmap.createBitmap(input.width, input.height, Bitmap.Config.ARGB_8888)
            val outAlloc = Allocation.createFromBitmap(rsScript, result)
            blur.forEach(outAlloc)
            outAlloc.copyTo(result)
            rsScript.destroy()
            return result
        } catch (e: Exception) {
            // TODO: handle exception
            return input
        }
    }


    private fun callAPIDislikeUser(s: String) {
        if (AppUtils.isConnectedToInternet(activity)) {
            AppUtils.showProgressDialog(activity)
            val params = java.util.HashMap<String, String?>()
            params["to_id"] = userId

            Log.e("param", params.toString())

            val call: Call<LikeResponse>
            call = RetrofitRestClient.instance!!.callSkipUser(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, params)
            Log.e("token", SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
            call.enqueue(object : Callback<LikeResponse> {
                @SuppressLint("SetTextI18n", "ShowToast")
                override fun onResponse(call: Call<LikeResponse>?, response: Response<LikeResponse>?) {
                    val basicModel: LikeResponse
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {
                            HomeFragment.selectedAction = "dislike"
                            Toast.makeText(activity, basicModel.message, Toast.LENGTH_SHORT).show()
                            callFinishMethod()
                            //ivVerify!!.setImageResource(R.drawable.ic_cross_white_circle)
                        } else {
                            showSnackBar(activity, basicModel.message)
                        }
                    } else {
                        showSnackBar(activity, response.message())
                    }
                    AppUtils.hideProgressDialog()
                }

                override fun onFailure(call: Call<LikeResponse>?, t: Throwable?) {
                    AppUtils.hideProgressDialog()
                    Log.e("message3", "fail")
                    if (t is SocketTimeoutException) {
                        showSnackBar(activity, getString(R.string.connection_timeout))
                    } else if (t is NetworkErrorException) {
                        showSnackBar(activity, getString(R.string.network_error))
                    } else if (t is AuthenticatorException) {
                        showSnackBar(activity, getString(R.string.authentiation_error))
                    } else {
                        showSnackBar(activity, getString(R.string.something_went_wrong))
                    }
                }
            })
        } else {
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }
    }

    private fun callFinishMethod() {
        finish()
        AppUtils.overridePendingTransitionExit(activity)
    }

    private fun callAPILikeUser(like: String) {
        if (AppUtils.isConnectedToInternet(activity)) {
            AppUtils.showProgressDialog(activity)
            val params = java.util.HashMap<String, String?>()
            params["to_id"] = userId

            Log.e("param", params.toString())

            val call: Call<LikeResponse>
            call = RetrofitRestClient.instance!!.callLikeUser(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, params)
            Log.e("token", SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
            call.enqueue(object : Callback<LikeResponse> {
                @SuppressLint("SetTextI18n", "ShowToast")
                override fun onResponse(call: Call<LikeResponse>?, response: Response<LikeResponse>?) {
                    val basicModel: LikeResponse
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {
                            HomeFragment.selectedAction = "like"
                            Toast.makeText(activity, basicModel.message, Toast.LENGTH_SHORT).show()
                            //ivVerify.setImageResource(R.drawable.ic_white_tick_green_circle)
                            if (basicModel.data!!.match != null) {
                                RedirectToItsStrikeScreen(basicModel.data.match)
                            }
                        } else {
                            showSnackBar(activity, basicModel.message)
                        }
                    } else {
                        showSnackBar(activity, response.message())
                    }
                    AppUtils.hideProgressDialog()
                }

                override fun onFailure(call: Call<LikeResponse>?, t: Throwable?) {
                    AppUtils.hideProgressDialog()
                    Log.e("message3", "fail")
                    if (t is SocketTimeoutException) {
                        showSnackBar(activity, getString(R.string.connection_timeout))
                    } else if (t is NetworkErrorException) {
                        showSnackBar(activity, getString(R.string.network_error))
                    } else if (t is AuthenticatorException) {
                        showSnackBar(activity, getString(R.string.authentiation_error))
                    } else {
                        showSnackBar(activity, getString(R.string.something_went_wrong))
                    }
                }
            })
        } else {
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }

    }

    private fun RedirectToItsStrikeScreen(match: String?) {
        if (match == "1") {
            try {
                if (userImage != null) {
                    val options = ActivityOptions.makeSceneTransitionAnimation(activity)
                    val i = Intent(activity, StrikeActivity::class.java)
                    i.putExtra("image", image_profile)
                    Log.e("strike_image1", image_profile.toString())
                    i.putExtra(Constants.KEY_ANIM_TYPE, Constants.TransitionType.ExplodeJava)
                    i.putExtra("to_user_name", paramName)
                    i.putExtra("to_user_id", userId)
                    startActivity(i, options.toBundle())
                    finish()
                }
            } catch (e: Exception) {
            }
        } else {
            callFinishMethod()
        }
    }

    private fun callSuperLikeAPI() {
        if (AppUtils.isConnectedToInternet(activity)) {
            AppUtils.showProgressDialog(activity)
            val params = java.util.HashMap<String, String?>()
            params["to_id"] = userId

            Log.e("param", params.toString())

            val call: Call<LikeResponse>
            call = RetrofitRestClient.instance!!.callSuperLikeUser(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, params)
            Log.e("token", SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
            call.enqueue(object : Callback<LikeResponse> {
                @SuppressLint("SetTextI18n", "ShowToast")
                override fun onResponse(call: Call<LikeResponse>?, response: Response<LikeResponse>?) {
                    val basicModel: LikeResponse
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {
                            Toast.makeText(activity, basicModel.message, Toast.LENGTH_SHORT).show()

                            HomeFragment.selectedAction = "superLike"

                            // ivVerify!!.setImageResource(R.drawable.ic_superlike)
                            if (basicModel.data!!.match != null) {
                                RedirectToItsStrikeScreen(basicModel.data.match)
                            }
                        } else {
                            AppUtils.hideProgressDialog()
                            val diff: String = getDifferenecOfTwoDate(applicationContext!!, basicModel.data!!.last_premium_like_time!!)
                            val message = getString(R.string.you_are_able_another_premium_like) + "" + diff
                            setErrorDialog(message)
                            // showSnackBar(activity, basicModel.message)
                        }
                    } else {
                        showSnackBar(activity, response.message())
                    }
                    AppUtils.hideProgressDialog()
                }

                override fun onFailure(call: Call<LikeResponse>?, t: Throwable?) {
                    AppUtils.hideProgressDialog()
                    Log.e("message3", "fail")
                    if (t is SocketTimeoutException) {
                        showSnackBar(activity, getString(R.string.connection_timeout))
                    } else if (t is NetworkErrorException) {
                        showSnackBar(activity, getString(R.string.network_error))
                    } else if (t is AuthenticatorException) {
                        showSnackBar(activity, getString(R.string.authentiation_error))
                    } else {
                        showSnackBar(activity, getString(R.string.something_went_wrong))
                    }
                }
            })
        } else {
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }
    }

    private fun setErrorDialog(message: String?) {
        val builder1 = AlertDialog.Builder(ContextThemeWrapper(activity, R.style.AlertDialogCustom))
        builder1.setMessage(message)
        builder1.setCancelable(true)
        builder1.setPositiveButton(getString(R.string.ok)) { dialog, id ->
            AppUtils.hideProgressDialog()
            dialog.cancel()
        }
        val alert11 = builder1.create()
        alert11.show()
    }

    private fun callDialogBlock() {
        userDialog = UserDialog(activity)
        userDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        userDialog!!.show()

        userDialog!!.tvBlockUser.setOnClickListener(View.OnClickListener {
            userDialog!!.dismiss()
            callDialogBlockUserAndAPI()
        })
        userDialog!!.tvReportUser.setOnClickListener(View.OnClickListener {
            userDialog!!.dismiss()
            callReportDialog()
        })
    }

    private fun callDialogBlockUserAndAPI() {
        val builder1 = AlertDialog.Builder(ContextThemeWrapper(activity, R.style.AlertDialogCustom))
        builder1.setMessage(getString(R.string.are_you_sure_want_to_block))
        builder1.setCancelable(true)
        builder1.setPositiveButton(getString(R.string.yes)) { dialog, id ->
            callBlockUserAPI()  //call block user API
            dialog.cancel()
        }
        builder1.setNegativeButton(getString(R.string.no)) { dialog, id -> dialog.cancel() }
        val alert11 = builder1.create()
        alert11.show()
    }

    private fun callBlockUserAPI() {
        if (AppUtils.isConnectedToInternet(activity)) {
            AppUtils.showProgressDialog(activity)
            val params = HashMap<String, String?>()

            params["to_id"] = userId
            Log.e("param", params.toString())

            val call: Call<GeneralModel>
            call = RetrofitRestClient.instance!!.callBlockUser(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, params)
            Log.e("token", SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
            call.enqueue(object : Callback<GeneralModel> {
                @SuppressLint("SetTextI18n", "ShowToast")
                override fun onResponse(call: Call<GeneralModel>?, response: Response<GeneralModel>?) {
                    AppUtils.hideProgressDialog()
                    val basicModel: GeneralModel
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {
                            Toast.makeText(activity, basicModel.message, Toast.LENGTH_SHORT).show()
                            finish()
                            AppUtils.overridePendingTransitionExit(activity)
                        } else {
                            showSnackBar(activity, basicModel.message)
                        }
                    } else {
                        showSnackBar(activity, response.message())
                    }
                }

                override fun onFailure(call: Call<GeneralModel>?, t: Throwable?) {
                    AppUtils.hideProgressDialog()
                    if (t is SocketTimeoutException) {
                        showSnackBar(activity, getString(R.string.connection_timeout))
                    } else if (t is NetworkErrorException) {
                        showSnackBar(activity, getString(R.string.network_error))
                    } else if (t is AuthenticatorException) {
                        showSnackBar(activity, getString(R.string.authentiation_error))
                    } else {
                        showSnackBar(activity, getString(R.string.something_went_wrong))
                    }
                }
            })
        } else {
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }
    }

    private fun callReportDialog() {
        reportDialog = ReportDialog(activity)
        reportDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        reportDialog!!.show()


        reportDialog!!.tvfake.setOnClickListener(View.OnClickListener {
            reportDialog!!.dismiss()
            callReportUserAPI(getString(R.string.fake_profile), "")
        })
        reportDialog!!.tvSpam.setOnClickListener(View.OnClickListener {
            reportDialog!!.dismiss()
            callReportUserAPI(getString(R.string.spam), "")
        })
        reportDialog!!.tvInappropriateProifle.setOnClickListener(View.OnClickListener {
            reportDialog!!.dismiss()
            callReportUserAPI(getString(R.string.inappropriate_profile), "")
        })

        reportDialog!!.tvOther.setOnClickListener(View.OnClickListener {
            reportDialog!!.dismiss()
            callOtherMessageDialog()

        })
    }

    private fun callReportUserAPI(isFrom: String, reason: String) {
        val builder1 = AlertDialog.Builder(ContextThemeWrapper(activity, R.style.AlertDialogCustom))
        builder1.setMessage(getString(R.string.are_you_sure_want_to_report))
        builder1.setCancelable(true)

        builder1.setPositiveButton(getString(R.string.yes)) { dialog, id ->
            dialog.cancel()
            reportUserAPI(isFrom, reason)  //call block user API
        }

        builder1.setNegativeButton(getString(R.string.no)) { dialog, id -> dialog.cancel() }
        val alert11 = builder1.create()
        alert11.show()
    }

    private fun reportUserAPI(from: String, reason: String) {
        if (AppUtils.isConnectedToInternet(activity)) {
            AppUtils.showProgressDialog(activity)
            val params = HashMap<String, String?>()

            params["to_id"] = userId
            params["report_screen"] = from
            params["report"] = reason
            Log.e("param", params.toString())

            val call: Call<GeneralModel>
            call = RetrofitRestClient.instance!!.callReportUser(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, params)
            Log.e("token", SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
            call.enqueue(object : Callback<GeneralModel> {
                @SuppressLint("SetTextI18n", "ShowToast")
                override fun onResponse(call: Call<GeneralModel>?, response: Response<GeneralModel>?) {
                    AppUtils.hideProgressDialog()
                    val basicModel: GeneralModel
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {
                            if (from == getString(R.string.other)) {
                                otherMessageDialog!!.dismiss()
                            }
                            Toast.makeText(activity, basicModel.message, Toast.LENGTH_SHORT).show()
                            finish()
                            AppUtils.overridePendingTransitionExit(activity)
                        } else {
                            showSnackBar(activity, basicModel.message)
                        }
                    } else {
                        showSnackBar(activity, response.message())
                    }
                }

                override fun onFailure(call: Call<GeneralModel>?, t: Throwable?) {
                    AppUtils.hideProgressDialog()
                    if (t is SocketTimeoutException) {
                        showSnackBar(activity, getString(R.string.connection_timeout))
                    } else if (t is NetworkErrorException) {
                        showSnackBar(activity, getString(R.string.network_error))
                    } else if (t is AuthenticatorException) {
                        showSnackBar(activity, getString(R.string.authentiation_error))
                    } else {
                        showSnackBar(activity, getString(R.string.something_went_wrong))
                    }
                }
            })
        } else {
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }

    }

    private fun callOtherMessageDialog() {
        otherMessageDialog = OtherMessageDialog(activity)
        otherMessageDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        otherMessageDialog!!.show()


        otherMessageDialog!!.tvSubmit.setOnClickListener(View.OnClickListener {
            if (otherMessageDialog!!.etProblem.text.toString().isEmpty()) {
                Toast.makeText(activity, getString(R.string.please_enter_reason), Toast.LENGTH_SHORT).show()
            } else {
                callReportUserAPI(getString(R.string.other), otherMessageDialog!!.etProblem.text.toString())
            }
        })
        otherMessageDialog!!.tvCancel.setOnClickListener(View.OnClickListener {
            otherMessageDialog!!.dismiss()
        })
    }

    private fun callUserProfileAPI() {
        if (AppUtils.isConnectedToInternet(activity)) {
            AppUtils.showProgressDialog(activity)
            val params = HashMap<String, String?>()
            params["latitude"] = paramCrossLatitude_current
            params["longitude"] = paramCrossLongitude_current

            Log.e("paramProfile", params.toString() + "user" + userId)

            val call: Call<PersonResponse>
            call = RetrofitRestClient.instance!!.getProfileByIdForOtherUser(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, userId, params)
            Log.e("token", SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
            call.enqueue(object : Callback<PersonResponse> {
                @SuppressLint("SetTextI18n")
                override fun onResponse(call: Call<PersonResponse>?, response: Response<PersonResponse>?) {
                    val basicModel: PersonResponse
                    if (response != null) {
                        if (response.isSuccessful) {
                            basicModel = response.body()!!
                            if (Objects.requireNonNull(basicModel).code == "1") {


                                baseUrl = basicModel.data!!.baseUrl!!

                                if (basicModel.data.images!!.isNotEmpty()) {
                                    imageProfilelist = basicModel.data!!.images!! as ArrayList<UserImageModel>
                                }
                                vpProfileUser!!.adapter = SlidingProfileImage_Adapter(activity!!, imageProfilelist!!, baseUrl, this@UserProfileActivity)
                                dots_indicator_profile.setViewPager(vpProfileUser!!)

                                if (imageProfilelist.size > 0) {
                                    image_profile = baseUrl + imageProfilelist[0].image
                                    setProfileImage(baseUrl + imageProfilelist[0].image)
                                }

                                llUsrProfile.visibility = View.VISIBLE
                                //make all data visible
                                llProfileUser.visibility = View.VISIBLE
                                tvNoDataProfile.visibility = View.GONE

                                tvNoDataProfile.visibility = View.GONE

                                if (isFrom == "home") {
                                    rlAllFavorite.visibility = View.VISIBLE
                                    llHome.visibility = View.VISIBLE
                                    llOther.visibility = View.GONE
                                } else {
                                    rlAllFavorite.visibility = View.GONE
                                    llHome.visibility = View.GONE
                                    llOther.visibility = View.VISIBLE
                                }

                                paramName = basicModel.data.username!!
                                paramAge = basicModel.data.age!!
                                try {
                                    if (basicModel.data.height!! != null) {
                                        paramHeight = basicModel.data.height!!
                                    }
                                } catch (e: Exception) {

                                }


                                paramGender = basicModel.data.gender!!

                                Log.e("distance", basicModel.data!!.distance!!)
                                if (basicModel.data!!.distance != null) {
                                    paramDistance = basicModel.data!!.distance!!

                                    if (SharedPreferenceManager.getMySharedPreferences()!!.getDistance().equals("Km", ignoreCase = true) || SharedPreferenceManager.getMySharedPreferences()!!.getDistance().equals("km", ignoreCase = true) || SharedPreferenceManager.getMySharedPreferences()!!.getDistance().equals("", ignoreCase = true)) {
                                        paramDistance = String.format("%.02f", java.lang.Float.valueOf(paramDistance)).replace(",", ".") + " " + getString(R.string.km)
                                    } else {
                                        paramDistance = String.format("%.02f", java.lang.Float.valueOf(convertKmsToMiles(java.lang.Float.valueOf(paramDistance)))).replace(",", ".") + " " + getString(R.string.mile)
                                    }
                                    tvDistance.text = paramDistance
                                    tvProfileDistance.text = paramDistance

                                    ivGPSUp.visibility = View.VISIBLE
                                    ivGps.visibility = View.VISIBLE
                                } else {
                                    tvDistance.text = ""
                                    tvProfileDistance.text = ""
                                    ivGPSUp.visibility = View.GONE
                                    ivGps.visibility = View.GONE
                                }

                                try {
                                    if (basicModel.data!!.education!! != null) {
                                        paramProfessioon = basicModel.data!!.education!!
                                    }
                                } catch (e: Exception) {

                                }


                                paramHeight = paramHeight.replace("m", "")
                                tvHeight.text = paramHeight + " " + getString(R.string.meter)


                                if (basicModel.data.cLatitude != null) {
                                    paramCrossLatitude = basicModel.data.cLatitude
                                    paramCrossLongitude = basicModel.data.cLongitude!!

                                }



                                if (paramName != null) {
                                    tvName!!.text = paramName
                                    tvProfileName!!.text = paramName
                                }

/*
                                if (paramProfessioon != null) {
                                    if (paramProfessioon == "null" || paramProfessioon.equals("")) {
                                        tvprofessionAge!!.text = paramAge + " " + getString(R.string.years)
                                        tvProfession!!.text = paramAge + " " + getString(R.string.years)
                                    } else {
                                        tvprofessionAge!!.text = paramProfessioon + ", " + paramAge
                                        tvProfession!!.text = paramProfessioon + ", " + paramAge

                                    }
                                } else {
                                    tvprofessionAge!!.text = paramAge + " " + getString(R.string.years)
                                    tvProfession!!.text = paramAge + " " + getString(R.string.years)
                                }*/
                                if (basicModel.data.location!!.size != 0) {
                                    if (basicModel.data.location!![0].toString() != null) {
                                        paramProfileLatitude = basicModel.data.location!![0].toString()
                                        paramProfileLongitude = basicModel.data.location!![1].toString()

                                        tvLocation.text = getAddress(paramProfileLatitude, paramProfileLongitude)
                                    }
                                }


                                if (paramCrossLatitude != null) {
                                    if (paramCrossLatitude != "") {

                                        tvCurrentLocation.text = getAddress(paramCrossLatitude, paramCrossLongitude)

                                        if (mMap != null) {
                                            mMap!!.addMarker(MarkerOptions().position(LatLng(paramCrossLatitude.toDouble(), paramCrossLongitude.toDouble())).title(paramName).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET)))
                                            mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(paramCrossLatitude.toDouble(), paramCrossLongitude.toDouble()), 15f))
                                        }

                                    }

                                }

                                //for show age if provide condition
                                if (basicModel.data.preferences!!.showAge != null) {
                                    if (basicModel.data.preferences.showAge == "1") {
                                        if (paramProfessioon == "") {
                                            tvprofessionAge!!.text = paramAge + " " + getString(R.string.years)
                                            tvProfession!!.text = paramAge + " " + getString(R.string.years)
                                        } else {
                                            tvprofessionAge!!.text = paramProfessioon + ", " + paramAge
                                            tvProfession!!.text = paramProfessioon + ", " + paramAge
                                        }
                                    } else {
                                        if (paramProfessioon == "") {
                                            tvprofessionAge!!.text = ""
                                            tvProfession!!.text = ""
                                        } else {
                                            tvprofessionAge!!.text = paramProfessioon
                                            tvProfession!!.text = paramProfessioon
                                        }

                                    }
                                }

                                if (basicModel.data.preferences!!.shareLocation != null) {
                                    if (basicModel.data.preferences!!.shareLocation == "1") {
                                        tvTitleLocation.visibility = View.VISIBLE
                                        tvLocation.visibility = View.VISIBLE
                                        if (basicModel.data.isCross == "1") {
                                            cvLocation.visibility = View.VISIBLE
                                            llCrossLocation.visibility = View.VISIBLE
                                        } else {
                                            cvLocation.visibility = View.GONE
                                            llCrossLocation.visibility = View.GONE
                                        }
                                    } else {
                                        tvTitleLocation.visibility = View.GONE
                                        tvLocation.visibility = View.GONE
                                        cvLocation.visibility = View.GONE
                                        llCrossLocation.visibility = View.GONE
                                    }
                                }

                               /* if (basicModel.data!!.origins!!.isNotEmpty()) {
                                    paramOrigin = basicModel.data!!.origins!!
                                }

                                Log.e("origin", paramOrigin)
                                if (paramOrigin != null) {
                                    if (paramOrigin != "null") {

                                        val lstValues: List<String> = paramOrigin.split(",").map { it -> it.trim() }
                                        lstValues.forEach { it ->
                                            Log.i("Values", "value=$it")
                                            //Do Something
                                        }
                                        originProfilelist = lstValues as ArrayList<String>
                                        rvOriginProfile!!.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false) //set adapter
                                        profileOriginAdapter = ProfileOriginAdapter(activity, originProfilelist)
                                        rvOriginProfile!!.adapter = profileOriginAdapter
                                    }
                                }*/

                                originProfilelist.addAll(basicModel.data.origins!!)
                                rvOriginProfile!!.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false) //set adapter
                                profileOriginAdapter = ProfileOriginAdapter(activity, originProfilelist)
                                rvOriginProfile!!.adapter = profileOriginAdapter

                                if (basicModel.data.isFbConnect != null) {
                                    paramFbConnect = basicModel.data.isFbConnect!!
                                }

                                if (basicModel.data.isInstaConnect != null) {
                                    paramInstaConnect = basicModel.data.isInstaConnect!!
                                }


                                if (paramFbConnect == "1") {
                                    fbMedialist.clear()
                                    fbMedialist = basicModel.data.fbJsonArray as ArrayList<String>
                                    if (fbMedialist.size > 0) {
                                        llFbOtherProfile.visibility = View.VISIBLE
                                        showAllFbMedia()
                                    } else {
                                        llFbOtherProfile.visibility = View.GONE
                                    }
                                } else {
                                    llFbOtherProfile.visibility = View.GONE
                                }

                                if (paramInstaConnect == "1") {
                                    instaMedialist.clear()
                                    instaMedialist = basicModel.data.instaJsonArray as ArrayList<String>
                                    if (instaMedialist.size > 0) {
                                        llInstaOtherProfile.visibility = View.VISIBLE
                                        showAllInstaMedia()
                                    } else {
                                        llInstaOtherProfile.visibility = View.GONE
                                    }
                                } else {
                                    llInstaOtherProfile.visibility = View.GONE
                                }



                                callBlurCrossIcon()
                                AppUtils.hideProgressDialog()

                                //for all layout visible using animation after set data
                                // initAnimation()

                            } else if (Objects.requireNonNull(basicModel).code == "-1") {
                                AppUtils.hideProgressDialog()
                                callLogoutFunction()
                            } else {
                                AppUtils.hideProgressDialog()
                                getNoDataLayout()
                                showSnackBar(activity, basicModel.message)
                            }
                        } else {
                            AppUtils.hideProgressDialog()
                            getNoDataLayout()
                            showSnackBar(activity, response.message())
                        }
                    }
                }

                override fun onFailure(call: Call<PersonResponse>?, t: Throwable?) {
                    getNoDataLayout()
                    AppUtils.hideProgressDialog()
                    if (t is SocketTimeoutException) {
                        showSnackBar(activity, getString(R.string.connection_timeout))
                    } else if (t is NetworkErrorException) {
                        showSnackBar(activity, getString(R.string.network_error))
                    } else if (t is AuthenticatorException) {
                        showSnackBar(activity, getString(R.string.authentiation_error))
                    } else {
                        showSnackBar(activity, getString(R.string.something_went_wrong))
                    }
                }
            })
        } else {
            getNoDataLayout()
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }

    }

    private fun showAllInstaMedia() {
        instalist.clear()

        val G: Int = 6;
        val NG: Int = (instaMedialist.size + G - 1) / G;
        val chunks: List<List<String>> = instaMedialist.chunked(6)
        Log.e("chunk", chunks.toString())
        Log.e("chunk", chunks.size.toString())

        for (i in chunks.indices) {
            var instaModel: InstaModel = InstaModel()
            instaModel.path = ""
            instaModel.list = chunks[i]
            instalist.add(instaModel)
            Log.e("main_size", instalist[i].list.toString())
        }

        vpInstaOther!!.adapter = InstaAdapter(activity, instalist)
        dots_indicator_insta_other.setViewPager(vpInstaOther)

    }

    private fun showAllFbMedia() {
        fblist.clear()
        val G: Int = 6;
        val NG: Int = (fbMedialist.size + G - 1) / G;
        val chunks: List<List<String>> = fbMedialist.chunked(6)
        Log.e("chunk", chunks.toString())
        Log.e("chunk", chunks.size.toString())

        for (i in chunks.indices) {
            var instaModel: InstaModel = InstaModel()
            instaModel.path = ""
            instaModel.list = chunks[i]
            fblist.add(instaModel)
            Log.e("main_size", fblist[i].list.toString())

        }
        vpFbOther!!.adapter = InstaAdapter(activity, fblist)
        dots_indicator_fb_other.setViewPager(vpFbOther)

    }


    private fun initAnimation() {
        //for make background layout visible
        Handler().postDelayed({
            llUsrProfile.visibility = View.INVISIBLE
            val cx = llUsrProfile.right - 200
            val cy = llUsrProfile.bottom
            val finalRadius = Math.max(llUsrProfile.width, llUsrProfile.getHeight())
            val anim = ViewAnimationUtils.createCircularReveal(llUsrProfile, cx, cy, 0F, finalRadius.toFloat())
            anim.duration = 900
            llUsrProfile.visibility = View.VISIBLE
            //make all data visible
            llProfileUser.visibility = View.VISIBLE
            tvNoDataProfile.visibility = View.GONE
            rlAllFavorite.visibility = View.VISIBLE
            tvNoDataProfile.visibility = View.GONE
            // overlay.setVisibility(View.VISIBLE)
            anim.start()
        }, 600)
    }


    fun getAddress(LATITUDE: String, LONGITUDE: String): String {

        try {
            geocoder = Geocoder(activity, Locale.ENGLISH)
            addresses = geocoder!!.getFromLocation(LATITUDE.toDouble(), LONGITUDE.toDouble(), 1) // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            Log.e("Address", addresses[0].toString())

            if (addresses.isNotEmpty()) {

                if (addresses[0].locality != null) {
                    if (addresses[0].locality == addresses[0].adminArea) {
                        address = addresses[0].adminArea + ", " + addresses[0].countryName
                    } else {
                        address = addresses[0].locality + ", " + addresses[0].adminArea + ", " + addresses[0].countryName
                    }
                } else {
                    address = addresses[0].adminArea + ", " + addresses[0].countryName
                }

                /* if (addresses[0].locality != null) {
                     address = addresses[0].locality
                 }
                 if (addresses[0].adminArea != null) {
                     address = address + "," + addresses[0].adminArea
                 }
                 if (addresses[0].countryName != null) {
                     address = address + "," + addresses[0].countryName
                 }*/
                Log.e("Name", address)

            }

            /*if (addresses.isNotEmpty()) {

                if (addresses[0].locality != null) {
                    address = addresses[0].locality
                }
                if (addresses[0].adminArea != null) {
                    address = address + "," + addresses[0].adminArea
                }
                if (addresses[0].countryName != null) {
                    address = address + "," + addresses[0].countryName
                }
                Log.e("Name", address)

            }*/

            if (address == "") {
                if (addresses[0].getAddressLine(0) != null) {
                    address = addresses[0].getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                }
            }
        } catch (e: Exception) {
        }
        return address;
    }

    private fun getNoDataLayout() {
        llUsrProfile.visibility = View.VISIBLE
        tvNoDataProfile.visibility = View.VISIBLE
        llProfileUser.visibility = View.GONE
        rlAllFavorite.visibility = View.GONE
        llUpView.visibility = View.GONE
    }

    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        return false
    }

    override fun onScrollChanged() {
        val view: View = svProfile.getChildAt(svProfile.getChildCount() - 1)
        val topDetector: Int = svProfile.getScrollY()
        val bottomDetector: Int = view.bottom - (svProfile.getHeight() + svProfile.getScrollY())
        if (bottomDetector == 0) {
            llUpView.visibility = View.VISIBLE
            // Toast.makeText(baseContext, "Scroll View bottom reached", Toast.LENGTH_SHORT).show()
        }
        if (topDetector <= 0) {
            llUpView.visibility = View.GONE
            //  Toast.makeText(baseContext, "Scroll View top reached", Toast.LENGTH_SHORT).show()
        }
    }

    private fun getPermissionforMapAndInitialization() {
        callFineLocation()
    }


    @AfterPermissionGranted(MapActivity.RC_FINE_LOCATION)
    fun callFineLocation() {
        if (EasyPermissions.hasPermissions(activity, Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Have permission, do the thing!
            callCorseLocation()
        } else {
            // Request one permission
            EasyPermissions.requestPermissions(
                    PermissionRequest.Builder(this, MapActivity.RC_FINE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
                            .setRationale(R.string.rationale_call)
                            .setPositiveButtonText(getString(R.string.ok))
                            .setNegativeButtonText(getString(R.string.cancel))
                            .build())
        }
    }

    //permisson read storage
    @AfterPermissionGranted(MapActivity.RC_ACCESSS_LOCATION)
    fun callCorseLocation() {
        if (EasyPermissions.hasPermissions(activity, Manifest.permission.ACCESS_COARSE_LOCATION)) {
            // Have permission, do the thing!

            getUserCurrentLocation()  //call google api client for get location
            getMapInit() //for initialize map
            allClicklistener()  // all click listener
            callUserProfileAPI()

        } else {
            // Request one permission
            EasyPermissions.requestPermissions(
                    PermissionRequest.Builder(this, MapActivity.RC_ACCESSS_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
                            .setRationale(R.string.rationale_call)
                            .setPositiveButtonText(getString(R.string.ok))
                            .setNegativeButtonText(getString(R.string.cancel))
                            .build())
        }
    }

    private fun getUserCurrentLocation() {
        locationTrack = LocationTrack(activity)
        if (locationTrack!!.canGetLocation()) {
            if (locationTrack!!.getLongitude().toString() != "0.0") {
                paramCrossLongitude_current = locationTrack!!.getLongitude().toString()
                paramCrossLatitude_current = locationTrack!!.getLatitude().toString()
                Log.e("latlng_home", paramCrossLatitude + "name" + paramCrossLongitude)

            }
        } else {
            // locationTrack.showSettingsAlert()
        }
    }

    private fun getMapInit() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        Objects.requireNonNull(mapFragment)!!.getMapAsync(this)
        mapFragment!!.getMapAsync(this)
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap?) {

        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            val success = googleMap!!.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            activity, R.raw.style_json_map))
            if (!success) {
                Log.e(ContentValues.TAG, "Style parsing failed.")
            }
        } catch (e: Resources.NotFoundException) {
            Log.e(ContentValues.TAG, "Can't find style. Error: ", e)
        }



        mMap = googleMap
        mMap!!.isMyLocationEnabled = true
        mMap!!.uiSettings.isMyLocationButtonEnabled = true
        mMap!!.setMinZoomPreference(15f)

        if (mMap != null) {
            if (paramCrossLatitude != "") {
                mMap!!.addMarker(MarkerOptions().position(LatLng(paramCrossLatitude.toDouble(), paramCrossLongitude.toDouble())).title(paramName).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET)))
                mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(paramCrossLatitude.toDouble(), paramCrossLongitude.toDouble()), 15f))
            }
        }

    }

    override fun clickOnImageItem(position: Int) {
        if (isClickMessage == false) {
            isClickMessage = true
            var i = Intent(activity, ProfileImageActivity::class.java)
            i.putExtra("baseUrl", baseUrl)
            i.putExtra("position", position.toString())
            i.putExtra("imagelist", imageProfilelist)
            startActivity(i)
        }
    }

    fun convertKmsToMiles(kms: Float): Float {
        return (0.621371 * kms).toFloat()
    }
}
