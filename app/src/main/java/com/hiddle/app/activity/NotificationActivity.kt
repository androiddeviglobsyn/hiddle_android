package com.hiddle.app.activity

import android.accounts.AuthenticatorException
import android.accounts.NetworkErrorException
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.View.OnTouchListener
import com.hiddle.app.R
import com.hiddle.app.model.NotificationModel.NotificationResponse
import com.hiddle.app.retrofit.RetrofitRestClient
import com.hiddle.app.util.AppUtils
import com.hiddle.app.util.SharedPreferenceManager
import kotlinx.android.synthetic.main.activity_notification.*
import kotlinx.android.synthetic.main.activity_setting.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketTimeoutException
import java.util.*
import kotlin.collections.HashMap


class NotificationActivity : BaseMainActivity(), View.OnClickListener {

    var paramStrike: String = ""
    var paramMessage: String = ""
    var paramLike: String = ""
    var paramSound: String = ""
    var paramVibration: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification)

        //for set default tag for switch
        swStrike.tag = "NO"
        swMessage.tag = "NO"
        swLike.tag = "NO"
        swSound.tag = "NO"
        swVibration.tag = "NO"

        //Get all detail of switch
        callSetNotification("No", "GET")
        allClicklistener()
        allCheckedMethod()
    }

    private fun allCheckedMethod() {
        // Add Touch listener.
        swStrike.setOnTouchListener(OnTouchListener { v, event ->
            swStrike.tag = "YES"
            false
        })
        swStrike.setOnCheckedChangeListener { buttonView, isChecked ->
            if (swStrike.tag == "YES") {
                callSetNotification("STRIKE", "SET")
                // callAPILoginUpdate("SET", "STRIKE")
            }
        }

        swMessage.setOnTouchListener(OnTouchListener { v, event ->
            swMessage.tag = "YES"
            false
        })

        swMessage.setOnCheckedChangeListener { buttonView, isChecked ->
            if (swMessage.tag == "YES") {
                //callAPILoginUpdate("SET","MESSAGE")
                callSetNotification("MESSAGE", "SET")
            }
        }

        swLike.setOnTouchListener(OnTouchListener { v, event ->
            swLike.tag = "YES"
            false
        })

        swLike.setOnCheckedChangeListener { buttonView, isChecked ->
            if (swLike.tag == "YES") {
                // callAPILoginUpdate("SET", "LIKE")
                callSetNotification("LIKE", "SET")
            }
        }

        swSound.setOnTouchListener(OnTouchListener { v, event ->
            swSound.tag = "YES"
            false
        })

        swSound.setOnCheckedChangeListener { buttonView, isChecked ->
            if (swSound.tag == "YES") {
                // callAPILoginUpdate("SET", "SOUND")
                callSetNotification("SOUND", "SET")
            }
        }
        swVibration.setOnTouchListener(OnTouchListener { v, event ->
            swVibration.tag = "YES"
            false
        })
        swVibration.setOnCheckedChangeListener { buttonView, isChecked ->
            if (swVibration.tag == "YES") {
                // callAPILoginUpdate("SET", "VIBRATION")
                callSetNotification("VIBRATION", "SET")

            }
        }
    }

    private fun callSetNotification(isClick: String, isFrom: String) {
        if (AppUtils.isConnectedToInternet(activity)) {

            if (isFrom == "GET") {
                AppUtils.showProgressDialog(activity)
            }
            val params = HashMap<String, String?>()


            if (isFrom == "SET") {
                if (isClick == "STRIKE") {
                    paramStrike = if (swStrike.isChecked) {
                        "1"
                    } else {
                        "0"
                    }
                    params["new_strike"] = paramStrike
                }
                if (isClick == "MESSAGE") {
                    paramMessage = if (swMessage.isChecked) {
                        "1"
                    } else {
                        "0"
                    }
                    params["new_message"] = paramMessage
                }
                if (isClick == "LIKE") {
                    paramLike = if (swLike.isChecked) {
                        "1"
                    } else {
                        "0"
                    }
                    params["likes"] = paramLike
                }

                if (isClick == "SOUND") {
                    paramSound = if (swSound.isChecked) {
                        "1"
                    } else {
                        "0"
                    }
                    params["sounds"] = paramSound

                }

                if (isClick == "VIBRATION") {
                    paramVibration = if (swVibration.isChecked) {
                        "1"
                    } else {
                        "0"
                    }
                    params["vibration"] = paramVibration

                }

            }


            val call: Call<NotificationResponse>
            call = RetrofitRestClient.instance!!.callNotification(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, params)
            call.enqueue(object : Callback<NotificationResponse> {
                override fun onResponse(call: Call<NotificationResponse>?, response: Response<NotificationResponse>?) {
                    AppUtils.hideProgressDialog()
                    val basicModel: NotificationResponse
                    Log.e("Response", response.toString())
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {

                            if (isFrom == "GET") {
                                //strike
                                if (basicModel.data!!.newStrike != null) {

                                    if (basicModel.data.newStrike == "1") {
                                        swStrike.isChecked = true
                                    } else {
                                        swStrike.isChecked = false
                                    }
                                }

                                if (basicModel.data.newMessage != null) {
                                    if (basicModel.data.newMessage == "1") {
                                        swMessage.isChecked = true
                                    } else {
                                        swMessage.isChecked = false
                                    }
                                }

                                if (basicModel.data.likes != null) {
                                    if (basicModel.data.likes == "1") {
                                        swLike.isChecked = true
                                    } else {
                                        swLike.isChecked = false
                                    }
                                }

                                if (basicModel.data.sounds != null) {
                                    if (basicModel.data.sounds == "1") {
                                        swSound.isChecked = true
                                    } else {
                                        swSound.isChecked = false
                                    }
                                }

                                if (basicModel.data.vibration != null) {
                                    if (basicModel.data.vibration == "1") {
                                        swVibration.isChecked = true

                                    } else {
                                        swVibration.isChecked = false
                                    }
                                }

                            }

                            SharedPreferenceManager.getMySharedPreferences()!!.setSound(basicModel.data!!.sounds!!)
                            SharedPreferenceManager.getMySharedPreferences()!!.setVibrate(basicModel.data.vibration!!)


                        } else if (Objects.requireNonNull(basicModel).code == "-1") {
                            callLogoutFunction()
                        } else {
                            showSnackBar(activity, basicModel.message)
                            callResultFail(isClick)
                        }
                    } else {
                        callResultFail(isClick)
                        showSnackBar(activity, response.message())
                    }
                }

                override fun onFailure(call: Call<NotificationResponse>?, t: Throwable?) {
                    callResultFail(isClick)
                    Log.e("callError", call.toString())
                    Log.e("Error", t.toString())
                    AppUtils.hideProgressDialog()
                    if (t is SocketTimeoutException) {
                        showSnackBar(activity, getString(R.string.connection_timeout))
                    } else if (t is NetworkErrorException) {
                        showSnackBar(activity, getString(R.string.network_error))
                    } else if (t is AuthenticatorException) {
                        showSnackBar(activity, getString(R.string.authentiation_error))
                    } else {
                        showSnackBar(activity, getString(R.string.something_went_wrong))
                    }
                }
            })


        } else {
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }
    }


    private fun allClicklistener() {
        ivBackNotification.setOnClickListener(this)
        llBackNotification.setOnClickListener(this)
    }


    override fun onClick(view: View) {
        when (view.id) {
            R.id.ivBackNotification -> {
                ivBackNotification.isEnabled = false
                llBackNotification.isEnabled = false
                finish()
                AppUtils.overridePendingTransitionExit(activity)
            }
            R.id.llBackNotification -> {
                ivBackNotification.isEnabled = false
                llBackNotification.isEnabled = false
                finish()
                AppUtils.overridePendingTransitionExit(activity)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        ivBackNotification.isEnabled = true
        llBackNotification.isEnabled = true
    }


    private fun callResultFail(click: String) {
        if (click == "STRIKE") {
            if (paramStrike == "1") {
                swStrike.isChecked = false
            } else {
                swStrike.isChecked = true
            }
        }
        if (click == "MESSAGE") {
            if (paramMessage == "1") {
                swMessage.isChecked = false
            } else {
                swMessage.isChecked = true
            }
        }
        if (click == "LIKE") {
            if (paramLike == "1") {
                swLike.isChecked = false
            } else {
                swLike.isChecked = true
            }
        }

        if (click == "SOUND") {
            if (paramSound == "1") {
                swSound.isChecked = false
            } else {
                swSound.isChecked = true
            }
        }

        if (click == "VIBRATION") {
            if (paramVibration == "1") {
                swVibration.isChecked = false
            } else {
                swVibration.isChecked = true
            }
        }
    }

    /* override fun onBackPressed()
     {
         callAPILoginUpdate("SET")
     }*/

}
