package com.hiddle.app.activity

import android.Manifest
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.StrictMode
import android.os.StrictMode.VmPolicy
import android.provider.MediaStore
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.hiddle.app.R
import com.hiddle.app.`interface`.AddImageClick
import com.hiddle.app.adapter.AddPictureAdapter
import com.hiddle.app.itemDrag.ItemMoveCallback
import com.hiddle.app.itemDrag.StartDragListener
import com.hiddle.app.util.AppUtils
import com.hiddle.app.util.CameraPhoto
import droidninja.filepicker.FilePickerBuilder
import droidninja.filepicker.FilePickerConst
import droidninja.filepicker.utils.ContentUriUtils
import kotlinx.android.synthetic.main.activity_add_picture.*
import kotlinx.android.synthetic.main.bottom_sheet_picture.view.*
import java.io.File
import java.io.IOException

class AddPictureScreenActivity : BaseMainActivity(), View.OnClickListener, AddImageClick, StartDragListener {

    var authorizationToken: String = ""
    var profile_url: String = ""
    var userChoosenTask: String = ""
    var clickedPosition: Int = -1
    var picturePath: String = ""
    val imagelist: ArrayList<String> = ArrayList()
    val uploadImagelist: ArrayList<String> = ArrayList()
    var addPictureAdapter: AddPictureAdapter? = null
    val PICK_CAMERA_REQUEST = 2
    val PICK_MULTIPLE_IMAGE_REQUEST = 532
    val MY_PERMISSIONS_REQUEST_WRITE_STORAGE = 101
    val MY_PERMISSIONS_REQUEST_STORAGE = 100
    val MY_PERMISSIONS_REQUEST_CAMERA = 150
    val PICK_IMAGE_REQUEST = 1
    var cameraPhoto: CameraPhoto? = null
    var isSelectPic: Boolean = false
    var paramEmail: String = ""
    var paramUserName: String = ""
    var isFromLogin: String = ""
    var socialId: String = ""

    //Bottom sheet dialog
    var dialog: BottomSheetDialog? = null
    var MAX_ATTACHMENT_COUNT = 9
    val photoPaths: ArrayList<Uri> = ArrayList()


    var touchHelper: ItemTouchHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_picture)
        val builder = VmPolicy.Builder() //for capture camera image it is imporatant
        StrictMode.setVmPolicy(builder.build())

        getIntentData()
        setRecyclerview()  // for set recyclerview to add picture
        allClicklistener()
    }

    fun getIntentData() {
         var i = intent
         if (i != null) {
             paramUserName = i.getStringExtra("paramUserName")!!
             paramEmail = i.getStringExtra("paramEmail")!!
             isFromLogin = i.getStringExtra("isFromLogin")!!
             socialId = i.getStringExtra("socialId")!!
             authorizationToken = i.getStringExtra("authorizationToken")!!
             profile_url = i.getStringExtra("profile_url")!!
             if (paramUserName != null) {
                 etUsername.setText(paramUserName)
             }
         }
    }


    fun setRecyclerview() {
        for (i in 0..8) {     //add 9 picture random for design

            if (i == 0) {
                if (profile_url != "") {
                    val file = File(profile_url)
                    if (file.exists()) {
                        imagelist.add(profile_url)
                    } else {
                        imagelist.add("")
                    }
                } else {
                    imagelist.add("")
                }
            } else {
                imagelist.add("")
            }

        }
        setImageAdapter()
    }

    fun setImageAdapter() {
        rvPicture.layoutManager = GridLayoutManager(activity, 3) //set adapter
        addPictureAdapter = AddPictureAdapter(activity, imagelist, this, this, this@AddPictureScreenActivity)

        /*  var callback:ItemTouchHelper.Callback = ItemTouchHelper.Callback(addPictureAdapter)
         touchHelper =ItemMoveCallback.ItemTouchHelperContract(callback)
         touchHelper!!.attachToRecyclerView(rvPicture)*/

        val callback = ItemMoveCallback(addPictureAdapter)
        touchHelper = ItemTouchHelper(callback)
        touchHelper!!.attachToRecyclerView(rvPicture)


        rvPicture.adapter = addPictureAdapter


    }


    fun allClicklistener() {
        llNextAddPicture.setOnClickListener(this)
        ivBackPicture.setOnClickListener(this)
        llBackPicture.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.ivBackPicture -> {
                callBack()
            }
            R.id.llBackPicture -> {
                callBack()

            }
            R.id.llNextAddPicture -> {

                // callClick()
                //make one list which only pass path which are not blank
                uploadImagelist.clear()
                for (i in imagelist.indices) {
                    if (imagelist[i] != "") {
                        uploadImagelist.add(imagelist[i])
                    }
                }
                paramUserName = etUsername.text.toString()
                if (validate()) {
                    llNextAddPicture.isEnabled = false
                    val i = Intent(activity, GenderSelectionActivity::class.java)
                    i.putExtra("paramUserName", paramUserName)
                    i.putExtra("paramEmail", paramEmail)
                    i.putExtra("isFromLogin", isFromLogin)
                    i.putExtra("socialId", socialId)
                    i.putExtra("authorizationToken", authorizationToken)
                    i.putExtra("uploadImagelist", uploadImagelist)
                    startActivity(i)
                    AppUtils.overridePendingTransitionEnter(activity)
                }
            }


        }
    }

    private fun callBack() {
        ivBackPicture.isEnabled = false
        llBackPicture.isEnabled = false
        finish()
        AppUtils.overridePendingTransitionExit(activity)
    }

    private fun callClick() {
        for (i in imagelist.indices) {

            if (imagelist[i] != "") {
                Log.e("path_value", i.toString() + "path" + imagelist[i].toString())
                uploadImagelist.add(imagelist[i])
            }
        }
    }

    override fun onResume() {
        super.onResume()
        ivBackPicture.isEnabled = true
        llBackPicture.isEnabled = true
        llNextAddPicture.isEnabled = true
    }


    fun callDragViewPos(removeImagePos: Int, dragViewPos: Int) {
        val dragitemImage = imagelist[dragViewPos]

        imagelist[dragViewPos] = imagelist[removeImagePos]
        imagelist[removeImagePos] = dragitemImage

        setImageAdapter()

    }

    fun validate(): Boolean {
        isSelectPic = false

        for (i in 0..8) {
            if (AppUtils.isContainText(imagelist[i].toString())) {
                isSelectPic = true
            }
        }

        if (!isSelectPic) {
            showSnackBar(activity, getString(R.string.please_select_picture))
            return false
        }

        if (paramUserName.isEmpty()) {
            showSnackBar(activity, getString(R.string.please_enter_user_name))
            return false
        }
        return true
    }

    fun selectImage(position: Int) {
        clickedPosition = position

        //Open bottom sheet dialog
        val dialog = BottomSheetDialog(this)
        val bottomSheet = layoutInflater.inflate(R.layout.bottom_sheet_picture, null)

        if (imagelist[clickedPosition] != "") {
            bottomSheet.tvRemoveImage.visibility = View.VISIBLE
            bottomSheet.tvTakePhoto.visibility = View.GONE
            bottomSheet.tvMultiple.visibility = View.GONE
        } else {
            bottomSheet.tvRemoveImage.visibility = View.GONE
            bottomSheet.tvTakePhoto.visibility = View.VISIBLE
            bottomSheet.tvMultiple.visibility = View.VISIBLE
        }

        if (imagelist[clickedPosition] != "") {
            if (clickedPosition != 0) {
                bottomSheet.tvProfilePicImage.visibility = View.VISIBLE
            }
        }

        bottomSheet.tvTakePhoto.setOnClickListener {
            clickedPosition = position
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                userChoosenTask = getString(R.string.take_photo)
                checkWritePermission()
            } else {
                CaptureImage()
            }
            dialog.dismiss()
        }

        bottomSheet.tvMultiple.setOnClickListener {
            clickedPosition = position
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                userChoosenTask = getString(R.string.choose_photo)
                checkWritePermission()
            } else {
                CaptureMultipleImage()
            }
            dialog.dismiss()
        }


        bottomSheet.tvGallery.setOnClickListener {
            /*clickedPosition = position
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                userChoosenTask = getString(R.string.choose_library)
                checkWritePermission()
            } else {
                PickGalleryImage()
            }
            dialog.dismiss()*/
        }

        bottomSheet.tvRemoveImage.setOnClickListener {
            clickedPosition = position
            imagelist[clickedPosition] = ""

            moveImageViewAndreplace()  //you can remove for replacement
            setImageAdapter()
            dialog.dismiss()
        }

        bottomSheet.tvProfilePicImage.setOnClickListener {
            clickedPosition = position
            var dragitemImage = imagelist[0]  //interchange path to each other iten and replace image

            imagelist[0] = imagelist[clickedPosition]
            imagelist[clickedPosition] = dragitemImage
            // imagelist[clickedPosition] = ""
            setImageAdapter()
            dialog.dismiss()
        }

        bottomSheet.tvCancel.setOnClickListener {
            clickedPosition = position
            dialog.dismiss()
        }

        dialog.setContentView(bottomSheet)
        dialog.show()

        /*
         val items = arrayOf<CharSequence>(getString(R.string.take_photo), getString(R.string.choose_library), getString(R.string.cancel))
         val builder = AlertDialog.Builder(this)
         builder.setTitle(getString(R.string.add_photo))
         builder.setItems(items) { dialog, item ->
             if (items[item] == getString(R.string.take_photo)) {
                 if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                     userChoosenTask = getString(R.string.take_photo)
                     checkWritePermission()
                 } else {
                     CaptureImage()
                 }
             } else if (items[item] == getString(R.string.choose_library)) {
                 if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                     userChoosenTask = getString(R.string.choose_library)
                     checkWritePermission()
                 } else {
                     PickGalleryImage()
                 }
             } else if (items[item] == getString(R.string.cancel)) {
                 dialog.dismiss()
             }
         }
         builder.show()*/
    }

    private fun moveImageViewAndreplace() {
        var previousimagelist: ArrayList<String> = ArrayList()

        for (i in imagelist.indices) {
            if (imagelist[i] != "") {
                previousimagelist.add(imagelist[i])
            }
        }
        imagelist.clear() //clear image list

        //now add previous imagelist in main imagelist
        for (i in previousimagelist.indices) {
            imagelist.add(previousimagelist[i])
        }

        //now add photopath in imagelist
        for (i in photoPaths.indices) {
            imagelist.add(ContentUriUtils.getFilePath(activity, photoPaths.get(i))!!)
        }

        //now add another box with blank image and set Adapter
        for (i in imagelist.size..8) {
            imagelist.add("")
        }
    }

    fun CaptureMultipleImage() {
        var k = 0
        for (i in imagelist.indices) {
            if (imagelist[i] == "") {
                k++
            }
            MAX_ATTACHMENT_COUNT = k
        }

        if ((photoPaths.size) == MAX_ATTACHMENT_COUNT) {
            Toast.makeText(this, "Cannot select more than " + MAX_ATTACHMENT_COUNT + " items",
                    Toast.LENGTH_SHORT).show()
        } else {
            photoPaths.clear()
            FilePickerBuilder.instance
                    .setMaxCount(MAX_ATTACHMENT_COUNT)
                    //.setSelectedFiles(photoPaths)
                    .setActivityTheme(R.style.FilePickerTheme)
                    .setActivityTitle("Please select media")
                    .enableVideoPicker(false)
                    .enableCameraSupport(false)
                    .showGifs(false)
                    .showFolderView(true)
                    .enableSelectAll(false)
                    .enableImagePicker(true)
                    .withOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                    .pickPhoto(this, PICK_MULTIPLE_IMAGE_REQUEST)
        }
    }

    fun checkWritePermission(): Boolean {
        return if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        MY_PERMISSIONS_REQUEST_WRITE_STORAGE)
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        MY_PERMISSIONS_REQUEST_WRITE_STORAGE)
            }
            false
        } else {
            checkStoragePermission()
            true
        }
    }

    fun checkStoragePermission(): Boolean {
        return if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                            Manifest.permission.READ_EXTERNAL_STORAGE)) {
                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                        MY_PERMISSIONS_REQUEST_STORAGE)
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                        MY_PERMISSIONS_REQUEST_STORAGE)
            }
            false
        } else {
            when (userChoosenTask) {
                getString(R.string.take_photo) -> checkCameraPermission()
                getString(R.string.choose_library) -> PickGalleryImage()
                getString(R.string.choose_photo) -> CaptureMultipleImage()
            }
            true
        }
    }

    /**
     * Runtime permission camera
     *
     * @return
     */
    fun checkCameraPermission(): Boolean {
        return if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                            Manifest.permission.CAMERA)) {
                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.CAMERA),
                        MY_PERMISSIONS_REQUEST_CAMERA)
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.CAMERA),
                        MY_PERMISSIONS_REQUEST_CAMERA)
            }
            false
        } else {
            when (userChoosenTask) {
                getString(R.string.take_photo) -> CaptureImage()
            }
            true
        }
    }

    /**
     * capture image from camera
     */
    fun CaptureImage() {
        try {
            cameraPhoto = CameraPhoto(this)
            startActivityForResult(cameraPhoto!!.takePhotoIntent(), PICK_CAMERA_REQUEST)
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    /**
     * pick image from gallery
     */
    fun PickGalleryImage() {
        val i = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(i, PICK_IMAGE_REQUEST)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_WRITE_STORAGE -> {
                run {
                    // If request is cancelled, the result arrays are empty.
                    if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        // permission was granted. Do the
                        // contacts-related task you need to do.
                        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                checkStoragePermission()
                            }
                        }
                    } else {
                        var i = 0
                        val len = permissions.size
                        while (i < len) {
                            if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                                // user rejected the permission
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    // Permission denied, Disable the functionality that depends on this permission.
                                    Toast.makeText(activity, "Permission Denied", Toast.LENGTH_LONG).show()
                                    AlertDialog.Builder(activity).setMessage("Approve All Permission")
                                            .setPositiveButton("Ok") { dialog, which ->
                                                //checkWritePermission()
                                                dialog.dismiss()
                                            }.setNegativeButton("Setting") { dialog, which ->
                                                val intent = Intent()
                                                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                                val uri = Uri.fromParts("package", packageName, null)
                                                intent.data = uri
                                                startActivity(intent)
                                                dialog.dismiss()
                                            }.show()
                                }
                            }
                            i++
                        }
                    }
                }
                return
            }
            MY_PERMISSIONS_REQUEST_STORAGE -> {
                run {
                    // If request is cancelled, the result arrays are empty.
                    if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        // permission was granted. Do the
                        // contacts-related task you need to do.
                        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                when (userChoosenTask) {
                                    getString(R.string.take_photo) -> checkCameraPermission()
                                    getString(R.string.choose_library) -> PickGalleryImage()
                                }
                            }
                        }
                    } else {
                        var i = 0
                        val len = permissions.size
                        while (i < len) {
                            if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                                // user rejected the permission
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    // Permission denied, Disable the functionality that depends on this permission.
                                    Toast.makeText(activity, "Permission Denied", Toast.LENGTH_LONG).show()
                                    AlertDialog.Builder(activity).setMessage("Approve All Permission")
                                            .setPositiveButton("Ok") { dialog, which -> dialog.dismiss() }
                                            .setNegativeButton("Setting") { dialog, which ->
                                                val intent = Intent()
                                                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                                val uri = Uri.fromParts("package", packageName, null)
                                                intent.data = uri
                                                startActivity(intent)
                                                dialog.dismiss()
                                            }.show()
                                }
                            }
                            i++
                        }
                    }
                }
                return
            }
            MY_PERMISSIONS_REQUEST_CAMERA -> {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted. Do the
                    // contacts-related task you need to do.
                    if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (userChoosenTask == getString(R.string.take_photo)) {
                                CaptureImage()
                            }
                        }
                    }
                } else {
                    var i = 0
                    val len = permissions.size
                    while (i < len) {
                        if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                            // user rejected the permission
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                // Permission denied, Disable the funct ionality that depends on this permission.
                                Toast.makeText(activity, "Permission Denied", Toast.LENGTH_LONG).show()
                                AlertDialog.Builder(activity).setMessage("Approve All Permission")
                                        .setPositiveButton("Ok") { dialog, which ->
                                            //checkCameraPermission()
                                            dialog.dismiss()
                                        }.setNegativeButton("Setting") { dialog, which ->
                                            val intent = Intent()
                                            intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                            val uri = Uri.fromParts("package", packageName, null)
                                            intent.data = uri
                                            startActivity(intent)
                                            dialog.dismiss()
                                        }.show()
                            }
                        }
                        i++
                    }
                }
                return
            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_CAMERA_REQUEST && resultCode == RESULT_OK) {
            try {
                cameraPhoto?.addToGallery()
                picturePath = cameraPhoto?.photoPath!!
                imagelist[clickedPosition] = compressImage(picturePath)
                setImageAdapter()
            } catch (e: Exception) {

            }
        } else if (requestCode == PICK_MULTIPLE_IMAGE_REQUEST && resultCode == RESULT_OK && data != null) {
            photoPaths.addAll(data.getParcelableArrayListExtra<Uri>(FilePickerConst.KEY_SELECTED_MEDIA)!!)
            /* val path = ContentUriUtils.getFilePath(activity, photoPaths.get(0))*/
            if (photoPaths.size > 0) {
                //first take whole imagelist in previous image
                var previousimagelist: ArrayList<String> = ArrayList()

                for (i in imagelist.indices) {
                    if (imagelist[i] != "") {
                        previousimagelist.add(imagelist[i])
                    }
                }
                imagelist.clear() //clear image list

                //now add previous imagelist in main imagelist
                for (i in previousimagelist.indices) {
                    imagelist.add(previousimagelist[i])
                }

                //now add photopath in imagelist
                for (i in photoPaths.indices) {
                    imagelist.add(ContentUriUtils.getFilePath(activity, photoPaths.get(i))!!)
                }

                //now add another box with blank image and set Adapter
                for (i in imagelist.size..8) {
                    imagelist.add("")
                }

                setImageAdapter()
                previousimagelist.clear()
                photoPaths.clear()
            } else {
                showSnackBar(activity, getString(R.string.you_have_not_picked_any_pick))
            }


        } else if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.data != null) {
            picturePath = getPathString(data.data!!)
            if (clickedPosition != -1) {
                imagelist[clickedPosition] = compressImage(picturePath)
                setImageAdapter()
            } else {
                showSnackBar(activity, getString(R.string.something_went_wrong))
            }
        } else {
            showSnackBar(activity, getString(R.string.you_have_not_picked_any_pick))
        }
    }

    fun getPathString(uri: Uri): String {
        var path: String = ""

        activity.contentResolver.query(uri, arrayOf(MediaStore.Images.Media.DATA),
                null, null, null
        )?.apply {
            val columnIndex = getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            moveToFirst()
            path = getString(columnIndex)
            close()
        }
        return path
    }

    override fun addImageSelection(position: Int) {
        selectImage(position)
    }


    override fun requestDrag(viewHolder: RecyclerView.ViewHolder?) {
        touchHelper!!.startDrag(viewHolder!!)
    }


}
