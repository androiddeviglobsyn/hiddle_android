package com.hiddle.app.activity

import android.accounts.AuthenticatorException
import android.accounts.NetworkErrorException
import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.animation.Animation
import android.view.animation.TranslateAnimation
import com.facebook.Profile

import com.hiddle.app.R
import com.hiddle.app.model.GeneralModel
import com.hiddle.app.retrofit.RetrofitRestClient
import com.hiddle.app.util.AppUtils
import com.hiddle.app.util.SharedPreferenceManager
import kotlinx.android.synthetic.main.activity_login_problem.*
import kotlinx.android.synthetic.main.activity_login_problem.btnContinue
import kotlinx.android.synthetic.main.activity_login_problem.ivBack
import kotlinx.android.synthetic.main.activity_login_problem.tvMobile
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketTimeoutException
import java.util.*
import kotlin.collections.HashMap

class LoginProblemScreenActivity : BaseMainActivity(), View.OnClickListener {

    var paramEmail: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_problem)


        allClicklistener()
    }

    private fun allClicklistener() {
        ivBack.setOnClickListener(this)
        btnSend.setOnClickListener(this)
        btnContinue.setOnClickListener(this)
    }


    override fun onClick(view: View) {
        when (view.id) {
            R.id.ivBack -> {
                ivBack.isEnabled = false
                finish()
                AppUtils.overridePendingTransitionExit(activity)
            }
            R.id.btnSend -> {
                paramEmail = etEmail.text.toString()
                if (validate()) {
                    callAPIRecoveryEmail()

                }
            }
            R.id.btnContinue -> {
                btnContinue.isEnabled = false
                finish()
                AppUtils.overridePendingTransitionExit(activity)
            }
        }
    }

    private fun callAPIRecoveryEmail() {
        if (AppUtils.isConnectedToInternet(activity)) {
            AppUtils.showProgressDialog(activity)
            val params = HashMap<String, String?>()
            params["email"] = paramEmail

            Log.e("param", params.toString())
            val call: Call<GeneralModel>
            call = RetrofitRestClient.instance!!.loginIssue(params)
            Log.e("token", SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
            call.enqueue(object : Callback<GeneralModel> {
                @SuppressLint("SetTextI18n", "ShowToast")
                override fun onResponse(call: Call<GeneralModel>?, response: Response<GeneralModel>?) {
                    AppUtils.hideProgressDialog()
                    val basicModel: GeneralModel
                    if (response!!.isSuccessful)
                    {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {
                            showThankyouView()
                        }
                        else if(Objects.requireNonNull(basicModel).code == "-1")
                        {
                            callLogoutFunction()
                        }else {
                            showSnackBar(activity, basicModel.message)
                        }
                    }
                   else {
                        showSnackBar(activity, response.message())
                    }
                }

                override fun onFailure(call: Call<GeneralModel>?, t: Throwable?) {
                    AppUtils.hideProgressDialog()
                    if (t is SocketTimeoutException) {
                        showSnackBar(activity, getString(R.string.connection_timeout))
                    } else if (t is NetworkErrorException) {
                        showSnackBar(activity, getString(R.string.network_error))
                    } else if (t is AuthenticatorException) {
                        showSnackBar(activity, getString(R.string.authentiation_error))
                    } else {
                        showSnackBar(activity, getString(R.string.something_went_wrong))
                    }
                }
            })
        } else {
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }
    }

    private fun showThankyouView() {
        llLoginProblem.visibility = View.GONE
        llThankyou.visibility = View.VISIBLE
        setAnimationForVerifyButton()
    }

    private fun setAnimationForVerifyButton() {
        val ani: Animation = TranslateAnimation(Animation.ABSOLUTE.toFloat(), Animation.ABSOLUTE.toFloat(), 150F, Animation.ABSOLUTE.toFloat())
        ani.duration = 1400
        ani.fillAfter = true
        llThankyou.startAnimation(ani)
    }

    private fun validate(): Boolean {
        if (paramEmail.isEmpty()) {
            showSnackBar(activity, getString(R.string.please_enter_email))
            return false
        }
        if (!AppUtils.isEmailValid(paramEmail)) {
            showSnackBar(activity, getString(R.string.please_enter_valid_email))
            return false
        }
        return true
    }

    override fun onResume() {
        super.onResume()

        ivBack.isEnabled = true
        btnContinue.isEnabled = true

    }
}
