package com.hiddle.app.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import com.hiddle.app.R
import com.hiddle.app.util.AppUtils
import com.hiddle.app.util.SharedPreferenceManager
import kotlinx.android.synthetic.main.activity_setting.*

class SettingActivity : BaseMainActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)

        SharedPreferenceManager.getMySharedPreferences()!!.setNotificationUser("")
        allClicklistener()
    }

    private fun allClicklistener() {
        ivBackSetting.setOnClickListener(this)
        rlNotication.setOnClickListener(this)
        rlSupport.setOnClickListener(this)
        rlNotication.setOnClickListener(this)
        rlContactUs.setOnClickListener(this)
        rlPreference.setOnClickListener(this)
        rlAccount.setOnClickListener(this)
        rlNotice.setOnClickListener(this)
        rlPrivacy.setOnClickListener(this)
        rlTerms.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.rlNotication -> {
                rlNotication.isEnabled = false
                val i = Intent(activity, NotificationActivity::class.java)
                startActivity(i)
                AppUtils.overridePendingTransitionEnter(activity)
            }
            R.id.rlSupport -> {

            }
            R.id.rlContactUs -> {
                rlContactUs.isEnabled = false
                val i = Intent(activity, ContactUsActivity::class.java)
                startActivity(i)
                AppUtils.overridePendingTransitionEnter(activity)
            }
            R.id.rlPreference -> {
                rlPreference.isEnabled = false
                val i = Intent(activity, PreferenceActivity::class.java)
                startActivity(i)
                AppUtils.overridePendingTransitionEnter(activity)
                finish()
            }
            R.id.rlAccount -> {
                rlAccount.isEnabled = false
                val i = Intent(activity, AccountActivity::class.java)
                startActivity(i)
                AppUtils.overridePendingTransitionEnter(activity)
            }
            R.id.rlNotice -> {
            }
            R.id.rlPrivacy -> {
                rlPrivacy.isEnabled = false
                val i = Intent(activity, WebviewActivity::class.java)
                startActivity(i)
                AppUtils.overridePendingTransitionEnter(activity)
            }
            R.id.rlTerms -> {
                rlTerms.isEnabled = false
                val i = Intent(activity, WebviewActivity::class.java)
                startActivity(i)
                AppUtils.overridePendingTransitionEnter(activity)
            }
            R.id.ivBackSetting -> {
                ivBackSetting.isEnabled = false
                //redirect to home because if change language for activity refrsh
               callMethodBackpress()
            }
        }
    }
    override fun onResume() {
        super.onResume()
        rlNotication.isEnabled = true
        rlContactUs.isEnabled = true
        rlPreference.isEnabled = true
        rlAccount.isEnabled = true
        rlPrivacy.isEnabled = true
        rlTerms.isEnabled = true
        ivBackSetting.isEnabled = true
    }
    private fun callMethodBackpress() {
        finish()
        AppUtils.overridePendingTransitionExit(activity)
       /* val i = Intent(activity, HomeActivity::class.java)
        i.putExtra("type", "profile")
        startActivity(i)
        AppUtils.overridePendingTransitionExit(activity)
        finish()*/
    }

    override fun onBackPressed() {
        super.onBackPressed()
        callMethodBackpress()
    }
}
