package com.hiddle.app.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View

import com.hiddle.app.R
import com.hiddle.app.util.AppUtils
import kotlinx.android.synthetic.main.activity_add_picture.*
import kotlinx.android.synthetic.main.activity_gender_selection.*
import kotlin.collections.ArrayList

class GenderSelectionActivity : BaseMainActivity(), View.OnClickListener {

    private var paramGender: String = ""
    var paramEmail: String = ""
    var paramUserName: String = ""
    private var isFromLogin: String = ""
    private var socialId: String = ""
    private var authorizationToken: String = ""
    private var uploadImagelist: ArrayList<String> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gender_selection)

        getIntentData()
        allClicklistener()
    }

    private fun getIntentData() {
        var i = intent
        if (i != null) {
            paramUserName = i.getStringExtra("paramUserName")!!
            paramEmail = i.getStringExtra("paramEmail")!!
            isFromLogin = i.getStringExtra("isFromLogin")!!
            socialId = i.getStringExtra("socialId")!!
            authorizationToken = i.getStringExtra("authorizationToken")!!
            uploadImagelist = i.getStringArrayListExtra("uploadImagelist")!!

            Log.e("gender_name", paramUserName)
            Log.e("gener_size",uploadImagelist.size.toString())

        }
    }

    private fun allClicklistener() {
        ivBack.setOnClickListener(this)
        ivMan.setOnClickListener(this)
        ivWoman.setOnClickListener(this)
        llBackGender.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.ivBack -> {
                ivBack.isEnabled = false
                llBackGender.isEnabled = false
                finish()
                AppUtils.overridePendingTransitionExit(activity)
            }
            R.id.llBackGender -> {
                ivBack.isEnabled = false
                llBackGender.isEnabled = false
                finish()
                AppUtils.overridePendingTransitionExit(activity)
            }
            R.id.ivMan -> {
                ivMan.isEnabled = false
                paramGender = "Man"
                callLayoutAge()

            }
            R.id.ivWoman -> {
                ivWoman.isEnabled = false
                paramGender = "Woman"
                callLayoutAge()

            }

        }
    }
    override fun onResume() {
        super.onResume()
        ivBack.isEnabled = true
        llBackGender.isEnabled = true
        ivMan.isEnabled = true
        ivWoman.isEnabled = true
    }

    private fun callLayoutAge() {
        val i = Intent(activity, MyAgeActivity::class.java)
        i.putExtra("paramUserName", paramUserName)
        i.putExtra("paramEmail", paramEmail)
        i.putExtra("isFromLogin", isFromLogin)
        i.putExtra("socialId", socialId)
        i.putExtra("authorizationToken", authorizationToken)
        i.putExtra("paramGender", paramGender)
        i.putStringArrayListExtra("uploadImagelist", uploadImagelist)
        startActivity(i)
        AppUtils.overridePendingTransitionEnterDown(activity)
    }
}
