package com.hiddle.app.activity

import android.accounts.AuthenticatorException
import android.accounts.NetworkErrorException
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.ContextThemeWrapper
import android.view.View
import android.widget.Toast
import com.hiddle.app.R
import com.hiddle.app.model.GeneralModel
import com.hiddle.app.retrofit.RetrofitRestClient
import com.hiddle.app.util.AppUtils
import com.hiddle.app.util.SharedPreferenceManager
import kotlinx.android.synthetic.main.activity_account.*
import kotlinx.android.synthetic.main.activity_preference.*
import kotlinx.android.synthetic.main.layout_back.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketTimeoutException
import java.util.*

class  AccountActivity : BaseMainActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account)

        allClicklistener()
    }

    private fun allClicklistener() {
        rlSignout.setOnClickListener(this)
        rlAddRecoveyEmail.setOnClickListener(this)
        rlChangeRecoveryEmail.setOnClickListener(this)
        rlChangePhone.setOnClickListener(this)
        rlDeleteAccount.setOnClickListener(this)
        ivBackAccount.setOnClickListener(this)
        llBackAccount.setOnClickListener(this)
    }

    override fun onResume() {
        super.onResume()
        rlAddRecoveyEmail.isEnabled = true
        rlChangeRecoveryEmail.isEnabled = true
        rlChangePhone.isEnabled = true
        ivBackAccount.isEnabled = true
        llBackAccount.isEnabled = true
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.rlSignout -> {
                val builder1 = AlertDialog.Builder(ContextThemeWrapper(activity, R.style.AlertDialogCustom))
                builder1.setMessage(getString(R.string.are_you_sure_logout))
                builder1.setCancelable(true)
                builder1.setPositiveButton(getString(R.string.yes)) { dialog, id ->
                    callAPIOnlineOfflineStatusChange("signOut")  //call online and offline API

                    dialog.cancel()
                }
                builder1.setNegativeButton(getString(R.string.no)) { dialog, id -> dialog.cancel() }
                val alert11 = builder1.create()
                alert11.show()

            }
            R.id.rlAddRecoveyEmail -> {
                rlAddRecoveyEmail.isEnabled = false
                val i = Intent(activity, AddRecoveryEmailActivity::class.java)
                startActivity(i)
                AppUtils.overridePendingTransitionEnter(activity)
            }
            R.id.rlChangeRecoveryEmail -> {
                rlChangeRecoveryEmail.isEnabled = false
                val i = Intent(activity, ChangeRecoveryEmailActivity::class.java)
                startActivity(i)
                AppUtils.overridePendingTransitionEnter(activity)
            }
            R.id.rlChangePhone -> {
                rlChangePhone.isEnabled = false
                val i = Intent(activity, ChangePhoneNumberActivity::class.java)
                startActivity(i)
                AppUtils.overridePendingTransitionEnter(activity)
            }
            R.id.rlDeleteAccount -> {
                val builder1 = AlertDialog.Builder(ContextThemeWrapper(activity, R.style.AlertDialogCustom))
                builder1.setMessage(getString(R.string.are_you_sure_delete_account))
                builder1.setCancelable(true)
                builder1.setPositiveButton(getString(R.string.yes)) { dialog, id ->
                    callAPIOnlineOfflineStatusChange("deleteAccount") //call online and offline API
                    dialog.cancel()
                }
                builder1.setNegativeButton(getString(R.string.cancel)) { dialog, id -> dialog.cancel() }
                val alert11 = builder1.create()
                alert11.show()
            }
            R.id.ivBackAccount -> {
                ivBackAccount.isEnabled = false
                llBackAccount.isEnabled = false
                callBackPress()
            }
            R.id.llBackAccount -> {
                ivBackAccount.isEnabled = false
                llBackAccount.isEnabled = false
                callBackPress()
            }
        }
    }

    private fun callBackPress() {
        finish()
        AppUtils.overridePendingTransitionExit(activity)
    }

    private fun callDeletAccountAPI() {
        if (AppUtils.isConnectedToInternet(activity)) {
            AppUtils.showProgressDialog(activity)
            val params = HashMap<String, String?>()
            params["nothing"] = ""

            val call: Call<GeneralModel>
            call = RetrofitRestClient.instance!!.deleteAccount(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, params)
            Log.e("token", SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
            call.enqueue(object : Callback<GeneralModel> {
                @SuppressLint("SetTextI18n")
                override fun onResponse(call: Call<GeneralModel>?, response: Response<GeneralModel>?) {
                    AppUtils.hideProgressDialog()
                    val basicModel: GeneralModel
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {
                            Toast.makeText(activity, getString(R.string.your_account_deleted_successfully), Toast.LENGTH_SHORT).show()
                            callLogoutFunction()

                        } else if (Objects.requireNonNull(basicModel).code == "-1") {
                            callLogoutFunction()
                        } else {

                            showSnackBar(activity, basicModel.message)
                        }
                    } else {

                        showSnackBar(activity, response.message())
                    }
                }

                override fun onFailure(call: Call<GeneralModel>?, t: Throwable?) {
                    AppUtils.hideProgressDialog()

                    if (t is SocketTimeoutException) {
                        showSnackBar(activity, getString(R.string.connection_timeout))
                    } else if (t is NetworkErrorException) {
                        showSnackBar(activity, getString(R.string.network_error))
                    } else if (t is AuthenticatorException) {
                        showSnackBar(activity, getString(R.string.authentiation_error))
                    } else {
                        showSnackBar(activity, getString(R.string.something_went_wrong))
                    }
                }
            })
        } else {
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }
    }

    private fun callSignOutAPI() {
        if (AppUtils.isConnectedToInternet(activity)) {
            AppUtils.showProgressDialog(activity)
            val params = HashMap<String, String?>()
            params["user_id"] = SharedPreferenceManager.getMySharedPreferences()!!.getUserId()

            val call: Call<GeneralModel>
            call = RetrofitRestClient.instance!!.getLogOut(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, params)
            Log.e("token", SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
            call.enqueue(object : Callback<GeneralModel> {
                @SuppressLint("SetTextI18n")
                override fun onResponse(call: Call<GeneralModel>?, response: Response<GeneralModel>?) {
                    AppUtils.hideProgressDialog()
                    val basicModel: GeneralModel
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {
                            callLogoutFunction()
                        } else if (Objects.requireNonNull(basicModel).code == "-1") {
                            callLogoutFunction()
                        } else {
                            //callLogoutFunction()
                            showSnackBar(activity, basicModel.message)
                        }
                    } else {
                        //callLogoutFunction()
                        showSnackBar(activity, response.message())
                    }
                }

                override fun onFailure(call: Call<GeneralModel>?, t: Throwable?) {
                    AppUtils.hideProgressDialog()
                    //callLogoutFunction()
                    if (t is SocketTimeoutException) {
                        showSnackBar(activity, getString(R.string.connection_timeout))
                    } else if (t is NetworkErrorException) {
                        showSnackBar(activity, getString(R.string.network_error))
                    } else if (t is AuthenticatorException) {
                        showSnackBar(activity, getString(R.string.authentiation_error))
                    } else {
                        showSnackBar(activity, getString(R.string.something_went_wrong))
                    }
                }
            })
        } else {
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }
    }

    private fun callAPIOnlineOfflineStatusChange(isFrom: String) {
        if (AppUtils.isConnectedToInternet(this)) {
            AppUtils.showProgressDialog(activity)
            val params = HashMap<String, String?>()
            //for offline process
            params["status"] = "0"
            Log.e("param_online", params.toString())

            val call: Call<GeneralModel>
            call = RetrofitRestClient.instance!!.changeStatusAPI(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, params)
            Log.e("token", SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
            call.enqueue(object : Callback<GeneralModel> {
                @SuppressLint("SetTextI18n")
                override fun onResponse(call: Call<GeneralModel>?, response: Response<GeneralModel>?) {
                    val basicModel: GeneralModel
                    AppUtils.hideProgressDialog()
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {
                            AppUtils.hideProgressDialog()
                            if (isFrom == "signOut") {
                                callSignOutAPI() //api for log out
                            }
                            if (isFrom == "deleteAccount") {
                                callDeletAccountAPI() //api for log out
                            }
                            Log.e("success_online", basicModel.toString() + "message" + basicModel.message)
                        } else if (Objects.requireNonNull(basicModel).code == "-1") {
                            AppUtils.hideProgressDialog()
                           callLogoutFunction()
                        } else {
                            AppUtils.hideProgressDialog()
                            showSnackBar(activity,getString(R.string.something_went_wrong))
                        }
                    } else {
                        AppUtils.hideProgressDialog()
                        showSnackBar(activity,getString(R.string.something_went_wrong))
                    }
                }

                override fun onFailure(call: Call<GeneralModel>?, t: Throwable?) {
                    AppUtils.hideProgressDialog()
                    //if fail then again call
                    //for call online and offline status
                    showSnackBar(activity,getString(R.string.something_went_wrong))
                    /*if (SharedPreferenceManager.getMySharedPreferences()!!.isLogin()) {
                        callAPIOnlineOfflineStatusChange(isFrom)
                    }*/

                }
            })
        } else {
            Log.e("no_internet", "no_internet")
        }
    }

}
