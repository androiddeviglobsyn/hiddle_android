package com.hiddle.app.activity

import android.accounts.AuthenticatorException
import android.accounts.NetworkErrorException
import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.ContextThemeWrapper
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.hiddle.app.R
import com.hiddle.app.`interface`.ClickDistance
import com.hiddle.app.`interface`.ClickonLanguage
import com.hiddle.app.adapter.DistanceAdapter
import com.hiddle.app.adapter.LanguageAdapter
import com.hiddle.app.dialog.DistanceDialog
import com.hiddle.app.dialog.LanguageDialog
import com.hiddle.app.model.DistanceModeel
import com.hiddle.app.model.PreferenceModel.PreferenceResponse

import com.hiddle.app.retrofit.RetrofitRestClient
import com.hiddle.app.util.AppUtils
import com.hiddle.app.util.SharedPreferenceManager
import kotlinx.android.synthetic.main.activity_contact_us.*
import kotlinx.android.synthetic.main.activity_preference.*
import kotlinx.android.synthetic.main.custom_dialog_distance.*
import kotlinx.android.synthetic.main.custom_dialog_language.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketTimeoutException
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class PreferenceActivity : BaseMainActivity(), View.OnClickListener, ClickonLanguage, ClickDistance {
    var paramAge: String = ""
    var paramLocation: String = ""
    var paramLanguage: String = ""
    var paramDistance: String = ""
    var languageList: List<String> = ArrayList()/*listOf("English", "Spanish", "German", getString(R.string.french), "Russian", "Italian", "Dutch", getString(R.string.turkish))*/
    var distanceList: ArrayList<DistanceModeel> = ArrayList()
    var distanceAdapter: DistanceAdapter? = null
    var languageAdapter: LanguageAdapter? = null
    var customDialog: LanguageDialog? = null
    var distanceDialog: DistanceDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_preference)

        //for set default tag for switch
        swAge.tag = "NO"
        swLocation.tag = "NO"
        addDataList()
        callPreferenceAPI("GET", "No")   //api for get data
        callSwitchCheckMethod()
        allClicklistener()

    }


    private fun addDataList() {
        languageList = listOf(getString(R.string.english), getString(R.string.french), getString(R.string.german), getString(R.string.spanish), getString(R.string.italian), getString(R.string.Portuguese), getString(R.string.Albanian), getString(R.string.turkish))

        addParameterinDistanceList()

        /*  distanceList=listOf("Km",getString(R.string.mile)*//*"Mile"*//*)*/

        //languageList =listOf("English", "Spanish", getString(R.string.german), getString(R.string.french), "Russian", "Italian", "Dutch", getString(R.string.turkish))

        // distanceList = listOf("Km", "Mile")
    }

    private fun addParameterinDistanceList() {
        val distanceModeel: DistanceModeel = DistanceModeel()
        distanceModeel.distance_language = getString(R.string.paramKm)
        distanceModeel.distance_parameter = "Km"
        distanceList.add(distanceModeel)

        val distanceModeel1: DistanceModeel = DistanceModeel()
        distanceModeel1.distance_language = getString(R.string.mile)
        distanceModeel1.distance_parameter = "Mile"
        distanceList.add(distanceModeel1)

    }

    private fun callSwitchCheckMethod() {
        // Add Touch listener.
        swAge.setOnTouchListener(View.OnTouchListener { v, event ->
            swAge.tag = "YES"
            false
        })
        swAge.setOnCheckedChangeListener { buttonView, isChecked ->
            if (swAge.tag == "YES") {
                callPreferenceAPI("SET", "Age")
            }
        }

        swLocation.setOnTouchListener(View.OnTouchListener { v, event ->
            swLocation.tag = "YES"
            false
        })

        swLocation.setOnCheckedChangeListener { buttonView, isChecked ->
            if (swLocation.tag == "YES") {
                callPreferenceAPI("SET", "Location")
            }
        }
    }

    private fun allClicklistener() {
        ivBackPreference.setOnClickListener(this)
        llBackPreference.setOnClickListener(this)
        rlLanguage.setOnClickListener(this)
        rlDistance.setOnClickListener(this)
        rlBlock.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.ivBackPreference -> {
                ivBackPreference.isEnabled = false
                llBackPreference.isEnabled = false
                onBackPressed()
            }
            R.id.llBackPreference -> {
                ivBackPreference.isEnabled = false
                llBackPreference.isEnabled = false
                onBackPressed()
            }
            R.id.rlLanguage -> {
                customDialog = LanguageDialog(activity)
                customDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                customDialog!!.show()

                setLanguageAdapter()
            }
            R.id.rlDistance -> {
                distanceDialog = DistanceDialog(activity)
                distanceDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                distanceDialog!!.show()

                setDistanceAdapter()
            }
            R.id.rlBlock -> {
                rlBlock.isEnabled = false
                val i = Intent(activity, BlockActivity::class.java)
                startActivity(i)
                AppUtils.overridePendingTransitionEnter(activity)

            }

        }
    }

    override fun onResume() {
        super.onResume()
        ivBackPreference.isEnabled = true
        llBackPreference.isEnabled = true
        rlBlock.isEnabled = true
    }


    private fun setDistanceAdapter() {
        setParameterDistance()
        distanceDialog!!.rvDistance.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false) //set adapter
        distanceAdapter = DistanceAdapter(activity, distanceList, paramDistance, this@PreferenceActivity)
        distanceDialog!!.rvDistance.adapter = distanceAdapter
    }

    private fun setParameterDistance() {
        if (paramDistance == "Mile" || paramDistance == "Meilen" || paramDistance == "Millas" || paramDistance == "Miglia" || paramDistance == "Milhas" || paramDistance == "Milje" || paramDistance == "Mil") {
            paramDistance = "Mile"
        }
    }

    private fun setLanguageAdapter() {
        customDialog!!.rvLanguage.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false) //set adapter
        languageAdapter = LanguageAdapter(activity, languageList, paramLanguage, this@PreferenceActivity)
        customDialog!!.rvLanguage.adapter = languageAdapter

    }

    private fun callPreferenceAPI(isFrom: String, isClick: String) {
        if (AppUtils.isConnectedToInternet(activity)) {

            val params = HashMap<String, String?>()
            if (isFrom == "GET") {
                AppUtils.showProgressDialog(activity)

            }
            if (isFrom == "SET") {

                if (isClick == "Language") {
                    AppUtils.showProgressDialog(activity)
                    params["language"] = paramLanguage
                }

                if (isClick == "Age") {
                    paramAge = if (swAge.isChecked) {
                        "1"
                    } else {
                        "0"
                    }
                    params["show_age"] = paramAge

                }
                if (isClick == "Location") {
                    paramLocation = if (swLocation.isChecked) {
                        "1"
                    } else {
                        "0"
                    }
                    params["share_location"] = paramLocation
                }
                if (isClick == "Distance") {
                    AppUtils.showProgressDialog(activity)
                    params["distance_in"] = paramDistance
                }
            }


            val call: Call<PreferenceResponse>
            call = RetrofitRestClient.instance!!.callPreferenceAPI(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, params)
            call.enqueue(object : Callback<PreferenceResponse> {
                override fun onResponse(call: Call<PreferenceResponse>?, response: Response<PreferenceResponse>?) {
                    AppUtils.hideProgressDialog()
                    val basicModel: PreferenceResponse
                    Log.e("Response", response.toString())
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {
                            if (basicModel.data!!.language != null) {
                                tvLanguage.text = basicModel.data.language!!.toLowerCase(Locale.getDefault()).capitalize()
                                paramLanguage = basicModel.data.language
                                SharedPreferenceManager.getMySharedPreferences()!!.setLanguahe(paramLanguage)
                            }

                            if (basicModel.data.distanceIn != null) {
                                paramDistance = basicModel.data.distanceIn!!
                                setParameterDistance()
                                if (paramDistance == "Mile") {
                                    tvDistanceValue.text = getString(R.string.mile)
                                } else {
                                    tvDistanceValue.text = getString(R.string.paramKm)
                                }
                                SharedPreferenceManager.getMySharedPreferences()!!.setDistance(paramDistance)
                            }


                            //for set language adapter
                            if (isClick == "Language") {
                                //setLanguageAdapter()
                                SharedPreferenceManager.getMySharedPreferences()!!.setLanguahe(paramLanguage)

                                // Toast.makeText(activity, getString(R.string.langauge_change_successffully), Toast.LENGTH_SHORT).show()
                                if (paramLanguage == getString(R.string.english)) { //english

                                    setLanguage("en")
                                    callHomeActivity()
                                    //  Toast.makeText(activity, "Language changed successfully", Toast.LENGTH_LONG).show()
                                }
                                if (paramLanguage == getString(R.string.turkish)) {
                                    setLanguage("tr")
                                    callHomeActivity()
                                    //   Toast.makeText(activity, "Dil başarıyla değiştirildi", Toast.LENGTH_LONG).show()
                                }
                                if (paramLanguage == getString(R.string.french)) {
                                    setLanguage("fr")
                                    callHomeActivity()
                                    // Toast.makeText(activity, "La langue a changé avec succès", Toast.LENGTH_LONG).show()
                                }
                                if (paramLanguage == getString(R.string.german)) {
                                    setLanguage("de")
                                    callHomeActivity()
                                    // Toast.makeText(activity, "Sprache erfolgreich geändert", Toast.LENGTH_LONG).show()
                                }
                                if (paramLanguage == getString(R.string.spanish)) {
                                    setLanguage("gsw")
                                    callHomeActivity()
                                    //  Toast.makeText(activity, "Idioma cambiado exitosamente", Toast.LENGTH_LONG).show()
                                }
                                if (paramLanguage == getString(R.string.italian)) {
                                    setLanguage("it")
                                    callHomeActivity()
                                    //  Toast.makeText(activity, "La lingua è cambiata correttamente", Toast.LENGTH_LONG).show()
                                }
                                if (paramLanguage == getString(R.string.Portuguese)) {
                                    setLanguage("pt")
                                    callHomeActivity()
                                    // Toast.makeText(activity, "Idioma alterado com sucesso", Toast.LENGTH_LONG).show()
                                }
                                if (paramLanguage == getString(R.string.Albanian)) {
                                    setLanguage("sq")
                                    callHomeActivity()
                                    //  Toast.makeText(activity, "Gjuha ndryshoi me sukses", Toast.LENGTH_LONG).show()
                                }


                            }


                            //for set distance adapter
                            if (isClick == "Distance") {
                                Toast.makeText(activity, getString(R.string.distance_change_successffully), Toast.LENGTH_SHORT).show()
                                setDistanceAdapter()
                                distanceDialog!!.dismiss()
                            }


                            //strike
                            if (isClick == "Age" || isClick == "No") {
                                if (basicModel.data.showAge != null) {
                                    if (basicModel.data.showAge == "1") {
                                        swAge.isChecked = true
                                    } else {
                                        swAge.isChecked = false
                                    }
                                }
                            }


                            if (isClick == "Location" || isClick == "No") {
                                if (basicModel.data.shareLocation != null) {
                                    if (basicModel.data.shareLocation == "1") {
                                        swLocation.isChecked = true
                                    } else {
                                        swLocation.isChecked = false
                                    }
                                }
                            }


                        } else if (Objects.requireNonNull(basicModel).code == "-1") {
                            callLogoutFunction()
                        } else {
                            callResultFail(isClick)
                            showSnackBar(activity, basicModel.message)
                            callResultFail(isClick)
                        }
                    } else {
                        callResultFail(isClick)
                        showSnackBar(activity, response.message())
                    }
                }

                override fun onFailure(call: Call<PreferenceResponse>?, t: Throwable?) {
                    callResultFail(isClick)
                    Log.e("callError", call.toString())
                    Log.e("Error", t.toString())
                    AppUtils.hideProgressDialog()
                    if (t is SocketTimeoutException) {
                        showSnackBar(activity, getString(R.string.connection_timeout))
                    } else if (t is NetworkErrorException) {
                        showSnackBar(activity, getString(R.string.network_error))
                    } else if (t is AuthenticatorException) {
                        showSnackBar(activity, getString(R.string.authentiation_error))
                    } else {
                        showSnackBar(activity, getString(R.string.something_went_wrong))
                    }
                }
            })


        } else {
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }


    }

    private fun callHomeActivity() {
        val i = Intent(activity, HomeActivity::class.java)
        i.putExtra("type", "home")
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        i.flags = Intent.FLAG_ACTIVITY_NO_HISTORY
        startActivity(i)
        finish()
        AppUtils.overridePendingTransitionEnter(activity)

    }

    private fun callResultFail(click: String) {
        if (click == "Language") {
            customDialog!!.dismiss()
        }
        if (click == "Distance") {
            distanceDialog!!.dismiss()
        }

        if (click == "Age") {
            if (paramAge == "1") {
                swAge.isChecked = false
            } else {
                swAge.isChecked = true
            }
        }
        if (click == "Location") {
            if (paramLocation == "1") {
                swLocation.isChecked = false
            } else {
                swLocation.isChecked = true
            }
        }
    }

    override fun clickOnLanguage(position: Int) {

        paramLanguage = languageList[position]
        customDialog!!.dismiss()
        OpenDialogChangeLanguage()

    }

    private fun OpenDialogChangeLanguage() {
        val builder1 = AlertDialog.Builder(ContextThemeWrapper(activity, R.style.AlertDialogCustom))
        builder1.setMessage(getString(R.string.restart_app_change_language))
        builder1.setCancelable(true)
        builder1.setPositiveButton(getString(R.string.ok)) { dialog, id ->
            callPreferenceAPI("SET", "Language")
            dialog.cancel()
        }
        builder1.setNegativeButton(getString(R.string.cancel)) { dialog, id ->
            customDialog!!.dismiss()
            dialog.cancel()
        }
        val alert11 = builder1.create()
        alert11.show()
    }

    override fun clickonDistance(position: Int) {
        paramDistance = distanceList[position].distance_parameter
        callPreferenceAPI("SET", "Distance")
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val i = Intent(activity, SettingActivity::class.java)
        i.putExtra("type", "profile")
        startActivity(i)
        AppUtils.overridePendingTransitionExit(activity)
    }
}
