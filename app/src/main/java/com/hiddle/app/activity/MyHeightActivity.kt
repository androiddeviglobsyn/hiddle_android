package com.hiddle.app.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import com.hiddle.app.R
import com.hiddle.app.util.AppUtils
import kotlinx.android.synthetic.main.activity_my_age.*
import kotlinx.android.synthetic.main.activity_my_height.*
import kotlin.collections.ArrayList

class MyHeightActivity : BaseMainActivity(), View.OnClickListener {
    var paramHeight: String = "1.1"
    var paramAge: String = ""
    var paramEmail: String = ""
    var paramUserName: String = ""
    private var isFromLogin: String = ""
    private var socialId: String = ""
    private var authorizationToken: String = ""
    private var paramGender: String = ""
    private var uploadImagelist: ArrayList<String> = ArrayList()
    var myIntArray = arrayOfNulls<String>(111)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_height)

        getIntentData()
        numberPickerHeightMethod()
        allClicklistener()
    }

    private fun getIntentData() {
        val i = intent
        if (i != null) {
            paramUserName = i.getStringExtra("paramUserName")!!
            paramEmail = i.getStringExtra("paramEmail")!!
            isFromLogin = i.getStringExtra("isFromLogin")!!
            socialId = i.getStringExtra("socialId")!!
            authorizationToken = i.getStringExtra("authorizationToken")!!
            paramGender = i.getStringExtra("paramGender")!!
            paramAge = i.getStringExtra("paramAge")!!
            uploadImagelist = i.getStringArrayListExtra("uploadImagelist")!!
            Log.e("height_image", uploadImagelist.get(0))
            Log.e("height_name", paramUserName)
        }
    }


    private fun allClicklistener() {
        ivBackHeight.setOnClickListener(this)
        llBackHeight.setOnClickListener(this)
        llNextHeight.setOnClickListener(this)
    }

    private fun numberPickerHeightMethod() {
        number_picker_height.minValue = 1// for set min and max value for height
        number_picker_height.maxValue = myIntArray.size

        for (i in 1..220) {  // add static data
            if (i >= 110) {
                var a = ((i / 100.0).toString())

                if (a.length == 3) {
                    a = a + "0" + " " + "m" /*getString(R.string.meter)*/
                } else {
                    a = a + " " + "m" /*getString(R.string.meter)*/
                }
                myIntArray[i - 110] = a
            }

        }
        number_picker_height.displayedValues = myIntArray    //set array here for add as list

        number_picker_height.setOnValueChangedListener { numberPicker, i, i1 ->
            //method for scroll
            // val valuePicker1: Int = number_picker_height.value
            //paramHeight = (i1 / 100.0).toString()
            paramHeight = (myIntArray[i1 - 1]!!).replace("m", "", true)
            // Log.e("Value", paramHeight + "jkjk" + myIntArray[i1 - 1])
            //Toast.makeText(activity, paramHeight, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.ivBackHeight -> {
                ivBackHeight.isEnabled = false
                finish()
                AppUtils.overridePendingTransitionExit(activity)
            }
            R.id.llBackHeight -> {
                llBackHeight.isEnabled = false
                finish()
                AppUtils.overridePendingTransitionExit(activity)
            }
            R.id.llNextHeight -> {
                llNextHeight.isEnabled = false
                paramHeight = paramHeight.replace(getString(R.string.meter), "")

                val i = Intent(activity, MyOriginActivity::class.java)
                i.putExtra("isFrom", "myHeight")
                i.putExtra("paramUserName", paramUserName)
                i.putExtra("paramEmail", paramEmail)
                i.putExtra("isFromLogin", isFromLogin)
                i.putExtra("socialId", socialId)
                i.putExtra("authorizationToken", authorizationToken)
                i.putExtra("paramGender", paramGender)
                i.putExtra("paramAge", paramAge)
                i.putExtra("paramHeight", paramHeight)
                i.putStringArrayListExtra("uploadImagelist", uploadImagelist)
                startActivity(i)
                AppUtils.overridePendingTransitionEnterDown(activity)
            }
        }
    }
    override fun onResume() {
        super.onResume()
        ivBackHeight.isEnabled = true
        llBackHeight.isEnabled = true
        llNextHeight.isEnabled = true
    }
}
