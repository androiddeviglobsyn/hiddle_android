package com.hiddle.app.activity

import android.Manifest
import android.accounts.AuthenticatorException
import android.accounts.NetworkErrorException
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.ActivityInfo
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.ViewTreeObserver
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.hiddle.app.R
import com.hiddle.app.adapter.ChatAdapter
import com.hiddle.app.adapter.GalleryAdapter
import com.hiddle.app.chat.MessageResponse
import com.hiddle.app.chat.MyApplication.Companion.mSocket
import com.hiddle.app.model.GeneralModel
import com.hiddle.app.model.PersonModel.PersonResponse
import com.hiddle.app.model.UserModel.UserImageModel
import com.hiddle.app.pagination.EndlessRecyclerOnScrollListener
import com.hiddle.app.retrofit.RetrofitRestClient
import com.hiddle.app.service.OfflineOnlineService
import com.hiddle.app.sqllitedatabase.ChatClass
import com.hiddle.app.sqllitedatabase.DatabaseChatHandler
import com.hiddle.app.util.AppUtils
import com.hiddle.app.util.SharedPreferenceManager
import com.picker.gallery.model.interactor.GalleryPicker
import droidninja.filepicker.FilePickerBuilder
import droidninja.filepicker.FilePickerConst
import droidninja.filepicker.utils.ContentUriUtils
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions.KeyboardListener
import io.socket.client.Socket
import io.socket.emitter.Emitter
import kotlinx.android.synthetic.main.activity_chat.*
import kotlinx.android.synthetic.main.bottom_sheet_image.view.*
import kotlinx.android.synthetic.main.item_type_indicator.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import pub.devrel.easypermissions.EasyPermissions
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*
import java.net.SocketTimeoutException
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.util.*
import kotlin.collections.ArrayList


class ChatActivity : BaseMainActivity(), View.OnClickListener, EasyPermissions.PermissionCallbacks {
    //10,8,9,3

    //sender 5,10,3
    var emojAction: EmojIconActions? = null
    var chatAdapter: ChatAdapter? = null
    private val photoPaths: ArrayList<Uri> = ArrayList()
    private val MAX_ATTACHMENT_COUNT = 4
    private val CUSTOM_REQUEST_CODE = 532
    private val CUSTOM_REQUEST_CODE_VIDEO = 534
    var isFrom: String = ""
    var isFromActivity: String = ""
    var galleyImagelist: ArrayList<String> = ArrayList()
    private var pagelist = ArrayList<String>()
    var itemCount = 0

    //socket chat
    private var mTyping: Boolean = false
    private var mUsername: String? = ""
    private var isConnected: Boolean? = null
    private var messageList = ArrayList<MessageResponse>()
    private var mainmessageList = ArrayList<MessageResponse>()
    var messageResponse: MessageResponse? = null
    private val TAG = "ChatActivity"
    var fromOtherUSerId = ""
    var to_user_id: String = ""
    var to_user_name: String = ""
    var user_image: String = ""
    var isFirst: Boolean = false
    var isFirstTime: Boolean = false
    var imageAdapter: GalleryAdapter? = null
    var baseUrl: String = ""
    var imageProfilelist: ArrayList<UserImageModel> = ArrayList()
    var layoutManager: LinearLayoutManager? = null
    var databaseHandler: DatabaseChatHandler? = null
    var endlessRecyclerOnScrollListener: EndlessRecyclerOnScrollListener? = null
    var mOffset: Int = 1
    var is_first_last_page_load: Boolean = false


    var chatjoin: Emitter.Listener = Emitter.Listener { args ->
        Log.e("chatjoin", "args$args")
        runOnUiThread {
            val array = args[0] as JSONArray
            messageList.clear()
            for (i in 0 until array.length()) {
                var o: JSONObject? = null
                try {
                    o = array.get(i) as JSONObject
                    val msg = MessageResponse()
                    Log.e("response1", o.toString())
                    Log.e("response", msg.toString())
                    msg.from_id = o.getString("from_id")
                    msg.to_id = o.getString("to_id")
                    msg.message = o.getString("message")
                    msg.message = o.getString("message")
                    //msg.sent_by = o.getString("sent_by")
                    msg.createdAt = o.getString("createdAt")
                    messageList.add(msg)

                    if (!pagelist.contains("1")) {
                        pagelist.add("1")
                    }

                    Log.e("message", msg.message + "size" + messageList.size.toString())

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }

            addMessage(messageList)
            //store offline chat when internet not connected  and server eror
            if (AppUtils.isConnectedToInternet(activity)) {
                /*if (mainmessageList.size > 0) {
                    addChatOfflineStorage()
                }*/
            }
            if (mOffset == 1) {
                if (mainmessageList.size > 0) {
                    callAPIReadMessage()
                }
            }

            if (messageList.size == 20) {
                mOffset = mOffset + 1;

            } else {
                is_first_last_page_load = true
                pbChat.visibility = View.GONE
            }

        }
    }

    private fun callAPIReadMessage() {
        if (AppUtils.isConnectedToInternet(activity)) {

            val params = HashMap<String, String?>()
            params["from_id"] = fromOtherUSerId  //means self user id
            params["to_id"] = to_user_id
            Log.e("param", params.toString())

            val call: Call<GeneralModel>
            call = RetrofitRestClient.instance!!.readMessageUpdateAPI(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, params)
            Log.e("token", SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
            call.enqueue(object : Callback<GeneralModel> {
                @SuppressLint("SetTextI18n", "ShowToast")
                override fun onResponse(call: Call<GeneralModel>?, response: Response<GeneralModel>?) {
                    val basicModel: GeneralModel
                    AppUtils.hideProgressDialog()
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {
                            AppUtils.hideProgressDialog()

                        } else if (Objects.requireNonNull(basicModel).code == "-1") {
                            callLogoutFunction()
                        } else {
                            callAPIReadMessage()
                            AppUtils.hideProgressDialog()
                            /*  showSnackBar(activity, basicModel.message)*/
                        }
                    } else {
                        callAPIReadMessage()
                        AppUtils.hideProgressDialog()
                        /* showSnackBar(activity, response.message())*/
                    }
                    AppUtils.hideProgressDialog()
                }

                override fun onFailure(call: Call<GeneralModel>?, t: Throwable?) {
                    callAPIReadMessage()
                    AppUtils.hideProgressDialog()
                    Log.e("message3", "fail")
                    if (t is SocketTimeoutException) {
                        Log.e("error", getString(R.string.connection_timeout))
                        // showSnackBar(activity, getString(R.string.connection_timeout))
                    } else if (t is NetworkErrorException) {
                        Log.e("error", getString(R.string.network_error))
                        //showSnackBar(activity, getString(R.string.network_error))
                    } else if (t is AuthenticatorException) {
                        Log.e("error", getString(R.string.authentiation_error))
                        //showSnackBar(activity, getString(R.string.authentiation_error))
                    } else {
                        Log.e("error", getString(R.string.something_went_wrong))
                        //showSnackBar(activity, getString(R.string.something_went_wrong))
                    }
                }
            })
        } else {
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }
    }

    private fun addChatOfflineStorage() {
        databaseHandler = DatabaseChatHandler(applicationContext)

        try {
            if (databaseHandler!!.checkRecordExitChat(to_user_id, SharedPreferenceManager.getMySharedPreferences()!!.getUserId()!!)) {
                //first delete all Record
                Log.e("delete_exit", "delete_exit")
                databaseHandler!!.deleteAllReecordChat(to_user_id, SharedPreferenceManager.getMySharedPreferences()!!.getUserId()!!) //for delete all recoed
                //for delete all
            }

            //insert all recoed
            for (i in 0..mainmessageList.size - 1) {
                if (databaseHandler!!.checkRecordExitCreatedDateChat(to_user_id, mainmessageList[i].createdAt!!, SharedPreferenceManager.getMySharedPreferences()!!.getUserId()!!)) {
                    //first delete all Record
                    Log.e("delete", "delete")
                    // databaseHandler!!.deleteAllReecordCreatedDate(to_user_id, mainmessageList[i].createdAt!!, SharedPreferenceManager.getMySharedPreferences()!!.getUserId()!!) //for delete all recoed
                    //for delete all
                } else {
                    if (mainmessageList.size > 0) {
                        var sent = ""
                        var messageId = ""
                        if (mainmessageList[i].sent_by == null) {
                            sent = ""
                        } else {
                            sent = mainmessageList[i].sent_by!!
                        }
                        if (mainmessageList[i].id == null) {
                            messageId = ""
                        } else {
                            messageId = mainmessageList[i].id!!
                        }
                        val status = databaseHandler!!.addChat(ChatClass(SharedPreferenceManager.getMySharedPreferences()!!.getUserId(), to_user_id,
                                mainmessageList[i].from_id, "", mainmessageList[i].to_id, mainmessageList[i].createdAt, messageId, mainmessageList[i].message))

                        if (status > -1) {
                            //Log.e("chat_save", "Save")
                        }
                    }
                }
            }
        } catch (e: Exception) {
            Log.e("chat_offline_error", "chat_insert_error" + "check" + e.toString())
        }
    }

    private fun viewAllChatByIdOffline() {
        if (mainmessageList.size == 0) {
            mainmessageList.clear()
            if (databaseHandler!!.viewChatMessageId(to_user_id, SharedPreferenceManager.getMySharedPreferences()!!.getUserId()!!) != null) {
                //creating the instance of DatabaseHandler class
                //calling the viewEmployee method of DatabaseHandler class to read the records
                val emp: List<ChatClass> = databaseHandler!!.viewChatMessageId(to_user_id, SharedPreferenceManager.getMySharedPreferences()!!.getUserId()!!)
                var index = 0
                for (e in emp) {

                    Log.e("user_id11", e.createdAt!!)
                    Log.e("user_name11", e.message!!)

                    messageResponse = MessageResponse()
                    messageResponse!!.from_id = e.from_id
                    messageResponse!!.sent_by = e.sent_by
                    messageResponse!!.to_id = e.to_id
                    messageResponse!!.createdAt = e.createdAt
                    messageResponse!!.id = e.id
                    messageResponse!!.message = e.message

                    mainmessageList.add(messageResponse!!)
                    index++
                }
                setChatAdapterAndInitializeSocket()
                rvChat.scrollToPosition(mainmessageList.size - 1)

            } else {

            }
        }


    }


    //handler
    private lateinit var timer: Timer
    private val noDelay = 0L
    private val everyFiveSeconds = 10000L  //16 second  //10000  10second

    companion object {   // for make static
        const val RC_ACCESSS_READ_STORAGE: Int = 126
        const val RC_WRITE_STORAGE: Int = 127
        const val RC_STORAGE_PERMISSION: Int = 128
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)


        runOnUiThread {
            try {
                callSocketAndClickMethod()
            } catch (e: Exception) {

            }
        }
        setUserProfileData()
        callPagination()


    }

    private fun callGalleryAdapter() {
        val images = GalleryPicker(activity).getImages()
        val videos = GalleryPicker(activity).getVideos()
        Log.e("RESULT", "IMAGES COUNT: ${images.size}\nVIDEOS COUNT: ${videos.size}")

        for (i in images.indices) {
            if (!galleyImagelist.contains(images[i].DATA)) {
                galleyImagelist.add(images[i].DATA!!)
            }
        }
        rvGallery!!.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false) //set adapter
        imageAdapter = GalleryAdapter(activity, galleyImagelist)
        rvGallery!!.adapter = imageAdapter

        GalleryAdapter.gallerylist.clear()

    }

    private fun getIntentData() {

        //chat url is in My application class

        val i = intent
        if (i != null) {
            to_user_id = i.getStringExtra("to_user_id")!!

            SharedPreferenceManager.getMySharedPreferences()!!.setNotificationUser(to_user_id)
            to_user_name = i.getStringExtra("to_user_name")!!
            user_image = i.getStringExtra("user_image")!!
            isFromActivity = i.getStringExtra("isFrom")!!

            tvProfileName.text = to_user_name
            if (user_image != null) {
                Glide.with(activity).load(user_image).placeholder(R.drawable.ic_placeholder_icn).into(ivUserImage)
            }
            Log.e("user_id_chat", fromOtherUSerId + "other" + to_user_id)
            Log.e("Name", to_user_name + "id" + to_user_id)
        }


    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun setSocketChat() {
        setUpUI()
        setChatAdapterAndInitializeSocket()
        getFirstPageOfSocketChat()  //for socket chat in list

    }

    private fun getFirstPageOfSocketChat() {

        if (!pagelist.contains("1")) {
            val jsonObject1 = JSONObject()
            jsonObject1.put("from_id", fromOtherUSerId)
            jsonObject1.put("to_id", to_user_id)
            jsonObject1.put("offset", 1)
            jsonObject1.put("limit", 20)
            Log.e("data_offset", jsonObject1.toString())
            mSocket?.emit("get_chat", jsonObject1)
        }

        Log.e("data", "data7")
        val linearLayout: LinearLayout = findViewById(R.id.rootChat)
        linearLayout.viewTreeObserver
                .addOnGlobalLayoutListener(ViewTreeObserver.OnGlobalLayoutListener {
                    val heightDiff = linearLayout.rootView.height - linearLayout.height
                    // IF height diff is more then 150, consider keyboard as visible.
                    if (heightDiff < 150) {
                        if (mTyping) {
                            mTyping = false
                            val jsonObject1 = JSONObject()
                            try {
                                jsonObject1.put("from_id", fromOtherUSerId)
                                jsonObject1.put("username", SharedPreferenceManager.getMySharedPreferences()!!.getUserName())
                                jsonObject1.put("to_id", to_user_id)
                            } catch (e: JSONException) {
                                e.printStackTrace()
                            }
                            mSocket?.emit("stop_typing", jsonObject1)
                        }
                    }
                })
    }

    private fun setChatAdapterAndInitializeSocket() {
        //add ArrayList() in adapter so we can add item one by one
        try {
            layoutManager = LinearLayoutManager(this@ChatActivity)
            rvChat?.layoutManager = layoutManager

            chatAdapter = ChatAdapter(activity, ArrayList(), SharedPreferenceManager.getMySharedPreferences()!!.getUserId())
            rvChat?.adapter = chatAdapter
            rvChat?.isNestedScrollingEnabled = false;

            setTypingViewAsItemInChatList()  //set typing view

            // rvChat?.setHasFixedSize(false);
        } catch (e: Exception) {
            Log.e("chat_error", "setAdapter")
        }

        initializeSocket()  //initialize socketChat

    }

    private fun setTypingViewAsItemInChatList() {
        val messageResponse = MessageResponse()
        messageResponse.from_id = ""
        messageResponse.to_id = ""
        messageResponse.message = ""
        messageResponse.isChatTypeVisible = "N"
        mainmessageList.add(messageResponse)

        var sList = ArrayList<MessageResponse>()
        sList.clear()
        sList.add(messageResponse)
        chatAdapter!!.addNewItems(sList);

        callHideChatView("N")
    }

    private fun connectSocket() {
        /* if (!mSocket?.connected()!!) {*/
        mSocket?.on(Socket.EVENT_CONNECT, onConnect)
        mSocket?.on(Socket.EVENT_CONNECT_ERROR, onConnectError)
        mSocket?.on("change_username", roomJoin)
        if (!mSocket?.connected()!!) {
            mSocket?.connect()  //for socket connect
        }
        /*  }*/
    }

    @RequiresApi(Build.VERSION_CODES.O)
    @SuppressLint("NewApi")
    fun initializeSocket() {
        mSocket?.on(Socket.EVENT_DISCONNECT, onDisconnect)
        mSocket?.on(Socket.EVENT_CONNECT_ERROR, onConnectError)
        mSocket?.on("new_message", onNewMessage)
        mSocket?.on("typing", onTyping)
        mSocket?.on("stop_typing", onStopTyping)
        mSocket?.on("get_chat", chatjoin)
    }


    private fun allClicklistener() {
        ivBackChat.setOnClickListener(this)
        ivSend.setOnClickListener(this)
        ivPlus.setOnClickListener(this)
        ivUserImage.setOnClickListener(this)
        tvProfileName.setOnClickListener(this)
        llBackChat.setOnClickListener(this)
        //ivCloseChat.setOnClickListener(this)
    }

    private fun setEmojiEdittext() {
        emojAction = EmojIconActions(activity, rootChat, etMessageChat, ivEmoji)
        emojAction!!.ShowEmojIcon()
        emojAction!!.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.ic_emoji)
        emojAction!!.setKeyboardListener(object : KeyboardListener {
            override fun onKeyboardOpen() {}
            override fun onKeyboardClose() {}
        })
    }

    override fun onPause() {
        super.onPause()
        timer.cancel() //for stop handelr every 15 second
        timer.purge()
    }

    override fun onResume() {
        super.onResume()
        checkLoginState()  //if user is not login

    }

    private fun setUserProfileData() {
        callUserProfileAPI()  //API for name and online and offline status
        callTimerForUsrProfileAPI()
    }

    private fun callPagination() {
        rvChat.isNestedScrollingEnabled = false
        (rvChat.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false

        // pagination for socket chat
        rvChat.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!pagelist.contains(mOffset.toString())) {
                    var itemno = layoutManager!!.findFirstVisibleItemPosition()
                    if (itemno == 0) {
                        if (is_first_last_page_load == false) {
                            if (mOffset != 1) {
                                pbChat.visibility = View.VISIBLE
                                callpaginationChat(mOffset)
                            }
                        }
                        if (is_first_last_page_load == true) {
                            pbChat.visibility = View.GONE
                        }

                    }
                }


            }
        })
    }

    private fun callTimerForUsrProfileAPI() {
        val timerTask = object : TimerTask() { //handler for call API every 10 second if new user crossed
            override fun run() {
                Log.e("handler", "APIcallUsinghandler")
                callUserProfileAPI()
            }
        }
        timer = Timer()
        timer.schedule(timerTask, noDelay, everyFiveSeconds)
    }

    private fun callpaginationChat(offset: Int) {

        pagelist.add(offset.toString())
        if (!is_first_last_page_load) {
            val jsonObject1 = JSONObject()
            try {
                jsonObject1.put("from_id", fromOtherUSerId)
                jsonObject1.put("to_id", to_user_id)
                jsonObject1.put("offset", mOffset)
                jsonObject1.put("limit", 20)
                Log.e("data_offset1", jsonObject1.toString())
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            mSocket?.emit("get_chat", jsonObject1)
        } else {

        }
    }


    private fun checkLoginState() {
        if (!SharedPreferenceManager.getMySharedPreferences()!!.isLogin()) {
            callLogoutFunction()
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun callSocketAndClickMethod() {

        AppUtils.showProgressDialog(activity)

        try {
            databaseHandler = DatabaseChatHandler(getApplicationContext())
            fromOtherUSerId = SharedPreferenceManager.getMySharedPreferences()!!.getUserId()!!

            // callGalleryAdapter()
            //Url for socket chat is put in MyApplication class
            getIntentData()
            setEmojiEdittext()
            allClicklistener()

            if (!AppUtils.isConnectedToInternet(activity)) { //when internet not connected
                //offline storage
                /* viewAllChatByIdOffline()*/
                AppUtils.showToast(activity, getString(R.string.no_internet_connection))
                Log.e("No_internet", "No internet")
            } else {
                //when internet connected
                //service for pass online and offline data
                try {
                    startService(Intent(baseContext, OfflineOnlineService::class.java)) //for online and offline
                } catch (e: Exception) {
                }
                connectSocket()
                setSocketChat()
            }
        } catch (e: Exception) {
            Log.e("error", e.message.toString())
        }
    }

    @SuppressLint("NewApi")
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onClick(view: View) {
        when (view.id) {
            R.id.ivBackChat -> {
                callBackClickEvent()
            }
            R.id.llBackChat -> {
                callBackClickEvent()
            }
            /* R.id.ivCloseChat -> {
                 finish()
                 AppUtils.overridePendingTransitionExit(activity)
             }*/
            R.id.ivSend -> {
                if (AppUtils.isConnectedToInternet(activity)) { //when internet not connected
                    if (!isFirst) {
                        if (etMessageChat.text.toString().isEmpty()) {
                            //Toast.makeText(activity, getString(R.string.please_enter_txt), Toast.LENGTH_SHORT).show()
                        } else {
                            attemptSend()
                            // etMessageChat.setText("")
                            // AppUtils.hideSoftKeyboard(activity)
                        }
                    } else {
                        showServerErrorMessage()
                    }
                } else {
                    //showSnackBar(activity,getString(R.string.no_internet_connection))
                    AppUtils.showToast(activity, getString(R.string.no_internet_connection))
                    Log.e("No_internet", "No internet")
                }
            }
            R.id.ivPlus -> {
                callDialogImagePicker()
            }
            R.id.ivUserImage -> {
                callProfileScreen()
            }
            R.id.tvProfileName -> {
                callProfileScreen()
            }
        }
    }


    private fun callProfileScreen() {
        val myactivity = Intent(activity, UserProfileActivity::class.java)
        myactivity.putExtra("userId", to_user_id)
        myactivity.putExtra("userImage", user_image)
        myactivity.putExtra("isFrom", "other")
        startActivity(myactivity)
        AppUtils.overridePendingTransitionEnter(activity)
    }

    private fun callBackClickEvent() {
        try {
            if (isFromActivity == "notification") {
                SharedPreferenceManager.getMySharedPreferences()!!.setNotificationUser("")
                val i = Intent(activity, HomeActivity::class.java)
                i.putExtra("type", "home")
                i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(i)
                finish()
                AppUtils.overridePendingTransitionExit(activity)
            } else {
                callAPIReadMessage()
                SharedPreferenceManager.getMySharedPreferences()!!.setNotificationUser("")
                finish()
                AppUtils.overridePendingTransitionExit(activity)
            }
        } catch (e: Exception) {

        }
    }

    private fun callDialogImagePicker() {
        //Open bottom sheet dialog
        val dialog = BottomSheetDialog(this)
        val bottomSheet = layoutInflater.inflate(R.layout.bottom_sheet_image, null)
        bottomSheet.tvVideo.setOnClickListener {
            isFrom = "video"
            getPermission()
            dialog.dismiss()
        }

        bottomSheet.tvImage.setOnClickListener {
            isFrom = "image"
            getPermission()
            dialog.dismiss()
        }
        dialog.setContentView(bottomSheet)
        dialog.show()
    }

    private fun getPermission() {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            if (isFrom == "image") {
                selectMultipleImage()
            }
            if (isFrom == "video") {
                selectMultipleVideo()
            }
        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.rationale_call), RC_ACCESSS_READ_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE);
        }
    }


    private fun selectMultipleVideo() {
        if ((photoPaths.size) == MAX_ATTACHMENT_COUNT) {
            Toast.makeText(this, "Cannot select more than " + MAX_ATTACHMENT_COUNT + " items",
                    Toast.LENGTH_SHORT).show();
        } else {
            photoPaths.clear();

            FilePickerBuilder.instance
                    .setMaxCount(MAX_ATTACHMENT_COUNT)
                    //.setSelectedFiles(photoPaths)
                    .setActivityTheme(R.style.FilePickerTheme)
                    .setActivityTitle("Please select media")
                    .enableVideoPicker(true)
                    .enableCameraSupport(false)
                    .showGifs(false)
                    .showFolderView(true)
                    .enableSelectAll(false)
                    .enableImagePicker(false)
                    .withOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                    .pickPhoto(activity, CUSTOM_REQUEST_CODE_VIDEO);
        }
    }

    private fun selectMultipleImage() {

        if ((photoPaths.size) == MAX_ATTACHMENT_COUNT) {
            Toast.makeText(this, "Cannot select more than " + MAX_ATTACHMENT_COUNT + " items",
                    Toast.LENGTH_SHORT).show();
        } else {
            photoPaths.clear();

            FilePickerBuilder.instance
                    .setMaxCount(MAX_ATTACHMENT_COUNT)
                    //.setSelectedFiles(photoPaths)
                    .setActivityTheme(R.style.FilePickerTheme)
                    .setActivityTitle("Please select media")
                    .enableVideoPicker(false)
                    .enableCameraSupport(false)
                    .showGifs(false)
                    .showFolderView(true)
                    .enableSelectAll(false)
                    .enableImagePicker(true)
                    .withOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                    .pickPhoto(this, CUSTOM_REQUEST_CODE);
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CUSTOM_REQUEST_CODE && resultCode == RESULT_OK && data != null) {

            photoPaths.addAll(data.getParcelableArrayListExtra<Uri>(FilePickerConst.KEY_SELECTED_MEDIA)!!);
            val path = ContentUriUtils.getFilePath(activity, photoPaths.get(0));
            Toast.makeText(this, path, Toast.LENGTH_SHORT).show()

        } else if (requestCode == CUSTOM_REQUEST_CODE_VIDEO && resultCode == RESULT_OK && data != null) {
            photoPaths.addAll(data.getParcelableArrayListExtra<Uri>(FilePickerConst.KEY_SELECTED_MEDIA)!!);
            Toast.makeText(this, "Num of files selected: " + photoPaths.size.toString(), Toast.LENGTH_SHORT).show()
        }
        /*  else if (resultCode == REQUEST_RESULT_CODE && data != null) {
              val mediaList = data.getParcelableArrayListExtra<GalleryData>("MEDIA")
              Log.e("SELECTED MEDIA", mediaList.toString())
          }*/
        else {
            //  showSnackBar(activity, getString(R.string.you_have_not_picked_any_pick))
        }

    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        getPermission()
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        getPermission()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)

    }

    //for socket coonect
    private val onConnectError = Emitter.Listener {
        if (!isFirst) {
            /*viewAllChatByIdOffline()*/  //when server not connected show offline chat
            showServerErrorMessage()
        }
        runOnUiThread {
            Log.e("LoginActivity", "error connecting" + it[0])
            isFirst = true
        }

    }

    private fun showServerErrorMessage() {
        AppUtils.hideProgressDialog()
        showSnackBar(activity, getString(R.string.server_not_connected))
    }

    private val onConnect = Emitter.Listener {
        if (mSocket?.connected()!!) {
            Log.e("LoginActivity", "onConnect true")
            isFirst = false
            val jsonObject = JSONObject()
            try {
                jsonObject.put("from_id", fromOtherUSerId)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            mSocket?.emit("change_username", jsonObject)

        } else {
            Log.e("LoginActivity", "onConnect Failure")
        }
    }
    private var roomJoin: Emitter.Listener = Emitter.Listener {
        if (mSocket?.connected()!!) {
            Log.e("data", "data1")
        } else {
            Log.e("LoginActivity", "onConnect Failure")
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onDestroy() {
        super.onDestroy()
        mSocket!!.disconnect();

        mSocket?.off(Socket.EVENT_CONNECT, onConnect)
        mSocket?.off(Socket.EVENT_CONNECT_ERROR, onConnectError)
        mSocket?.off("change_username", roomJoin)

        mSocket!!.off(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket?.off("new_message", onNewMessage)
        mSocket?.off("typing", onTyping)
        mSocket?.off("stop_typing", onStopTyping)
        mSocket?.off("get_chat", chatjoin)


    }

    //new socket coonect
    internal fun addMessage(messageList: java.util.ArrayList<MessageResponse>) {

        var sList = ArrayList<MessageResponse>()
        sList.clear()
        var convertlist = ArrayList<MessageResponse>()
        convertlist.clear()
        for (i in messageList.size - 1 downTo 0) {
            itemCount++;
            val messageResponse = MessageResponse()
            messageResponse.from_id = messageList[i].from_id
            messageResponse.to_id = messageList[i].to_id
            messageResponse.message = messageList[i].message
            messageResponse.createdAt = messageList[i].createdAt

            convertlist.add(messageResponse)

            Log.e("data", "data10")
            /*messageList.add(j, messageResponse)*/

            // chatAdapter!!.notifyItemInserted(0)
        }

        for (i in convertlist.indices) {
            val messageResponse = MessageResponse()
            messageResponse.from_id = messageList[i].from_id
            messageResponse.to_id = messageList[i].to_id
            messageResponse.message = messageList[i].message
            messageResponse.createdAt = messageList[i].createdAt
            /* mainmessageList.add(0, messageResponse)*/

            sList.add(0, messageResponse)
            /* chatAdapter!!.notifyItemInserted(mainmessageList.size - 1)*/

            mainmessageList.add(messageResponse)

        }

        chatAdapter!!.addItems(sList);
        pbChat.visibility = View.GONE
        AppUtils.hideProgressDialog()

    }

    fun setUpUI() {
        try {
            etMessageChat!!.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                }

                override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                    if (!mSocket?.connected()!!)
                        return
                    if (TextUtils.isEmpty(etMessageChat!!.text))
                        return
                    if (mTyping == false) {
                        mTyping = true
                        val jsonObject = JSONObject()
                        try {
                            jsonObject.put("from_id", fromOtherUSerId)
                            jsonObject.put("username", SharedPreferenceManager.getMySharedPreferences()!!.getUserName())
                            jsonObject.put("to_id", to_user_id)
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }

                        mSocket?.emit("typing", jsonObject)
                    }
                }

                override fun afterTextChanged(editable: Editable) {}
            })
        } catch (e: Exception) {
        }
    }

    private val onDisconnect = Emitter.Listener {
        runOnUiThread {
            isConnected = false
            Log.e(TAG, "onDisconnect")
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private val onNewMessage = Emitter.Listener { args ->
        Log.e(TAG, "onNewMesage")
        runOnUiThread {
            val data = args[0] as JSONObject
            var username: String? = null
            var message: String? = null
            var from_id = ""
            var to_id = ""
            var tdate = ""
            try {
                username = data.getString("username")
                message = data.getString("message")
                from_id = data.getString("from_id")
                to_id = data.getString("to_id")
                //tdate = data.getString("created_at")
                /* val mCurrentDateTime = convertdateformat(tdate, "EEE MMM dd HH:mm:ss z yyyy",
                         "yyyy-MM-dd HH:mm:ss"
                 )*/
/*
                convertdateformat(
                    tdate.toString(), "EEE MMM dd HH:mm:ss z yyyy",
                    "yyyy-MM-dd HH:mm:ss"
                )
*/
                // Log.e("formatdate", mCurrentDateTime)
            } catch (e: JSONException) {
                Log.e(TAG, e.message!!)
                e.printStackTrace()
            }

            if (from_id == to_user_id && to_id == fromOtherUSerId)
                addMessage(from_id, message!!, to_id, "Sender")
        }
        callAPIReadMessage() //if new message come then this API call
        Log.e("data", "data5")
    }
    private val onTyping = Emitter.Listener { args ->
        Log.e(TAG, "onTyping")

        runOnUiThread(Runnable
        {
            val data = args[0] as JSONObject
            val username: String
            val from_id: String
            val to_id: String

            try {
                username = data.getString("username")
                from_id = data.getString("from_id")
                to_id = data.getString("to_id")
            } catch (e: JSONException) {
                Log.e(TAG, e.message!!)
                return@Runnable
            }

            if (from_id == to_user_id && to_id == fromOtherUSerId)
                addTyping(username)

        })
    }

    fun convertdateformat(
            mDate: String,
            fromDateFormate: String,
            toDateFormate: String
    ): String {
        var mDate = mDate
        val mInputDate = SimpleDateFormat(fromDateFormate, Locale.ENGLISH)

        try {
            val date = mInputDate.parse(mDate)
            val mConvertedDate = SimpleDateFormat(toDateFormate, Locale.ENGLISH)
            mConvertedDate.timeZone = TimeZone.getDefault()

            return mConvertedDate.format(date)
        } catch (e: Exception) {
            mDate = "00-00-0000 00:00"
            return mDate
        }
    }

    private val onStopTyping = Emitter.Listener {

        Log.e(TAG, "onStopTyping")
        runOnUiThread { removeTyping() }
    }


    private fun addTyping(username: String) {
        callHideChatView("Y")  //show typingview
        tvType.visibility = View.VISIBLE
        // tvType?.text = "typing..."
        tvType?.text = username.trim { it <= ' ' } + " is typing...."
        var sp: String? = null
        rvChat.scrollToPosition(mainmessageList.size - 1)

    }

    private fun callHideChatView(s: String) {
        Log.e("call1", "call" + mainmessageList.size.toString())

        if (mainmessageList.size > 0) {
            var sList = ArrayList<MessageResponse>()
            sList.clear()
            val messageResponse = MessageResponse()
            messageResponse.from_id = ""
            messageResponse.to_id = ""
            messageResponse.message = ""
            messageResponse.isChatTypeVisible = s
            mainmessageList[mainmessageList.size - 1] = messageResponse
            sList.add(messageResponse)
            chatAdapter!!.updateLaseItems(sList);


        }

        if (mOffset == 1 || s == "Y") {
            rvChat.scrollToPosition(mainmessageList.size - 1)
        }


    }

    /*  private fun checkScrollAtBottom() {
          nsView.viewTreeObserver.addOnScrollChangedListener {
              if (nsView != null) {
                  if (nsView.scrollY === 1) {
                      Lsog.e("scroll", "bottom")
                  } else {
                      Log.e("scroll", "top")
                  }
              }
          }
      }*/

    private fun removeTyping() {
        callHideChatView("N")

        tvType.visibility = View.GONE
        tvType?.text = null
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun attemptSend() {
        if (!mSocket?.connected()!!) return
        val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(this)
        }
        // imm.hideSoftInputFromWindow(view.windowToken, 0)
        if (mTyping) {
            mTyping = false
            val jsonObject1 = JSONObject()
            try {
                jsonObject1.put("from_id", fromOtherUSerId)
                jsonObject1.put("username", mUsername)
                jsonObject1.put("to_id", to_user_id)
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            mSocket?.emit("stop_typing", jsonObject1)
        }
        val message = etMessageChat?.text.toString().trim { it <= ' ' }
        if (TextUtils.isEmpty(message)) {
            etMessageChat?.requestFocus()
            return
        }

        callAPISendNotification(message)  //check notification send API

        etMessageChat?.setText("")
        addMessage(fromOtherUSerId, message, to_user_id, "Receiver")

        // perform the sending message attempt.

        val jsonObject = JSONObject()
        try {
            jsonObject.put("from_id", fromOtherUSerId)
            jsonObject.put("username", mUsername)
            jsonObject.put("message", message)
            jsonObject.put("to_id", to_user_id)

            Log.e("data", "data8")
            // jsonObject.put("image", encodeImage(path)) //for image upload

            //jsonObject.put("created_at", Calendar.getInstance().time.toString())
        } catch (e: JSONException) {
            e.printStackTrace()
            Log.e("Sorry", "Srerver Problem")
        }

        mSocket?.emit("new_message", jsonObject)
        Log.e("data", "data9")
        //        mSocket.emit("peekaboo.chat.",jsonObject);
    }

    private fun callAPISendNotification(message: String) {
        if (AppUtils.isConnectedToInternet(this)) {
            val params = HashMap<String, String?>()

            params["to_id"] = to_user_id
            params["message"] = message

            Log.e("param", params.toString())
            val call: Call<GeneralModel>
            call = RetrofitRestClient.instance!!.callSendAPI(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, params)
            //Log.e("token", SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
            call.enqueue(object : Callback<GeneralModel> {
                @SuppressLint("SetTextI18n", "ShowToast")
                override fun onResponse(call: Call<GeneralModel>?, response: Response<GeneralModel>?) {
                    val basicModel: GeneralModel
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {
                            //Log.e("success", basicModel.message + "message" + message)
                        } else if (Objects.requireNonNull(basicModel).code == "-1") {
                            callLogoutFunction()
                        } else {
                            Log.e("error1", basicModel.message!!)
                        }
                    } else {
                        Log.e("error2", response.message())
                    }
                }

                override fun onFailure(call: Call<GeneralModel>?, t: Throwable?) {
                    callAPISendNotification(message)
                    if (t is SocketTimeoutException) {
                        Log.e("error3", "connection timeout")
                    } else if (t is NetworkErrorException) {
                        Log.e("error3", "network_error")
                    } else if (t is AuthenticatorException) {
                        Log.e("error3", "authentiation_error")
                    } else {
                        Log.e("error3", "something_went_wrong")
                    }
                }
            })
        } else {
            Log.e("error4", "No internet")
        }
    }

    fun encodeImage(filePath: String): String {
        val bytes = File(filePath).readBytes()
        val base64 = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Base64.getEncoder().encodeToString(bytes)
        } else {
            TODO("VERSION.SDK_INT < O")
        }
        return base64
    }

    /*  private fun encodeImage(path:String):String {
          val imagefile = File(path)
          var fis:FileInputStream? = null
          try
          {
              fis = FileInputStream(imagefile)
          }
          catch (e:FileNotFoundException) {
              e.printStackTrace()
          }
          val bm = BitmapFactory.decodeStream(fis)
          val baos = ByteArrayOutputStream()
          bm.compress(Bitmap.CompressFormat.JPEG, 100, baos)
          val b = baos.toByteArray()
          val encImage = Base64.encodeToString(b, Base64.DEFAULT)
          //Base64.de
          return encImage
      }
  */


    @SuppressLint("NewApi")
    @RequiresApi(Build.VERSION_CODES.O)
    private fun addMessage(from_id: String, message: String, to_id: String, from: String) {

        Log.e("data", message + "from" + from_id + "to_id :" + to_id)
        /*  messageList.add(new Message(messageType, username, message));
        mAdapter.notifyItemInserted(messageList.size() - 1);
        scrollUp();*/

        val messageResponse = MessageResponse()
        messageResponse.from_id = from_id
        messageResponse.to_id = to_id
        messageResponse.message = message
        mainmessageList.add(messageResponse)
        Log.e("data", "data10")

        var sList = ArrayList<MessageResponse>()
        sList.clear()
        sList.add(messageResponse)
        chatAdapter!!.addNewItemsNew(sList);

        callHideChatView("N")
        //   chatAdapter?.notifyItemChanged(chatAdapter?.itemCount!! - 1)


        //if get new message and send message store it in offline chat
        val current = LocalDateTime.now()
        Log.e("current", current.toString())

        /* try {
             //check this data is added or not
             if (databaseHandler!!.checkRecordExitCreatedDateChat(to_user_id, current.toString(), SharedPreferenceManager.getMySharedPreferences()!!.getUserId()!!)) {
                 Log.e("record_exist", "record_exist")
             } else {
                 //if record not exist then add data using query

                 val status = databaseHandler!!.addChat(ChatClass(SharedPreferenceManager.getMySharedPreferences()!!.getUserId(), to_user_id,
                         from_id, "", to_id, LocalDateTime.now().toString(), "", message))

             }
         } catch (e: Exception) {
             Log.e("exception", "chat_offline_error")
         }*/
/*
        if (messageList.size > 0) {
            addChatOfflineStorage(messageList.size-1,"new_message")
        }*/


        /* if (from == "Sender") {

         }

         if (from == "Receiver") {*/
        // chatAdapter?.itemCount?.let { rvChat?.smoothScrollToPosition(it) }
        rvChat.scrollToPosition(mainmessageList.size - 1)
        /* }*/


    }

    private fun callUserProfileAPI() {
        if (AppUtils.isConnectedToInternet(activity)) {
            /* if (!isFirstTime) {
                 AppUtils.showProgressDialog(activity)
             }*/
            val params = HashMap<String, String?>()
            params["latitude"] = ""
            params["longitude"] = ""
            /*  params["user_id"] = ""*/
            // Log.e("paramProfile", params.toString())

            val call: Call<PersonResponse>
            call = RetrofitRestClient.instance!!.getProfileByIdForOtherUser(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, to_user_id, params)
            // Log.e("token", SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
            call.enqueue(object : Callback<PersonResponse> {
                @SuppressLint("SetTextI18n")
                override fun onResponse(call: Call<PersonResponse>?, response: Response<PersonResponse>?) {
                    val basicModel: PersonResponse

                    if (response != null) {
                        if (response.isSuccessful) {
                            basicModel = response.body()!!
                            if (Objects.requireNonNull(basicModel).code == "1") {
                                isFirstTime = true
                                /* AppUtils.hideProgressDialog()*/
                                if (to_user_name == "") {
                                    to_user_name = basicModel.data!!.username!!
                                    if (to_user_name != null) {
                                        tvProfileName!!.text = to_user_name
                                    }

                                    baseUrl = basicModel.data.baseUrl!!
                                    imageProfilelist.clear()
                                    try {
                                        if (basicModel.data.images!!.isNotEmpty()) {
                                            imageProfilelist = basicModel.data.images as ArrayList<UserImageModel>
                                        }
                                        if (imageProfilelist.size > 0) {
                                            Glide.with(activity).load(baseUrl + imageProfilelist[0].image.toString()).placeholder(R.drawable.ic_placeholder_icn).into(ivUserImage)
                                        }
                                    } catch (e: Exception) {

                                    }

                                }

                                if (basicModel.data!!.isOnline == "1") {
                                    ivOnline.visibility = View.VISIBLE
                                } else {
                                    ivOnline.visibility = View.INVISIBLE
                                }

                                AppUtils.hideProgressDialog()


                            } else if (Objects.requireNonNull(basicModel).code == "-1") {
                                AppUtils.hideProgressDialog()
                                callLogoutFunction()
                            } else {
                                AppUtils.hideProgressDialog()
                                //showSnackBar(activity, basicModel.message)
                            }
                        } else {
                            AppUtils.hideProgressDialog()
                            // showSnackBar(activity, response.message())
                        }
                    }

                }

                override fun onFailure(call: Call<PersonResponse>?, t: Throwable?) {
                    callUserProfileAPI()
                    AppUtils.hideProgressDialog()
                    if (t is SocketTimeoutException) {
                        // showSnackBar(activity, getString(R.string.connection_timeout))
                    } else if (t is NetworkErrorException) {
                        // showSnackBar(activity, getString(R.string.network_error))
                    } else if (t is AuthenticatorException) {
                        // showSnackBar(activity, getString(R.string.authentiation_error))
                    } else {
                        // showSnackBar(activity, getString(R.string.something_went_wrong))
                    }
                }
            })
        } else {
            //showSnackBar(activity, getString(R.string.no_internet_connection))
            //AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }

    }

    override fun onBackPressed() {
        super.onBackPressed()
        callBackClickEvent()
    }
}


