package com.hiddle.app.activity
import android.app.ActivityOptions
import android.content.Intent
import android.os.Bundle

import android.transition.Slide
import android.view.Gravity
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.hiddle.app.R
import com.hiddle.app.util.Constants

class MainAnimationActivity: AppCompatActivity() {
  private var imgLogo:ImageView ? =null
  private var imgProfilePic:ImageView ?=null
  private var txvShared:TextView?=null
  override fun onCreate(savedInstanceState: Bundle?) {
    window.requestFeature(Window.FEATURE_CONTENT_TRANSITIONS)
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)
            imgLogo = findViewById(R.id.imgSmartherdLogo) as ImageView
            imgProfilePic = findViewById(R.id.imgAnnie) as ImageView
            txvShared = findViewById(R.id.txvSharedElement) as TextView
            setupWindowAnimations()
  }
  private fun setupWindowAnimations() {
    // Re-enter transition is executed when returning back to this activity
    val slideTransition = Slide()
    slideTransition.setSlideEdge(Gravity.LEFT) // Use START if using right - to - left locale
    slideTransition.setDuration(1000)
    getWindow().setReenterTransition(slideTransition) // When MainActivity Re-enter the Screen
    getWindow().setExitTransition(slideTransition) // When MainActivity Exits the Screen
    // For overlap of Re Entering Activity - MainActivity.java and Exiting TransitionActivity.java
    getWindow().setAllowReturnTransitionOverlap(false)
  }
  fun checkRippleAnimation(view:View) {
    val intent = Intent(this,TransitionActivity ::class.java)
    startActivity(intent)
  }
  fun sharedElementTransition(view:View) {

  }
  fun explodeTransitionByCode(view:View) {
    val options = ActivityOptions.makeSceneTransitionAnimation(this)
    val i = Intent(this@MainAnimationActivity, TransitionActivity::class.java)
    i.putExtra(Constants.KEY_ANIM_TYPE, Constants.TransitionType.ExplodeJava)
    i.putExtra(Constants.KEY_TITLE, "Explode By Java")
    startActivity(i, options.toBundle())
  }
  fun explodeTransitionByXML(view:View) {
    val options = ActivityOptions.makeSceneTransitionAnimation(this)
    val i = Intent(this@MainAnimationActivity, TransitionActivity::class.java)
    i.putExtra(Constants.KEY_ANIM_TYPE, Constants.TransitionType.ExplodeXML)
    i.putExtra(Constants.KEY_TITLE, "Explode By Xml")
    startActivity(i, options.toBundle())
  }
  fun slideTransitionByCode(view:View) {
    val options = ActivityOptions.makeSceneTransitionAnimation(this)
    val i = Intent(this@MainAnimationActivity, TransitionActivity::class.java)
    i.putExtra(Constants.KEY_ANIM_TYPE, Constants.TransitionType.SlideJava)
    i.putExtra(Constants.KEY_TITLE, "Slide By Java Code")
    startActivity(i, options.toBundle())
  }
  fun slideTransitionByXML(view:View) {
    val options = ActivityOptions.makeSceneTransitionAnimation(this)
    val i = Intent(this@MainAnimationActivity, TransitionActivity::class.java)
    i.putExtra(Constants.KEY_ANIM_TYPE, Constants.TransitionType.SlideXML)
    i.putExtra(Constants.KEY_TITLE, "Slide By XML")
    startActivity(i, options.toBundle())
  }
  fun fadeTransitionByJava(view:View) {
    val options = ActivityOptions.makeSceneTransitionAnimation(this)
    val i = Intent(this@MainAnimationActivity, TransitionActivity::class.java)
    i.putExtra(Constants.KEY_ANIM_TYPE, Constants.TransitionType.FadeJava)
    i.putExtra(Constants.KEY_TITLE, "Fade By Java")
    startActivity(i, options.toBundle())
  }
  fun fadeTransitionByXML(view:View) {
    val options = ActivityOptions.makeSceneTransitionAnimation(this)
    val i = Intent(this@MainAnimationActivity, TransitionActivity::class.java)
    i.putExtra(Constants.KEY_ANIM_TYPE, Constants.TransitionType.FadeXML)
    i.putExtra(Constants.KEY_TITLE, "Fade By XML")
    startActivity(i, options.toBundle())
  }
  }