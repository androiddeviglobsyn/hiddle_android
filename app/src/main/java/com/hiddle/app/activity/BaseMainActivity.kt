package com.hiddle.app.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.database.Cursor
import android.graphics.*
import android.media.ExifInterface
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import com.hiddle.app.R
import com.hiddle.app.language.LocalizationActivityDelegate
import com.hiddle.app.language.OnLocaleChangedListener
import com.hiddle.app.service.ForegroundService
import com.hiddle.app.service.OfflineOnlineService
import com.hiddle.app.sqllitedatabase.DatabaseChatHandler
import com.hiddle.app.util.AppUtils
import com.hiddle.app.util.SharedPreferenceManager
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.util.*

public open class BaseMainActivity : AppCompatActivity(), OnLocaleChangedListener {
    private val localizationDelegate: LocalizationActivityDelegate = LocalizationActivityDelegate(activity)
    var databaseChatHandler: DatabaseChatHandler? = null

   /* companion object {


        public var dLocale: Locale? = null
    }

    init {
        updateConfig(this)
    }

    fun updateConfig(wrapper: ContextThemeWrapper) {
        if(dLocale==Locale("") ) // Do nothing if dLocale is null
            return

        Locale.setDefault(dLocale)
        val configuration = Configuration()
        configuration.setLocale(dLocale)
        wrapper.applyOverrideConfiguration(configuration)
    }*/

    override fun onCreate(savedInstanceState: Bundle?) {
        localizationDelegate.addOnLocaleChangedListener(this)
        localizationDelegate.onCreate(savedInstanceState)
        super.onCreate(savedInstanceState)
    }
    /**
     * SIMPLE SNACKBAR
     */
    fun showSnackBar(context: Context?, message: String?) {
        val view = (findViewById<View>(android.R.id.content) as ViewGroup).getChildAt(0) as ViewGroup
        val snackbar = Snackbar.make(view, message!!, Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE)
        val viewGroup = snackbar.view as ViewGroup
        viewGroup.setBackgroundColor(ContextCompat.getColor(context!!, R.color.background_tab_grey))
        val viewTv = snackbar.view
        val tv = viewTv.findViewById<TextView>(R.id.snackbar_text)
        tv.setTextColor(ContextCompat.getColor(context, R.color.white))
        tv.maxLines = 5
        snackbar.show()
    }


    val activity: Activity
        get() = this

    override fun onResume() {
        super.onResume()
        localizationDelegate.onResume(this)
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onBackPressed() {
        finish()
        AppUtils.overridePendingTransitionExit(activity)
        super.onBackPressed()
    }


    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(localizationDelegate.attachBaseContext(newBase))
    }

    override fun getApplicationContext(): Context? {
        return localizationDelegate.getApplicationContext(super.getApplicationContext())
    }

    override fun getResources(): Resources? {
        return localizationDelegate.getResources(super.getResources())
    }

    fun setLanguage(language: String?) {
        localizationDelegate.setLanguage(this, language)
    }

    fun setLanguage(locale: Locale?) {
        localizationDelegate.setLanguage(this, locale)
    }

    fun setDefaultLanguage(language: String?) {
        localizationDelegate.setDefaultLanguage(language)
    }

    fun setDefaultLanguage(locale: Locale?) {
        localizationDelegate.setDefaultLanguage(locale)
    }

    fun getCurrentLanguage(): Locale {
        return localizationDelegate.getLanguage(this)
    }

    // Just override method locale change event
    override fun onBeforeLocaleChanged() {}

    override fun onAfterLocaleChanged() {}

    /**
     * SIMPLE DIALOG PROGRESS
     */

    fun getPicturePath(uri: Uri): String {
        val projection = arrayOf<String>(MediaStore.Images.Media.DATA)
        var cursor: Cursor = getContentResolver().query(uri, projection, null, null, null)!!
        Objects.requireNonNull(cursor).moveToFirst()
        var columnIndex: Int = cursor.getColumnIndex(projection[0])
        var path: String = cursor.getString(columnIndex) // returns null
        cursor.close()
        return path
    }

    fun callLogoutFunction() {

        //delete all stored record in offline sqllite
        try {
            databaseChatHandler = DatabaseChatHandler(applicationContext)
            databaseChatHandler!!.deleteAllReecordMessage()
            databaseChatHandler!!.deleteAllReecordChat()
            Log.e("delete_all", "delete_all")

            //for stop location service
            ForegroundService.stopService(this)
            //for online and offline service for API
            stopService(Intent(this, OfflineOnlineService::class.java)) //for online and offline


        } catch (e: Exception) {

        }


        val i = Intent(activity, LoginScreenActivity::class.java)
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        SharedPreferenceManager.getMySharedPreferences()!!.setAuthorizationToken("")
        SharedPreferenceManager.getMySharedPreferences()!!.setUserId("")
        SharedPreferenceManager.getMySharedPreferences()!!.setCountryCode("")
        SharedPreferenceManager.getMySharedPreferences()!!.setLogin(false)
        SharedPreferenceManager.getMySharedPreferences()!!.setMobileNumber("")
        SharedPreferenceManager.getMySharedPreferences()!!.setEmail("")
        SharedPreferenceManager.getMySharedPreferences()!!.setUserName("")
        SharedPreferenceManager.getMySharedPreferences()!!.setProfileImage("")
        SharedPreferenceManager.getMySharedPreferences()!!.setNotificationUser("")
        SharedPreferenceManager.getMySharedPreferences()!!.setSound("1")
        SharedPreferenceManager.getMySharedPreferences()!!.setVibrate("")
        SharedPreferenceManager.getMySharedPreferences()!!.setDistance("")
        startActivity(i)

        finish()
        AppUtils.overridePendingTransitionEnter(activity)
    }

    public fun compressImage(imageUri: String): String {
        val filePath = getRealPathFromURI(imageUri)
        var scaledBitmap: Bitmap? = null
        val options = BitmapFactory.Options()
        // by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
        // you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true
        var bmp = BitmapFactory.decodeFile(filePath, options)
        var actualHeight = options.outHeight
        var actualWidth = options.outWidth
        // max Height and width values of the compressed image is taken as 816x612
        val maxHeight = 816.0f
        val maxWidth = 612.0f
        var imgRatio = (actualWidth / actualHeight).toFloat()
        val maxRatio = maxWidth / maxHeight
        // width and height values are set maintaining the aspect ratio of the image
        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight
                actualWidth = (imgRatio * actualWidth).toInt()
                actualHeight = maxHeight.toInt()
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth
                actualHeight = (imgRatio * actualHeight).toInt()
                actualWidth = maxWidth.toInt()
            } else {
                actualHeight = maxHeight.toInt()
                actualWidth = maxWidth.toInt()
            }
        }
        // setting inSampleSize value allows to load a scaled down version of the original image
        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight)
        // inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false
        // this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true
        options.inInputShareable = true
        options.inTempStorage = ByteArray(16 * 1024)
        try {
            // load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options)
        } catch (exception: Throwable) {
            exception.printStackTrace()
        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888)
        } catch (exception: OutOfMemoryError) {
            exception.printStackTrace()
        }
        val ratioX = actualWidth / options.outWidth.toFloat()
        val ratioY = actualHeight / options.outHeight.toFloat()
        val middleX = actualWidth / 2.0f
        val middleY = actualHeight / 2.0f
        val scaleMatrix = Matrix()
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY)
        val canvas = Canvas(scaledBitmap!!)
        canvas.setMatrix(scaleMatrix)
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, Paint(Paint.FILTER_BITMAP_FLAG))
        // check the rotation of the image and display it properly
        val exif: ExifInterface
        try {
            exif = ExifInterface(filePath)
            val orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0)
            Log.d("EXIF", "Exif: " + orientation)
            val matrix = Matrix()
            if (orientation == 6) {
                matrix.postRotate(90F)
                Log.d("EXIF", "Exif: " + orientation)
            } else if (orientation == 3) {
                matrix.postRotate(180F)
                Log.d("EXIF", "Exif: " + orientation)
            } else if (orientation == 8) {
                matrix.postRotate(270F)
                Log.d("EXIF", "Exif: " + orientation)
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true)
        } catch (e: IOException) {
            e.printStackTrace()
        }
        var out: FileOutputStream? = null
        val filename = getFilename()
        try {
            out = FileOutputStream(filename)
            // write the compressed bitmap at the destination specified by filename.
            scaledBitmap!!.compress(Bitmap.CompressFormat.JPEG, 80, out)
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }
        return filename
    }

    public fun getFilename(): String {
        val file = File(Environment.getExternalStorageDirectory().getPath(), "Hiddle/Images")
        if (!file.exists()) {
            file.mkdirs()
        }
        val uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg")
        return uriSting
    }

    public fun calculateInSampleSize(options: BitmapFactory.Options, reqWidth: Int, reqHeight: Int): Int {
        val height = options.outHeight
        val width = options.outWidth
        var inSampleSize = 1
        if (height > reqHeight || width > reqWidth) {
            val heightRatio = Math.round(height.toFloat() / reqHeight.toFloat())
            val widthRatio = Math.round(width.toFloat() / reqWidth.toFloat())
            inSampleSize = if (heightRatio < widthRatio) heightRatio else widthRatio
        }
        val totalPixels = (width * height).toFloat()
        val totalReqPixelsCap = (reqWidth * reqHeight * 2).toFloat()
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++
        }
        return inSampleSize
    }

    public fun getRealPathFromURI(contentURI: String): String {
        val contentUri = Uri.parse(contentURI)
        val cursor = getContentResolver().query(contentUri, null, null, null, null)
        if (cursor == null) {
            return contentUri.getPath()!!
        } else {
            cursor.moveToFirst()
            val index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
            return cursor.getString(index)
        }
    }
}
