package com.hiddle.app.retrofit

object
RestConstant {

    //local base  url
    const val BASE_URL: String = "http://217.163.23.182:3000/"
    //const val BASE_URL: String = "http://217.163.23.182:5000/"
    //ALL API URL
    const val GET_ACCESS_TOKEN: String = "access_token"

    const val SOCIAL_LOGIN: String = "social_login"/*"fb_login"*/
    //const val INSTA_LOGIN: String = "insta_login"
    const val SEND_OTP: String = "send_code"
    const val VERIFY_CODE: String = "verify_code"
    const val LOGIN_UPDATE: String = "login_update"
    const val GET_PROFILE: String = "myprofile"
    const val GET_PROFILE_BY_ID: String = "get_profile"


    /* const val LOG_OUT: String = "logout_all"*/
    const val LOG_OUT: String = "user_logout"

    const val DELETE_ACCOUNT: String = "delete"
    const val CHANGE_EMAIL: String = "change_email"
    const val CONTACT_US = "contact_us"
    const val ALL_USERS = "all_users"   
    const val MAP_USER = "map_list"
    // const val GET_FITER = "get_filter"
    const val GET_FITER = "get_user_detail"
    const val GET_MAP_FITER = "get_map_filter"
    /* const val ADD_HOME_FITER = "add_filter"*/
    const val ADD_HOME_FITER = "add_user_filter"
    const val ADD_MAP_FITER = "add_map_filter"
    const val CHANGE_NUMBER = "change_number"
    const val REMOVE_IMAGE = "remove_image"
    const val LOGIN_ISSUE = "login_issue"
    const val SET_PROFILE_PICTURE = "make_profile"
    const val LIKE_USER = "like_user"
    const val SKIP_USER = "skip_user"
    const val SUPERLIKE_USER = "superlike_user"
    const val BLOCK_USER = "block_user"
    const val REPORT_USER = "report_user"
    const val BLOCK_LIST = "block_list"
    const val UNBLOCK_USER = "unblock_user"
    const val STRIKE_LIST = "strike_list"
    const val LIKE_LIST = "like_list"
    const val NOTIFICATION = "notification"
    const val REMOVE_STRIKE = "remove_strike"
    const val PREFERENCE = "preference"
    const val NEAR_ME = "near_me"
    const val CHANGE_STATUS = "change_status"
    const val SEND = "send"
    /*const val CHAT_LIST = "chat_list"*/
    const val CHAT_LIST = "user_chat_list"
    const val REMOVE_CHAT = "remove_chat"
    const val READ_MESSAGE = "update_chat"

    //Sharedpreferencemanager
    const val IS_LOGIN: String = "isLogin"
    const val DEVICE_ID: String = "deviceId"
    const val USER_ID: String = "userId"
    const val FCM_TOKEN: String = "fcmToken"
    const val USER_NAME: String = "username"
    const val COUNTRY_CODE: String = "countryCode"
    const val MOBILE_NUMBER: String = "mobileNumber"
    const val AUTHORIZATION_TOKEN: String = "authorizationToken"
    const val EMAIL: String = "email"
    const val PROFILE_IMAGE: String = "profileImage"
    const val LANGUAGE: String = "language"
    const val NOTIFICATON_USER: String = "notificationUser"
    const val SOUND: String = "sound"
    const val VIBRATE: String = "vibrate"
    const val DISTANCE: String = "distance"
}

//https://graph.instagram.com/17841427418357185/?fields=id,media_count,media,username,account_type&user_id=17841427418357185&access_token=IGQVJVeUdMU2Nqa1p6XzVBNVh1YXhpYnhmUlR1RHRHX1RuTW1DZAjZAmRUVncmNPN0dnX2doNnBvNmFJVU1DMlgyZA2ZATOVhDM0xId0tINnRMTjBpZA2dYbkdiU3F0T0RvZA29WamZAfc2ZA1Ynp3UG51SWRUZAGdVZAlN5bV9ITlVN
