package com.hiddle.app.retrofit

import android.util.Log
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitRestClient {
    private const val TIME = 32
    private var baseApiService: ApiInterface? = null
    //Log.d("Response", bodyString);
    private var httpClient: OkHttpClient? = null
        private get() {
            field = OkHttpClient().newBuilder()
                    .connectTimeout(TIME.toLong(), TimeUnit.SECONDS)
                    .readTimeout(TIME.toLong(), TimeUnit.SECONDS)
                    .writeTimeout(TIME.toLong(), TimeUnit.SECONDS)
                    .addNetworkInterceptor { chain ->
                        val response = chain.proceed(chain.request())
                        val body = response.body
                        val bodyString = body!!.string()
                        val contentType = body.contentType()
                        Log.e("Response", bodyString);
                        response.newBuilder().body(ResponseBody.create(contentType, bodyString)).build()
                    }
                    .build()
            return field
        }

    val instance: ApiInterface?
        get() {
            if (baseApiService == null) {
                val retrofit: Retrofit = Retrofit.Builder()
                        .baseUrl(RestConstant.BASE_URL)
                        .addConverterFactory(ToStringConverterFactory())
                        .addConverterFactory(GsonConverterFactory.create())
                        /*.addConverterFactory(ScalarsConverterFactory.create())*/
                        .client(httpClient)
                        .build()
                baseApiService = retrofit.create<ApiInterface>(ApiInterface::class.java)
            }
            return baseApiService
        }
}