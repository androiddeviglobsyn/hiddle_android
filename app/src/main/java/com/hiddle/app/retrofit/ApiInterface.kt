package com.hiddle.app.retrofit

import com.hiddle.app.model.BlockModel.BlockResponse
import com.hiddle.app.model.ChatMessageModel.ChatMessageResponse
import com.hiddle.app.model.FavoriteModel.FavoriteResponse
import com.hiddle.app.model.FbMediaModel.FbMediaResponse
import com.hiddle.app.model.FilterModel.FilterResponse
import com.hiddle.app.model.GeneralModel
import com.hiddle.app.model.HomeFilterModel
import com.hiddle.app.model.InstaMediaModel.InstaMediaResponse
import com.hiddle.app.model.InstagramModel.InstagramAccessToken
import com.hiddle.app.model.InstagramUserDetailModel.InstagramUserDetailModel
import com.hiddle.app.model.LikeModel.LikeResponse
import com.hiddle.app.model.MapFilterModel.MapFilterResponse
import com.hiddle.app.model.NotificationModel.NotificationResponse
import com.hiddle.app.model.PersonModel.PersonResponse
import com.hiddle.app.model.PlaceSearchModel.SearchDataResponseModel
import com.hiddle.app.model.PreferenceModel.PreferenceResponse
import com.hiddle.app.model.ProfileModel.ProfileResponse
import com.hiddle.app.model.SendOTPModel
import com.hiddle.app.model.StrikeModelClass.StrikeResponse
import com.hiddle.app.model.UserModel.UserResponse
import com.hiddle.app.model.UsermapModel.UsermapResponse
import com.hiddle.app.model.VerifyCodeModel.VerifyModelResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*
import java.util.*

interface ApiInterface {

    @GET("place/textsearch/json")// origins/destinations:  LatLng as string
    fun getData(@Query("query") query: String?, @Query("key") key: String?): Call<SearchDataResponseModel?>?

    //GET INSTAGARAM ACCESS TOKEN
    @FormUrlEncoded
    @POST(RestConstant.GET_ACCESS_TOKEN)
    fun getInstagramAceesToken(@FieldMap params: HashMap<String, String?>): Call<InstagramAccessToken>


    @GET("{user_id}")
    fun getInstaUserDetailMethod(@Path("user_id") id: String?, @Query("fields") fields: String?, @Query("access_token") access_token: String?, @Query("user_id") user_id: String?): Call<InstagramUserDetailModel>


    //Get Profile API
    @GET("me/media")
    fun getInstaMedia(@Query("access_token") access_token: String?,@Query("fields") fields: String?,@Query("after") after: String?): Call<InstaMediaResponse>


    @GET("{user_id}/photos")
    fun getFbUserMedia(@Path("user_id") id: String?, @Query("fields") fields: String?, @Query("access_token") access_token: String?, @Query("type") type: String?,@Query("after") after: String?, @Query("before") before: String?): Call<FbMediaResponse>



    //send Otp API
    @FormUrlEncoded
    @POST(RestConstant.VERIFY_CODE)
    fun verifyCodeApi(@FieldMap parameters: HashMap<String, String?>?): Call<VerifyModelResponse>

    //verification code API
    @FormUrlEncoded
    @POST(RestConstant.SEND_OTP)
    fun sendOTPApi(@FieldMap parameters: HashMap<String, String?>?): Call<SendOTPModel>

    //Send OTP  API
    @FormUrlEncoded
    @POST(RestConstant.SOCIAL_LOGIN)
    fun socialLoginApi(@FieldMap parameters: HashMap<String, String?>?): Call<VerifyModelResponse>

    /*//Instagram Login API
    @FormUrlEncoded
    @POST(RestConstant.INSTA_LOGIN)
    fun instagramLoginApi(@FieldMap parameters: HashMap<String, String?>?): Call<VerifyModelResponse>*/

    //Get Profile API
    @GET(RestConstant.GET_PROFILE)
    fun getProfile(@Header("Authorization") authHeader: String): Call<ProfileResponse>

    /* //Get Profile By Id  API
     @FormUrlEncoded
     @POST(RestConstant.GET_PROFILE_BY_ID)
     fun getProfileByIdForOtherUser(@Header("Authorization") authHeader: String,@FieldMap parameters: HashMap<String, String?>?): Call<PersonResponse>
 */
    //Get Profile By Id  API
    @FormUrlEncoded
    @POST(RestConstant.GET_PROFILE_BY_ID + "/" + "{user_id}")
    fun getProfileByIdForOtherUser(@Header("Authorization") authHeader: String, @Path("user_id") id: String?, @FieldMap parameters: HashMap<String, String?>?): Call<PersonResponse>

    //Logout  API
    @FormUrlEncoded
    @POST(RestConstant.LOG_OUT)
    fun getLogOut(@Header("Authorization") authHeader: String, @FieldMap parameters: HashMap<String, String?>?): Call<GeneralModel>

    //Logout  API
    @FormUrlEncoded
    @POST(RestConstant.DELETE_ACCOUNT)
    fun deleteAccount(@Header("Authorization") authHeader: String, @FieldMap parameters: HashMap<String, String?>?): Call<GeneralModel>

    //ADD_CHAT_Image
    @Multipart
    @POST("user/"+RestConstant.LOGIN_UPDATE)
    fun loginUpdateApi(@Header("Authorization") authHeader: String, @PartMap params: HashMap<String, RequestBody?>, @Part partbody: Array<MultipartBody.Part?>): Call<VerifyModelResponse>

    //Change recovery email
    @FormUrlEncoded
    @POST(RestConstant.CHANGE_EMAIL)
    fun changeEmail(@Header("Authorization") authHeader: String, @FieldMap parameters: HashMap<String, String?>?): Call<GeneralModel>

    //Set Profile picture
    @FormUrlEncoded
    @POST(RestConstant.SET_PROFILE_PICTURE)
    fun setProfilePicture(@Header("Authorization") authHeader: String, @FieldMap parameters: HashMap<String, String?>?): Call<GeneralModel>



    //Add Home Filter
   /* @FormUrlEncoded*/
    /*@Headers("Content-Type: application/json")*/
    @POST(RestConstant.ADD_HOME_FITER)
    fun addHomeFiter(@Header("Authorization") authHeader: String,@Body body: HomeFilterModel /*@Body*//*@FieldMap*//* parameters: HashMap<String, String?>?*/): Call<GeneralModel>


    //Add Map Filter
    @FormUrlEncoded
    @POST(RestConstant.ADD_MAP_FITER)
    fun addMapFiter(@Header("Authorization") authHeader: String, @FieldMap parameters: HashMap<String, String?>?): Call<GeneralModel>


    //Change Phone number
    @FormUrlEncoded
    @POST(RestConstant.CHANGE_NUMBER)
    fun changePhoneNumberAPI(@Header("Authorization") authHeader: String, @FieldMap parameters: HashMap<String, String?>?): Call<GeneralModel>


    //Remove Image
    @FormUrlEncoded
    @POST(RestConstant.REMOVE_IMAGE)
    fun removeImage(@Header("Authorization") authHeader: String, @FieldMap parameters: HashMap<String, String?>?): Call<GeneralModel>

    //Login issue
    @FormUrlEncoded
    @POST(RestConstant.LOGIN_ISSUE)
    fun loginIssue(@FieldMap parameters: HashMap<String, String?>?): Call<GeneralModel>


    //Contact us
    @FormUrlEncoded
    @POST(RestConstant.CONTACT_US)
    fun contactUsApi(@FieldMap parameters: HashMap<String, String?>?): Call<GeneralModel>


    //ALL USER
    @FormUrlEncoded
    @POST(RestConstant.ALL_USERS)
    fun callALLUserAPI(@Header("Authorization") authHeader: String, @FieldMap parameters: HashMap<String, String?>?): Call<UserResponse>


    //Map USER
    @GET(RestConstant.MAP_USER)
    fun callALLMapUserAPI(@Header("Authorization") authHeader: String): Call<UsermapResponse>


    /* //GET FILTER
     @GET(RestConstant.GET_FITER)
     fun getFilterAPI(@Header("Authorization") authHeader: String): Call<FilterResponse>*/

    //GET FILTER
    @FormUrlEncoded
    @POST(RestConstant.GET_FITER)
    fun getFilterAPI(@Header("Authorization") authHeader: String,@FieldMap parameters: HashMap<String, String?>?): Call<FilterResponse>


    //GET Map FILTER
    @GET(RestConstant.GET_MAP_FITER)
    fun getFilterMapAPI(@Header("Authorization") authHeader: String): Call<MapFilterResponse>


    //Like User
    @FormUrlEncoded
    @POST(RestConstant.LIKE_USER)
    fun callLikeUser(@Header("Authorization") authHeader: String, @FieldMap parameters: HashMap<String, String?>?): Call<LikeResponse>

    //Skip User
    @FormUrlEncoded
    @POST(RestConstant.SKIP_USER)
    fun callSkipUser(@Header("Authorization") authHeader: String, @FieldMap parameters: HashMap<String, String?>?): Call<LikeResponse>


    //SuperLike User
    @FormUrlEncoded
    @POST(RestConstant.SUPERLIKE_USER)
    fun callSuperLikeUser(@Header("Authorization") authHeader: String, @FieldMap parameters: HashMap<String, String?>?): Call<LikeResponse>

    //Block User
    @FormUrlEncoded
    @POST(RestConstant.BLOCK_USER)
    fun callBlockUser(@Header("Authorization") authHeader: String, @FieldMap parameters: HashMap<String, String?>?): Call<GeneralModel>


    //Report User
    @FormUrlEncoded
    @POST(RestConstant.REPORT_USER)
    fun callReportUser(@Header("Authorization") authHeader: String, @FieldMap parameters: HashMap<String, String?>?): Call<GeneralModel>

    //BLOCK USER LIST
    @GET(RestConstant.BLOCK_LIST)
    fun blockListAPI(@Header("Authorization") authHeader: String): Call<BlockResponse>


    //CHAT_LIST
    @FormUrlEncoded
    @POST(RestConstant.CHAT_LIST)
    fun messageListAPI(@Header("Authorization") authHeader: String, @FieldMap parameters: HashMap<String, String?>?): Call<ChatMessageResponse>

    //STRIKE LIST
    @FormUrlEncoded
    @POST(RestConstant.STRIKE_LIST)
    fun strikeListAPI(@Header("Authorization") authHeader: String, @FieldMap parameters: HashMap<String, String?>?): Call<StrikeResponse>


    //LIKE LIST
    @FormUrlEncoded
    @POST(RestConstant.LIKE_LIST)
    fun likeListAPI(@Header("Authorization") authHeader: String, @FieldMap parameters: HashMap<String, String?>?): Call<FavoriteResponse>


    //UnBlock User
    @FormUrlEncoded
    @POST(RestConstant.UNBLOCK_USER)
    fun callUnBlockUser(@Header("Authorization") authHeader: String, @FieldMap parameters: HashMap<String, String?>?): Call<GeneralModel>

    // Notification
    @FormUrlEncoded
    @POST(RestConstant.NOTIFICATION)
    fun callNotification(@Header("Authorization") authHeader: String, @FieldMap parameters: HashMap<String, String?>?): Call<NotificationResponse>


    // Remove strike
    @FormUrlEncoded
    @POST(RestConstant.REMOVE_STRIKE)
    fun removeStrike(@Header("Authorization") authHeader: String, @FieldMap parameters: HashMap<String, String?>?): Call<GeneralModel>

    // Notification
    @FormUrlEncoded
    @POST(RestConstant.PREFERENCE)
    fun callPreferenceAPI(@Header("Authorization") authHeader: String, @FieldMap parameters: HashMap<String, String?>?): Call<PreferenceResponse>

    // Near me
    @FormUrlEncoded
    @POST(RestConstant.NEAR_ME)
    fun callNearMeAPI(@Header("Authorization") authHeader: String, @FieldMap parameters: HashMap<String, String?>?): Call<GeneralModel>

    //CHANGE_STATUS
    @FormUrlEncoded
    @POST(RestConstant.CHANGE_STATUS)
    fun changeStatusAPI(@Header("Authorization") authHeader: String, @FieldMap parameters: HashMap<String, String?>?): Call<GeneralModel>


    // Send chat notification
    @FormUrlEncoded
    @POST(RestConstant.SEND)
    fun callSendAPI(@Header("Authorization") authHeader: String, @FieldMap parameters: HashMap<String, String?>?): Call<GeneralModel>


    //Remove Message
    @FormUrlEncoded
    @POST(RestConstant.REMOVE_CHAT)
    fun removeMessageListAPI(@Header("Authorization") authHeader: String, @FieldMap parameters: HashMap<String, String?>?): Call<GeneralModel>


    //Read Message
    @FormUrlEncoded
    @POST(RestConstant.READ_MESSAGE)
    fun readMessageUpdateAPI(@Header("Authorization") authHeader: String, @FieldMap parameters: HashMap<String, String?>?): Call<GeneralModel>

}