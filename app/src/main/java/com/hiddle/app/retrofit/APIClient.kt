package com.hiddle.app.retrofit

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

public class APIClient {

    companion object {   // for make static
        @JvmStatic  private var retrofit: Retrofit? = null
        @JvmStatic var base_url_map= "https://maps.googleapis.com/maps/api/"
        @JvmStatic var base_url_insta_accesstoken = "https://api.instagram.com/oauth/"
        @JvmStatic var base_url_user = "https://graph.instagram.com/"
        @JvmStatic var base_url_user_fb = "https://graph.facebook.com/"

        @JvmStatic  fun getClientMap(): Retrofit? {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            val client = OkHttpClient.Builder().readTimeout(80, TimeUnit.SECONDS).writeTimeout(80, TimeUnit.SECONDS).addInterceptor(interceptor).build()
            retrofit = null
            retrofit = Retrofit.Builder()
                    .baseUrl(base_url_map)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build()
            return retrofit
        }


        @JvmStatic  fun getClientInstaToken(): Retrofit? {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            val client = OkHttpClient.Builder().readTimeout(80, TimeUnit.SECONDS).writeTimeout(80, TimeUnit.SECONDS).addInterceptor(interceptor).build()
            retrofit = null
            retrofit = Retrofit.Builder()
                    .baseUrl(base_url_insta_accesstoken)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build()
            return retrofit
        }

        @JvmStatic  fun getClientUser(): Retrofit? {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            val client = OkHttpClient.Builder().readTimeout(80, TimeUnit.SECONDS).writeTimeout(80, TimeUnit.SECONDS).addInterceptor(interceptor).build()
            retrofit = null
            retrofit = Retrofit.Builder()
                    .baseUrl(base_url_user)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addConverterFactory(ToStringConverterFactory())
                    .client(client)
                    .build()
            return retrofit
        }


        @JvmStatic  fun getClientFbUser(): Retrofit? {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            val client = OkHttpClient.Builder().readTimeout(80, TimeUnit.SECONDS).writeTimeout(80, TimeUnit.SECONDS).addInterceptor(interceptor).build()
            retrofit = null
            retrofit = Retrofit.Builder()
                    .baseUrl(base_url_user_fb)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addConverterFactory(ToStringConverterFactory())
                    .client(client)
                    .build()
            return retrofit
        }

    }




}