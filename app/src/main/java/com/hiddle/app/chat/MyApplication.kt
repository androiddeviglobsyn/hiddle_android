package com.hiddle.app.chat

import android.app.Application
import io.socket.client.IO
import io.socket.client.Socket

class MyApplication : Application() {

    companion object {

        // val CHAT_URL =  "http://217.163.23.182:5000"
        val CHAT_URL = "http://217.163.23.182:3000" //for hiddle


        //private val CHAT_URL =  "http://ec2-34-227-96-74.compute-1.amazonaws.com:3333" //for test
        // val CHAT_URL = "http://ec2-34-227-96-74.compute-1.amazonaws.com:9000"
        var mSocket: Socket? = IO.socket(CHAT_URL)

    }
}