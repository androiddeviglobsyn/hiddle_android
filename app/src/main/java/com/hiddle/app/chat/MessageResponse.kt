package com.hiddle.app.chat

import java.io.Serializable

class MessageResponse : Serializable {
    var from_id: String? = null

    var sent_by: String? = null

    var to_id: String? = null

    var createdAt: String? = null

    var id: String? = null

    var message: String? = null

    var isChatTypeVisible: String ?=null

   /* override fun toString(): String {
        return "ClassPojo [from_id = $from_id, sent_by = $sent_by, to_id = $to_id, created_at = $created_at, id = $id, message = $message]"
    }*/
}
