package com.hiddle.app.adapter

import android.content.Context
import android.os.Parcelable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.hiddle.app.R
import com.hiddle.app.model.UserModel.UserImageModel


class SlidingImage_Adapter(private val context: Context, private val imageModelArrayList: ArrayList<UserImageModel>,  baseUrl: String?) : PagerAdapter() {
    private val inflater: LayoutInflater
   var url : String = ""

    init {
         url = baseUrl!!
        inflater = LayoutInflater.from(context)
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun getCount(): Int {
        return imageModelArrayList.size
    }

    override fun instantiateItem(view: ViewGroup, position: Int): Any {
        val imageLayout = inflater.inflate(R.layout.slidingimages_layout_home_1, view, false)!!

        val imageView = imageLayout.findViewById(R.id.image) as ImageView
        Glide.with(context).load(url+imageModelArrayList[position].image).placeholder(R.drawable.ic_placeholder_icn).into(imageView)

        Log.e("url_profile",url+imageModelArrayList[position].image)


        view.addView(imageLayout, 0)

        return imageLayout
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun restoreState(state: Parcelable?, loader: ClassLoader?) {}

    override fun saveState(): Parcelable? {
        return null
    }

}