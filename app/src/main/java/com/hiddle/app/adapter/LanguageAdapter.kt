package com.hiddle.app.adapter

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.hiddle.app.R
import com.hiddle.app.`interface`.ClickonLanguage

class LanguageAdapter(private val context: Context? = null, var list: List<String>, var language: String,var clickonLanguage: ClickonLanguage) :
        RecyclerView.Adapter<LanguageAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, p1: Int) = MyViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_language, parent, false)
    )


    override fun getItemCount() = list.size


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {


        holder.tvName.text = list[position]

        if (language != "") {
            if (list[position].equals(language,true)) {
                holder.ivCheck.visibility = View.VISIBLE
                holder.tvName.setTextColor(context!!.getColor(R.color.light_blue))
            } else {
                holder.ivCheck.visibility = View.GONE
                holder.tvName.setTextColor(context!!.getColor(R.color.white))
            }
        }

        holder.itemView.setOnClickListener(View.OnClickListener {
             clickonLanguage.clickOnLanguage(position)
        })


    }

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        public val ivCheck = itemView.findViewById(R.id.ivCheck) as AppCompatImageView

        public val tvName = itemView.findViewById(R.id.tvName) as AppCompatTextView

    }

    fun updateList(list: ArrayList<String>) {
        this.list = list
        notifyDataSetChanged()
    }

}
