package com.hiddle.app.adapter

import android.content.Context
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.hiddle.app.R
import com.hiddle.app.`interface`.ClickOnImage
import com.hiddle.app.model.UserModel.UserImageModel


class SlidingProfileImage_Adapter(private val context: Context, private val imageModelArrayList: ArrayList<UserImageModel>,  baseUrl: String?,val clickOnImage: ClickOnImage) : PagerAdapter() {
    private val inflater: LayoutInflater
   var url : String = ""

    init {
         url = baseUrl!!
        inflater = LayoutInflater.from(context)
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun getCount(): Int {
        return imageModelArrayList.size
    }

    override fun instantiateItem(view: ViewGroup, position: Int): Any {
        val imageLayout = inflater.inflate(R.layout.slidingimages_layout, view, false)!!

        val imageView = imageLayout.findViewById(R.id.image) as ImageView
        Glide.with(context).load(url+imageModelArrayList[position].image.toString()).placeholder(R.drawable.ic_placeholder_icn).into(imageView)

        imageView.setOnClickListener(View.OnClickListener {
            clickOnImage.clickOnImageItem(position)
        })
        
        
        view.addView(imageLayout, 0)

        return imageLayout
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun restoreState(state: Parcelable?, loader: ClassLoader?) {}

    override fun saveState(): Parcelable? {
        return null
    }

}