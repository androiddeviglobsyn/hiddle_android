package com.hiddle.app.adapter

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.hiddle.app.R
import com.hiddle.app.`interface`.ClickLocation


class SearchMapAdapter(private val context: Context? = null, var list: ArrayList<com.hiddle.app.model.PlaceSearchModel.Result>, val clickLocation: ClickLocation) :
        RecyclerView.Adapter<SearchMapAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, p1: Int) = MyViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_place, parent, false)
    )


    override fun getItemCount() = list.size


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        if (context != null) {


            holder.tvName.text = list[position].name

            holder.tvAddress.text = list[position].formattedAddress

            holder.itemView.setOnClickListener{
                var name = list[position].name
                var address = list[position].formattedAddress
                var lat = list[position].geometry!!.location!!.lat.toString()
                var lng = list[position].geometry!!.location!!.lng.toString()
              clickLocation.clickOnLocation(position, name!!,address!!,lat,lng)
            }

        }
    }

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        public val tvName = itemView.findViewById(R.id.tvName) as AppCompatTextView
        public val tvAddress = itemView.findViewById(R.id.tvAddress) as AppCompatTextView

    }


}

