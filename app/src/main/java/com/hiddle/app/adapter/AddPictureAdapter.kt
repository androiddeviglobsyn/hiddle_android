package com.hiddle.app.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.util.Log
import android.view.*
import android.widget.RelativeLayout
import androidx.appcompat.widget.AppCompatImageView
import androidx.cardview.widget.CardView
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.hiddle.app.R
import com.hiddle.app.`interface`.AddImageClick
import com.hiddle.app.activity.AddPictureScreenActivity
import com.hiddle.app.itemDrag.ItemMoveCallback
import com.hiddle.app.itemDrag.StartDragListener
import com.hiddle.app.model.DragDataPicture
import de.hdodenhof.circleimageview.CircleImageView
import java.util.*


class AddPictureAdapter(private val context: Context? = null, var picturelist: ArrayList<String>, val addImageClick: AddImageClick, val mstartDragListener: StartDragListener, val addPictureScreenActivity: AddPictureScreenActivity) :
        RecyclerView.Adapter<AddPictureAdapter.MyViewHolder>(), ItemMoveCallback.ItemTouchHelperContract {
    companion object {
        @JvmStatic
        var selectedImageItem: Int = -1
    }

    private val HEADER_VIEW = 0
    private val FOOTER_VIEW = 1

    override fun onCreateViewHolder(viewGroup: ViewGroup, type: Int): MyViewHolder {


        var v: View? = null
        if (type === FOOTER_VIEW) {
            v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_add_picture, viewGroup, false)
        } else {
            v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_add_picture_header, viewGroup, false)
        }
        return MyViewHolder(v)


        /*
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.item_add_picture, parent, false)
        val holder = MyViewHolder(view)
        val shape: View = holder.ivPictureImage*/


        /* holder.itemView.setOnTouchListener(object : View.OnTouchListener {
             override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                 when (event?.action) {
                     MotionEvent.ACTION_DOWN -> setItemView(holder,shape) //Do Something
                 }
                 return v?.onTouchEvent(event) ?: true
             }
         })*/

        /*holder.itemView.setOnLongClickListener(View.OnLongClickListener {
            setItemView(holder, shape)

            true
        })*/

        /*  return holder*/

    }

    private fun setItemView(holder: MyViewHolder, shape: View) {
        selectedImageItem = holder.adapterPosition
        if (picturelist[selectedImageItem] != "") {
            val item: String = picturelist[holder.adapterPosition]
            val state = DragDataPicture(item, shape.width, shape.height)
            val shadow = View.DragShadowBuilder(shape)
            ViewCompat.startDragAndDrop(shape, null, shadow, state, 0)
        }
    }

    override fun getItemCount() = picturelist.size


    @SuppressLint("ClickableViewAccessibility")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {


        /*if (position == 0) {
            holder.cvBorder.visibility = View.VISIBLE
            holder.cvPicture.visibility = View.GONE
            holder.cvBorder.visibility = View.GONE
            holder.cvMain.visibility = View.VISIBLE

        } else {
            holder.cvBorder.visibility = View.INVISIBLE
            holder.cvPicture.visibility = View.VISIBLE
            holder.cvMain.visibility = View.GONE
        }*/




        if (context != null) {

            if (!picturelist[position].equals("")) {
                //for other image
                Glide.with(context).load(picturelist[position]).placeholder(R.drawable.ic_placeholder_icn).into(holder.ivPictureImage)
                //for circle image view
                Glide.with(context).load(picturelist[position]).placeholder(R.drawable.ic_placeholder_icn).into(holder.cvMain)
            } else {
                if (position == 0) {
                    holder.ivPictureImage.setImageResource(R.drawable.ic_plus_profile_bg)
                } else {
                    holder.ivPictureImage.setImageResource(R.drawable.ic_plus_green)
                }

            }

            holder.itemView.setOnClickListener {
                addImageClick.addImageSelection(position)   // method for open camera
            }
            holder.cvPicture.setOnClickListener {
                addImageClick.addImageSelection(position)   // method for open camera
            }
            holder.ivPictureImage.setOnClickListener {
                addImageClick.addImageSelection(position)   // method for open camera
            }
            holder.cvMain.setOnClickListener {
                addImageClick.addImageSelection(position)   // method for open camera
            }
             holder.itemView.setOnTouchListener { v, event ->
                 if (picturelist[position].toString() != "") {
                     if ((event.getAction() === MotionEvent.ACTION_DOWN)) {
                         mstartDragListener.requestDrag(holder)
                     }
                 }
                 true
             }

            holder.itemView.setOnLongClickListener(View.OnLongClickListener {
                if (picturelist[position].toString() != "") {
                    mstartDragListener.requestDrag(holder)
                }
                true
            })

            /*   holder.ivPictureImage.setOnTouchListener { v, event ->
                   if (picturelist[position].toString() != "") {
                       if ((event.getAction() === MotionEvent.ACTION_DOWN)) {
                           mstartDragListener.requestDrag(holder)
                       }
                   }
                   true
               }*/

            /* holder.cvPicture.setOnDragListener(View.OnDragListener { v, event ->
                 when (event.action) {

                     DragEvent.ACTION_DRAG_ENTERED -> holder.ivPictureImage.setBackgroundColor(Color.TRANSPARENT)
                     DragEvent.ACTION_DRAG_EXITED -> holder.ivPictureImage.setBackgroundColor(Color.TRANSPARENT);
                     DragEvent.ACTION_DRAG_ENDED -> holder.ivPictureImage.setBackgroundColor(Color.TRANSPARENT);
                     DragEvent.ACTION_DROP -> addPictureScreenActivity.callDragViewPos(selectedImageItem, position)

                 }
                 true
             })*/

        }
    }


    public class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        public val ivPictureImage = itemView.findViewById(R.id.ivPictureImage) as AppCompatImageView
        public val cvPicture = itemView.findViewById(R.id.cvPicture) as CardView
        public val cvBorder = itemView.findViewById(R.id.cvBorder) as CardView
        /*  public val cvImageView = itemView.findViewById(R.id.cvImageView) as RelativeLayout*/

        public val cvMain = itemView.findViewById(R.id.cvMain) as CircleImageView

        //public val viewPicture = itemView.findViewById(R.id.viewPicture) as LinearLayout


    }

    override fun onRowMoved(fromPosition: Int, toPosition: Int) {
        if (picturelist[fromPosition].toString() != "" && picturelist[toPosition].toString() != "") {
            if (fromPosition < toPosition) {
                for (i in fromPosition until toPosition) {
                    Collections.swap(picturelist, i, i + 1)
                }
            } else {
                for (i in fromPosition downTo toPosition + 1) {
                    Collections.swap(picturelist, i, i - 1)
                }
            }
            notifyItemMoved(fromPosition, toPosition)
        }


    }

    override fun onRowSelected(myViewHolder: MyViewHolder?) {

        Log.e("select", "gray")
        /*  myViewHolder.rowView.setBackgroundColor(Color.GRAY)*/
    }

    override fun onRowClear(myViewHolder: MyViewHolder?) {
        Log.e("select", "white")
        /*  myViewHolder.rowView.setBackgroundColor(Color.WHITE)*/
    }

    override fun getItemViewType(position: Int): Int {
        if (position == 0) {
            return HEADER_VIEW
        }
        return FOOTER_VIEW
    }
}