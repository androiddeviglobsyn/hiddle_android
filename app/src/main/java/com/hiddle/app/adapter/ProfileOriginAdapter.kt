package com.hiddle.app.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.blongho.country_data.World
import com.hiddle.app.R


class ProfileOriginAdapter(private val context: Context? = null, var list: List<String>) :
        RecyclerView.Adapter<ProfileOriginAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, p1: Int) = MyViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_profile_origin, parent, false)
    )


    override fun getItemCount() = list.size


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        /*if (context != null) {*/
        // holder.tvName.text = list[position]
        /* if(position == 0) {
             holder.tvName.text = "Poland"
         } else if (position == 1) {
             holder.tvName.text = "London"
         } else
         {
             holder.tvName.text = "Poland"
         }*/

        holder.tvName.text = list[position]

        World.init(context);

        if (list[position] == "Antarctica") {
            holder.ivFlag.setImageResource(R.drawable.flag_antarctica)
        }  else if (list[position] == "Antigua & Barbuda") {
            holder.ivFlag.setImageResource(R.drawable.flag_antigua_and_barbuda)
        } else if (list[position] == "Bosnia & Harzegovina") {
            holder.ivFlag.setImageResource(R.drawable.flag_bosnia)
        } else if (list[position] == "Cape Verde") {
            holder.ivFlag.setImageResource(R.drawable.flag_cape_verde)
        } else if (list[position] == "Côte d’Ivoire") {
            holder.ivFlag.setImageResource(R.drawable.flag_cote_divoire)
        } else if (list[position] == "Congo - Brazzaville") {
            holder.ivFlag.setImageResource(R.drawable.flag_republic_of_the_congo)
        } else if (list[position] == "Falkland Islands (Islas Malvinas)") {
            holder.ivFlag.setImageResource(R.drawable.flag_falkland_islands)
        } else if (list[position] == "Macau") {
            holder.ivFlag.setImageResource(R.drawable.flag_macao)
        } else if (list[position] == "Aland Islands") {
            holder.ivFlag.setImageResource(R.drawable.flag_aland)
        } else if (list[position] == "Kosovo") {
            holder.ivFlag.setImageResource(R.drawable.flag_kosovo)
        } else if (list[position] == "Tokelau") {
            holder.ivFlag.setImageResource(R.drawable.flag_tokelau)
        } else if (list[position] == "AmericanSamoa") {
            holder.ivFlag.setImageResource(R.drawable.flag_american_samoa)
        } else if (list[position] == "Swaziland") {
            holder.ivFlag.setImageResource(R.drawable.flag_swaziland)
        } else if (list[position] == "Uzbekistan") {
            holder.ivFlag.setImageResource(R.drawable.flag_uzbekistan)
        } else if (list[position] == "Sri Lanka") {
            holder.ivFlag.setImageResource(R.drawable.flag_sri_lanka)
        } else if (list[position] == "Syrian Arab Republic") {
            holder.ivFlag.setImageResource(R.drawable.flag_syria)
        } else if (list[position] == "Macedonia") {
            holder.ivFlag.setImageResource(R.drawable.flag_macedonia)
        } else if (list[position] == "Virgin Islands, U.S.") {
            holder.ivFlag.setImageResource(R.drawable.flag_us_virgin_islands)
        } else if (list[position] == "Virgin Islands, British") {
            holder.ivFlag.setImageResource(R.drawable.flag_british_virgin_islands)
        } else if (list[position] == "Venezuela, Bolivarian Republic of Venezuela") {
            holder.ivFlag.setImageResource(R.drawable.flag_venezuela)
        } else if (list[position] == "Tanzania, United Republic of Tanzania") {
            holder.ivFlag.setImageResource(R.drawable.flag_tanzania)
        } else if (list[position] == "South Georgia and the South Sandwich Islands") {
            holder.ivFlag.setImageResource(R.drawable.flag_south_georgia)
        } else if (list[position] == "Saudi Arabia") {
            holder.ivFlag.setImageResource(R.drawable.flag_saudi_arabia)
        } else if (list[position] == "Sao Tome and Principe") {
            holder.ivFlag.setImageResource(R.drawable.flag_sao_tome_and_principe)
        } else if (list[position] == "Saint Barthelemy") {
            holder.ivFlag.setImageResource(R.drawable.flag_saint_barthelemy)
        } else if (list[position] == "Saint Helena, Ascension and Tristan Da Cunha") {
            holder.ivFlag.setImageResource(R.drawable.flag_saint_helena)
        } else if (list[position] == "Saint Kitts and Nevis") {
            holder.ivFlag.setImageResource(R.drawable.flag_saint_kitts_and_nevis)
        } else if (list[position] == "Saint Martin") {
            holder.ivFlag.setImageResource(R.drawable.flag_saint_martin)
        } else if (list[position] == "Saint Vincent and the Grenadines") {
            holder.ivFlag.setImageResource(R.drawable.flag_saint_vicent_and_the_grenadines)
        } else if (list[position] == "Palestinian Territory, Occupied") {
            holder.ivFlag.setImageResource(R.drawable.flag_palestine)
        } else if (list[position] == "Pitcairn") {
            holder.ivFlag.setImageResource(R.drawable.flag_pitcairn_islands)
        } else if (list[position] == "Netherlands Antilles") {
            holder.ivFlag.setImageResource(R.drawable.flag_netherlands_antilles)
        } else if (list[position] == "Micronesia, Federated States of Micronesia") {
            holder.ivFlag.setImageResource(R.drawable.flag_micronesia)
        } else if (list[position] == "Libyan Arab Jamahiriya") {
            holder.ivFlag.setImageResource(R.drawable.flag_libya)
        } else if (list[position] == "Iran, Islamic Republic of Persian Gulf") {
            holder.ivFlag.setImageResource(R.drawable.flag_iran)
        } else if (list[position] == "Holy See (Vatican City State)") {
            holder.ivFlag.setImageResource(R.drawable.flag_vatican_city)
        } else if (list[position] == "Falkland Islands (Malvinas)") {
            holder.ivFlag.setImageResource(R.drawable.flag_falkland_islands)
        } else if (list[position] == "Congo") {
            holder.ivFlag.setImageResource(R.drawable.flag_republic_of_the_congo)
        } else if (list[position] == "Congo, The Democratic Republic of the Congo") {
            holder.ivFlag.setImageResource(R.drawable.flag_democratic_republic_of_the_congo)
        } else if (list[position] == "Cote d'Ivoire") {
            holder.ivFlag.setImageResource(R.drawable.flag_cote_divoire)
        } else if (list[position] == "Czech Republic") {
            holder.ivFlag.setImageResource(R.drawable.flag_czech_republic)
        } else if (list[position] == "Brunei Darussalam") {
            holder.ivFlag.setImageResource(R.drawable.flag_brunei)
        } else if (list[position] == "Bolivia, Plurinational State of") {
            holder.ivFlag.setImageResource(R.drawable.flag_bolivia)
        } else if (list[position] == "Svalbard and Jan Mayen") {
            holder.ivFlag.setImageResource(R.drawable.flag_norway)
        } else if (list[position] == "Korea, Democratic People's Republic of Korea") {
            holder.ivFlag.setImageResource(R.drawable.flag_north_korea)
        } else if (list[position] == "Korea, Republic of South Korea") {
            holder.ivFlag.setImageResource(R.drawable.flag_south_korea)
        } else if (list[position] == "Albania") {
            holder.ivFlag.setImageResource(R.drawable.flag_albania)
        } else if (list[position] == "Australia") {
            holder.ivFlag.setImageResource(R.drawable.flag_australia)
        } else if (list[position] == "Bahamas") {
            holder.ivFlag.setImageResource(R.drawable.flag_bahamas)
        } else if (list[position] == "Bosnia and Herzegovina") {
            holder.ivFlag.setImageResource(R.drawable.flag_bosnia)
        } else if (list[position] == "Cameroon") {
            holder.ivFlag.setImageResource(R.drawable.flag_cameroon)
        } else if (list[position] == "Cocos (Keeling) Islands") {
            holder.ivFlag.setImageResource(R.drawable.flag_cocos)
        } else if (list[position] == "Chad") {
            holder.ivFlag.setImageResource(R.drawable.flag_chad)
        } else if (list[position] == "Finland") {
            holder.ivFlag.setImageResource(R.drawable.flag_finland)
        } else if (list[position] == "Reunion") {
            holder.ivFlag.setImageResource(R.drawable.flag_france)
        } else {
            holder.ivFlag.setImageResource(World.getFlagOf(list[position]))
        }





/*

        if (list[position] == "Antarctica") {
            holder.ivFlag.setImageResource(R.drawable.flag_antarctica)
        } else if (list[position] == "Antigua & Barbuda") {
            holder.ivFlag.setImageResource(R.drawable.flag_antigua_and_barbuda)
        } else if (list[position] == "Bosnia & Harzegovina") {
            holder.ivFlag.setImageResource(R.drawable.flag_bosnia)
        } else if (list[position] == "Cape Verde") {
            holder.ivFlag.setImageResource(R.drawable.flag_cape_verde)
        } else if (list[position] == "Côte d’Ivoire") {
            holder.ivFlag.setImageResource(R.drawable.flag_cote_divoire)
        } else if (list[position] == "Congo - Brazzaville") {
            holder.ivFlag.setImageResource(R.drawable.flag_republic_of_the_congo)
        } else if (list[position] == "Falkland Islands (Islas Malvinas)") {
            holder.ivFlag.setImageResource(R.drawable.flag_falkland_islands)
        } else if (list[position] == "Macau") {
            holder.ivFlag.setImageResource(R.drawable.flag_macao)
        } else if (list[position] == "Åland Islands") {
            holder.ivFlag.setImageResource(R.drawable.flag_aland)
        }
        else if (list[position] == "Kosovo") {
            holder.ivFlag.setImageResource(R.drawable.flag_kosovo)
        } else {
            holder.ivFlag.setImageResource(World.getFlagOf(list[position]))
        }
*/


        holder.itemView.setOnClickListener {

        }

    }

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        public val tvName = itemView.findViewById(R.id.tvName) as AppCompatTextView
        val ivFlag = itemView.findViewById(R.id.ivFlag) as AppCompatImageView

    }


}


