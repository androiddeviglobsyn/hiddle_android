package com.hiddle.app.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.blongho.country_data.World
import com.hiddle.app.R
import com.hiddle.app.activity.MyOriginActivity
import com.hiddle.app.model.OriginModel


class OriginAdapter(private val context: Context? = null, var list: List<OriginModel>, var isFrom: String, var originActivity: MyOriginActivity) :
        RecyclerView.Adapter<OriginAdapter.MyViewHolder>() {
    companion object {
        @JvmStatic
        var selectedOriginList: ArrayList<String> = ArrayList()
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int) = MyViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_origin, parent, false)
    )


    override fun getItemCount() = list.size


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.tvName.text = list[position].countryName


        /*if (position == 0) {
            if (list[position].countryName == context!!.getString(R.string.select_all)) {
                holder.ivFlag.visibility = View.GONE
            } else {
                holder.ivFlag.visibility = View.VISIBLE
            }
        }*/


/*
        if (isFrom == "homeFilter") {
            if (selectedOriginList.contains(context!!.getString(R.string.select_all))) {
                if (!selectedOriginList.contains(list[position].countryName)) {
                    selectedOriginList.add(list[position].countryName!!)
                }
            }

        }*/



        Log.e("list", selectedOriginList.toString())

        if (context != null) {

            World.init(context);


            if (list[position].countryName == "Antarctica") {
                holder.ivFlag.setImageResource(R.drawable.flag_antarctica)
                holder.ivFlag.visibility= View.VISIBLE
            } else if (list[position].countryName == context.getString(R.string.select_all)) {
                try {
                    holder.ivFlag.setImageResource(0)
                } catch (e: Exception) {
                }
            } else if (list[position].countryName == "Antigua & Barbuda") {
                holder.ivFlag.setImageResource(R.drawable.flag_antigua_and_barbuda)
                holder.ivFlag.visibility= View.VISIBLE
            } else if (list[position].countryName == "Bosnia & Harzegovina") {
                holder.ivFlag.setImageResource(R.drawable.flag_bosnia)
                holder.ivFlag.visibility= View.VISIBLE
            } else if (list[position].countryName == "Cape Verde") {
                holder.ivFlag.setImageResource(R.drawable.flag_cape_verde)
                holder.ivFlag.visibility= View.VISIBLE
            } else if (list[position].countryName == "Côte d’Ivoire") {
                holder.ivFlag.setImageResource(R.drawable.flag_cote_divoire)
                holder.ivFlag.visibility= View.VISIBLE
            } else if (list[position].countryName == "Congo - Brazzaville") {
                holder.ivFlag.setImageResource(R.drawable.flag_republic_of_the_congo)
            } else if (list[position].countryName == "Falkland Islands (Islas Malvinas)") {
                holder.ivFlag.setImageResource(R.drawable.flag_falkland_islands)
            } else if (list[position].countryName == "Macau") {
                holder.ivFlag.setImageResource(R.drawable.flag_macao)
            } else if (list[position].countryName == "Aland Islands") {
                holder.ivFlag.setImageResource(R.drawable.flag_aland)
            } else if (list[position].countryName == "Kosovo") {
                holder.ivFlag.setImageResource(R.drawable.flag_kosovo)
            } else if (list[position].countryName == "Tokelau") {
                holder.ivFlag.setImageResource(R.drawable.flag_tokelau)
            } else if (list[position].countryName == "AmericanSamoa") {
                holder.ivFlag.setImageResource(R.drawable.flag_american_samoa)
            } else if (list[position].countryName == "Swaziland") {
                holder.ivFlag.setImageResource(R.drawable.flag_swaziland)
            } else if (list[position].countryName == "Uzbekistan") {
                holder.ivFlag.setImageResource(R.drawable.flag_uzbekistan)
            } else if (list[position].countryName == "Sri Lanka") {
                holder.ivFlag.setImageResource(R.drawable.flag_sri_lanka)
            } else if (list[position].countryName == "Syrian Arab Republic") {
                holder.ivFlag.setImageResource(R.drawable.flag_syria)
            } else if (list[position].countryName == "Macedonia") {
                holder.ivFlag.setImageResource(R.drawable.flag_macedonia)
            } else if (list[position].countryName == "Virgin Islands, U.S.") {
                holder.ivFlag.setImageResource(R.drawable.flag_us_virgin_islands)
            } else if (list[position].countryName == "Virgin Islands, British") {
                holder.ivFlag.setImageResource(R.drawable.flag_british_virgin_islands)
            } else if (list[position].countryName == "Venezuela, Bolivarian Republic of Venezuela") {
                holder.ivFlag.setImageResource(R.drawable.flag_venezuela)
            } else if (list[position].countryName == "Tanzania, United Republic of Tanzania") {
                holder.ivFlag.setImageResource(R.drawable.flag_tanzania)
            } else if (list[position].countryName == "South Georgia and the South Sandwich Islands") {
                holder.ivFlag.setImageResource(R.drawable.flag_south_georgia)
            } else if (list[position].countryName == "Saudi Arabia") {
                holder.ivFlag.setImageResource(R.drawable.flag_saudi_arabia)
            } else if (list[position].countryName == "Sao Tome and Principe") {
                holder.ivFlag.setImageResource(R.drawable.flag_sao_tome_and_principe)
            } else if (list[position].countryName == "Saint Barthelemy") {
                holder.ivFlag.setImageResource(R.drawable.flag_saint_barthelemy)
            } else if (list[position].countryName == "Saint Helena, Ascension and Tristan Da Cunha") {
                holder.ivFlag.setImageResource(R.drawable.flag_saint_helena)
            } else if (list[position].countryName == "Saint Kitts and Nevis") {
                holder.ivFlag.setImageResource(R.drawable.flag_saint_kitts_and_nevis)
            } else if (list[position].countryName == "Saint Martin") {
                holder.ivFlag.setImageResource(R.drawable.flag_saint_martin)
            } else if (list[position].countryName == "Saint Vincent and the Grenadines") {
                holder.ivFlag.setImageResource(R.drawable.flag_saint_vicent_and_the_grenadines)
            } else if (list[position].countryName == "Palestinian Territory, Occupied") {
                holder.ivFlag.setImageResource(R.drawable.flag_palestine)
            } else if (list[position].countryName == "Pitcairn") {
                holder.ivFlag.setImageResource(R.drawable.flag_pitcairn_islands)
            } else if (list[position].countryName == "Netherlands Antilles") {
                holder.ivFlag.setImageResource(R.drawable.flag_netherlands_antilles)
            } else if (list[position].countryName == "Micronesia, Federated States of Micronesia") {
                holder.ivFlag.setImageResource(R.drawable.flag_micronesia)
            } else if (list[position].countryName == "Libyan Arab Jamahiriya") {
                holder.ivFlag.setImageResource(R.drawable.flag_libya)
            } else if (list[position].countryName == "Iran, Islamic Republic of Persian Gulf") {
                holder.ivFlag.setImageResource(R.drawable.flag_iran)
            } else if (list[position].countryName == "Holy See (Vatican City State)") {
                holder.ivFlag.setImageResource(R.drawable.flag_vatican_city)
            } else if (list[position].countryName == "Falkland Islands (Malvinas)") {
                holder.ivFlag.setImageResource(R.drawable.flag_falkland_islands)
            } else if (list[position].countryName == "Congo") {
                holder.ivFlag.setImageResource(R.drawable.flag_republic_of_the_congo)
            } else if (list[position].countryName == "Congo, The Democratic Republic of the Congo") {
                holder.ivFlag.setImageResource(R.drawable.flag_democratic_republic_of_the_congo)
            } else if (list[position].countryName == "Cote d'Ivoire") {
                holder.ivFlag.setImageResource(R.drawable.flag_cote_divoire)
            } else if (list[position].countryName == "Czech Republic") {
                holder.ivFlag.setImageResource(R.drawable.flag_czech_republic)
            } else if (list[position].countryName == "Brunei Darussalam") {
                holder.ivFlag.setImageResource(R.drawable.flag_brunei)
            } else if (list[position].countryName == "Bolivia, Plurinational State of") {
                holder.ivFlag.setImageResource(R.drawable.flag_bolivia)
            } else if (list[position].countryName == "Svalbard and Jan Mayen") {
                holder.ivFlag.setImageResource(R.drawable.flag_norway)
            } else if (list[position].countryName == "Korea, Democratic People's Republic of Korea") {
                holder.ivFlag.setImageResource(R.drawable.flag_north_korea)
            } else if (list[position].countryName == "Korea, Republic of South Korea") {
                holder.ivFlag.setImageResource(R.drawable.flag_south_korea)
            } else if (list[position].countryName == "Albania") {
                holder.ivFlag.setImageResource(R.drawable.flag_albania)
            } else if (list[position].countryName == "Australia") {
                holder.ivFlag.setImageResource(R.drawable.flag_australia)
            } else if (list[position].countryName == "Bahamas") {
                holder.ivFlag.setImageResource(R.drawable.flag_bahamas)
            } else if (list[position].countryName == "Bosnia and Herzegovina") {
                holder.ivFlag.setImageResource(R.drawable.flag_bosnia)
            } else if (list[position].countryName == "Cameroon") {
                holder.ivFlag.setImageResource(R.drawable.flag_cameroon)
            } else if (list[position].countryName == "Cocos (Keeling) Islands") {
                holder.ivFlag.setImageResource(R.drawable.flag_cocos)
            } else if (list[position].countryName == "Chad") {
                holder.ivFlag.setImageResource(R.drawable.flag_chad)
            } else if (list[position].countryName == "Finland") {
                holder.ivFlag.setImageResource(R.drawable.flag_finland)
            } else if (list[position].countryName == "Reunion") {
                holder.ivFlag.setImageResource(R.drawable.flag_france)
            } else {
                holder.ivFlag.visibility= View.VISIBLE
                holder.ivFlag.setImageResource(World.getFlagOf(list[position].countryName))
            }




            if (selectedOriginList.contains(list[position].countryName.toString())) {
                holder.ivCheck.setImageResource(R.drawable.ic_white_tick_green_circle)
            } else {
                holder.ivCheck.setImageResource(R.drawable.ic_ring_white)
            }


            holder.itemView.setOnClickListener {

                if (isFrom == "homeFilter") {
                    if (selectedOriginList.size > 0) {
                        try {
                            if (!selectedOriginList.contains((list[position].countryName).toString())) {
                                selectedOriginList.add((list[position].countryName).toString())
                                holder.ivCheck.setImageResource(R.drawable.ic_white_tick_green_circle)
                                if (list[position].countryName == context.getString(R.string.select_all)) {
                                    originActivity.setOriginAdapter()
                                } else {
                                    if (selectedOriginList.contains(context.getString(R.string.select_all))) {
                                        selectedOriginList.remove(context.getString(R.string.select_all))
                                        originActivity.removeSelectAllOrigin()
                                    }
                                }
                            } else {
                                for (p in selectedOriginList.indices) {
                                    if (selectedOriginList[p] == (list[position].countryName).toString()) {
                                        selectedOriginList.remove(list[position].countryName)
                                        holder.ivCheck.setImageResource(R.drawable.ic_ring_white)
                                        if (list[position].countryName == context.getString(R.string.select_all)) {
                                            selectedOriginList.clear()
                                            originActivity.setOriginAdapter()
                                        } else {
                                            if (selectedOriginList.contains(context.getString(R.string.select_all))) {
                                                selectedOriginList.remove(context.getString(R.string.select_all))
                                                originActivity.removeSelectAllOrigin()
                                            }

                                        }
                                    }
                                }

                            }
                        } catch (e: Exception) {

                        }

                    } else {
                        selectedOriginList.add(list[position].countryName.toString())
                        holder.ivCheck.setImageResource(R.drawable.ic_white_tick_green_circle)
                        if (list[position].countryName == context.getString(R.string.select_all)) {
                            originActivity.setOriginAdapter()
                        } else {
                            if (selectedOriginList.contains(context.getString(R.string.select_all))) {
                                selectedOriginList.remove(context.getString(R.string.select_all))
                                originActivity.removeSelectAllOrigin()
                            }
                        }
                    }
                } else {
                    if (selectedOriginList.size > 0) {
                        try {
                            if (!selectedOriginList.contains((list[position].countryName).toString())) {
                                if (selectedOriginList.size == 5) {
                                    Toast.makeText(context, "You can  add maximum 5 origins", Toast.LENGTH_SHORT).show()
                                }
                                if (selectedOriginList.size != 5) {
                                    selectedOriginList.add((list[position].countryName).toString()!!)
                                    holder.ivCheck.setImageResource(R.drawable.ic_white_tick_green_circle)
                                }

                            } else {
                                for (p in selectedOriginList.indices) {
                                    if (selectedOriginList[p] == (list[position].countryName).toString()) {
                                        selectedOriginList.removeAt(p)
                                        holder.ivCheck.setImageResource(R.drawable.ic_ring_white)
                                    }
                                }

                            }
                        } catch (e: Exception) {

                        }

                    } else {
                        selectedOriginList.add(list[position].countryName.toString())
                        holder.ivCheck.setImageResource(R.drawable.ic_white_tick_green_circle)
                    }
                }



                Log.e("list", selectedOriginList.toString())

            }
        }
    }

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        public val ivCheck = itemView.findViewById(R.id.ivCheck) as AppCompatImageView
        public val ivFlag = itemView.findViewById(R.id.ivFlag) as AppCompatImageView
        public val tvName = itemView.findViewById(R.id.tvName) as AppCompatTextView

    }

    fun updateList(list: ArrayList<OriginModel>) {
        this.list = list
        notifyDataSetChanged()
    }

}
