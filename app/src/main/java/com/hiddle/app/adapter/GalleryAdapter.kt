package com.hiddle.app.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.hiddle.app.R
import kotlin.collections.ArrayList


class GalleryAdapter(private val context: Context? = null, var list: ArrayList<String>) :
        RecyclerView.Adapter<GalleryAdapter.MyViewHolder>() {
    companion object {
        @JvmStatic
        var gallerylist: ArrayList<String> = ArrayList()
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int) = MyViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_image_gallery, parent, false)
    )


    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        if (list.get(position) != null) {
            Glide.with(context!!).load(list.get(position)).placeholder(R.drawable.ic_placeholder_icn).into(holder.ivImage)
        }

        holder.itemView.setOnClickListener {
            if (gallerylist.size > 0) {
                try {
                    if (!gallerylist.contains((list[position]))) {
                        gallerylist.add((list[position]).toString()!!)
                        holder.llView.visibility = View.VISIBLE
                        holder.ivTick.visibility = View.VISIBLE

                    } else {
                        for (p in gallerylist.indices) {
                            if (gallerylist[p] == (list[position])) {
                                gallerylist.removeAt(p)
                                holder.llView.visibility = View.GONE
                                holder.ivTick.visibility = View.GONE

                            }
                        }

                        /* holder.ivCheck.setImageResource(R.drawable.ic_ring_white)*/
                    }
                } catch (e: Exception) {

                }

            } else {
                gallerylist.add(list[position])
                holder.llView.visibility = View.VISIBLE
                holder.ivTick.visibility = View.VISIBLE

            }
            Log.e("list", gallerylist.toString())

        }

    }

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val ivImage = itemView.findViewById(R.id.ivImage) as AppCompatImageView
        val item: LinearLayout = itemView.findViewById(R.id.item) as LinearLayout
        val llView: LinearLayout = itemView.findViewById(R.id.llView) as LinearLayout
        val ivTick: AppCompatImageView = itemView.findViewById(R.id.ivTick) as AppCompatImageView

    }


}

