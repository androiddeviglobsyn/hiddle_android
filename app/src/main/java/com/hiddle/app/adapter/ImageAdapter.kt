package com.hiddle.app.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.hiddle.app.R
import com.hiddle.app.model.UserModel.UserDataResponse
import com.hiddle.app.model.UserModel.UserImageModel
import java.util.*
import kotlin.collections.ArrayList


class ImageAdapter(private val context: Context? = null, var list: ArrayList<UserDataResponse>, var selectposition: Int) :
        RecyclerView.Adapter<ImageAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, p1: Int) = MyViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_image, parent, false)
    )


    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {


        if (list.get(position).images != null) {
            if (Objects.requireNonNull<List<UserImageModel>>(list.get(position).images).isNotEmpty()) {
                if (list[position].images!![0] != null) {
                    Glide.with(context!!).load(list.get(position).baseUrl + list[position].images!![0].image).placeholder(R.drawable.ic_placeholder_icn).into(holder.ivImage)
                }
            }
        }

        if (selectposition == position) {
            holder.itemView.visibility = View.VISIBLE
            holder.item.visibility = View.VISIBLE
        } else if (selectposition + 1 == position) {
            holder.itemView.visibility = View.VISIBLE
            holder.item.visibility = View.VISIBLE
        } else if (selectposition + 2 == position) {
            holder.itemView.visibility = View.VISIBLE
            holder.item.visibility = View.VISIBLE
        } else {
            holder.itemView.visibility = View.GONE
            holder.item.visibility = View.GONE
            holder.item.layoutParams = RecyclerView.LayoutParams(0, 0)
        }


        /*if (list.size > 3) {
            if (position > 2) {
                holder.itemView.visibility = View.GONE
            }
        }*/



        holder.itemView.setOnClickListener {


        }
        /*}*/
    }

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        public val ivImage = itemView.findViewById(R.id.ivImage) as AppCompatImageView
        val item: LinearLayout = itemView.findViewById(R.id.item) as LinearLayout


    }


}

