package com.hiddle.app.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.hiddle.app.R
import com.hiddle.app.fragment.HomeFragment
import com.hiddle.app.model.UserModel.UserDataResponse
import com.hiddle.app.model.UserModel.UserImageModel
import com.hiddle.app.util.SharedPreferenceManager.Companion.getMySharedPreferences
import com.hiddle.app.util.SwipeMultipleDeck
import com.hiddle.app.util.VerticalViewPager
import com.hiddle.app.verticaldotindicator.DotsIndicatorVertical
import java.util.*
import kotlin.collections.ArrayList

class CardStackAdapter(private var context: Context, private var data: List<UserDataResponse>, private var homeFragment: HomeFragment) : RecyclerView.Adapter<CardStackAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(inflater.inflate(R.layout.test_card2_stack, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        /* if (data.get(position).images!!.get(0) != null) {
             Glide.with(context).load(data.get(position).baseUrl + Objects.requireNonNull<List<String>>(data.get(position).images)[0]).placeholder(R.drawable.ic_placeholder_icn).into(holder.imageView)
         }*/


        try {
            holder.vpHome.adapter = SlidingHomeImage_Adapter(context, data[position].images as ArrayList<UserImageModel>, data.get(position).baseUrl, position, homeFragment);
            holder.dots_indicator_home.setViewPager(holder.vpHome);
        } catch (e: Exception) {

        }


        /* adapter = SwipeDeckAdapter(userList, mContext, this@HomeFragment)
        cardStack!!.setAdapter(adapter)
        cardStack!!.setSelection(Companion.selectedPosition)*/
        holder.tvUserName.setText(data.get(position).username)
        if (data.get(position).distance != null) {
            try {
                if (getMySharedPreferences()!!.getDistance().equals("Km", ignoreCase = true) || getMySharedPreferences()!!.getDistance().equals("km", ignoreCase = true) || getMySharedPreferences()!!.getDistance().equals("", ignoreCase = true)) {
                    holder.tvDistance.setText(String.format("%.02f", java.lang.Float.valueOf(data.get(position).distance)).replace(",", ".") + " " + context!!.getString(R.string.km))
                } else {
                    holder.tvDistance.setText(String.format("%.02f", java.lang.Float.valueOf(convertKmsToMiles(java.lang.Float.valueOf(data.get(position).distance)))).replace(",", ".") + " " + context!!.getString(R.string.mile))
                }
            } catch (e: Exception) {
            }

            holder.ivGPS.setVisibility(View.VISIBLE)
            holder.tvDistance.setVisibility(View.VISIBLE)
        } else {
            holder.ivGPS.setVisibility(View.GONE)
            holder.tvDistance.setVisibility(View.GONE)
        }




        holder.itemView.setOnClickListener { v ->
            homeFragment.redirectToUserProfile(position)
        }


    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun setSpots(data: List<UserDataResponse>) {
        this.data = data
    }

    fun getSpots(): List<UserDataResponse> {
        return data
    }

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {

        //((TextView) v.findViewById(R.id.textView2)).setText(data.get(position));
        var imageView = v.findViewById(R.id.offer_image) as ImageView
        var tvUserName = v.findViewById(R.id.tvUsername) as AppCompatTextView
        var tvDistance = v.findViewById(R.id.tvDistance) as AppCompatTextView
        var ivGPS = v.findViewById(R.id.ivGPS) as AppCompatImageView
        var vpHome = v.findViewById(R.id.vpHome) as VerticalViewPager
        var rvHome = v.findViewById(R.id.rvHome) as RecyclerView

        var dots_indicator_home = v.findViewById(R.id.dots_indicator_home) as DotsIndicatorVertical
        var swipe_deck_multiple = v.findViewById(R.id.swipe_deck_multiple) as SwipeMultipleDeck

    }

    fun convertKmsToMiles(kms: Float): Float {
        return (0.621371 * kms).toFloat()
    }
}
