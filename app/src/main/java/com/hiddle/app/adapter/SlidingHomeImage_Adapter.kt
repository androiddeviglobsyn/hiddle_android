package com.hiddle.app.adapter

import android.content.Context
import android.os.Parcelable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.hiddle.app.R
import com.hiddle.app.fragment.HomeFragment
import com.hiddle.app.model.UserModel.UserImageModel


class SlidingHomeImage_Adapter(private val context: Context, private val imageModelArrayList: ArrayList<UserImageModel>, baseUrl: String?, private var pos: Int, private var homeFragment: HomeFragment) : PagerAdapter() {
    private val inflater: LayoutInflater
    var url: String = ""

    init {
        url = baseUrl!!
        inflater = LayoutInflater.from(context)
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun getCount(): Int {
        return imageModelArrayList.size
    }

    override fun instantiateItem(view: ViewGroup, position: Int): Any {
        val imageLayout = inflater.inflate(R.layout.slidingimages_layout, view, false)!!

        val imageView = imageLayout.findViewById(R.id.image) as ImageView

        Glide.with(context).load(url + imageModelArrayList[position].image).placeholder(R.drawable.ic_placeholder_icn).into(imageView)


        imageView.setOnClickListener(View.OnClickListener {
            Log.e("click_call", "click_call")
            homeFragment.redirectToUserProfile(pos)
        })

        imageLayout.setOnClickListener(View.OnClickListener {
            // Log.e("click_call1","click_call1")
            homeFragment.redirectToUserProfile(pos)
        })


        view.addView(imageLayout, 0)

        return imageLayout
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun restoreState(state: Parcelable?, loader: ClassLoader?) {}

    override fun saveState(): Parcelable? {
        return null
    }

}