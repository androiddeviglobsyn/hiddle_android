package com.hiddle.app.adapter;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import com.bumptech.glide.Glide;
import com.hiddle.app.R;
import com.hiddle.app.fragment.LikesFragment;
import com.hiddle.app.model.FavoriteModel.FavoriteResponseData;
import com.hiddle.app.util.SharedPreferenceManager;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Objects;

public class SwipeDeckLikeAdapter extends BaseAdapter {
    LayoutInflater inflater;
    private ArrayList<FavoriteResponseData> data;
    private Context context;
    LikesFragment likesFragment;


    public SwipeDeckLikeAdapter(ArrayList<FavoriteResponseData> data, Context context, LikesFragment likesFragment) {
        inflater = LayoutInflater.from(context);
        this.data = data;
        this.context = context;
        this.likesFragment = likesFragment;

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View v = convertView;
        if (v == null) {

            // normally use a viewholder
            v = inflater.inflate(R.layout.test_card2, parent, false);
        }

       ImageView imageView = (ImageView) v.findViewById(R.id.offer_image);
        AppCompatTextView tvUserName = (AppCompatTextView) v.findViewById(R.id.tvUsername);
        AppCompatTextView tvDistance = (AppCompatTextView) v.findViewById(R.id.tvDistance);
        AppCompatImageView ivGPS = (AppCompatImageView) v.findViewById(R.id.ivGPS);

        if (data.get(position).getProfile() != null) {

            if (data.get(position).getProfile() != null) {
                Glide.with(context).load(data.get(position).getBaseUrl() + Objects.requireNonNull(data.get(position).getProfile())).placeholder(R.drawable.ic_placeholder_icn).into(imageView);
            }
        }


        tvUserName.setText(data.get(position).getName());

        ivGPS.setVisibility(View.GONE);
        tvDistance.setVisibility(View.GONE);

        if (data.get(position).getDistance() != null) {

            if (SharedPreferenceManager.Companion.getMySharedPreferences().getDistance().equalsIgnoreCase("Km")||SharedPreferenceManager.Companion.getMySharedPreferences().getDistance().equalsIgnoreCase("km")||SharedPreferenceManager.Companion.getMySharedPreferences().getDistance().equalsIgnoreCase("")) {
                tvDistance.setText(String.format("%.02f", Float.valueOf(data.get(position).getDistance())).replace(",", ".") + " " + context.getString(R.string.km));
            } else {
                tvDistance.setText(String.format("%.02f", Float.valueOf(convertKmsToMiles(Float.valueOf(data.get(position).getDistance())))) + " " + context.getString(R.string.mile));
            }
            /*if (data.get(position).getDistance().length() > 7) {
                tvDistance.setText(data.get(position).getDistance().substring(0, 7) + " " + context.getString(R.string.km));
            } else {
                tvDistance.setText(data.get(position).getDistance() + " " + context.getString(R.string.km));
            }*/
            ivGPS.setVisibility(View.VISIBLE);
            tvDistance.setVisibility(View.VISIBLE);
        } else {
            ivGPS.setVisibility(View.GONE);
            tvDistance.setVisibility(View.GONE);
        }


        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                likesFragment.redirectToLikeUserProfile(position);
            }
        });
        return v;
    }

    public float convertKmsToMiles(float kms) {
        float miles = (float) (0.621371 * kms);
        return miles;
    }
}

