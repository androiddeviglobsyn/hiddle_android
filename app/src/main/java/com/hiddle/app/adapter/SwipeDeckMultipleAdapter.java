package com.hiddle.app.adapter;

import android.content.Context;
import android.util.Log;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import com.bumptech.glide.Glide;
import com.hiddle.app.R;
import com.hiddle.app.fragment.HomeFragment;

import com.hiddle.app.model.UserModel.UserImageModel;
import com.hiddle.app.util.SharedPreferenceManager;
import com.hiddle.app.util.SwipeMultipleDeck;
import com.hiddle.app.util.VerticalViewPager;
import com.hiddle.app.verticaldotindicator.DotsIndicatorVertical;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Objects;

public class SwipeDeckMultipleAdapter extends BaseAdapter/* implements View.OnTouchListener */ {
    LayoutInflater inflater;
    private ArrayList<UserImageModel> data;
    private Context context;
    private String path;
    private Integer pos;


    public SwipeDeckMultipleAdapter(ArrayList<UserImageModel> data, Context context, String path, Integer pos) {
        inflater = LayoutInflater.from(context);
        this.data = data;
        this.context = context;
        this.path = path;
        this.pos = pos;

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        Log.e("item12", String.valueOf(position));
        return data.get(position);

    }

    @Override
    public long getItemId(int position) {
        Log.e("item", String.valueOf(position));
        return position;

    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View v = convertView;

        Log.e("Main", String.valueOf(position));
        if (v == null) {
            // normally use a viewholder
            v = inflater.inflate(R.layout.test_card_multiple, parent, false);
        }

        ImageView imageView = (ImageView) v.findViewById(R.id.offer_image);

        if (data.get(position).getImage() == null || data.get(position).getImage() == "") {
            v.setVisibility(View.GONE);
        } else {
            v.setVisibility(View.VISIBLE);
        }

        Glide.with(context).load(path + data.get(position).getImage()).placeholder(R.drawable.ic_placeholder_icn).into(imageView);


        return v;
    }


}
