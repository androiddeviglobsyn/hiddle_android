package com.hiddle.app.adapter

import android.content.Context
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.hiddle.app.R
import com.hiddle.app.`interface`.ClickonLanguage

class InstaMediaAdapter(private val context: Context, var list: List<String>? = null) :
        RecyclerView.Adapter<InstaMediaAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, p1: Int) = MyViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_media, parent, false)
    )


    override fun getItemCount() = list!!.size


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        Log.e("recycler", list!![position].toString())
        Glide.with(context).load(list!![position].toString()).placeholder(R.drawable.ic_placeholder_icn).into(holder.ivSelectedImage)

        holder.itemView.setOnClickListener(View.OnClickListener {

        })


    }

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        public val ivSelectedImage = itemView.findViewById(R.id.ivSelectedImage) as AppCompatImageView



    }



}
