package com.hiddle.app.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

import com.hiddle.app.R
import com.hiddle.app.`interface`.ClickUnblockUser
import com.hiddle.app.model.BlockModel.BlockDataResponse


class BlockAdapter(private val context: Context? = null, var list: ArrayList<BlockDataResponse>, var clickUnblockUser: ClickUnblockUser) :
        RecyclerView.Adapter<BlockAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, p1: Int) = MyViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_block, parent, false)
    )


    override fun getItemCount() = list.size


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {


        if (context != null) {
            holder.tvName.text = list[position].name
            Log.e("New_image",list[position].baseUrl + list[position].profile!![0].image)
            Glide.with(context).load(list[position].baseUrl + list[position].profile!![0].image.toString()).placeholder(R.drawable.ic_placeholder_icn).into(holder.ivUser)
        }


        holder.itemView.setOnClickListener {
            clickUnblockUser.clickOnUser(position)

        }
        /*}*/
    }


    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        public val tvName = itemView.findViewById(R.id.tvName) as AppCompatTextView
        public val tvUnblock = itemView.findViewById(R.id.tvUnblock) as AppCompatTextView
        public val ivUser = itemView.findViewById(R.id.ivUser) as AppCompatImageView

    }


}



