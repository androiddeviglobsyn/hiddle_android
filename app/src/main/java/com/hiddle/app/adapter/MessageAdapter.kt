package com.hiddle.app.adapter

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.facebook.share.Share
import com.hiddle.app.R
import com.hiddle.app.`interface`.MessageItemClick
import com.hiddle.app.activity.ChatActivity
import com.hiddle.app.activity.UserProfileActivity
import com.hiddle.app.model.ChatMessageModel.ChatDataResponse
import com.hiddle.app.model.StrikeModelClass.StrikeResponseData
import com.hiddle.app.util.AppUtilMethod
import com.hiddle.app.util.AppUtils
import com.hiddle.app.util.AppUtils.isSameDay
import com.hiddle.app.util.AppUtils.isToday
import com.hiddle.app.util.SharedPreferenceManager
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit


class MessageAdapter(private val context: Context? = null, var list: ArrayList<ChatDataResponse>, var messageItemClick: MessageItemClick) :
        RecyclerView.Adapter<MessageAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, p1: Int) = MyViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_message, parent, false)
    )


    override fun getItemCount() = list.size


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        if (context != null) {
            holder.tvName.text = list[position].name
            Glide.with(context).load(list[position].baseUrl + list[position].profile!!.image).placeholder(R.drawable.ic_placeholder_icn).into(holder.ivUser)
        }

        if (list[position].status == "1") {
            holder.ivOnline.visibility = View.VISIBLE
        } else {
            holder.ivOnline.visibility = View.GONE
        }
        holder.tvMessage.text = list[position].msg
        if (list[position].time != null) {
            holder.tvTime.text = convertDate(context, list[position].time)
        }


        if (list[position].count == "0") {
            holder.rlCount.visibility = View.GONE
            holder.tvCount.text = "0"
        } else {
            holder.rlCount.visibility = View.VISIBLE
            holder.tvCount.text = list[position].count
        }

        holder.cvOtherImage.visibility = View.GONE


        holder.ivMenuMessage.setOnClickListener {
            /* if (list[position].fromId == SharedPreferenceManager.getMySharedPreferences()!!.getUserId()) {
                 messageItemClick.itemClick(position, list[position].toId!!)
             } else {*/
            messageItemClick.itemClick(position, list[position].userId!!)

            /*  }*/


        }

        holder.itemView.setOnClickListener {
            messageItemClick.itemChatClick(position)
        }

        holder.ivUser.setOnClickListener {
            messageItemClick.itemUserProfileClick(position)
            }
    }

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        public val tvName = itemView.findViewById(R.id.tvName) as AppCompatTextView
        public val cvOtherImage = itemView.findViewById(R.id.cvOtherImage) as CardView
        public val ivUser = itemView.findViewById(R.id.ivUser) as AppCompatImageView
        public val ivMenuMessage = itemView.findViewById(R.id.ivMenuMessage) as AppCompatImageView
        public val ivOnline = itemView.findViewById(R.id.ivOnline) as AppCompatImageView

        public val tvTime = itemView.findViewById(R.id.tvTime) as AppCompatTextView
        public val tvMessage = itemView.findViewById(R.id.tvMessage) as AppCompatTextView
        public val rlCount = itemView.findViewById(R.id.rlCount) as RelativeLayout
        public val tvCount = itemView.findViewById(R.id.tvCount) as AppCompatTextView
    }

    public fun convertDate(context: Context?, dateFromServer: String?): String {

        //convert date one format to another format
        var originalFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH)
        var targetFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        var date = originalFormat.parse(dateFromServer)
        var formattedServerDate = targetFormat.format(date) // 20120821
        Log.e("format", formattedServerDate)


        //convert server time in local time
        val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"))
        val value = formatter.parse(formattedServerDate)
        val dateFormatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss") //this format changeable
        dateFormatter.setTimeZone(TimeZone.getDefault())
        var server_date = dateFormatter.format(value)   //main server date

        var main_date = formatToYesterdayOrToday(server_date)  //get today,yesterday and date

        return main_date
    }

    @Throws(ParseException::class)
    fun formatToYesterdayOrToday(date: String): String {
        val dateTime = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(date)
        val calendar = Calendar.getInstance()
        calendar.setTime(dateTime)
        val today = Calendar.getInstance()
        val yesterday = Calendar.getInstance()
        yesterday.add(Calendar.DATE, -1)
        // val timeFormatter = SimpleDateFormat("hh:mma")
        val timeFormatter = SimpleDateFormat("HH:mm")
        val dateFormatter = SimpleDateFormat("dd/MM/yy")
        if (calendar.get(Calendar.YEAR) === today.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) === today.get(Calendar.DAY_OF_YEAR)) {
            return /*"Today " +*/ timeFormatter.format(dateTime)
        } else if (calendar.get(Calendar.YEAR) === yesterday.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) === yesterday.get(Calendar.DAY_OF_YEAR)) {
            return "Yesterday "/* + timeFormatter.format(dateTime)*/
        } else {
            return timeFormatter.format(dateTime)
        }
    }


}


