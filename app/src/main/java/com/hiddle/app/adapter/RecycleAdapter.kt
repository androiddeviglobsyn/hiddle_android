package com.hiddle.app.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

import com.hiddle.app.R
import com.hiddle.app.`interface`.ClickUnblockUser
import com.hiddle.app.model.BlockModel.BlockDataResponse


class RecycleAdapter(private val context: Context? = null, var list: MutableList<String>,var path:String) :
        RecyclerView.Adapter<RecycleAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, p1: Int) = MyViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.test_card_multiple, parent, false)
    )


    override fun getItemCount() = list.size


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {


        if (context != null) {

            Glide.with(context).load(path + list[position]).placeholder(R.drawable.ic_placeholder_icn).into(holder.offer_image)
        }



    }


    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        public val offer_image = itemView.findViewById(R.id.offer_image) as ImageView

    }


}



