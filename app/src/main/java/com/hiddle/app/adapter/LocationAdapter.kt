package com.hiddle.app.adapter

import android.content.Context
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.hiddle.app.R
import com.hiddle.app.`interface`.DeleteLocationClick
import com.hiddle.app.activity.HomeActivity

import com.hiddle.app.model.LocationModel
import java.util.*
import kotlin.collections.ArrayList


class LocationAdapter(private val context: Context? = null, var list: ArrayList<LocationModel>, var delteLocationClick: DeleteLocationClick, val homeActivity: HomeActivity) :
        RecyclerView.Adapter<LocationAdapter.MyViewHolder>() {
    companion object {
        @JvmStatic
        var selectedLocationList: ArrayList<String> = ArrayList()
    }

    var geocoder: Geocoder? = null
    var addresses: List<Address> = ArrayList()
    var address: String = ""
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int) = MyViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_location, parent, false)
    )


    override fun getItemCount() = list.size


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        if (context != null) {

            if(list[position].name==""){
                holder.tvName.text = getAddressFrmLatlangLocation(list[position].lat!!, list[position].lang!!, position)
                holder.tvName.text = getAddressFrmLatlangLocation(list[position].lat!!, list[position].lang!!, position)
            }else{
                holder.tvName.text = list[position].name
            }


            if (list[position].isSelected==true) {
                holder.ivCheck.visibility = View.VISIBLE
                holder.tvName.setTextColor(context.getColor(R.color.light_blue))
            } else {
                holder.ivCheck.visibility = View.GONE
                holder.tvName.setTextColor(context.getColor(R.color.white))
            }



            holder.ivDelete.setOnClickListener {
                delteLocationClick.clickOnLocatioDelete(position)
            }

            holder.itemView.setOnClickListener {

               /* selectedLocationList.clear()
                selectedLocationList.add(list[position].id!!)*/
                homeActivity.setAdapterLocationPos(position)


                /* if ( selectedLocationList.size > 0) {
                     try {
                         if (! selectedLocationList.contains(list[position].id)) {
                             selectedLocationList.add(list[position].id!!)
                             holder.ivCheck.visibility =  View.VISIBLE
                             holder.tvName.setTextColor(context.getColor(R.color.light_blue))
                         } else {
                             for (p in  selectedLocationList.indices) {
                                 if ( selectedLocationList[p] == list[position].id) {
                                     selectedLocationList.removeAt(p)
                                 }
                             }
                             holder.ivCheck.visibility =  View.GONE
                             holder.tvName.setTextColor(context.getColor(R.color.white))
                         }
                     } catch (e: Exception) {

                     }

                 } else {
                     selectedLocationList.add(list[position].id!!)
                     holder.ivCheck.visibility =  View.VISIBLE
                     holder.tvName.setTextColor(context.getColor(R.color.light_blue))
                 }*/


            }
        }
    }

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        public val ivCheck = itemView.findViewById(R.id.ivCheck) as AppCompatImageView
        public val ivDelete = itemView.findViewById(R.id.ivDelete) as LinearLayout
        public val tvName = itemView.findViewById(R.id.tvName) as AppCompatTextView

    }


    fun updateList(list: ArrayList<LocationModel>) {
        this.list = list
        notifyDataSetChanged()
    }

    private fun getAddressFrmLatlangLocation(paramLatitude: String, paramLongitude: String, position: Int): String {
        try {
            address = ""
            geocoder = Geocoder(context, Locale.ENGLISH)
            addresses = geocoder!!.getFromLocation(paramLatitude.toDouble(), paramLongitude.toDouble(), 1) // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            if (addresses.isNotEmpty()) {
                /*if (addresses[0].countryCode != null) {
                    if (addresses[0].locality != null) {
                        address = addresses[0].locality
                    }*/
                /*if (addresses[0].adminArea != null) {
                    address = addresses[0].adminArea
                    Log.e("address1",address)
                }*/
                if (addresses.isNotEmpty()) {
                    if (addresses[0].locality != null) {
                        address = addresses[0].locality
                        Log.e("address5", address)
                    } else {
                        if (addresses[0].adminArea != null) {
                            address = addresses[0].adminArea
                        } else {
                            if (address == "") {
                                if (addresses[0].getAddressLine(0) != null) {
                                    address = addresses[0].getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                                    Log.e("address2", address)
                                }
                            }
                        }
                    }
                }
                /* if (addresses[0].countryName != null) {
                     address = "," + addresses[0].countryName
                 }
                 Log.e("Name", address)
             }*/
            } else {
                if (address == "") {
                    address = list[position].name!!
                }

            }


            /* if (addresses[0].getAddressLine(0) != null) {
                 address = addresses[0].getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
             }*/


        } catch (e: Exception) {

        }
        return address
    }


}
