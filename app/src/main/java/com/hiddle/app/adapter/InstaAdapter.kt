package com.hiddle.app.adapter

import android.content.Context
import android.os.Parcelable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.PagerAdapter
import com.hiddle.app.R
import com.hiddle.app.model.InstaModel
import kotlinx.android.synthetic.main.activity_block.*


class InstaAdapter(private val context: Context, private val imageModelArrayList: List<InstaModel>) : PagerAdapter() {
    private val inflater: LayoutInflater


    init {

        inflater = LayoutInflater.from(context)
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun getCount(): Int {
        return imageModelArrayList.size
    }

    override fun instantiateItem(view: ViewGroup, position: Int): Any {
        val imageLayout = inflater.inflate(R.layout.item_insta, view, false)!!

        val rvInstaItem = imageLayout.findViewById(R.id.rvInstaItem) as RecyclerView

        Log.e("viewpager", imageModelArrayList[position].toString())

        /*   val imageView = imageLayout.findViewById(R.id.image) as ImageView
           Glide.with(context).load(url+imageModelArrayList[position]).placeholder(R.drawable.ic_placeholder_icn).into(imageView)
   */

        //this for row
        //val layoutManager: GridLayoutManager = GridLayoutManager(context, 2, RecyclerView.HORIZONTAL, false) //set adapter

        /*    layoutManager.setSpanSizeLookup(onSpanSizeLookup)
            val onSpanSizeLookup = object:GridLayoutManager.SpanSizeLookup() {
                fun getSpanSize(position:Int):Int {
                    return if (mHomeListAdapter.getItemViewType(position) === TYPE_PREMIUM) 2 else 1
                }
            }
    */
     /*   val spanCount = 3 // 3 columns
        val spacing = 20 // 50px
        val includeEdge = true
        rvInstaItem.addItemDecoration(GridSpacingItemDecoration(spanCount, spacing, includeEdge))*/

        val numberOfColumns = 3



        Log.e("viewpager", imageModelArrayList[position].list!!.get(0).toString())


        val gridLayoutManager = GridLayoutManager(context, 3, LinearLayoutManager.VERTICAL, false)
        rvInstaItem.layoutManager = gridLayoutManager // set LayoutManager to RecyclerView
        val instaMediaAdapter: InstaMediaAdapter = InstaMediaAdapter(context, imageModelArrayList[position].list)
        rvInstaItem.adapter = instaMediaAdapter

        view.addView(imageLayout, 0)

        return imageLayout
    }

    fun RecyclerView.autoFitColumns(columnWidth: Int) {
        val displayMetrics = this.context.resources.displayMetrics
        val noOfColumns = ((displayMetrics.widthPixels / displayMetrics.density) / columnWidth).toInt()
        this.layoutManager = GridLayoutManager(this.context, noOfColumns)
    }
    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun restoreState(state: Parcelable?, loader: ClassLoader?) {}

    override fun saveState(): Parcelable? {
        return null
    }

}