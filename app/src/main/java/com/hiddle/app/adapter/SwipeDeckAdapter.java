package com.hiddle.app.adapter;

import android.content.Context;
import android.util.Log;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.hiddle.app.R;
import com.hiddle.app.fragment.HomeFragment;
import com.hiddle.app.horizontaldotindicator.DotsIndicator;
import com.hiddle.app.model.UserModel.UserDataResponse;
import com.hiddle.app.model.UserModel.UserImageModel;
import com.hiddle.app.util.SharedPreferenceManager;
import com.hiddle.app.util.SwipeMultipleDeck;
import com.hiddle.app.util.VerticalViewPager;
import com.hiddle.app.verticaldotindicator.DotsIndicatorVertical;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Objects;

public class SwipeDeckAdapter extends BaseAdapter/* implements View.OnTouchListener */ {
    LayoutInflater inflater;
    private ArrayList<UserDataResponse> data;
    private Context context;
    HomeFragment homeFragment;
    public static ImageView imageView;
    public static Integer itemsize = 1;

    public SwipeDeckAdapter(ArrayList<UserDataResponse> data, Context context, HomeFragment homeFragment) {
        inflater = LayoutInflater.from(context);
        this.data = data;
        this.context = context;
        this.homeFragment = homeFragment;

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        Log.e("item12", String.valueOf(position));
        return data.get(position);

    }

    @Override
    public long getItemId(int position) {
        Log.e("item", String.valueOf(position));
        return position;

    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View v = convertView;

        Log.e("Main", String.valueOf(position));
        if (v == null) {
            // normally use a viewholder
            v = inflater.inflate(R.layout.test_card2, parent, false);
        }
        //((TextView) v.findViewById(R.id.textView2)).setText(data.get(position));
        imageView = (ImageView) v.findViewById(R.id.offer_image);
        AppCompatTextView tvUserName = (AppCompatTextView) v.findViewById(R.id.tvUsername);
        AppCompatTextView tvDistance = (AppCompatTextView) v.findViewById(R.id.tvDistance);
        AppCompatImageView ivGPS = (AppCompatImageView) v.findViewById(R.id.ivGPS);
        VerticalViewPager vpHome = (VerticalViewPager) v.findViewById(R.id.vpHome);
        RecyclerView rvHome = (RecyclerView)v.findViewById(R.id.rvHome) ;

        DotsIndicatorVertical dots_indicator_home = (DotsIndicatorVertical) v.findViewById(R.id.dots_indicator_home);
        SwipeMultipleDeck swipe_deck_multiple = (SwipeMultipleDeck) v.findViewById(R.id.swipe_deck_multiple);


        if (data.get(position).getImages() != null) {
            if (Objects.requireNonNull(data.get(position).getImages()).size() > 0) {
                if (data.get(position).getImages().get(0) != null) {
                    Glide.with(context).load(data.get(position).getBaseUrl() + Objects.requireNonNull(data.get(position).getImages()).get(0)).placeholder(R.drawable.ic_placeholder_icn).into(imageView);
                }
            }
        }
        Log.e("listname",data.get(position).getImages().toString());
       /* vpHome.setAdapter(new SlidingHomeImage_Adapter(context, (ArrayList<String>) data.get(position).getImages(), data.get(position).getBaseUrl()));
        dots_indicator_home.setViewPager(vpHome);

        Log.e("size", String.valueOf(data.get(position).getImages().size()));*/

       /* rvHome.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));  //set adapter
        RecycleAdapter blockAdapter = new  RecycleAdapter(context, data.get(position).getImages(), data.get(position).getBaseUrl());
        rvHome.setAdapter(blockAdapter);*/

        SwipeDeckMultipleAdapter adapter = new SwipeDeckMultipleAdapter((ArrayList<UserImageModel>) data.get(position).getImages(), context, data.get(position).getBaseUrl(), position);
        swipe_deck_multiple.setAdapter(adapter);

        swipe_deck_multiple.setEventCallback(new SwipeMultipleDeck.SwipeEventCallback() {
            @Override
            public void cardSwipedLeft(int p) {
                homeFragment.callLeftEvent(position);
                Log.e("adapter_profile1", String.valueOf(position));
            }

            @Override
            public void cardSwipedRight(int p) {
                homeFragment.callRightEvent(position);
                Log.e("adapter_profile2", String.valueOf(position));
            }

            @Override
            public void cardsDepleted() {

            }

            @Override
            public void cardActionDown() {

            }

            @Override
            public void cardActionUp(int p, int size) {
                SwipeMultipleDeck.main_size = data.get(position).getImages().size();
                //itemsize =  data.get(position).getImages().size();
            }
        });
        swipe_deck_multiple.setLeftImage(R.id.left_image);
        swipe_deck_multiple.setRightImage(R.id.right_image);
       /* adapter = SwipeDeckAdapter(userList, mContext, this@HomeFragment)
        cardStack!!.setAdapter(adapter)
        cardStack!!.setSelection(Companion.selectedPosition)*/

        tvUserName.setText(data.get(position).getUsername());
        if (data.get(position).getDistance() != null) {
            try {
                if (SharedPreferenceManager.Companion.getMySharedPreferences().getDistance().equalsIgnoreCase("Km") || SharedPreferenceManager.Companion.getMySharedPreferences().getDistance().equalsIgnoreCase("km") || SharedPreferenceManager.Companion.getMySharedPreferences().getDistance().equalsIgnoreCase("")) {
                    tvDistance.setText(String.format("%.02f", Float.valueOf(data.get(position).getDistance())) + " " + context.getString(R.string.km));
                } else {
                    tvDistance.setText(String.format("%.02f", Float.valueOf(convertKmsToMiles(Float.valueOf(data.get(position).getDistance())))) + " " + context.getString(R.string.mile));
                }
            } catch (Exception e) {
            }
           /* if (data.get(position).getDistance().length() > 7) {
                tvDistance.setText(data.get(position).getDistance().substring(0, 7) + " " + context.getString(R.string.km));
            } else {
                tvDistance.setText(data.get(position).getDistance() + " " + context.getString(R.string.km));
            }*/
            ivGPS.setVisibility(View.VISIBLE);
            tvDistance.setVisibility(View.VISIBLE);
        } else {
            ivGPS.setVisibility(View.GONE);
            tvDistance.setVisibility(View.GONE);
        }


       /* vpHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                homeFragment.redirectToUserProfile(position);

            }
        });*/
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeFragment.redirectToUserProfile(position);
            }
        });
        return v;
    }

    public float convertKmsToMiles(float kms) {
        float miles = (float) (0.621371 * kms);
        return miles;
    }
}
