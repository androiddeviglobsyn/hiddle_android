package com.hiddle.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.hiddle.app.R;
import com.hiddle.app.chat.MessageResponse;

import android.util.Log;

import java.util.List;
import java.util.Objects;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {

    private int MSG_TYPE_LEFT = 0;
    private int MSG_TYPE_RIGHT = 1;
    private int RIGHT_MSG_IMG = 2;
    private int LEFT_MSG_IMG = 3;
    private int RIGHT_MSG_Loc = 4;
    private int LEFT_MSG_Loc = 5;
    private int RIGHT_AUDIO = 6;
    private int LEFT_AUDIO = 7;
    private int RIGHT_CONTACT = 8;
    private int LEFT_CONTACT = 9;
    private int RIGHT_BLANK = 10;
    private int LEFT_SUGGESTION = 11;
    private int RIGHT_SUGGESTION = 12;
    private int RIGHT_MSG_Loc_Venue = 13;
    private int LEFT_MSG_Loc_Venue = 14;
    private int MSG_UPDATE_SEEK_BAR = 1845;
    private Context mContext;
    public List<MessageResponse> mChat;
    private int MSG_TYPE_Bottom = 222;
    String UserId;


    public ChatAdapter(Context mContext, List<MessageResponse> mChat, String UserId) {
        this.mChat = mChat;
        this.mContext = mContext;
        this.UserId = UserId;
    }

    @NonNull
    @Override
    public ChatAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == MSG_TYPE_RIGHT) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.item_right_text_chat, parent, false);
            return new ChatAdapter.ViewHolder(view);
        } else if (viewType == MSG_TYPE_LEFT) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.item_left_text_chat, parent, false);
            return new ChatAdapter.ViewHolder(view);
        } else if (viewType == RIGHT_MSG_IMG) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_right_img, parent, false);
            return new ChatAdapter.ViewHolder(view);
        } else if (viewType == LEFT_MSG_IMG) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_left_img, parent, false);
            return new ChatAdapter.ViewHolder(view);
        } else if (viewType == MSG_TYPE_Bottom) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_type_indicator_chat_left_main, parent, false);
            return new ChatAdapter.ViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_left_text_chat, parent, false);
            return new ChatAdapter.ViewHolder(view);
        }
    }


    public void addItems(List<MessageResponse> postItems) {
        for (int i = 0; i < postItems.size(); i++) {
            mChat.add(i, postItems.get(i));
            notifyItemInserted(i);
        }
    }

    public void addNewItems(List<MessageResponse> postItems) {
        mChat.add(0, postItems.get(0));
        notifyItemInserted(0);
    }

    public void addNewItemsNew(List<MessageResponse> postItems) {
        mChat.add(mChat.size() - 1, postItems.get(0));
        notifyItemInserted(mChat.size() - 1);
    }
    public void updateLaseItems(List<MessageResponse> postItems) {

        //for update typing view as last item of list
        mChat.set(mChat.size() - 1, postItems.get(0));
        notifyItemChanged(mChat.size() - 1);
    }
    @Override
    public void onBindViewHolder(@NonNull ChatAdapter.ViewHolder holder, int position) {
        if (mChat.size() > 0) {
            Log.e("position", String.valueOf(position));
            if (mChat.get(position).isChatTypeVisible() != null) {
                if (mChat.get(position).isChatTypeVisible().equals("N")||mChat.get(position).isChatTypeVisible().equals("")) {
                    holder.llTypeIndicatorChat.setVisibility(View.INVISIBLE);
                } else {
                    holder.llTypeIndicatorChat.setVisibility(View.VISIBLE);
                }
            }
        }


        if (mChat.size() > 0) {
            if (position == mChat.size()) {
                Log.e("position", String.valueOf(position) + "size" + String.valueOf(mChat.size()));
               //this position for typing view
            } else {
                //set chat data
                if (mChat.get(position).getFrom_id() != "") {
                    MessageResponse model = mChat.get(position);
                    if (Objects.equals(model.getFrom_id(), UserId)) {
                        holder.tvMessage.setText(model.getMessage());
                    } else {
                        holder.tvMessage.setText(model.getMessage());
                    }
                }

            }

        }

    }


    @Override
    public int getItemCount() {
        Log.e("size_main", String.valueOf(mChat.size()));
        return mChat.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        if (mChat.get(position).getFrom_id() == "" && mChat.get(position).getTo_id() == "") {
            return MSG_TYPE_Bottom;
        } else {
            if (Objects.equals(mChat.get(position).getFrom_id(), UserId)) {
                return MSG_TYPE_RIGHT;
            } else {
                return MSG_TYPE_LEFT;
            }
        }
    }
    /*@Override
    public int getItemViewType(int position) {
        MessageResponse model = mChat.get(position);
        model.getMedia_type();

        if (model.getMedia_type().equals("Media")) {
            if (model.getFrom_user_id().equals(UserId)) {
                return RIGHT_MSG_IMG;
            } else {
                return LEFT_MSG_IMG;
            }
        } else if (model.getMedia_type().equals("Text")) {
            if (model.getFrom_user_id().equals(UserId)) {
                return MSG_TYPE_RIGHT;
            } else {
                return MSG_TYPE_LEFT;
            }
        }
        return 0;
    }*/


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvMessage, tvTime;
        LinearLayout llTypeIndicatorChat;

        public ViewHolder(View itemView) {
            super(itemView);

            tvTime = itemView.findViewById(R.id.tvTime);
            tvMessage = itemView.findViewById(R.id.tvMessage);

            llTypeIndicatorChat = itemView.findViewById(R.id.llTypeIndicatorChat);


        }


    }


}
