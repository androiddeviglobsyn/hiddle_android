package com.hiddle.app.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.*
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.appcompat.widget.AppCompatImageView
import androidx.cardview.widget.CardView
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.hiddle.app.*
import com.hiddle.app.`interface`.AddImageClick
import com.hiddle.app.`interface`.StartProfileDragListener
import com.hiddle.app.itemDrag.ItemMoveProfileCallback
import com.hiddle.app.model.DragData
import com.hiddle.app.model.ImageModel
import java.util.*


class EditProfileAdapter(private val context: Context? = null, var picturelist: ArrayList<ImageModel>, val addImageClick: AddImageClick, val mstartDragListener: StartProfileDragListener) :
        RecyclerView.Adapter<EditProfileAdapter.MyViewHolder>(), ItemMoveProfileCallback.ItemTouchHelperContract {
    private val HEADER_VIEW = 0
    private val FOOTER_VIEW = 1

    companion object {
        @JvmStatic
        var selectedItem: Int = -1
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, type: Int): EditProfileAdapter.MyViewHolder {
        var v: View? = null
        if (type === FOOTER_VIEW) {
            v = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_edit_picture, viewGroup, false)
        } else {
            v = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_edit_picture_new, viewGroup, false)
            /* return HeaderViewHolder(v)*/
        }
        return MyViewHolder(v)

        /*val view: View = LayoutInflater.from(parent.context).inflate(R.layout.item_edit_picture, parent, false)
        val holder = MyViewHolder(view)
        val shape: View = holder.rlPicture


        holder.itemView.setOnLongClickListener(OnLongClickListener {
            setViewMethod(holder, shape)
            true
        })
        return holder*/
    }

    private fun setViewMethod(holder: MyViewHolder, shape: View) {
        selectedItem = holder.adapterPosition
        if (picturelist[selectedItem].is_new != "") {
            selectedItem = holder.getAdapterPosition()
            val item: ImageModel = picturelist.get(holder.getAdapterPosition())
            val state = DragData(item, shape.width, shape.height)
            val shadow = DragShadowBuilder(shape)
            ViewCompat.startDragAndDrop(shape, null, shadow, state, 0)
        }


    }


    override fun getItemCount() = picturelist.size


    /*  @SuppressLint("ClickableViewAccessibility")
      override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

          if (context != null) {

              if (picturelist[position].is_new == "Y") {
                  if (!picturelist[position].equals("")) {
                      Glide.with(context).load(picturelist[position].new_image).placeholder(R.drawable.ic_placeholder_icn).into(holder.ivPicture)
                  }
              }
              if (picturelist[position].is_new == "N") {
                  Glide.with(context).load(picturelist[position].old_image).placeholder(R.drawable.ic_placeholder_icn).into(holder.ivPicture)
              }
              if (picturelist[position].is_new == "") {
                  holder.ivPicture.setImageResource(R.drawable.ic_picture_placeholder)


              }

              holder.itemView.setOnClickListener {
                  addImageClick.addImageSelection(position)   // method for open camera
              }

              holder.itemView.setOnTouchListener { v, event ->
                  if (picturelist[position].is_new != "") {
                      if ((event.getAction() === MotionEvent.ACTION_DOWN)) {
                          mstartDragListener.requestDrag(holder)
                      }
                  }
                  false
              }
          }
      }*/


    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        internal var ivPicture: AppCompatImageView? = null
        internal var cvPicture: CardView? = null
        internal var rlPicture: RelativeLayout? = null
        internal var ivRemoveMainProfile: AppCompatImageView? = null

        init {
            ivPicture = itemView.findViewById(R.id.ivPicture) as AppCompatImageView
            cvPicture = itemView.findViewById(R.id.cvPicture) as CardView
            rlPicture = itemView.findViewById(R.id.rlPicture) as RelativeLayout
            ivRemoveMainProfile = itemView.findViewById(R.id.ivRemoveMainProfile) as AppCompatImageView
        }
    }

    /*private class HeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var ivPicture: AppCompatImageView? = null
        internal var cvPicture: CardView? = null
        internal var rlPicture: RelativeLayout? = null
        internal var ivRemoveMainProfile: AppCompatImageView? = null

        init {
            ivPicture = itemView.findViewById(R.id.ivPicture) as AppCompatImageView
            cvPicture = itemView.findViewById(R.id.cvPicture) as CardView
            rlPicture = itemView.findViewById(R.id.rlPicture) as RelativeLayout
            ivRemoveMainProfile = itemView.findViewById(R.id.ivRemoveMainProfile) as AppCompatImageView
        }
    }*/

    /* class MyViewHolder internal constructor(view: View) : RecyclerView.ViewHolder(view) {
         internal var ivPicture: AppCompatImageView? = null
         internal var cvPicture: CardView? = null
         internal var rlPicture: RelativeLayout? = null

         init {
             ivPicture = itemView.findViewById(R.id.ivPicture) as AppCompatImageView
             cvPicture = itemView.findViewById(R.id.cvPicture) as CardView
             rlPicture = itemView.findViewById(R.id.rlPicture) as RelativeLayout
         }
     }*/


    override fun onRowMoved(fromPosition: Int, toPosition: Int) {
        if (picturelist[fromPosition].is_new != "" && picturelist[toPosition].is_new != "") {
            if (fromPosition < toPosition) {
                for (i in fromPosition until toPosition) {
                    Collections.swap(picturelist, i, i + 1)
                }
            } else {
                for (i in fromPosition downTo toPosition + 1) {
                    Collections.swap(picturelist, i, i - 1)
                }

            }
            notifyItemMoved(fromPosition, toPosition)
        }

        Log.e("swap_position", fromPosition.toString() + "to" + toPosition.toString())

    }


    override fun onRowSelected(myViewHolder: MyViewHolder?) {
        Log.e("select", "gray")
        /*  myViewHolder.rowView.setBackgroundColor(Color.GRAY)*/
    }

    override fun onRowClear(myViewHolder: MyViewHolder?) {
        Log.e("select", "white")
        /*  myViewHolder.rowView.setBackgroundColor(Color.WHITE)*/
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onBindViewHolder(viewHolder: MyViewHolder, position: Int) {


        /*val position = viewHolder.adapterPosition*/
        /*if (viewHolder is MyViewHolder) {
            val currentView = viewHolder as MyViewHolder
*/
        if (context != null) {
            /* if (picturelist[position].is_new == "Y") {
                 if (!picturelist[position].is_new.equals("")) {
                     Glide.with(context).load(picturelist[position].new_image).placeholder(R.drawable.ic_placeholder_icn).into(viewHolder.ivPicture!!)
                 }
             }*/
            if (picturelist[position].is_new == "N" || picturelist[position].is_new == "Y") {
                Glide.with(context).load(picturelist[position].new_image).placeholder(R.drawable.ic_placeholder_icn).into(viewHolder.ivPicture!!)
            }
            if (picturelist[position].is_new == "") {
                viewHolder.ivPicture!!.setImageResource(R.drawable.ic_picture_placeholder)
            }
/*
                currentView.itemView.setOnTouchListener { v, event ->
                    if (picturelist[position].is_new != "") {
                        if ((event.getAction() === MotionEvent.ACTION_DOWN)) {
                            mstartDragListener.requestDrag(currentView)
                        }
                    }
                    false*/
        }
        /*  }*/

        /*if (viewHolder is HeaderViewHolder)
        {
            val currentView = viewHolder as HeaderViewHolder

            if (context != null) {

                if (picturelist[position].is_new == "Y") {
                    if (!picturelist[position].equals("")) {
                        Glide.with(context).load(picturelist[position].new_image).placeholder(R.drawable.ic_placeholder_icn).into(currentView.ivPicture!!)
                    }
                }
                if (picturelist[position].is_new == "N") {
                    Glide.with(context).load(picturelist[position].old_image).placeholder(R.drawable.ic_placeholder_icn).into(currentView.ivPicture!!)
                }
                if (picturelist[position].is_new == "") {
                    currentView.ivPicture!!.setImageResource(R.drawable.ic_picture_placeholder)
                }
*//*
                currentView.itemView.setOnTouchListener { v, event ->
                    if (picturelist[position].is_new != "") {
                        if ((event.getAction() === MotionEvent.ACTION_DOWN)) {
                            mstartDragListener.requestDrag(currentView)
                        }
                    }
                    false*//*
            }
        }
*/
        viewHolder.itemView.setOnClickListener {
            addImageClick.addImageSelection(position)   // method for open camera
        }
        viewHolder.cvPicture!!.setOnClickListener {
            addImageClick.addImageSelection(position)   // method for open camera
        }

        viewHolder.ivRemoveMainProfile!!.setOnClickListener {
            addImageClick.addImageSelection(position)   // method for open camera
        }

        viewHolder.ivPicture!!.setOnClickListener {
            addImageClick.addImageSelection(position)   // method for open camera
        }
        viewHolder.itemView.setOnLongClickListener(OnLongClickListener {
            if (picturelist[position].is_new != "") {
                mstartDragListener.requestProfileDrag(viewHolder)
            }
            true
        })
        viewHolder.cvPicture!!.setOnLongClickListener(OnLongClickListener {
            if (picturelist[position].is_new != "") {
                mstartDragListener.requestProfileDrag(viewHolder)
            }
            true
        })
        viewHolder.ivPicture!!.setOnLongClickListener(OnLongClickListener {
            if (picturelist[position].is_new != "") {
                mstartDragListener.requestProfileDrag(viewHolder)
            }
            true
        })
        /* if (position != 0)
         {
             viewHolder.itemView.setOnTouchListener { v, event ->
                 if (picturelist[position].is_new != "") {
                     if (position != 0) {
                         if ((event.getAction() === MotionEvent.ACTION_DOWN)) {
                             mstartDragListener.requestProfileDrag(viewHolder)
                         }
                     }
                 }
                 true
             }
             viewHolder.cvPicture!!.setOnTouchListener { v, event ->
                 if (picturelist[position].is_new != "") {
                     if (position != 0) {
                         if ((event.getAction() === MotionEvent.ACTION_DOWN)) {
                             mstartDragListener.requestProfileDrag(viewHolder)
                         }
                     }
                 }
                 true
             }
             viewHolder.ivPicture!!.setOnTouchListener { v, event ->
                 if (picturelist[position].is_new != "") {
                     if (position != 0) {
                         if ((event.getAction() === MotionEvent.ACTION_DOWN)) {
                             mstartDragListener.requestProfileDrag(viewHolder)
                         }
                     }
                 }
                 true
             }
         }*/

    }

    /* fun updateUsers(newUsers: List<AString>) {
         picturelist.clear()
         picturelist.addAll(newUsers)
         notifyDataSetChanged()
     }*/

    override fun getItemViewType(position: Int): Int {
        if (position == 0) {
            return HEADER_VIEW
        }
        return FOOTER_VIEW
    }
}
