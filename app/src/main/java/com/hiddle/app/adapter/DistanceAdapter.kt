package com.hiddle.app.adapter

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.hiddle.app.R
import com.hiddle.app.`interface`.ClickDistance
import com.hiddle.app.model.DistanceModeel

class DistanceAdapter(private val context: Context? = null, var list: List<DistanceModeel>, var language: String, var clickDistance: ClickDistance) :
        RecyclerView.Adapter<DistanceAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, p1: Int) = MyViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_language, parent, false)
    )


    override fun getItemCount() = list.size


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {


        holder.tvName.text = list[position].distance_language

        if (language != "") {
            if (list[position].distance_parameter.equals(language,true)) {
                holder.ivCheck.visibility = View.VISIBLE
                holder.tvName.setTextColor(context!!.getColor(R.color.light_blue))
            } else {
                holder.ivCheck.visibility = View.GONE
                holder.tvName.setTextColor(context!!.getColor(R.color.white))
            }
        } else {
            if (position == 0) {
                holder.ivCheck.visibility = View.VISIBLE
                holder.tvName.setTextColor(context!!.getColor(R.color.light_blue))
            } else {
                holder.ivCheck.visibility = View.GONE
                holder.tvName.setTextColor(context!!.getColor(R.color.white))
            }
        }

        holder.itemView.setOnClickListener(View.OnClickListener {
            clickDistance.clickonDistance(position)
        })


    }

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        public val ivCheck = itemView.findViewById(R.id.ivCheck) as AppCompatImageView

        public val tvName = itemView.findViewById(R.id.tvName) as AppCompatTextView

    }

    fun updateList(list: ArrayList<DistanceModeel>) {
        this.list = list
        notifyDataSetChanged()
    }

}
