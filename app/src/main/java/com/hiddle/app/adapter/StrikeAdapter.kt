package com.hiddle.app.adapter

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.chauthai.swipereveallayout.SwipeRevealLayout
import com.chauthai.swipereveallayout.ViewBinderHelper
import com.hiddle.app.R
import com.hiddle.app.`interface`.ClickForDeleteItem
import com.hiddle.app.activity.ChatActivity
import com.hiddle.app.activity.UserProfileActivity

import com.hiddle.app.model.StrikeModelClass.StrikeResponseData
import com.hiddle.app.util.AppUtils


class StrikeAdapter(private val context: Context? = null, var list: ArrayList<StrikeResponseData>, var clickForDeleteItem: ClickForDeleteItem) :
        RecyclerView.Adapter<StrikeAdapter.MyViewHolder>() {
    private val binderHelper = ViewBinderHelper()

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int) = MyViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_strike, parent, false)
    )


    override fun getItemCount() = list.size


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val dataObject: StrikeResponseData = list[position]

        binderHelper.bind(holder.swipe_layout, dataObject.userId);
        binderHelper.setOpenOnlyOne(true);


        if (context != null) {
            holder.tvName.text = list[position].name
            Glide.with(context).load(list[position].baseUrl + list[position].profile).placeholder(R.drawable.ic_placeholder_icn).into(holder.ivUser)

            if (list[position].education != null) {
                if (list[position].education == "") {
                    holder.tvModel.text = list[position].age + " " + context.getString(R.string.years)
                } else {
                    holder.tvModel.text = list[position].education + " , " + list[position].age
                }

            } else {
                holder.tvModel.text = list[position].age + " " + context.getString(R.string.years)
            }

            holder.tvPlace.text = list[position].address
            holder.tvPlace.isSelected = true;

            if (list[position].is_online == "1") {
                holder.ivOnline.visibility = View.VISIBLE
            } else {
                holder.ivOnline.visibility = View.GONE
            }


        }

        holder.ivDelete.setOnClickListener {
            clickForDeleteItem.deleteItem(position)
        }

        holder.llMessage.setOnClickListener {
            clickForDeleteItem.redirectToChatScreen(position)
        }
        holder.ivUser.setOnClickListener {
            clickForDeleteItem.redirectUserProfile(position)

        }
        holder.llMiddleMessage.setOnClickListener {
           /* clickForDeleteItem.redirectUserProfile(position)*/
        }

    }

    fun saveStates(outState: Bundle?) {
        binderHelper.saveStates(outState)
    }

    /**
     * Only if you need to restore open/close state when the orientation is changed.
     * Call this method in
     */
    fun restoreStates(inState: Bundle?) {
        binderHelper.restoreStates(inState)
    }

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        public val tvName = itemView.findViewById(R.id.tvName) as AppCompatTextView
        public val swipe_layout = itemView.findViewById(R.id.swipe_layout) as SwipeRevealLayout
        public val ivUser = itemView.findViewById(R.id.ivUser) as AppCompatImageView
        public val tvModel = itemView.findViewById(R.id.tvModel) as AppCompatTextView
        public val tvPlace = itemView.findViewById(R.id.tvPlace) as AppCompatTextView
        public val ivDelete = itemView.findViewById(R.id.ivDelete) as AppCompatImageView
        public val llMessage = itemView.findViewById(R.id.llMessage) as LinearLayout
        public val ivOnline = itemView.findViewById(R.id.ivOnline) as AppCompatImageView
        public val llMiddleMessage: LinearLayout = itemView.findViewById(R.id.llMiddleMessage) as LinearLayout
    }


}



