package com.hiddle.app.service

import android.accounts.AuthenticatorException
import android.accounts.NetworkErrorException
import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.hiddle.app.R
import com.hiddle.app.activity.HomeActivity
import com.hiddle.app.model.GeneralModel
import com.hiddle.app.retrofit.RetrofitRestClient
import com.hiddle.app.util.AppUtils
import com.hiddle.app.util.SharedPreferenceManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketTimeoutException
import java.util.*

public class ForegroundService : Service()/*, LocationListener */ {
    private val CHANNEL_ID = "ForegroundService Kotlin"
    var paramLongitude: String = ""
    var paramLatitude: String = ""

    private val mContext: Context? = null

    companion object {
        fun startService(context: Context, message: String) {
            val startIntent = Intent(context, ForegroundService::class.java)
            startIntent.putExtra("inputExtra", message)
            ContextCompat.startForegroundService(context, startIntent)
        }

        fun stopService(context: Context) {
            val stopIntent = Intent(context, ForegroundService::class.java)
            context.stopService(stopIntent)
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        //do heavy work on a background thread
        val input = intent?.getStringExtra("inputExtra")
        createNotificationChannel()


        var locationTrack: LocationTrack = LocationTrack(this)
        if (locationTrack.canGetLocation()) {

            if (locationTrack.longitude.toString() != "0.0") {
                paramLongitude = locationTrack.longitude.toString()
                paramLatitude = locationTrack.latitude.toString()

                if (SharedPreferenceManager.getMySharedPreferences()!!.isLogin()) {
                    callAPISendLocation(paramLatitude, paramLongitude)  //api for send location
                }
            }
        } else {
            // locationTrack.showSettingsAlert()
        }


        val timer = Timer()
        val hourlyTask = object : TimerTask() {
            override fun run() {

                if (SharedPreferenceManager.getMySharedPreferences()!!.isLogin()) {
                    if (locationTrack.canGetLocation()) {
                        if (locationTrack.getLongitude().toString() != "0.0") {
                            paramLongitude = locationTrack.getLongitude().toString()
                            paramLatitude = locationTrack.getLatitude().toString()

                            callAPISendLocation(paramLatitude, paramLongitude)  //api for send location
                        }
                    } else {
                        // locationTrack.showSettingsAlert()
                    }


                }


            }
        }
        // timer.schedule(hourlyTask, 0L, 30000) // 1000*10*60 every 10 minut}//30 second
        // timer.schedule(hourlyTask, 0L, 300000)//5 min every

        //timer.schedule(hourlyTask, 0L, 30000)//30 second every
        timer.schedule(hourlyTask, 0L, 50000)//50 second every
        // timer.schedule(hourlyTask, 0L, 600000)//10 min every
        /* timer.schedule(hourlyTask, 0L, 900000)//15 min every*/
        //timer.schedule(hourlyTask, 0L, 1800000)//30 min every


        val notificationIntent = Intent(this, HomeActivity::class.java)
        notificationIntent.putExtra("type", "home")
        val pendingIntent = PendingIntent.getActivity(
                this,
                0, notificationIntent, 0
        )
        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("Hiddle")
                .setContentText("Location service running")
                .setSmallIcon(R.drawable.ic_hiddle_text)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .build()
        startForeground(1, notification)

        stopSelf();
        return START_STICKY
    }

    private fun callAPISendLocation(paramLatitude: String, paramLongitude: String) {
        if (AppUtils.isConnectedToInternet(this)) {
            val params = HashMap<String, String?>()

            params["latitude"] = paramLatitude
            params["longitude"] = paramLongitude
            Log.e("param", params.toString())

            val call: Call<GeneralModel>
            call = RetrofitRestClient.instance!!.callNearMeAPI(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, params)
            call.enqueue(object : Callback<GeneralModel> {
                @SuppressLint("SetTextI18n", "ShowToast")
                override fun onResponse(call: Call<GeneralModel>?, response: Response<GeneralModel>?) {
                    val basicModel: GeneralModel
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {

                            Log.e("success", basicModel.message + "lat" + paramLatitude)
                        } else if (Objects.requireNonNull(basicModel).code == "-1") {
                        } else {
                            Log.e("error1", basicModel.message!!)
                        }
                    } else {
                        Log.e("error2", response.message())
                    }

                }

                override fun onFailure(call: Call<GeneralModel>?, t: Throwable?) {
                    if (t is SocketTimeoutException) {
                        Log.e("error3", "connection timeout")
                    } else if (t is NetworkErrorException) {
                        Log.e("error3", "network_error")
                    } else if (t is AuthenticatorException) {
                        Log.e("error3", "authentiation_error")
                    } else {
                        Log.e("error3", "something_went_wrong")
                    }
                }
            })
        } else {
            Log.e("error4", "No internet")
        }
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val serviceChannel = NotificationChannel(CHANNEL_ID, "Location service running",
                    NotificationManager.IMPORTANCE_DEFAULT)
            val manager = getSystemService(NotificationManager::class.java)
            manager!!.createNotificationChannel(serviceChannel)
        }
    }
/*
    override fun onLocationChanged(location: Location?) {
        paramLongitude = location!!.longitude.toString()
        paramLatitude = location.latitude.toString()
        Log.e("location_service_change", paramLatitude + "long" + paramLongitude)
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
    }

    override fun onProviderEnabled(provider: String?) {
    }

    override fun onProviderDisabled(provider: String?) {

    }*/

    override fun onTaskRemoved(rootIntent: Intent) {

        if (SharedPreferenceManager.getMySharedPreferences()!!.isLogin()) {
            SharedPreferenceManager.getMySharedPreferences()!!.setNotificationUser("")
            callAPIOnlineOfflineStatusChange("0")
        }
        Log.e("ClearFromRecentService", "Service Destroyed")
    }

    private fun callAPIOnlineOfflineStatusChange(isFrom: String) {
        if (SharedPreferenceManager.getMySharedPreferences()!!.getUserId() != "") {
            if (AppUtils.isConnectedToInternet(this)) {
                val params = HashMap<String, String?>()
                params["status"] = isFrom
                Log.e("online_foreground", params.toString())

                val call: Call<GeneralModel>
                call = RetrofitRestClient.instance!!.changeStatusAPI(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, params)

                call.enqueue(object : Callback<GeneralModel> {
                    @SuppressLint("SetTextI18n")
                    override fun onResponse(call: Call<GeneralModel>?, response: Response<GeneralModel>?) {

                        val basicModel: GeneralModel
                        if (response!!.isSuccessful) {
                            basicModel = response.body()!!
                            if (Objects.requireNonNull(basicModel).code == "1") {
                                Log.e("success_online", basicModel.toString() + "message" + basicModel.message)
                            } else if (Objects.requireNonNull(basicModel).code == "-1") {
                                // Log.e("message1",basicModel.message)
                            } else {
                                //Log.e("message2",basicModel.message)
                            }
                        } else {
                            //Log.e("message",response.message())

                        }
                    }

                    override fun onFailure(call: Call<GeneralModel>?, t: Throwable?) {
                        //if fail then again call
                        if (SharedPreferenceManager.getMySharedPreferences()!!.isLogin()) {
                            callAPIOnlineOfflineStatusChange(isFrom)
                        }
                        //Log.e("message","failure")
                    }
                })
            } else {
                //  Log.e("no_internet", "no_internet")
            }
        }

    }
}