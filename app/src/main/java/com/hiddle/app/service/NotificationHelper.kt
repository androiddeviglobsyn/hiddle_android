package com.hiddle.app.service

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.ContextWrapper
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import androidx.annotation.RequiresApi
import com.hiddle.app.R
import com.hiddle.app.util.SharedPreferenceManager

@Suppress("DEPRECATION")
class NotificationHelper(base: Context, sound: String) : ContextWrapper(base) {
    var manager: NotificationManager? = null
    var GROUP_KEY_WORK_EMAIL = "com.android.example.WORK_EMAIL"
    val manger: NotificationManager
        get() {
            if (manager == null)
                manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            return manager!!
        }
    /**
     * Get the small icon for this app
     *
     * @return The small icon resource id
     */
    private val smallIcon: Int
        get() {
            return R.drawable.ic_hiddle_text
        }
    private val largeIcon: Bitmap
        get() {
            return BitmapFactory.decodeResource(getResources(), R.mipmap.hiddle_launch_icon)
        }

    init {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            createChannels(sound)
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private fun createChannels(sound: String) {
        val notifyChannels: NotificationChannel
        if (SharedPreferenceManager.getMySharedPreferences()!!.getSound() == "1") {
            notifyChannels = NotificationChannel(CHANEL_ID, CHANEL_NAME, NotificationManager.IMPORTANCE_DEFAULT)

        } else {
            notifyChannels = NotificationChannel(CHANEL_ID, CHANEL_NAME, NotificationManager.IMPORTANCE_LOW)
            notifyChannels.setSound(null, null)
        }

        if (SharedPreferenceManager.getMySharedPreferences()!!.getVibrate() == "1") {
            notifyChannels.enableVibration(true)
        } else {
            notifyChannels.enableVibration(false)
        }

        notifyChannels.enableLights(false)
        notifyChannels.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        manger.createNotificationChannel(notifyChannels)
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    fun getNotification(title: String, content: String, contentIntent: PendingIntent,
                        soundUri: Uri, sound: String): Notification.Builder {
        return Notification.Builder(applicationContext, CHANEL_ID)
                .setContentText(content)
                /*.setContentTitle(title)*/
                .setAutoCancel(true)
                .setSound(soundUri)
                .setVibrate(longArrayOf(500, 500))
                .setSmallIcon(smallIcon)
                .setDefaults(Notification.DEFAULT_SOUND)
                /* .setLargeIcon(largeIcon)*/
                .setPriority(Notification.PRIORITY_MAX)
                .setContentIntent(contentIntent)

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    fun getNotificationNoSound(title: String, content: String, contentIntent: PendingIntent,
                               soundUri: Uri, sound: String): Notification.Builder {
        return Notification.Builder(applicationContext, CHANEL_ID)
                .setContentText(content)
               /* .setContentTitle(title)*/
                .setAutoCancel(true)
                .setDefaults(0)
                .setSound(null)
                //.setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE)
                .setSound(null, null)
                .setVibrate(longArrayOf(500, 500))
                .setSmallIcon(smallIcon)
                /*.setGroup(GROUP_KEY_WORK_EMAIL)*/

                /*.setLargeIcon(largeIcon)*/
                .setPriority(Notification.PRIORITY_MAX)
                .setContentIntent(contentIntent)

        /*.addAction(R.drawable.ic_contact_grey, "Play", contentIntent)
     .addAction(R.drawable.icon_file_doc, "Cancle", contentIntent);*/
    }

    companion object {
        val CHANEL_ID = "com.hiddle.app"
        private val CHANEL_NAME = "hiddle"
    }
}