package com.hiddle.app.service

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.hiddle.app.R
import com.hiddle.app.activity.HomeActivity
import com.hiddle.app.activity.SplashScreenActivity
import com.hiddle.app.util.SharedPreferenceManager
import java.util.*


@Suppress("DEPRECATION")
class MyFirebaseMessagingService : FirebaseMessagingService() {
    lateinit var intent: Intent
    var message: String = ""
    var sound: String = "Enabled"
    var name: String = ""
    var from_id: String = ""
    var click_action: String = "88"
    //var bundleNotificationId:Int = 100
    var GROUP_KEY_WORK_EMAIL = "com.android.example.WORK_EMAIL"
    private val NOTIFICATION_ID = 237
    private val value = 0
    /*var notificationId = 1*/
    val SUMMARY_ID = 0
    var inboxStyle = Notification.InboxStyle()
    var summaryNotificationBuilder: NotificationCompat.Builder? = null
    var bundleNotificationId = 100
    override fun onNewToken(firebaseId: String) {
        // String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        SharedPreferenceManager.getMySharedPreferences()!!.setFcmToken(firebaseId)
        Log.e("FirebaseId", "firebase id == " + firebaseId)
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Log. e("message_receive", "messge receive")
        //message

        val title = remoteMessage.notification.toString()

        val notificationId = Random().nextInt(60000)

        // val notificationId = (Date().time / 1000L % Int.MAX_VALUE).toInt()
        //val notificationId = 207/*(Date().time / 1000L % Int.MAX_VALUE).toInt()*/

        Log.e("number", notificationId.toString())
        message = remoteMessage.notification!!.title!!
        val dd = remoteMessage.notification!!.clickAction //this is open activity name

        val data = remoteMessage.notification
        Log.e("main payload: ", remoteMessage.data.toString())
        /*Log.e("main payload: ", remoteMessage.notification!!.clickAction!!)*/
        /*Log.e("mainNotification:", remoteMessage.toString() + title + "date" + data)*/

        var data1: Map<String, String> = remoteMessage.getData()
        click_action = data1["type"]!!

        if (data1.contains("type")) {
            intent = Intent(dd)
        } else {
            click_action = remoteMessage.notification!!.clickAction!!
            intent = Intent(applicationContext, SplashScreenActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        }

        if (SharedPreferenceManager.getMySharedPreferences()!!.isLogin()) {
            /* if (click_action == "4") {
                 var id = data1["from_id"]  //get notification data in killed and background
                 val intent = Intent(applicationContext, ChatActivity::class.java)
                 intent.putExtra("to_user_name", "")
                 intent.putExtra("to_user_id",id)
                 intent.putExtra("user_image","")
                 intent.putExtra("NotificationMessage",click_action)
                 intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
             } else {*/
            Log.e("click_action", click_action)
            from_id = data1["from_id"]!!
            intent = Intent(applicationContext, HomeActivity::class.java) // in both case foreground and background
            /* intent.putExtra("NotificationMessage",click_action)*/
            intent.putExtra("type", click_action)
            intent.putExtra("from_id", from_id)
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        } else {
            intent = Intent(applicationContext, SplashScreenActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        }


        if (from_id != SharedPreferenceManager.getMySharedPreferences()!!.getNotificationUser()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                showArrivedNotificationAPI26(message, notificationId, intent, click_action)
            else
                showArrivedNotification(message, notificationId, intent)

        } else {

        }

    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private fun showArrivedNotificationAPI26(body: String, notificationId: Int, intent: Intent, clickAction: String) {

        var builder: Notification.Builder? = null
        try {
            val contentIntent = PendingIntent.getActivity(applicationContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
            val defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            val notificationHelper = NotificationHelper(baseContext, sound)

            if (SharedPreferenceManager.getMySharedPreferences()!!.getSound() == "1") {
                builder = notificationHelper.getNotification(getString(R.string.app_name), body, contentIntent, defaultSound, sound)
            } else {
                builder = notificationHelper.getNotificationNoSound(getString(R.string.app_name), body, contentIntent, defaultSound, sound)
            }
            builder.setWhen(System.currentTimeMillis())
            builder.setSmallIcon(R.drawable.ic_hiddle_text)
            /* builder.setContentTitle(getString(R.string.app_name))*/
            builder.setContentText(body)
            builder.setGroup(GROUP_KEY_WORK_EMAIL)
            /* if (SharedPreferenceManager.getMySharedPreferences()!!.getSound() == "1") {
                 builder.setDefaults(Notification.DEFAULT_LIGHTS or Notification.DEFAULT_SOUND)
                 builder.setSound(defaultSound)
             } else {
                 builder.setDefaults(0)
                 builder.setSound(null)
                 builder.setSound(null, null)
             }*/
            if (SharedPreferenceManager.getMySharedPreferences()!!.getVibrate() == "1") {
                builder.setVibrate(longArrayOf(500, 500))
            }
            builder.setAutoCancel(true)
            builder.setPriority(Notification.PRIORITY_MAX)
            builder.setContentIntent(contentIntent)


            val summaryNotification = NotificationCompat.Builder(applicationContext, NotificationHelper.CHANEL_ID)
                    //set content text to support devices running API level < 24
                    .setContentText(body)
                    .setSmallIcon(R.drawable.ic_hiddle_text)
                    //build summary info into InboxStyle template
                    .setStyle(NotificationCompat.InboxStyle()
                            .setSummaryText(""))
                    .setContentIntent(contentIntent)
                    //specify which group this notification belongs to
                    .setGroup(GROUP_KEY_WORK_EMAIL)
                    //set this notification as the summary for the group
                    .setGroupSummary(true)
                    .setAutoCancel(true)
                    .build()


            notificationHelper.manager!!.notify(notificationId, builder.build())

            notificationHelper.manager!!.notify(SUMMARY_ID, summaryNotification)


        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun showArrivedNotification(body: String, notificationId: Int, intent: Intent) {
        try {
            val defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            val contentIntent = PendingIntent.getActivity(baseContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
            val builder = NotificationCompat.Builder(getBaseContext())
            builder.setAutoCancel(true)
            // builder.setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND)
            if (SharedPreferenceManager.getMySharedPreferences()!!.getSound() == "1") {
                builder.setDefaults(Notification.DEFAULT_LIGHTS or Notification.DEFAULT_SOUND)
                builder.setSound(defaultSound)
            } else {
                builder.setDefaults(0)
                builder.setSound(null)
            }
            if (SharedPreferenceManager.getMySharedPreferences()!!.getVibrate() == "1") {
                builder.setVibrate(longArrayOf(500, 500))
            }
            builder.setWhen(System.currentTimeMillis())
            builder.setSmallIcon(R.drawable.ic_hiddle_text)
            /*builder.setContentTitle(getString(R.string.app_name))*/
            builder.setContentText(body)
            builder.setPriority(Notification.PRIORITY_MAX)
            builder.setWhen(0)
            builder.setGroup(GROUP_KEY_WORK_EMAIL)
            builder.setContentIntent(contentIntent)
            builder.build()

            val manager = baseContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            //manager.cancelAll();

            val summaryNotification = NotificationCompat.Builder(applicationContext, NotificationHelper.CHANEL_ID)
                    //set content text to support devices running API level < 24
                    .setContentText(body)
                    .setSmallIcon(R.drawable.ic_hiddle_text)
                    //build summary info into InboxStyle template
                    .setStyle(NotificationCompat.InboxStyle()
                            .setSummaryText(""))
                    .setContentIntent(contentIntent)
                    //specify which group this notification belongs to
                    .setGroup(GROUP_KEY_WORK_EMAIL)
                    .setAutoCancel(true)
                    //set this notification as the summary for the group
                    .setGroupSummary(true)
                    .build()






            if (manager != null) {
                manager.notify(notificationId, builder.build())
                manager.notify(SUMMARY_ID, summaryNotification)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    companion object {
        private val TAG = "MyFirebaseMessagingService"
    }
}

