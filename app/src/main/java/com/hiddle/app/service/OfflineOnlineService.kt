package com.hiddle.app.service

import android.annotation.SuppressLint
import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log

import com.hiddle.app.model.GeneralModel
import com.hiddle.app.retrofit.RetrofitRestClient
import com.hiddle.app.util.AppUtils
import com.hiddle.app.util.SharedPreferenceManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception
import java.util.*

class OfflineOnlineService : Service() {
    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        /* Log.e("Online", "Online")
         Log.e("ClearFromRecentService", "Service Started")*/

        try {
            //API for offline and online status
            if (SharedPreferenceManager.getMySharedPreferences()!!.isLogin()) {
                callAPIOnlineOfflineStatusChange("1")
            }
        } catch (e: Exception) {

        }

        return START_STICKY
    }


    override fun onDestroy() {
        super.onDestroy()
        if (SharedPreferenceManager.getMySharedPreferences()!!.isLogin()) {
            SharedPreferenceManager.getMySharedPreferences()!!.setNotificationUser("")
            callAPIOnlineOfflineStatusChange("0")
            callAPIOnlineOfflineStatusChange("0")
        }
        Log.e("ClearFromRecentService", "Service Destroyed")
    }

    override fun onTaskRemoved(rootIntent: Intent) {
        Log.e("ClearFromRecentService", "END")
        Log.e("Offline", "Offline")
        if (SharedPreferenceManager.getMySharedPreferences()!!.isLogin()) {
            SharedPreferenceManager.getMySharedPreferences()!!.setNotificationUser("")
            callAPIOnlineOfflineStatusChange("0")
            callAPIOnlineOfflineStatusChange("0")
        }

        //Code here
        stopSelf()
    }

    private fun callAPIOnlineOfflineStatusChange(isFrom: String) {
        if(SharedPreferenceManager.getMySharedPreferences()!!.getUserId()!="")
        {
            if (AppUtils.isConnectedToInternet(this)) {
                val params = HashMap<String, String?>()
                params["status"] = isFrom
                Log.e("param_online", params.toString())
                val call: Call<GeneralModel>
                call = RetrofitRestClient.instance!!.changeStatusAPI(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, params)
                //Log.e("token", SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
                call.enqueue(object : Callback<GeneralModel> {
                    @SuppressLint("SetTextI18n")
                    override fun onResponse(call: Call<GeneralModel>?, response: Response<GeneralModel>?) {
                        val basicModel: GeneralModel
                        if (response!!.isSuccessful) {
                            basicModel = response.body()!!
                            if (Objects.requireNonNull(basicModel).code == "1") {
                                Log.e("success_online", basicModel.toString() + "message" + basicModel.message)
                            } else if (Objects.requireNonNull(basicModel).code == "-1") {
                                // Log.e("message1",basicModel.message)
                            } else {
                                //Log.e("message2",basicModel.message)
                            }
                        } else {
                            //Log.e("message",response.message())

                        }
                    }

                    override fun onFailure(call: Call<GeneralModel>?, t: Throwable?) {
                        //if fail then again call
                        if (SharedPreferenceManager.getMySharedPreferences()!!.isLogin()) {
                            callAPIOnlineOfflineStatusChange(isFrom)
                        }
                        //Log.e("message","failure")
                    }
                })
            } else {
                //  Log.e("no_internet", "no_internet")
            }
        }

    }
}