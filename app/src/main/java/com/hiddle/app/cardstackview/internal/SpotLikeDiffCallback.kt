package com.hiddle.app.cardstackview.internal

import androidx.recyclerview.widget.DiffUtil
import com.hiddle.app.model.FavoriteModel.FavoriteResponseData

class SpotLikeDiffCallback(
        private val old: List<FavoriteResponseData>,
        private val new: List<FavoriteResponseData>
) : DiffUtil.Callback() {

    override fun getOldListSize(): Int {
        return old.size
    }

    override fun getNewListSize(): Int {
        return new.size
    }

    override fun areItemsTheSame(oldPosition: Int, newPosition: Int): Boolean {
        return old[oldPosition].userId == new[newPosition].userId
    }

    override fun areContentsTheSame(oldPosition: Int, newPosition: Int): Boolean {
        return old[oldPosition] == new[newPosition]
    }

}
