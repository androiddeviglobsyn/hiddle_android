package com.hiddle.app.cardstackview;

public enum StackFrom {
    None,
    Top,
    TopAndLeft,
    TopAndRight,
    Bottom,
    BottomAndLeft,
    BottomAndRight,
    Left,
    Right,
}
