package com.hiddle.app.cardstackview.internal

import androidx.recyclerview.widget.DiffUtil
import com.hiddle.app.model.UserModel.UserDataResponse

class SpotDiffCallback(
        private val old: List<UserDataResponse>,
        private val new: List<UserDataResponse>
) : DiffUtil.Callback() {

    override fun getOldListSize(): Int {
        return old.size
    }

    override fun getNewListSize(): Int {
        return new.size
    }

    override fun areItemsTheSame(oldPosition: Int, newPosition: Int): Boolean {
        return old[oldPosition].id == new[newPosition].id
    }

    override fun areContentsTheSame(oldPosition: Int, newPosition: Int): Boolean {
        return old[oldPosition] == new[newPosition]
    }

}
