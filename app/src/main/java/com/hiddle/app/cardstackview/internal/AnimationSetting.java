package com.hiddle.app.cardstackview.internal;

import android.view.animation.Interpolator;

import com.hiddle.app.cardstackview.Direction;


public interface AnimationSetting {
    Direction getDirection();
    int getDuration();
    Interpolator getInterpolator();
}
