package com.hiddle.app;


import android.content.Context;
import android.content.res.Configuration;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.hiddle.app.activity.BaseMainActivity;
import com.hiddle.app.language.LocalizationApplicationDelegate;
import com.hiddle.app.util.SharedPreferenceManager;


public class Hiddleapp extends android.app.Application {
    LocalizationApplicationDelegate localizationDelegate = new LocalizationApplicationDelegate(this);

    private static Hiddleapp application;


    public static Hiddleapp getHiddleApplication() {

        return application;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseApp.initializeApp(this);
        application = this;

    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(localizationDelegate.attachBaseContext(base));
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        localizationDelegate.onConfigurationChanged(this);
    }

    @Override
    public Context getApplicationContext() {
        return localizationDelegate.getApplicationContext(super.getApplicationContext());
    }
}










