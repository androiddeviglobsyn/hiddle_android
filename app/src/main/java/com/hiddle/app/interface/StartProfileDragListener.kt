package com.hiddle.app.`interface`

import androidx.recyclerview.widget.RecyclerView

interface StartProfileDragListener {
    fun requestProfileDrag(viewHolder: RecyclerView.ViewHolder?)
}