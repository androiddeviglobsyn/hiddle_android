package com.hiddle.app.`interface`

import androidx.appcompat.widget.AppCompatImageView

interface FilterButtonClick {
    fun filterClick(ivBlur: AppCompatImageView?)
}