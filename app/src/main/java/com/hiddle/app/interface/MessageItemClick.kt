package com.hiddle.app.`interface`

interface MessageItemClick {
    fun itemClick(position: Int,id:String)

    fun itemChatClick(position: Int)

    fun itemUserProfileClick(position: Int)
}