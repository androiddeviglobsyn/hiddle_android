package com.hiddle.app.`interface`

interface ClickForDeleteItem {
    fun deleteItem(position: Int)

    fun redirectUserProfile(position: Int)

    fun redirectToChatScreen(position: Int)
}