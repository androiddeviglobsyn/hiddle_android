package com.hiddle.app.sqllitedatabase

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log

//creating the database logic, extending the SQLiteOpenHelper base class  

class DatabaseChatHandler(context: Context?) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    companion object {
        private val DATABASE_VERSION = 1
        private val DATABASE_NAME = "EmployeeDatabase"
        public val TABLE_SOCKET_CHAT = "ChatTable"

        //for message table
        private val KEY_CHAT_OWN_ID = "own_id"
        private val KEY_CHAT_TO_USERID = "to_user_id"
        private val KEY_CHAT_FROM_ID = "from_id"
        private val KEY_CHAT_SENT_BY = "sent_by"
        private val KEY_CHAT_TO_ID = "to_id"
        private val KEY_CHAT_CREATED_AT = "createdAt"
        private val KEY_CHAT_ID = "id"
        private val KEY_CHAT_MESSAGE = "message"


        //for  message
        private val TABLE_MESSAGE = "MessageTable"
        //for message table
        private val KEY_ID = "id"
        private val KEY_USERID = "user_id"
        private val KEY_NAME = "name"
        private val KEY_PROFILE = "profile"
        private val KEY_TIME = "time"
        private val BASE_URL = "base_url"
        private val KEY_MSG = "msg"
        private val KEY_STATUS = "status"
        private val KEY_OWN_USERID = "own_userid"


    }

    override fun onCreate(db: SQLiteDatabase?) {
        // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.  
        //creating table chat with fields
        val CREATE_CHAT_TABLE = ("CREATE TABLE " + TABLE_SOCKET_CHAT + "("
                + KEY_CHAT_OWN_ID + " TEXT," + KEY_CHAT_TO_USERID + " TEXT," + KEY_CHAT_FROM_ID + " TEXT,"
                + KEY_CHAT_SENT_BY + " TEXT," + KEY_CHAT_TO_ID + " TEXT," + KEY_CHAT_CREATED_AT + " TEXT," + KEY_CHAT_ID + " TEXT," + KEY_CHAT_MESSAGE + " TEXT" + ")")
        db?.execSQL(CREATE_CHAT_TABLE)


        //Create table Message
        val CREATE_CONTACTS_TABLE = ("CREATE TABLE " + TABLE_MESSAGE + "("
                + KEY_ID + " TEXT," + KEY_USERID + " TEXT," + KEY_NAME + " TEXT,"
                + KEY_PROFILE + " TEXT," + KEY_TIME + " TEXT," + BASE_URL + " TEXT," + KEY_MSG + " TEXT," + KEY_STATUS + " TEXT," + KEY_OWN_USERID + " TEXT" + ")")
        db?.execSQL(CREATE_CONTACTS_TABLE)

    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        //  TODO("not implemented") //To change body of created functions use File | Settings | File Templates.  
        db!!.execSQL("DROP TABLE IF EXISTS " + TABLE_SOCKET_CHAT)
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MESSAGE)

        onCreate(db)
    }

    //query for chat
    fun deleteAllReecordChat() {
        val db = this.writableDatabase
        db.execSQL("DELETE FROM $TABLE_SOCKET_CHAT") //delete all rows in a table
        db.close()
    }
    fun deleteAllReecordChat(toUserId: String, ownId: String) {
        val db = this.writableDatabase
        db.execSQL("DELETE FROM $TABLE_SOCKET_CHAT WHERE to_user_id = '$toUserId' AND own_id = '$ownId'") //delete all rows in a table
        db.close()
        Log.e("success_delete", "suceess")
    }

    fun checkRecordExitChat(to_user_id: String, ownId: String): Boolean {

        var success = false
        val db = this.readableDatabase

        val mCursor: Cursor = db.rawQuery("SELECT * FROM $TABLE_SOCKET_CHAT WHERE  to_user_id = '$to_user_id' AND own_id = '$ownId'", null);

        if (mCursor.moveToFirst()) {
            success = true
            db.close()
            Log.e("record_exits", "record exist")

        } else {
            Log.e("record_not_exits", "record_not exist")
        }
        db.close();
        return success
    }

    fun checkRecordExitCreatedDateChat(to_user_id: String, createdDate: String, ownId: String): Boolean {

        var success = false
        val db = this.readableDatabase

        val mCursor: Cursor = db.rawQuery("SELECT * FROM $TABLE_SOCKET_CHAT WHERE  to_user_id = '$to_user_id' AND createdAt = '$createdDate' AND own_id = '$ownId'", null);

        if (mCursor.moveToFirst()) {
            success = true
            db.close()
           // Log.e("record_exits", "record exist")

        } else {
           // Log.e("record_not_exits", "record_not exist")
        }
        db.close();

        return success
    }

    fun deleteAllReecordCreatedDateChat(toUserId: String, createdDate: String, ownId: String) {
        val db = this.writableDatabase
        db.execSQL("DELETE FROM $TABLE_SOCKET_CHAT WHERE to_user_id = '$toUserId' AND createdAt = '$createdDate' AND own_id = '$ownId'") //delete all rows in a table
        db.close()
        Log.e("success_delete", "suceess")
    }

    //method to insert data
    fun addChat(emp: ChatClass): Long {
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(KEY_CHAT_OWN_ID, emp.own_id)
        contentValues.put(KEY_CHAT_TO_USERID, emp.to_user_id) // EmpModelClass Name
        contentValues.put(KEY_CHAT_FROM_ID, emp.from_id) // EmpModelClass Phone
        contentValues.put(KEY_CHAT_SENT_BY, emp.sent_by)
        contentValues.put(KEY_CHAT_TO_ID, emp.to_id) // EmpModelClass Name
        contentValues.put(KEY_CHAT_CREATED_AT, emp.createdAt) // EmpModelClass Phone
        contentValues.put(KEY_CHAT_ID, emp.id)
        contentValues.put(KEY_CHAT_MESSAGE, emp.message)

        // Inserting Row  
        val success = db.insert(TABLE_SOCKET_CHAT, null, contentValues)
        //2nd argument is String containing nullColumnHack  
        db.close() // Closing database connection  
        return success
    }

    //method to read data
    @SuppressLint("Recycle")
    fun viewChatMessageId(to_user_id: String, ownId: String): List<ChatClass> {
        val empList: ArrayList<ChatClass> = ArrayList<ChatClass>()
        val selectQuery = "SELECT  * FROM $TABLE_SOCKET_CHAT WHERE to_user_id = '$to_user_id' AND own_id = '$ownId'"

        val db = this.readableDatabase
        var cursor: Cursor? = null
        try {
            cursor = db.rawQuery(selectQuery, null)
        } catch (e: SQLiteException) {
            db.execSQL(selectQuery)
            return ArrayList()
        }
        var ownId: String = "own_id"
        var touserId = "to_user_id"
        var fromId = "from_id"
        var sentBy = "sent_by"
        var toId = "to_id"
        var createdAT = "createdAt"
        var id = "id"
        var message = "message"


        if (cursor.moveToFirst()) {
            do {
                ownId = cursor.getInt(cursor.getColumnIndex("own_id")).toString()
                touserId = cursor.getString(cursor.getColumnIndex("to_user_id"))
                fromId = cursor.getString(cursor.getColumnIndex("from_id"))
                sentBy = cursor.getString(cursor.getColumnIndex("sent_by"))
                toId = cursor.getString(cursor.getColumnIndex("to_id"))
                createdAT = cursor.getString(cursor.getColumnIndex("createdAt"))
                id = cursor.getString(cursor.getColumnIndex("id"))
                message = cursor.getString(cursor.getColumnIndex("message"))


                val emp = ChatClass(own_id = ownId, to_user_id = touserId, from_id = fromId,
                        sent_by = sentBy, to_id = toId,
                        createdAt = createdAT, id = id, message = message)
                empList.add(emp)
            } while (cursor.moveToNext())
        } else {
            Log.e("no_data", "no_data+found")
        }
        return empList
    }

    //method to read data
    fun viewChatEmployee(): List<ChatClass> {
        val empList: ArrayList<ChatClass> = ArrayList<ChatClass>()
        val selectQuery = "SELECT  * FROM $TABLE_SOCKET_CHAT"
        val db = this.readableDatabase
        var cursor: Cursor? = null
        try {
            cursor = db.rawQuery(selectQuery, null)
        } catch (e: SQLiteException) {
            db.execSQL(selectQuery)
            return ArrayList()
        }
        var ownId: String = "own_id"
        var touserId = "to_user_id"
        var fromId = "from_id"
        var sentBy = "sent_by"
        var toId = "to_id"
        var createdAT = "createdAt"
        var id = "id"
        var message = "message"


        if (cursor.moveToFirst()) {
            do {
                ownId = cursor.getInt(cursor.getColumnIndex("own_id")).toString()
                touserId = cursor.getString(cursor.getColumnIndex("to_user_id"))
                fromId = cursor.getString(cursor.getColumnIndex("from_id"))
                sentBy = cursor.getString(cursor.getColumnIndex("sent_by"))
                toId = cursor.getString(cursor.getColumnIndex("to_id"))
                createdAT = cursor.getString(cursor.getColumnIndex("createdAt"))
                id = cursor.getString(cursor.getColumnIndex("id"))
                message = cursor.getString(cursor.getColumnIndex("message"))


                val emp = ChatClass(own_id = ownId, to_user_id = touserId, from_id = fromId,
                        sent_by = sentBy, to_id = toId,
                        createdAt = createdAT, id = id, message = message)
                empList.add(emp)
            } while (cursor.moveToNext())
        }
        return empList
    }



    // all query for message table and message fragmnent
    //message


    fun deleteAllReecordMessage() {
        val db = this.writableDatabase
        db.execSQL("DELETE FROM $TABLE_MESSAGE") //delete all rows in a table
        db.close()
    }


    fun checkRecordExitMessage(ownId: String): Boolean {
        var success = false
        val db = this.readableDatabase
        val mCursor: Cursor = db.rawQuery("SELECT * FROM $TABLE_MESSAGE WHERE  id = '$ownId'", null);
        if (mCursor.moveToFirst()) {
            success = true
            db.close()
            Log.e("record_exits", "record exist")

        } else {
            Log.e("record_not_exits", "record_not exist")
        }
        db.close()
        return success
    }

    //method to insert data
    fun addEmployee(emp: MessageClass): Long {
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(KEY_ID, emp.id)
        contentValues.put(KEY_USERID, emp.user_id) // EmpModelClass Name
        contentValues.put(KEY_NAME, emp.name) // EmpModelClass Phone
        contentValues.put(KEY_PROFILE, emp.profile)
        contentValues.put(KEY_TIME, emp.time) // EmpModelClass Name
        contentValues.put(BASE_URL, emp.base_url) // EmpModelClass Phone
        contentValues.put(KEY_MSG, emp.msg)
        contentValues.put(KEY_STATUS, emp.status) // EmpModelClass Name
        contentValues.put(KEY_OWN_USERID, emp.own_userid) // EmpModelClass Phone
        // Inserting Row
        val success = db.insert(TABLE_MESSAGE, null, contentValues)
        //2nd argument is String containing nullColumnHack
        db.close() // Closing database connection
        return success
    }

    //method to read data
    @SuppressLint("Recycle")
    fun viewMessageId(ownId: String): List<MessageClass> {
        val empList: ArrayList<MessageClass> = ArrayList<MessageClass>()
        val selectQuery = "SELECT  * FROM $TABLE_MESSAGE WHERE id = '$ownId'"

        val db = this.readableDatabase
        var cursor: Cursor? = null
        try {
            cursor = db.rawQuery(selectQuery, null)
        } catch (e: SQLiteException) {
            db.execSQL(selectQuery)
            return ArrayList()
        }
        var Id: String = "id"
        var userId = "user_id"
        var userName = "name"
        var userProfile = "profile"
        var userTime = "time"
        var userBaseUrl = "base_url"
        var userMsg = "msg"
        var userStatus = "status"
        var userOwnUserId = "own_userid"

        if (cursor.moveToFirst()) {
            do {
                Id = cursor.getInt(cursor.getColumnIndex("id")).toString()
                userId = cursor.getString(cursor.getColumnIndex("user_id"))
                userName = cursor.getString(cursor.getColumnIndex("name"))
                userProfile = cursor.getString(cursor.getColumnIndex("profile"))
                userTime = cursor.getString(cursor.getColumnIndex("time"))
                userBaseUrl = cursor.getString(cursor.getColumnIndex("base_url"))
                userMsg = cursor.getString(cursor.getColumnIndex("msg"))
                userStatus = cursor.getString(cursor.getColumnIndex("status"))
                userOwnUserId = cursor.getString(cursor.getColumnIndex("own_userid"))

                val emp = MessageClass(id = Id, user_id = userId, name = userName,
                        profile = userProfile, time = userTime,
                        base_url = userBaseUrl, msg = userMsg, status = userStatus, own_userid = userOwnUserId)
                empList.add(emp)
            } while (cursor.moveToNext())
        }
        return empList
    }


    //method to read data
    fun viewEmployee(): List<MessageClass> {
        val empList: ArrayList<MessageClass> = ArrayList<MessageClass>()
        val selectQuery = "SELECT  * FROM $TABLE_MESSAGE"
        val db = this.readableDatabase
        var cursor: Cursor? = null
        try {
            cursor = db.rawQuery(selectQuery, null)
        } catch (e: SQLiteException) {
            db.execSQL(selectQuery)
            return ArrayList()
        }
        var Id: String = "id"
        var userId = "user_id"
        var userName = "name"
        var userProfile = "profile"
        var userTime = "time"
        var userBaseUrl = "base_url"
        var userMsg = "msg"
        var userStatus = "status"
        var userOwnUserId = "own_userid"

        if (cursor.moveToFirst()) {
            do {
                Id = cursor.getInt(cursor.getColumnIndex("id")).toString()
                userId = cursor.getString(cursor.getColumnIndex("user_id"))
                userName = cursor.getString(cursor.getColumnIndex("name"))
                userProfile = cursor.getString(cursor.getColumnIndex("profile"))
                userTime = cursor.getString(cursor.getColumnIndex("time"))
                userBaseUrl = cursor.getString(cursor.getColumnIndex("base_url"))
                userMsg = cursor.getString(cursor.getColumnIndex("msg"))
                userStatus = cursor.getString(cursor.getColumnIndex("status"))
                userOwnUserId = cursor.getString(cursor.getColumnIndex("own_userid"))

                val emp = MessageClass(id = Id, user_id = userId, name = userName,
                        profile = userProfile, time = userTime,
                        base_url = userBaseUrl, msg = userMsg, status = userStatus, own_userid = userOwnUserId)
                empList.add(emp)
            } while (cursor.moveToNext())
        }
        return empList
    }


    //method to delete data
    fun deleteEmployee(emp: MessageClass): Int {
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(KEY_ID, emp.id) // EmpModelClass UserId
        // Deleting Row
        val success = db.delete(TABLE_MESSAGE, "id=" + emp.id, null)
        //2nd argument is String containing nullColumnHack
        db.close() // Closing database connection
        return success
    }


}

