package com.hiddle.app.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.Window
import com.hiddle.app.R

class NameDialog (context: Context?) : Dialog(context!!) {

    init {
        setCancelable(false)
        setCanceledOnTouchOutside(false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.custom_dialog_name)


    }
}