package com.hiddle.app.fragment

import android.Manifest
import android.accounts.AuthenticatorException
import android.accounts.NetworkErrorException
import android.annotation.SuppressLint
import android.app.ActivityOptions
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.renderscript.Allocation
import android.renderscript.Element
import android.renderscript.RenderScript
import android.renderscript.ScriptIntrinsicBlur
import android.util.Log
import android.view.ContextThemeWrapper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.*
import android.widget.*
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.hiddle.app.R
import com.hiddle.app.`interface`.FragmentClick
import com.hiddle.app.activity.HomeActivity
import com.hiddle.app.activity.MyOriginActivity
import com.hiddle.app.activity.StrikeActivity
import com.hiddle.app.activity.UserProfileActivity
import com.hiddle.app.adapter.ImageAdapter
import com.hiddle.app.adapter.SwipeDeckAdapter
import com.hiddle.app.cardstackview.*
import com.hiddle.app.adapter.CardStackAdapter
import com.hiddle.app.cardstackview.internal.SpotDiffCallback
import com.hiddle.app.common.SwipeDeck
import com.hiddle.app.model.GeneralModel
import com.hiddle.app.model.LikeModel.LikeResponse
import com.hiddle.app.model.UserModel.UserDataResponse
import com.hiddle.app.model.UserModel.UserImageModel
import com.hiddle.app.model.UserModel.UserResponse
import com.hiddle.app.retrofit.RetrofitRestClient
import com.hiddle.app.service.LocationTrack
import com.hiddle.app.util.AppUtilMethod
import com.hiddle.app.util.AppUtilMethod.getDifferenecOfTwoDate
import com.hiddle.app.util.AppUtils
import com.hiddle.app.util.Constants
import com.hiddle.app.util.SharedPreferenceManager
import com.skyfishjy.library.RippleBackground
import de.hdodenhof.circleimageview.CircleImageView
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions
import pub.devrel.easypermissions.PermissionRequest
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketTimeoutException
import java.util.*
import kotlin.collections.ArrayList

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class HomeFragment : BaseFragment(), View.OnClickListener, CardStackListener {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var rvImage: RecyclerView? = null
    var imagelist: ArrayList<UserDataResponse> = ArrayList()
    var imageAdapter: ImageAdapter? = null
    private var mContext: Context? = null
    var p = 0

    //button


    private var adapter: SwipeDeckAdapter? = null
    private var userList: ArrayList<UserDataResponse> = ArrayList()
    private var tvNoCard: AppCompatTextView? = null
    private var ivFavorite: RelativeLayout? = null
    private var ivRight: AppCompatImageView? = null
    private var ivCross: AppCompatImageView? = null
    private var rlAllFavorite: RelativeLayout? = null
    private var rlSwipeImageView: RelativeLayout? = null
    private var ivSelectedImage: ImageView? = null
    private var ivVerify: AppCompatImageView? = null
    private var ivHeartAnim: AppCompatImageView? = null
    private var llNodata: LinearLayout? = null
    private var llrvView: LinearLayout? = null
    private var rbRipple: RippleBackground? = null
    private var ivProfileView: CircleImageView? = null
    private var pbHome: ProgressBar? = null


    var view1: View? = null

    /*var progressDialog: CustomProgressMainDialog? = null*/
    var locationTrack: LocationTrack? = null
    var paramLongitude: String = ""
    var paramLatitude: String = ""
    var isClickProfile: Boolean = false

    var manager: CardStackLayoutManager? = null
    private var cardStackAdapter: CardStackAdapter? = null
    var cardStackView: CardStackView? = null

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_home_layout, container, false)
        rvImage = view.findViewById(R.id.rvImage) as RecyclerView

        ivRight = view.findViewById(R.id.ivRight) as AppCompatImageView
        ivCross = view.findViewById(R.id.ivCross) as AppCompatImageView
        ivFavorite = view.findViewById(R.id.ivFavorite) as RelativeLayout
        tvNoCard = view.findViewById(R.id.tvNoCard) as AppCompatTextView
        rlAllFavorite = view.findViewById(R.id.rlAllFavorite) as RelativeLayout
        rlSwipeImageView = view.findViewById(R.id.rlSwipeImageView) as RelativeLayout
        ivSelectedImage = view.findViewById(R.id.ivSelectedImage) as ImageView
        ivVerify = view.findViewById(R.id.ivVerify) as AppCompatImageView
        ivHeartAnim = view.findViewById(R.id.ivHeartAnim) as AppCompatImageView
        llNodata = view.findViewById(R.id.llNodata) as LinearLayout
        llrvView = view.findViewById(R.id.llrvView) as LinearLayout
        rbRipple = view.findViewById(R.id.rbRipple) as RippleBackground
        ivProfileView = view.findViewById(R.id.ivProfileView) as CircleImageView
        pbHome = view.findViewById(R.id.pbHome) as ProgressBar
        view1 = view.findViewById(R.id.rlHome)
        cardStackView = view.findViewById(R.id.card_stack_view);
        mContext = activity



        allClicklistener()
        setRotationOfFavorite()
        setPulseAnimation()
        permissionForlocation()
        (activity as HomeActivity).updateApi(object : FragmentClick {  //for apply filter from activity click
            override fun clickonFragment(isFrom: String) {
                if (isFrom == "HomeFilter") {
                    callAPIAllUserList()
                }
            }
        })
        return view
    }

    private fun permissionForlocation() {
        callFineLocation()
    }

    private fun setPulseAnimation() {
        ivProfileView?.let { Glide.with(context!!).load(SharedPreferenceManager.getMySharedPreferences()!!.getProfileImage()).placeholder(R.drawable.ic_placeholder_icn).into(it) }
        rbRipple!!.startRippleAnimation()
    }


    private fun setRotationOfFavorite() {
        val aniRotate: Animation = AnimationUtils.loadAnimation(activity!!.applicationContext, R.anim.rotate_backward)
        aniRotate.repeatCount = Animation.INFINITE
        aniRotate.fillAfter = true;
        ivFavorite!!.startAnimation(aniRotate)

    }

    private fun allClicklistener() {
        ivCross!!.setOnClickListener(this)
        ivRight!!.setOnClickListener(this)
        ivFavorite!!.setOnClickListener(this)
        ivSelectedImage!!.setOnClickListener(this)

    }


    private fun callAPIAllUserList() {
        if (AppUtils.isConnectedToInternet(activity)) {

            pbHome!!.visibility = View.VISIBLE
            // AppUtils.showProgressDialogTouchRemove(activity)

            val params = HashMap<String, String?>()
            params["latitude"] = paramLatitude
            params["longitude"] = paramLongitude
            if (paramLatitude != "") {
                var paramAddress = AppUtilMethod.getAddress(paramLatitude.toDouble(), paramLongitude.toDouble(), mContext)
                params["address"] = paramAddress
            }


            Log.e("paramSkip", params.toString())

            val call: Call<UserResponse>
            call = RetrofitRestClient.instance!!.callALLUserAPI(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, params)
            //Log.e("token", SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
            call.enqueue(object : Callback<UserResponse> {
                @SuppressLint("SetTextI18n")
                override fun onResponse(call: Call<UserResponse>?, response: Response<UserResponse>?) {
                    pbHome!!.visibility = View.GONE
                    AppUtils.hideProgressDialog()
                    val basicModel: UserResponse
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {
                            Log.e("response_home", response.body().toString())

                            selectedPosition = 0
                            rlSwipeImageView!!.visibility = View.GONE
                            userList.clear()

                            userList = (basicModel.data as ArrayList<UserDataResponse>? )!!

                            if (userList.size > 0) {
                                rlAllFavorite!!.visibility = View.VISIBLE  //for make bottom layout visible
                            }

                            setCardSwipeView()
                            //setAdapterForImageList(0)
                            setAdapterForImageList(1)
                            makeVisibleItem()


                        } else if (Objects.requireNonNull(basicModel).code == "-1") {
                            callLogoutFunction()
                        } else {
                            showSnackBar(activity, basicModel.message)
                            makeVisibleItem()
                        }
                    } else {
                        showSnackBar(activity, response.message())
                        makeVisibleItem()
                    }
                }

                override fun onFailure(call: Call<UserResponse>?, t: Throwable?) {
                    pbHome!!.visibility = View.GONE
                    makeVisibleItem()
                    Log.e("error_message", t!!.message!! + "12"+t.localizedMessage)
                    // AppUtils.hideProgressDialog()
                    try {
                        if (t is SocketTimeoutException) {
                            AppUtils.hideProgressDialog()
                            showSnackBar(activity, getString(R.string.connection_timeout))
                        } else if (t is NetworkErrorException) {
                            showSnackBar(activity, getString(R.string.network_error))
                        } else if (t is AuthenticatorException) {
                            showSnackBar(activity, getString(R.string.authentiation_error))
                        } else {
                            showSnackBar(activity, getString(R.string.something_went_wrong))
                        }
                    } catch (e: Exception) {
                    }
                }
            })
        } else {
            makeVisibleItem()
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }
    }


    private fun setCardSwipeView() {
        manager = CardStackLayoutManager(activity, this)
        cardStackAdapter = CardStackAdapter(mContext!!, userList, this@HomeFragment)

        manager!!.setStackFrom(StackFrom.None)
        manager!!.setVisibleCount(7)
        manager!!.setTranslationInterval(8.0f)
        manager!!.setScaleInterval(0.95f)
        manager!!.setSwipeThreshold(0.3f)
        manager!!.setMaxDegree(20.0f)
        manager!!.setDirections(Direction.HORIZONTAL)
        manager!!.setCanScrollHorizontal(true)
        manager!!.setCanScrollVertical(false)
        manager!!.setSwipeableMethod(SwipeableMethod.AutomaticAndManual)
        manager!!.setOverlayInterpolator(LinearInterpolator())
        cardStackView!!.layoutManager = manager
        cardStackView!!.adapter = cardStackAdapter
        cardStackView!!.itemAnimator.apply {
            if (this is DefaultItemAnimator) {
                supportsChangeAnimations = false
            }
        }
    }

    fun callRightEvent(position: Int) {
        selectedPosition = position
        callAPILikeUser("like", selectedPosition)
    }

    public fun callLeftEvent(position: Int) {
        selectedPosition = position
        callAPISkipUser("dislike", selectedPosition)
    }

    private fun callAPISendLocation(paramLatitude: String, paramLongitude: String) {
        if (AppUtils.isConnectedToInternet(activity)) {
            val params = HashMap<String, String?>()

            params["latitude"] = paramLatitude
            params["longitude"] = paramLongitude
            Log.e("param", params.toString())

            val call: Call<GeneralModel>
            call = RetrofitRestClient.instance!!.callNearMeAPI(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, params)
            // Log.e("token", SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
            call.enqueue(object : Callback<GeneralModel> {
                @SuppressLint("SetTextI18n", "ShowToast")
                override fun onResponse(call: Call<GeneralModel>?, response: Response<GeneralModel>?) {
                    val basicModel: GeneralModel
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {
                            Log.e("success", basicModel.message + "lat" + paramLatitude)
                        } else if (Objects.requireNonNull(basicModel).code == "-1") {
                            callLogoutFunction()
                        } else {
                            Log.e("error1", basicModel.message!!)
                        }
                    } else {
                        Log.e("error2", response.message())
                    }
                }

                override fun onFailure(call: Call<GeneralModel>?, t: Throwable?) {
                    if (t is SocketTimeoutException) {
                        Log.e("error3", "connection timeout")
                    } else if (t is NetworkErrorException) {
                        Log.e("error3", "network_error")
                    } else if (t is AuthenticatorException) {
                        Log.e("error3", "authentiation_error")
                    } else {
                        Log.e("error3", "something_went_wrong")
                    }
                }
            })
        } else {
            Log.e("error4", "No internet")
        }
    }

    private fun callAPISkipUser(s: String, selectedPosition: Int) {
        if (AppUtils.isConnectedToInternet(activity)) {
            //AppUtils.showProgressDialog(activity)
            val params = java.util.HashMap<String, String?>()
            params["to_id"] = userList[selectedPosition].id

            Log.e("paramSkip", params.toString())

            val call: Call<LikeResponse>
            call = RetrofitRestClient.instance!!.callSkipUser(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, params)
            // Log.e("token", SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
            call.enqueue(object : Callback<LikeResponse> {
                @SuppressLint("SetTextI18n", "ShowToast")
                override fun onResponse(call: Call<LikeResponse>?, response: Response<LikeResponse>?) {
                    val basicModel: LikeResponse
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {

                            showSwipeViewForVisibile()
                            makeSelectedImageWhenSwipe(selectedPosition, "dislike")
                            //setAdapterForImageList(selectedPosition + 1)
                            setAdapterForImageList(selectedPosition+2)
                            ifSuccessAndLastItem(selectedPosition)


                        } else if (Objects.requireNonNull(basicModel).code == "-1") {
                            callLogoutFunction()
                        } else {
                            setAdapterIfFail(selectedPosition)
                            showSnackBar(activity, basicModel.message)
                        }
                    } else {
                        setAdapterIfFail(selectedPosition)
                        showSnackBar(activity, response.message())
                    }
                    AppUtils.hideProgressDialog()
                }

                override fun onFailure(call: Call<LikeResponse>?, t: Throwable?) {

                    setAdapterIfFail(selectedPosition)
                    AppUtils.hideProgressDialog()
                    Log.e("message3", "fail")
                    if (t is SocketTimeoutException) {
                        showSnackBar(activity, getString(R.string.connection_timeout))
                    } else if (t is NetworkErrorException) {
                        showSnackBar(activity, getString(R.string.network_error))
                    } else if (t is AuthenticatorException) {
                        showSnackBar(activity, getString(R.string.authentiation_error))
                    } else {
                        showSnackBar(activity, getString(R.string.something_went_wrong))
                    }
                }
            })
        } else {
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }
    }

    private fun callAPILikeUser(like: String, selectedPosition: Int) {
        if (AppUtils.isConnectedToInternet(activity)) {
            //AppUtils.showProgressDialog(activity)
            val params = java.util.HashMap<String, String?>()
            params["to_id"] = userList[selectedPosition].id

            Log.e("paramlike", params.toString())

            val call: Call<LikeResponse>
            call = RetrofitRestClient.instance!!.callLikeUser(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, params)
            // Log.e("token", SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
            call.enqueue(object : Callback<LikeResponse> {
                @SuppressLint("SetTextI18n", "ShowToast")
                override fun onResponse(call: Call<LikeResponse>?, response: Response<LikeResponse>?) {
                    val basicModel: LikeResponse
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {

                            showSwipeViewForVisibile()
                            makeSelectedImageWhenSwipe(selectedPosition, "like")
                           // setAdapterForImageList(selectedPosition + 1)
                            setAdapterForImageList(selectedPosition + 2)
                            ifSuccessAndLastItem(selectedPosition)

                            if (basicModel.data!!.match != null) {
                                if (basicModel.data.match == "1") {
                                    try {
                                        redirectToStrikeScreen(selectedPosition)
                                    } catch (e: Exception) {
                                    }
                                } else {
                                }
                            }
                        } else if (Objects.requireNonNull(basicModel).code == "-1") {
                            callLogoutFunction()
                        } else {
                            setAdapterIfFail(selectedPosition)
                            showSnackBar(activity, basicModel.message)
                        }
                    } else {
                        setAdapterIfFail(selectedPosition)
                        showSnackBar(activity, response.message())
                    }
                    AppUtils.hideProgressDialog()
                }

                override fun onFailure(call: Call<LikeResponse>?, t: Throwable?) {

                    setAdapterIfFail(selectedPosition)
                    AppUtils.hideProgressDialog()
                    Log.e("message3", "fail")
                    try {
                        if (t is SocketTimeoutException) {
                            showSnackBar(activity, getString(R.string.connection_timeout))
                        } else if (t is NetworkErrorException) {
                            showSnackBar(activity, getString(R.string.network_error))
                        } else if (t is AuthenticatorException) {
                            showSnackBar(activity, getString(R.string.authentiation_error))
                        } else {
                            showSnackBar(activity, getString(R.string.something_went_wrong))
                        }
                    } catch (e: Exception) {

                    }

                }
            })
        } else {
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }

    }



    private fun ifSuccessAndLastItem(selectedPosition: Int) {
        if (selectedPosition == userList.size - 1) {
            llrvView!!.visibility = View.GONE
            llNodata!!.visibility = View.VISIBLE
        }
    }


    private fun redirectToStrikeScreen(selectedPosition: Int) {

        if (userList[selectedPosition].images!![0] != null) {
            val options = ActivityOptions.makeSceneTransitionAnimation(activity)
            val i = Intent(activity, StrikeActivity::class.java)
            i.putExtra("to_user_name", userList[selectedPosition].username)
            i.putExtra("to_user_id", userList[selectedPosition].id)
            i.putExtra(Constants.KEY_ANIM_TYPE, Constants.TransitionType.ExplodeJava)
            i.putExtra("image", userList[selectedPosition].baseUrl + userList[selectedPosition].images!![0].image)
            Log.e("strike_image2",userList[selectedPosition].baseUrl + userList[selectedPosition].images!![0].image)
            startActivity(i, options.toBundle())
        }

    }

    private fun setAdapterIfFail(selectedPosition: Int) {

        setAboveCardAndRemoveFrontFromStackView()

        if (selectedPosition == userList.size - 1) {
            llrvView!!.visibility = View.VISIBLE
            rlAllFavorite!!.visibility = View.VISIBLE
            llNodata!!.visibility = View.GONE
        }



    }

    private fun showSwipeViewForVisibile() {
        try {
            val animFadeIn = AnimationUtils.loadAnimation(activity, R.anim.zoom_out)  //fade_in
            rlSwipeImageView!!.startAnimation(animFadeIn)
            rlSwipeImageView!!.visibility = View.VISIBLE
        } catch (e: Exception) {

        }

    }

    private fun makeVisibleItem() {
        if (userList.isEmpty()) {
            llrvView!!.visibility = View.GONE
            llNodata!!.visibility = View.VISIBLE
        } else {
            llrvView!!.visibility = View.VISIBLE
            llNodata!!.visibility = View.GONE
        }

    }

    private fun makeSelectedImageWhenSwipe(position: Int, isFrom: String) {
        if (userList[position].images!!.isNotEmpty()) {
            if (userList[position].images!![0] != null) {
                ivSelectedImage?.let { Glide.with(context!!).load(userList[position].baseUrl + userList[position].images!![0].image).placeholder(R.drawable.ic_placeholder_icn).into(it) }
            }
        }

        if (isFrom == "dislike") {
            ivVerify!!.setImageResource(R.drawable.ic_cross_white_circle)
            userList[position].isLike = "dislike"
        }
        if (isFrom == "like") {
            ivVerify!!.setImageResource(R.drawable.ic_white_tick_green_circle)
            userList[position].isLike = "like"
        }
        if (isFrom == "superLike") {
            ivVerify!!.setImageResource(R.drawable.ic_superlike)
            userList[position].isLike = "superLike"
        }
    }


    private fun setAdapterForImageList(position: Int) {
        // imagelist.clear()
        rvImage!!.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false) //set adapter
        imageAdapter = ImageAdapter(activity, userList, position)
        rvImage!!.adapter = imageAdapter
    }


    companion object {

        @kotlin.jvm.JvmField
        var selectedPosition: Int = 0

        // TODO: Rename and change types and number of parameters

        @JvmStatic
        var selectedAction: String = ""


        fun newInstance(param1: String, param2: String) =
                HomeFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }


    override fun onClick(v: View) {
        when (v.id) {
            R.id.ivRight -> {
                callRightEvent(selectedPosition)
                val setting = SwipeAnimationSetting.Builder()
                        .setDirection(Direction.Right)
                        .setDuration(Duration.Normal.duration)
                        .setInterpolator(AccelerateInterpolator())
                        .build()
                manager!!.setSwipeAnimationSetting(setting)
                cardStackView!!.swipe()
                /*   cardStack!!.swipeTopCardRight(700);*/
            }
            R.id.ivCross -> {
                callLeftEvent(selectedPosition)
                val setting = SwipeAnimationSetting.Builder()
                        .setDirection(Direction.Left)
                        .setDuration(Duration.Normal.duration)
                        .setInterpolator(AccelerateInterpolator())
                        .build()
                manager!!.setSwipeAnimationSetting(setting)
                cardStackView!!.swipe()

                /*cardStack!!.swipeTopCardLeft(700);*/
            }
            R.id.ivFavorite -> {

                if (userList.size > 0) {
                    Log.e("name", userList[selectedPosition].username.toString())
                    callSuperLikeAPI(selectedPosition)  //call API super like
                }

                setAboveCardAndRemoveFrontFromStackView()
            }
            R.id.ivSelectedImage -> {
                val setting = RewindAnimationSetting.Builder()
                        .setDirection(Direction.Bottom)
                        .setDuration(Duration.Normal.duration)
                        .setInterpolator(DecelerateInterpolator())
                        .build()
                manager!!.setRewindAnimationSetting(setting)
                cardStackView!!.rewind()

                setAdapterForImageList(selectedPosition)

                if (selectedPosition != 0) {
                    rlSwipeImageView!!.visibility = View.VISIBLE

                    if (selectedPosition - 2 >= 0) {
                        if (userList[selectedPosition - 2].isLike == "like") {
                            makeSelectedImageWhenSwipe(selectedPosition - 2, "like")
                        }
                        if (userList[selectedPosition - 2].isLike == "dislike") {
                            makeSelectedImageWhenSwipe(selectedPosition - 2, "dislike")
                        }
                        if (userList[selectedPosition - 2].isLike == "superLike") {
                            makeSelectedImageWhenSwipe(selectedPosition - 2, "superLike")
                        }
                    }


                } else {
                    rlSwipeImageView!!.visibility = View.GONE
                    //  makeSelectedImageWhenSwipe(baseUrl, selectedPosition - 1, "dislike")
                }

                Log.e("pos", selectedPosition.toString())

            }
        }
    }

    private fun setAboveCardAndRemoveFrontFromStackView() {
        val setting = SwipeAnimationSetting.Builder()
                .setDirection(Direction.Top)
                .setDuration(100/*Duration.Normal.duration*/)
                .setInterpolator(AccelerateInterpolator())
                .build()
        manager!!.setSwipeAnimationSetting(setting)
        cardStackView!!.swipe()
    }

    private fun callSuperLikeAPI(Position: Int) =
            if (AppUtils.isConnectedToInternet(activity)) {


                //AppUtils.showProgressDialog(activity)
                val params = java.util.HashMap<String, String?>()
                params["to_id"] = userList[Position].id

                Log.e("param", params.toString())

                val call: Call<LikeResponse>
                call = RetrofitRestClient.instance!!.callSuperLikeUser(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, params)
                // Log.e("token", SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
                call.enqueue(object : Callback<LikeResponse> {
                    @SuppressLint("SetTextI18n", "ShowToast")
                    override fun onResponse(call: Call<LikeResponse>?, response: Response<LikeResponse>?) {
                        val basicModel: LikeResponse
                        if (response!!.isSuccessful) {
                            basicModel = response.body()!!
                            if (Objects.requireNonNull(basicModel).code == "1") {

                                showSwipeViewForVisibile()
                                makeSelectedImageWhenSwipe(selectedPosition - 1, "superLike")
                               // setAdapterForImageList(selectedPosition)
                                setAdapterForImageList(selectedPosition + 1)

                                ifSuccessAndLastItem(selectedPosition)
                                if (basicModel.data!!.match != null) {
                                    if (basicModel.data.match == "1") {
                                        try {
                                            redirectToStrikeScreen(Position)
                                        } catch (e: Exception) {

                                        }
                                    } else {

                                    }
                                }
                            } else if (Objects.requireNonNull(basicModel).code == "-1") {

                                callLogoutFunction()
                            } else {
                                setAdapterIfFail(selectedPosition)
                                //showSnackBar(activity, basicModel.message)
                                val diff = getDifferenecOfTwoDate(context!!, basicModel.data!!.last_premium_like_time!!)
                                val message = getString(R.string.you_are_able_another_premium_like) + "" + diff
                                setErrorDialog(message, diff)
                            }
                        } else {
                            setAdapterIfFail(selectedPosition)
                            showSnackBar(activity, response.message())
                        }
                        AppUtils.hideProgressDialog()
                    }

                    override fun onFailure(call: Call<LikeResponse>?, t: Throwable?) {
                        setAdapterIfFail(selectedPosition)
                        AppUtils.hideProgressDialog()
                        Log.e("message3", "fail")
                        if (t is SocketTimeoutException) {
                            showSnackBar(activity, getString(R.string.connection_timeout))
                        } else if (t is NetworkErrorException) {
                            showSnackBar(activity, getString(R.string.network_error))
                        } else if (t is AuthenticatorException) {
                            showSnackBar(activity, getString(R.string.authentiation_error))
                        } else {
                            showSnackBar(activity, getString(R.string.something_went_wrong))
                        }
                    }
                })
            } else {
                AppUtils.showToast(activity, getString(R.string.no_internet_connection))
            }

    private fun setErrorDialog(message: String?, diff: String) {

        val builder1 = AlertDialog.Builder(ContextThemeWrapper(activity, R.style.AlertDialogCustom))
        builder1.setMessage(message)
        builder1.setCancelable(true)
        builder1.setPositiveButton(getString(R.string.ok)) { dialog, id ->
            dialog.cancel()
        }
        val alert11 = builder1.create()
        alert11.show()
    }


    public fun redirectToUserProfile(position: Int) {

        selectedPosition = position
        selectedAction = ""
        if (Objects.requireNonNull<List<UserImageModel>>(userList[position].images).isNotEmpty()) {

            if (Objects.requireNonNull<List<UserImageModel?>>(userList[position].images)[0] != null) {
                if (isClickProfile == false) {
                    isClickProfile = true
                    val myactivity = Intent(context, UserProfileActivity::class.java)
                    myactivity.putExtra("isFrom", "home")
                    myactivity.putExtra("userId", userList[position].id)
                    myactivity.putExtra("userImage", userList[position].baseUrl + userList[position].images!![0].image)
                    Log.e("image_new",userList[position].baseUrl + userList[position].images!![0].image)
                    startActivity(myactivity)
                    AppUtils.overridePendingTransitionEnter(activity)
                }
            }
        }

    }

    override fun onResume() {
        super.onResume()
        isClickProfile = false
        try {
            if (selectedPosition != null) {
                if (selectedAction != "") {

                    setAboveCardAndRemoveFrontFromStackView()
                    // cardStack!!.setSelection(selectedPosition + 1)

                    showSwipeViewForVisibile()
                    makeSelectedImageWhenSwipe(selectedPosition, selectedAction)
                    //setAdapterForImageList(selectedPosition + 1)

                    setAdapterForImageList(selectedPosition + 2)
                    ifSuccessAndLastItem(selectedPosition)
                }
                selectedAction = ""
            }
        } catch (e: Exception) {

        }


    }

    @SuppressLint("NewApi")
    fun BlurImage(input: Bitmap): Bitmap {
        try {
            val rsScript = RenderScript.create(activity)
            val alloc = Allocation.createFromBitmap(rsScript, input)
            val blur = ScriptIntrinsicBlur.create(rsScript, Element.U8_4(rsScript))
            blur.setRadius(21F)
            blur.setInput(alloc)
            val result = Bitmap.createBitmap(input.getWidth(), input.getHeight(), Bitmap.Config.ARGB_8888)
            val outAlloc = Allocation.createFromBitmap(rsScript, result)
            blur.forEach(outAlloc)
            outAlloc.copyTo(result)
            rsScript.destroy()
            return result
        } catch (e: Exception) {
            // TODO: handle exception
            return input
        }
    }


    @AfterPermissionGranted(MyOriginActivity.RC_FINE_LOCATION)
    fun callFineLocation() {
        if (EasyPermissions.hasPermissions(context!!, Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Have permission, do the thing!
            callCorseLocation()
        } else {
            // Request one permission
            EasyPermissions.requestPermissions(
                    PermissionRequest.Builder(this, MyOriginActivity.RC_FINE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
                            .setRationale(R.string.rationale_call)
                            .setPositiveButtonText(getString(R.string.ok))
                            .setNegativeButtonText(getString(R.string.cancel))
                            .build())
        }
    }

    //permisson read storage
    @AfterPermissionGranted(MyOriginActivity.RC_ACCESSS_LOCATION)
    fun callCorseLocation() {
        if (EasyPermissions.hasPermissions(context!!, Manifest.permission.ACCESS_COARSE_LOCATION)) {
            // Have permission, do the thing!

            getUserCurrentLocation()  //call google api client for get location
            getUserCurrentLocation()  //call google api client for get location
            callAPIAllUserList()//this is important

        } else {
            // Request one permission
            EasyPermissions.requestPermissions(
                    PermissionRequest.Builder(this, MyOriginActivity.RC_ACCESSS_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
                            .setRationale(R.string.rationale_call)
                            .setPositiveButtonText(getString(R.string.ok))
                            .setNegativeButtonText(getString(R.string.cancel))
                            .build())
        }
    }

    private fun getUserCurrentLocation() {
        locationTrack = LocationTrack(activity)
        if (locationTrack!!.canGetLocation()) {
            if (locationTrack!!.getLongitude().toString() != "0.0") {
                paramLongitude = locationTrack!!.getLongitude().toString()
                paramLatitude = locationTrack!!.getLatitude().toString()
                Log.e("latlng_home", paramLatitude + "name" + paramLongitude)

                callAPISendLocation(paramLatitude, paramLongitude)
            }
        } else {
            // locationTrack.showSettingsAlert()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onCardDragging(direction: Direction, ratio: Float) {
        Log.d("CardStackView", "onCardDragging: d = ${direction.name}, r = $ratio")
    }

    override fun onCardSwiped(direction: Direction) {
        Log.d("CardStackView", "onCardSwiped: p = ${manager!!.topPosition}, d = $direction")
        selectedPosition = manager!!.topPosition
        if (manager!!.topPosition == cardStackAdapter!!.itemCount - 5) {
           /* paginate()*/  //if we call this then reverse flow work for swipedeskview
        }
    }

    override fun onCardRewound() {
        selectedPosition = manager!!.topPosition
        if (selectedPosition == 0) {
            rlSwipeImageView!!.visibility = View.GONE
        }
        Log.d("CardStackView", "onCardRewound: ${manager!!.topPosition}")
    }

    override fun onCardCanceled() {
        Log.d("CardStackView", "onCardCanceled: ${manager!!.topPosition}")
    }

    override fun onCardAppeared(view: View, position: Int) {
        val textView = view.findViewById<TextView>(R.id.tvUsername)
        Log.d("CardStackView", "onCardAppeared: ($position)+ ${manager!!.topPosition} ${textView.text}")
    }

    override fun onCardDisappeared(view: View, position: Int, direction: Direction) {
        val textView = view.findViewById<TextView>(R.id.tvUsername)
        Log.d("CardStackView", "onCardDisappeared: ($position)+ ${direction} ${textView.text}")

        if (direction.toString() == "Left") {
            Log.e("onCard", "Skip_API_call")
            callLeftEvent(position)
        }
        if (direction.toString() == "Right") {
            Log.e("onCard", "Like_API_call")
            callRightEvent(position)
        }
    }

    private fun paginate() {
        val old = cardStackAdapter!!.getSpots()
        val new = old.plus(userList)
        val callback = SpotDiffCallback(old, new)
        val result = DiffUtil.calculateDiff(callback)
        cardStackAdapter!!.setSpots(new)
        result.dispatchUpdatesTo(cardStackAdapter!!)
    }
}

