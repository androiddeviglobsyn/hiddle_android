package com.hiddle.app.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import com.google.android.material.tabs.TabLayout
import com.hiddle.app.R
import com.hiddle.app.adapter.ViewPagerAdapter
import com.hiddle.app.util.CustomViewPager


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class FavoriteFragment : BaseFragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    var tabs: TabLayout? = null
    var viewpagerFavorite: CustomViewPager? = null
    val text: TextView? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_favorite, container, false)
        tabs = view.findViewById(R.id.tabs) as TabLayout
        viewpagerFavorite = view.findViewById(R.id.viewpagerFavorite) as CustomViewPager
        llCount = view.findViewById(R.id.llCount) as LinearLayout
        tvCount = view.findViewById(R.id.tvCount) as AppCompatTextView

        setTabwithViewpager()

        return view
    }

    private fun setTabwithViewpager() {
        val adapter = ViewPagerAdapter(activity!!.supportFragmentManager)
        adapter.addFragment(StrikeFragment(), getString(R.string.strikes))
        adapter.addFragment(LikesFragment(), getString(R.string.likes))

        viewpagerFavorite!!.adapter = adapter

        tabs!!.setupWithViewPager(viewpagerFavorite)

        if (param1 == "like") {
            //for like and superlike notification click :set like tab as default
            tabs!!.getTabAt(1)!!.select()
        } else {
            //for default and strike notification click
            // tabs!!.getTabAt(0)!!.select()
        }

    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FavoriteFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        public var llCount: LinearLayout? = null
        public var tvCount: AppCompatTextView? = null
        fun newInstance(param1: String, param2: String) =
                FavoriteFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}
