package com.hiddle.app.fragment

import android.R
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.hiddle.app.activity.LoginScreenActivity
import com.hiddle.app.service.ForegroundService
import com.hiddle.app.service.OfflineOnlineService
import com.hiddle.app.sqllitedatabase.DatabaseChatHandler
import com.hiddle.app.util.AppUtils
import com.hiddle.app.util.SharedPreferenceManager

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [BaseFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
abstract class BaseFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    var databaseChatHandler: DatabaseChatHandler? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, parent: ViewGroup?, savedInstanseState: Bundle?): View? {
        val view: View? = provideYourFragmentView(inflater, parent, savedInstanseState)

        return view
    }

    private fun provideYourFragmentView(inflater: LayoutInflater, parent: ViewGroup?, savedInstanseState: Bundle?): View? {

        return view
    }

    fun callLogoutFunction() {
        //delete all stored record in offline sqllite
        try {
            databaseChatHandler = DatabaseChatHandler(activity)
            databaseChatHandler!!.deleteAllReecordMessage()
            databaseChatHandler!!.deleteAllReecordChat()
            Log.e("delete_all","delete_all")

            //for stop location service
            activity!!.stopService(Intent(activity, ForegroundService::class.java))
            //for online and offline service for API
            activity!!.stopService(Intent(activity, OfflineOnlineService::class.java))


            val i = Intent(activity, LoginScreenActivity::class.java)
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            SharedPreferenceManager.getMySharedPreferences()!!.setAuthorizationToken("")
            SharedPreferenceManager.getMySharedPreferences()!!.setUserId("")
            SharedPreferenceManager.getMySharedPreferences()!!.setUserName("")
            SharedPreferenceManager.getMySharedPreferences()!!.setCountryCode("")
            SharedPreferenceManager.getMySharedPreferences()!!.setLogin(false)
            SharedPreferenceManager.getMySharedPreferences()!!.setMobileNumber("")
            SharedPreferenceManager.getMySharedPreferences()!!.setEmail("")
            SharedPreferenceManager.getMySharedPreferences()!!.setNotificationUser("")
            SharedPreferenceManager.getMySharedPreferences()!!.setSound("1")
            SharedPreferenceManager.getMySharedPreferences()!!.setVibrate("0")
            SharedPreferenceManager.getMySharedPreferences()!!.setDistance("")

            startActivity(i)
            activity!!.finish()
            AppUtils.overridePendingTransitionExit(activity)

        } catch (e: Exception) {

        }


    }
    /* override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                               savedInstanceState: Bundle?): View? {
         return TextView(activity).apply {
             setText(R.string.hello_blank_fragment)
         }
     }*/

   /* companion object {
        *//**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment BaseFragment.
         *//*
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                BaseFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }*/



    fun showSnackBar(context:Context?, message:String?) {
        try
        {
            val view = (activity!!.findViewById(R.id.content) as ViewGroup).getChildAt(0) as ViewGroup
            val snackbar = Snackbar.make(view, message!!, Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE)
            val viewGroup = snackbar.view as ViewGroup
            viewGroup.setBackgroundColor(ContextCompat.getColor(context!!, com.hiddle.app.R.color.themeColor))
            val viewTv = snackbar.view
            val tv = viewTv.findViewById<TextView>(com.hiddle.app.R.id.snackbar_text)
            tv.setTextColor(ContextCompat.getColor(context, R.color.white))
            tv.maxLines = 5
            snackbar.show()
        }
        catch (e:Exception) {
            e.printStackTrace()
        }
    }



}
