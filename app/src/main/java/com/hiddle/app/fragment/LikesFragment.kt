package com.hiddle.app.fragment

import android.Manifest
import android.accounts.AuthenticatorException
import android.accounts.NetworkErrorException
import android.annotation.SuppressLint
import android.app.ActivityOptions
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.ContextThemeWrapper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.*
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DiffUtil
import com.daprlabs.cardstack.SwipeFrameLayout
import com.daprlabs.cardstack.SwipeLinearLayout

import com.hiddle.app.R
import com.hiddle.app.activity.MyOriginActivity
import com.hiddle.app.activity.StrikeActivity
import com.hiddle.app.activity.UserProfileActivity
import com.hiddle.app.adapter.CardStackLikeAdapter
import com.hiddle.app.adapter.SwipeDeckLikeAdapter
import com.hiddle.app.cardstackview.*
import com.hiddle.app.cardstackview.internal.SpotDiffCallback
import com.hiddle.app.cardstackview.internal.SpotLikeDiffCallback
import com.hiddle.app.common.SwipeDeck
import com.hiddle.app.model.FavoriteModel.FavoriteResponse
import com.hiddle.app.model.FavoriteModel.FavoriteResponseData
import com.hiddle.app.model.LikeModel.LikeResponse

import com.hiddle.app.retrofit.RetrofitRestClient
import com.hiddle.app.service.LocationTrack
import com.hiddle.app.util.AppUtilMethod.getDifferenecOfTwoDate
import com.hiddle.app.util.AppUtils
import com.hiddle.app.util.Constants
import com.hiddle.app.util.CustomSwipeDesk
import com.hiddle.app.util.SharedPreferenceManager
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions
import pub.devrel.easypermissions.PermissionRequest
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketTimeoutException
import java.util.*
import kotlin.collections.ArrayList

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [LikesFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class LikesFragment : BaseFragment(), View.OnClickListener, CardStackListener {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    //button
    //swipe desk main tinder  view

    private var adapter: SwipeDeckLikeAdapter? = null
    private var likeList: ArrayList<FavoriteResponseData> = ArrayList()
    private var tvNoCard: AppCompatTextView? = null
    private var mContext: Context? = null
    private var ivFavorite: RelativeLayout? = null
    private var ivRight: AppCompatImageView? = null
    private var ivCross: AppCompatImageView? = null
    private var rlAllFavorite: RelativeLayout? = null
    private var ivHeartAnimLike: AppCompatImageView? = null
    var pbLike: ProgressBar? = null
    var locationTrack: LocationTrack? = null
    var paramLongitude: String = ""
    var paramLatitude: String = ""
    var isFirst: Boolean = false
    var isClickMessage: Boolean = false
    var manager: CardStackLayoutManager? = null
    private var cardStackAdapter: CardStackLikeAdapter? = null
    var cardStackView: CardStackView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_likes, container, false)
        ivRight = view.findViewById(R.id.ivRight) as AppCompatImageView
        ivCross = view.findViewById(R.id.ivCross) as AppCompatImageView
        ivFavorite = view.findViewById(R.id.ivFavorite) as RelativeLayout
        tvNoCard = view.findViewById(R.id.tvNoCard) as AppCompatTextView
        rlAllFavorite = view.findViewById(R.id.rlAllFavorite) as RelativeLayout
        ivHeartAnimLike = view.findViewById(R.id.ivHeartAnimLike) as AppCompatImageView
        pbLike = view.findViewById(R.id.pbLike) as ProgressBar
        mContext = activity

        cardStackView = view.findViewById(R.id.card_stack_view_like);


        //API is calling in onResume method-callLikeListAPI()
        allClicklistener()
        setRotationOfFavorite()


        return view
    }

    private fun callLikeListAPI() {
        if (AppUtils.isConnectedToInternet(activity)) {
            pbLike!!.visibility = View.VISIBLE
            // AppUtils.showProgressDialog(activity)
            val params = HashMap<String, String?>()
            params["latitude"] = paramLatitude
            params["longitude"] = paramLongitude
            Log.e("paramLike", params.toString())
            val call: Call<FavoriteResponse>
            call = RetrofitRestClient.instance!!.likeListAPI(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, params)
            Log.e("token", SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
            call.enqueue(object : Callback<FavoriteResponse> {
                @SuppressLint("SetTextI18n")
                override fun onResponse(call: Call<FavoriteResponse>?, response: Response<FavoriteResponse>?) {
                    pbLike!!.visibility = View.GONE
                    AppUtils.hideProgressDialog()
                    val basicModel: FavoriteResponse
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {

                            isFirst = true
                            likeList.clear()
                            likeList = basicModel.data as ArrayList<FavoriteResponseData>

                            if (likeList.size > 0) {
                                selectedLikePosition = 0
                            }
                            setCardSwipeView()
                            callNoData()
                        } else if (Objects.requireNonNull(basicModel).code == "-1") {
                            callLogoutFunction()
                        } else {
                            callNoData()
                            Log.e("error", basicModel.message.toString())
                            /*showSnackBar(activity, basicModel.message)*/
                        }
                    } else {
                        callNoData()
                        Log.e("error", response.message())
                        /*showSnackBar(activity, response.message())*/
                    }
                }

                override fun onFailure(call: Call<FavoriteResponse>?, t: Throwable?) {
                    callNoData()
                    pbLike!!.visibility = View.GONE
                    //AppUtils.hideProgressDialog()
                    if (t is SocketTimeoutException) {
                        Log.e("error", getString(R.string.connection_timeout))
                        //showSnackBar(activity, getString(R.string.connection_timeout))
                    } else if (t is NetworkErrorException) {
                        Log.e("error", getString(R.string.network_error))
                        //showSnackBar(activity, getString(R.string.network_error))
                    } else if (t is AuthenticatorException) {
                        Log.e("error", getString(R.string.authentiation_error))
                        //showSnackBar(activity, getString(R.string.authentiation_error))
                    } else {
                        Log.e("error", getString(R.string.something_went_wrong))
                        //showSnackBar(activity, getString(R.string.something_went_wrong))
                    }
                }
            })
        } else {
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }
    }

    private fun setCardSwipeView() {

        manager = CardStackLayoutManager(activity, this)
        cardStackAdapter = CardStackLikeAdapter(mContext!!, likeList, this@LikesFragment)

        manager!!.setStackFrom(StackFrom.None)
        manager!!.setVisibleCount(7)
        manager!!.setTranslationInterval(8.0f)
        manager!!.setScaleInterval(0.95f)
        manager!!.setSwipeThreshold(0.3f)
        manager!!.setMaxDegree(20.0f)
        manager!!.setDirections(Direction.HORIZONTAL)
        manager!!.setCanScrollHorizontal(true)
        manager!!.setCanScrollVertical(false)
        manager!!.setSwipeableMethod(SwipeableMethod.AutomaticAndManual)
        manager!!.setOverlayInterpolator(LinearInterpolator())
        cardStackView!!.layoutManager = manager
        cardStackView!!.adapter = cardStackAdapter
        cardStackView!!.itemAnimator.apply {
            if (this is DefaultItemAnimator) {
                supportsChangeAnimations = false
            }
        }
    }




    private fun callNoData() {
        try {
            if (likeList.size > 0) {
                rlAllFavorite!!.visibility = View.VISIBLE
                tvNoCard!!.visibility = View.GONE

                FavoriteFragment.llCount!!.visibility = View.VISIBLE
                if (likeList.size > 0) {
                    FavoriteFragment.tvCount!!.text = likeList.size.toString()
                }
            } else {
                tvNoCard!!.visibility = View.VISIBLE
                rlAllFavorite!!.visibility = View.GONE
                FavoriteFragment.llCount!!.visibility = View.INVISIBLE
            }
        } catch (e: Exception) {
        }

    }

    public fun redirectToLikeUserProfile(position: Int) {
        if (likeList[position].profile != null) {
            if (isClickMessage == false) {
                isClickMessage = true
                val myactivity = Intent(context, UserProfileActivity::class.java)
                /*myactivity.putExtra("isFrom","Like")*/
                myactivity.putExtra("userId", likeList[position].userId)
                myactivity.putExtra("isFrom", "home")
                myactivity.putExtra("userImage", likeList[position].baseUrl + likeList[position].profile!![0].image)
                startActivity(myactivity)
                AppUtils.overridePendingTransitionEnter(activity)
            }
        }
    }

    private fun setRotationOfFavorite() {
        val aniRotate: Animation = AnimationUtils.loadAnimation(activity!!.applicationContext, R.anim.rotate_backward)
        aniRotate.repeatCount = Animation.INFINITE
        aniRotate.fillAfter = true;
        ivFavorite!!.startAnimation(aniRotate)
    }

    private fun allClicklistener() {
        ivCross!!.setOnClickListener(this)
        ivRight!!.setOnClickListener(this)
        ivFavorite!!.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.ivRight -> {
                if (selectedLikePosition >= 0) {
                    callRightEvent(selectedLikePosition)
                    val setting = SwipeAnimationSetting.Builder()
                            .setDirection(Direction.Right)
                            .setDuration(Duration.Normal.duration)
                            .setInterpolator(AccelerateInterpolator())
                            .build()
                    manager!!.setSwipeAnimationSetting(setting)
                    cardStackView!!.swipe()
                }
            }
            R.id.ivCross -> {
                if (selectedLikePosition >= 0) {
                    callLeftEvent(selectedLikePosition)
                    val setting = SwipeAnimationSetting.Builder()
                            .setDirection(Direction.Left)
                            .setDuration(Duration.Normal.duration)
                            .setInterpolator(AccelerateInterpolator())
                            .build()
                    manager!!.setSwipeAnimationSetting(setting)
                    cardStackView!!.swipe()
                }
            }
            R.id.ivFavorite -> {
                if (selectedLikePosition >= 0) {
                    if (likeList.size > 0) {
                        callSuperLikeAPI(selectedLikePosition)  //call API super like
                    }
                    setAboveCardAndRemoveFrontFromStackView()
                }
            }
        }
    }

    private fun callRightEvent(position: Int) {
        callAPILikeUser("like", selectedLikePosition)
    }

    private fun setAboveCardAndRemoveFrontFromStackView() {
        val setting = SwipeAnimationSetting.Builder()
                .setDirection(Direction.Top)
                .setDuration(100/*Duration.Normal.duration*/)
                .setInterpolator(AccelerateInterpolator())
                .build()
        manager!!.setSwipeAnimationSetting(setting)
        cardStackView!!.swipe()
    }

    private fun callAPISkipUser(s: String, selectedLikePosition: Int) {
        if (AppUtils.isConnectedToInternet(activity)) {
            //AppUtils.showProgressDialog(activity)
            val params = java.util.HashMap<String, String?>()

            Log.e("pos1", selectedLikePosition.toString())
            params["to_id"] = likeList[selectedLikePosition].userId

            Log.e("paramSkip", params.toString())

            val call: Call<LikeResponse>
            call = RetrofitRestClient.instance!!.callSkipUser(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, params)
            Log.e("token", SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
            call.enqueue(object : Callback<LikeResponse> {
                @SuppressLint("SetTextI18n", "ShowToast")
                override fun onResponse(call: Call<LikeResponse>?, response: Response<LikeResponse>?) {
                    val basicModel: LikeResponse
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {
                            setTabCount()
                            //Toast.makeText(activity, basicModel.message, Toast.LENGTH_SHORT).show()
                            ifSuccessAndLastItem(selectedLikePosition)
                        } else if (Objects.requireNonNull(basicModel).code == "-1") {
                            callLogoutFunction()
                        } else {
                            setAdapterIfFail(selectedLikePosition)
                            showSnackBar(activity, basicModel.message)
                        }
                    } else {
                        setAdapterIfFail(selectedLikePosition)
                        showSnackBar(activity, response.message())
                    }
                    AppUtils.hideProgressDialog()
                }

                override fun onFailure(call: Call<LikeResponse>?, t: Throwable?) {
                    setAdapterIfFail(selectedLikePosition)
                    AppUtils.hideProgressDialog()
                    Log.e("message3", "fail")
                    if (t is SocketTimeoutException) {
                        showSnackBar(activity, getString(R.string.connection_timeout))
                    } else if (t is NetworkErrorException) {
                        showSnackBar(activity, getString(R.string.network_error))
                    } else if (t is AuthenticatorException) {
                        showSnackBar(activity, getString(R.string.authentiation_error))
                    } else {
                        showSnackBar(activity, getString(R.string.something_went_wrong))
                    }
                }
            })
        } else {
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }
    }

    private fun callAPILikeUser(like: String, selectedLikePosition: Int) {
        if (AppUtils.isConnectedToInternet(activity)) {
            //AppUtils.showProgressDialog(activity)
            val params = java.util.HashMap<String, String?>()
            params["to_id"] = likeList[selectedLikePosition].userId

            Log.e("paramlike", params.toString())

            val call: Call<LikeResponse>
            call = RetrofitRestClient.instance!!.callLikeUser(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, params)
            Log.e("token", SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
            call.enqueue(object : Callback<LikeResponse> {
                @SuppressLint("SetTextI18n", "ShowToast")
                override fun onResponse(call: Call<LikeResponse>?, response: Response<LikeResponse>?) {
                    val basicModel: LikeResponse
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {
                            //Toast.makeText(activity, basicModel.message, Toast.LENGTH_SHORT).show()
                            ifSuccessAndLastItem(selectedLikePosition)

                            setTabCount()
                            if (basicModel.data!!.match != null) {
                                if (basicModel.data.match == "1") {
                                    try {
                                        redirectToStrikeScreen(selectedLikePosition)
                                    } catch (e: Exception) {
                                    }
                                } else {
                                }
                            }
                        } else if (Objects.requireNonNull(basicModel).code == "-1") {
                            callLogoutFunction()
                        } else {
                            setAdapterIfFail(selectedLikePosition)
                            showSnackBar(activity, basicModel.message)
                        }
                    } else {
                        setAdapterIfFail(selectedLikePosition)
                        showSnackBar(activity, response.message())
                    }
                    AppUtils.hideProgressDialog()
                }

                override fun onFailure(call: Call<LikeResponse>?, t: Throwable?) {
                    setAdapterIfFail(selectedLikePosition)
                    AppUtils.hideProgressDialog()
                    Log.e("message3", "fail")
                    if (t is SocketTimeoutException) {
                        showSnackBar(activity, getString(R.string.connection_timeout))
                    } else if (t is NetworkErrorException) {
                        showSnackBar(activity, getString(R.string.network_error))
                    } else if (t is AuthenticatorException) {
                        showSnackBar(activity, getString(R.string.authentiation_error))
                    } else {
                        showSnackBar(activity, getString(R.string.something_went_wrong))
                    }
                }
            })
        } else {
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }
    }

    private fun setTabCount() {
        try{
            var count = FavoriteFragment.tvCount!!.text.toString().toInt()
            if (count - 1 > 0) {
                FavoriteFragment.tvCount!!.text = (count - 1).toString()
            } else {
                FavoriteFragment.tvCount!!.text = ""
                FavoriteFragment.llCount!!.visibility = View.GONE
            }

            if (count - 1 == 0) {
                FavoriteFragment.llCount!!.visibility = View.GONE
            } else {
                FavoriteFragment.llCount!!.visibility = View.VISIBLE
            }
        }catch (e:Exception) {

        }
    }

    private fun callSuperLikeAPI(Position: Int) =
            if (AppUtils.isConnectedToInternet(activity)) {
                //AppUtils.showProgressDialog(activity)
                val params = java.util.HashMap<String, String?>()
                params["to_id"] = likeList[Position].userId

                Log.e("param", params.toString())

                val call: Call<LikeResponse>
                call = RetrofitRestClient.instance!!.callSuperLikeUser(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, params)
                Log.e("tokenLike", SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
                call.enqueue(object : Callback<LikeResponse> {
                    @SuppressLint("SetTextI18n", "ShowToast")
                    override fun onResponse(call: Call<LikeResponse>?, response: Response<LikeResponse>?) {
                        val basicModel: LikeResponse
                        if (response!!.isSuccessful) {
                            basicModel = response.body()!!
                            if (Objects.requireNonNull(basicModel).code == "1") {
                                //Toast.makeText(activity, basicModel.message, Toast.LENGTH_SHORT).show()

                                ifSuccessAndLastItem(selectedLikePosition)
                                setTabCount()

                                if (basicModel.data!!.match != null) {
                                    if (basicModel.data.match == "1") {
                                        try {
                                            redirectToStrikeScreen(Position)
                                        } catch (e: Exception) {
                                        }
                                    } else {

                                    }
                                }
                            } else if (Objects.requireNonNull(basicModel).code == "-1") {
                                callLogoutFunction()
                            } else {
                                setAdapterIfFail(selectedLikePosition)
                                val diff: String = getDifferenecOfTwoDate(context!!, basicModel.data!!.last_premium_like_time!!)
                                val message = getString(R.string.you_are_able_another_premium_like) + "" + diff
                                setErrorDialog(message)
                                // showSnackBar(activity, basicModel.message)
                            }
                        } else {
                            setAdapterIfFail(selectedLikePosition)
                            showSnackBar(activity, response.message())
                        }
                        AppUtils.hideProgressDialog()
                    }

                    override fun onFailure(call: Call<LikeResponse>?, t: Throwable?) {
                        setAdapterIfFail(selectedLikePosition)
                        AppUtils.hideProgressDialog()
                        Log.e("message3", "fail")
                        if (t is SocketTimeoutException) {
                            showSnackBar(activity, getString(R.string.connection_timeout))
                        } else if (t is NetworkErrorException) {
                            showSnackBar(activity, getString(R.string.network_error))
                        } else if (t is AuthenticatorException) {
                            showSnackBar(activity, getString(R.string.authentiation_error))
                        } else {
                            showSnackBar(activity, getString(R.string.something_went_wrong))
                        }
                    }
                })
            } else {
                AppUtils.showToast(activity, getString(R.string.no_internet_connection))
            }


    private fun setErrorDialog(message: String?) {
        val builder1 = AlertDialog.Builder(ContextThemeWrapper(activity, R.style.AlertDialogCustom))
        builder1.setMessage(message)
        builder1.setCancelable(true)
        builder1.setPositiveButton(getString(R.string.ok)) { dialog, id ->
            dialog.cancel()
        }
        val alert11 = builder1.create()
        alert11.show()
    }

    private fun setAdapterIfFail(selectedLikePosition: Int) {

        val setting = RewindAnimationSetting.Builder()
                .setDirection(Direction.Bottom)
                .setDuration(Duration.Normal.duration)
                .setInterpolator(DecelerateInterpolator())
                .build()
        manager!!.setRewindAnimationSetting(setting)
        cardStackView!!.rewind()

        if (selectedLikePosition == likeList.size - 1) {
            rlAllFavorite!!.visibility = View.VISIBLE
            tvNoCard!!.visibility = View.GONE
        }
    }


    private fun redirectToStrikeScreen(selectedLikePosition: Int) {
        if (likeList[selectedLikePosition].profile != null) {
            val options = ActivityOptions.makeSceneTransitionAnimation(activity)
            val i = Intent(activity, StrikeActivity::class.java)
            i.putExtra(Constants.KEY_ANIM_TYPE, Constants.TransitionType.ExplodeJava)
            i.putExtra("image", likeList[selectedLikePosition].baseUrl + likeList[selectedLikePosition].profile!![0].image)
            i.putExtra("to_user_name", likeList[selectedLikePosition].name)
            i.putExtra("to_user_id", likeList[selectedLikePosition].userId)
            startActivity(i, options.toBundle())
        }
    }

    private fun ifSuccessAndLastItem(selectedLikePosition: Int) {
        if (selectedLikePosition == likeList.size - 1) {
            tvNoCard!!.visibility = View.VISIBLE
            rlAllFavorite!!.visibility = View.GONE
            FavoriteFragment.llCount!!.visibility = View.GONE
        }
    }

    companion object {
        @kotlin.jvm.JvmField
        var selectedLikePosition: Int = -1
        /* var selectedLikePosition: Int = -1*/
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment LikesFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                LikesFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }

    override fun setUserVisibleHint(visible: Boolean) {
        super.setUserVisibleHint(visible)
        if (visible && isResumed()) {
            //Only manually call onResume if fragment is already visible
            //Otherwise allow natural fragment lifecycle to call onResume
            onResume()
        }
    }

    override fun onResume() {
        super.onResume()

        if (!isFirst) {
            callLikeListAPI()
        }

       //when user see this fragment only when API call
        if (userVisibleHint) {
            isClickMessage = false
            callFineLocation()
            return
        }
    }

    @AfterPermissionGranted(MyOriginActivity.RC_FINE_LOCATION)
    fun callFineLocation() {
        if (EasyPermissions.hasPermissions(context!!, Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Have permission, do the thing!
            callCorseLocation()
        } else {
            // Request one permission
            EasyPermissions.requestPermissions(
                    PermissionRequest.Builder(this, MyOriginActivity.RC_FINE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
                            .setRationale(R.string.rationale_call)
                            .setPositiveButtonText(getString(R.string.ok))
                            .setNegativeButtonText(getString(R.string.cancel))
                            .build())
        }
    }

    //permisson read storage
    @AfterPermissionGranted(MyOriginActivity.RC_ACCESSS_LOCATION)
    fun callCorseLocation() {
        if (EasyPermissions.hasPermissions(context!!, Manifest.permission.ACCESS_COARSE_LOCATION)) {
            // Have permission, do the thing!
            getUserCurrentLocation()  //call  for get location
            getUserCurrentLocation()  //call for get location
            callLikeListAPI()  //call API  //call API//this is important
        } else {
            // Request one permission
            EasyPermissions.requestPermissions(
                    PermissionRequest.Builder(this, MyOriginActivity.RC_ACCESSS_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
                            .setRationale(R.string.rationale_call)
                            .setPositiveButtonText(getString(R.string.ok))
                            .setNegativeButtonText(getString(R.string.cancel))
                            .build())
        }
    }

    private fun getUserCurrentLocation() {
        locationTrack = LocationTrack(activity)
        if (locationTrack!!.canGetLocation()) {
            if (locationTrack!!.getLongitude().toString() != "0.0") {
                paramLongitude = locationTrack!!.getLongitude().toString()
                paramLatitude = locationTrack!!.getLatitude().toString()
                Log.e("latlng_home", paramLatitude + "name" + paramLongitude)

            }
        } else {
            // locationTrack.showSettingsAlert()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onCardDragging(direction: Direction, ratio: Float) {
        Log.d("CardStackView", "onCardDragging: d = ${direction.name}, r = $ratio")
    }

    override fun onCardSwiped(direction: Direction) {
        Log.d("CardStackView", "onCardSwiped: p = ${manager!!.topPosition}, d = $direction")
        selectedLikePosition = manager!!.topPosition
        if (manager!!.topPosition == cardStackAdapter!!.itemCount - 5) {
            paginate()
        }
    }

    override fun onCardRewound() {
        Log.d("CardStackView", "onCardRewound: ${manager!!.topPosition}")
    }

    override fun onCardCanceled() {
        Log.d("CardStackView", "onCardCanceled: ${manager!!.topPosition}")
    }

    override fun onCardAppeared(view: View, position: Int) {
        val textView = view.findViewById<TextView>(R.id.tvUsername)
        Log.d("CardStackView", "onCardAppeared: ($position)+ ${manager!!.topPosition} ${textView.text}")
    }

    override fun onCardDisappeared(view: View, position: Int, direction: Direction) {
        val textView = view.findViewById<TextView>(R.id.tvUsername)
        Log.d("CardStackView", "onCardDisappeared: ($position)+ ${direction} ${textView.text}")

        if (direction.toString() == "Left") {
            Log.e("onCard", "Skip_API_call")
            selectedLikePosition = position
            callLeftEvent(selectedLikePosition)
        }
        if (direction.toString() == "Right") {
            Log.e("onCard", "Like_API_call")
            selectedLikePosition = position
            callRightEvent(selectedLikePosition)
        }
    }

    private fun callLeftEvent(pos: Int) {
        callAPISkipUser("dislike", pos)
    }

    private fun paginate() {
        val old = cardStackAdapter!!.getSpots()
        val new = old.plus(likeList)
        val callback = SpotLikeDiffCallback(old, new)
        val result = DiffUtil.calculateDiff(callback)
        cardStackAdapter!!.setSpots(new)
        result.dispatchUpdatesTo(cardStackAdapter!!)
    }
}
