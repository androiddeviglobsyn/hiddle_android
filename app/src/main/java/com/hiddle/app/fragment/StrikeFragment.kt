package com.hiddle.app.fragment

import android.Manifest
import android.accounts.AuthenticatorException
import android.accounts.NetworkErrorException
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.ContextThemeWrapper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.hiddle.app.R
import com.hiddle.app.`interface`.ClickForDeleteItem
import com.hiddle.app.activity.ChatActivity
import com.hiddle.app.activity.MyOriginActivity
import com.hiddle.app.activity.UserProfileActivity
import com.hiddle.app.adapter.StrikeAdapter
import com.hiddle.app.model.GeneralModel


import com.hiddle.app.model.StrikeModelClass.StrikeResponse
import com.hiddle.app.model.StrikeModelClass.StrikeResponseData
import com.hiddle.app.retrofit.RetrofitRestClient
import com.hiddle.app.service.LocationTrack
import com.hiddle.app.util.AppUtils
import com.hiddle.app.util.SharedPreferenceManager
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions
import pub.devrel.easypermissions.PermissionRequest
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketTimeoutException
import java.util.*
import kotlin.collections.ArrayList

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [StrikeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class StrikeFragment : BaseFragment(), ClickForDeleteItem {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    var strikeAdapter: StrikeAdapter? = null
    var strikelist: ArrayList<StrikeResponseData> = ArrayList()
    private var rvStrike: RecyclerView? = null
    private var llNoData: LinearLayout? = null
    private var isfirst: Boolean = false
    var locationTrack: LocationTrack? = null
    var pbStrike: ProgressBar? = null
    var paramLongitude: String = ""
    var paramLatitude: String = ""
    var isClickMessage: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_strike, container, false)
        rvStrike = view.findViewById(R.id.rvStrike) as RecyclerView
        llNoData = view.findViewById(R.id.llNoData) as LinearLayout
        pbStrike = view.findViewById(R.id.pbStrike) as ProgressBar

        //API called in on Resume

        return view
    }

    private fun callAPIStrikeList() {
        if (AppUtils.isConnectedToInternet(activity)) {
            if (!isfirst) {
                pbStrike!!.visibility = View.VISIBLE
                //AppUtils.showProgressDialogTouchRemove(activity)
            }
            val params = HashMap<String, String?>()
            params["latitude"] = paramLatitude
            params["longitude"] = paramLongitude
            Log.e("paramProfile", params.toString())

            val call: Call<StrikeResponse>
            call = RetrofitRestClient.instance!!.strikeListAPI(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, params)
            Log.e("tokenStrike", SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
            call.enqueue(object : Callback<StrikeResponse> {
                @SuppressLint("SetTextI18n")
                override fun onResponse(call: Call<StrikeResponse>?, response: Response<StrikeResponse>?) {
                    pbStrike!!.visibility = View.GONE
                    //AppUtils.hideProgressDialog()
                    val basicModel: StrikeResponse
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {
                            strikelist.clear()
                            strikelist = basicModel.data as ArrayList<StrikeResponseData>
                            isfirst = true
                            callNoData()
                            setAdapter()
                        } else if (Objects.requireNonNull(basicModel).code == "-1") {
                            callLogoutFunction()
                        } else {
                            callNoData()
                           // showSnackBar(activity, basicModel.message)
                        }
                    } else {
                        callNoData()
                       // showSnackBar(activity, response.message())
                    }
                }

                override fun onFailure(call: Call<StrikeResponse>?, t: Throwable?) {
                    callNoData()
                    pbStrike!!.visibility = View.GONE
                    // AppUtils.hideProgressDialog()
                    try {
                        if (t is SocketTimeoutException) {
                            showSnackBar(activity, getString(R.string.connection_timeout))
                        } else if (t is NetworkErrorException) {
                            showSnackBar(activity, getString(R.string.network_error))
                        } else if (t is AuthenticatorException) {
                            showSnackBar(activity, getString(R.string.authentiation_error))
                        } else {
                            showSnackBar(activity, getString(R.string.something_went_wrong))
                        }
                    } catch (e: Exception) {
                    }
                }
            })
        } else {
            pbStrike!!.visibility = View.GONE
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }
    }

    private fun callNoData() {
        if (strikelist.size > 0) {
            rvStrike!!.visibility = View.VISIBLE
            llNoData!!.visibility = View.GONE
        } else {
            llNoData!!.visibility = View.VISIBLE
            rvStrike!!.visibility = View.GONE
        }
    }

    private fun setAdapter() {
        rvStrike!!.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false) //set adapter
        strikeAdapter = StrikeAdapter(activity, strikelist, this@StrikeFragment)
        rvStrike!!.adapter = strikeAdapter
    }

    companion object {
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                StrikeFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }

    override fun deleteItem(position: Int) {
        val builder1 = AlertDialog.Builder(ContextThemeWrapper(activity, R.style.AlertDialogCustom))
        builder1.setMessage(getString(R.string.are_you_sure_want_remove_strike))
        builder1.setCancelable(true)
        builder1.setPositiveButton(getString(R.string.yes)) { dialog, id ->
            dialog.dismiss()
            callDeleteStrikeAPI(position) //api for log out
            //dialog.cancel()
        }
        builder1.setNegativeButton(getString(R.string.no)) { dialog, id -> dialog.cancel() }
        val alert11 = builder1.create()
        alert11.show()
    }

    override fun redirectUserProfile(position: Int) {
        if (isClickMessage == false) {
            isClickMessage = true
            val myactivity = Intent(activity, UserProfileActivity::class.java)
            myactivity.putExtra("userId", strikelist[position].userId)
            myactivity.putExtra("userImage", strikelist[position].baseUrl + strikelist[position].profile)
            myactivity.putExtra("isFrom", "other")
            startActivity(myactivity)
            AppUtils.overridePendingTransitionEnter(activity)
        }
    }

    override fun redirectToChatScreen(position: Int) {
        if (isClickMessage == false) {
            isClickMessage = true
            val myactivity = Intent(activity, ChatActivity::class.java)
            myactivity.putExtra("to_user_name", strikelist[position].name)
            myactivity.putExtra("user_image", strikelist[position].baseUrl + strikelist[position].profile)
            myactivity.putExtra("to_user_id", strikelist[position].userId)
            myactivity.putExtra("isFrom", "")
            startActivity(myactivity)
            AppUtils.overridePendingTransitionEnter(activity)
        }
    }

    private fun callDeleteStrikeAPI(position: Int) {
        if (AppUtils.isConnectedToInternet(activity)) {
            AppUtils.showProgressDialog(activity)
            val params = HashMap<String, String?>()

            params["user_id"] = strikelist[position].userId
            Log.e("param", params.toString())

            val call: Call<GeneralModel>
            call = RetrofitRestClient.instance!!.removeStrike(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, params)
            Log.e("token", SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
            call.enqueue(object : Callback<GeneralModel> {
                @SuppressLint("SetTextI18n", "ShowToast")
                override fun onResponse(call: Call<GeneralModel>?, response: Response<GeneralModel>?) {
                    val basicModel: GeneralModel
                    AppUtils.hideProgressDialog()
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {
                            AppUtils.hideProgressDialog()
                            strikelist.removeAt(position)
                            setAdapter()
                            Toast.makeText(activity, basicModel.message, Toast.LENGTH_SHORT).show()
                            if (strikelist.size == 0) {
                                llNoData!!.visibility = View.VISIBLE
                                rvStrike!!.visibility = View.GONE
                            }
                        } else if (Objects.requireNonNull(basicModel).code == "-1") {
                            callLogoutFunction()
                        } else {
                            AppUtils.hideProgressDialog()
                            Log.e("error", basicModel.message.toString())
                            /*howSnackBar(activity, basicModel.message)*/
                        }
                    } else {
                        AppUtils.hideProgressDialog()
                        Log.e("error", response.message())
                        /* showSnackBar(activity, response.message())*/
                    }
                    AppUtils.hideProgressDialog()
                }

                override fun onFailure(call: Call<GeneralModel>?, t: Throwable?) {
                    AppUtils.hideProgressDialog()
                    Log.e("message3", "fail")
                    if (t is SocketTimeoutException) {
                        Log.e("error", getString(R.string.connection_timeout))
                        //showSnackBar(activity, getString(R.string.connection_timeout))
                    } else if (t is NetworkErrorException) {
                        Log.e("error", getString(R.string.network_error))
                        //showSnackBar(activity, getString(R.string.network_error))
                    } else if (t is AuthenticatorException) {
                        Log.e("error", getString(R.string.authentiation_error))
                        //showSnackBar(activity, getString(R.string.authentiation_error))
                    } else {
                        Log.e("error", getString(R.string.something_went_wrong))
                        //showSnackBar(activity, getString(R.string.something_went_wrong))
                    }
                }
            })
        } else {
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }

    }

    override fun setUserVisibleHint(visible: Boolean) {
        super.setUserVisibleHint(visible)
        if (visible && isResumed()) { //Only manually call onResume if fragment is already visible
            //Otherwise allow natural fragment lifecycle to call onResume
            onResume()
        }
    }

    override fun onResume() {
        super.onResume()
        if (userVisibleHint)//when user see this fragment only when API call
        {
            isClickMessage = false
            callFineLocation()
            return
        }
    }

    @AfterPermissionGranted(MyOriginActivity.RC_FINE_LOCATION)
    fun callFineLocation() {
        if (EasyPermissions.hasPermissions(context!!, Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Have permission, do the thing!
            callCorseLocation()
        } else {
            // Request one permission
            EasyPermissions.requestPermissions(
                    PermissionRequest.Builder(this, MyOriginActivity.RC_FINE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
                            .setRationale(R.string.rationale_call)
                            .setPositiveButtonText(getString(R.string.ok))
                            .setNegativeButtonText(getString(R.string.cancel))
                            .build())
        }
    }

    //permisson read storage
    @AfterPermissionGranted(MyOriginActivity.RC_ACCESSS_LOCATION)
    fun callCorseLocation() {
        if (EasyPermissions.hasPermissions(context!!, Manifest.permission.ACCESS_COARSE_LOCATION)) {
            // Have permission, do the thing!
            getUserCurrentLocation()  //call  for get location
            getUserCurrentLocation()  //call for get location
            callAPIStrikeList()  //call API//this is important

        } else {
            // Request one permission
            EasyPermissions.requestPermissions(
                    PermissionRequest.Builder(this, MyOriginActivity.RC_ACCESSS_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
                            .setRationale(R.string.rationale_call)
                            .setPositiveButtonText(getString(R.string.ok))
                            .setNegativeButtonText(getString(R.string.cancel))
                            .build())
        }
    }

    private fun getUserCurrentLocation() {
        locationTrack = LocationTrack(activity)
        if (locationTrack!!.canGetLocation()) {
            if (locationTrack!!.getLongitude().toString() != "0.0") {
                paramLongitude = locationTrack!!.getLongitude().toString()
                paramLatitude = locationTrack!!.getLatitude().toString()
                Log.e("latlng_home", paramLatitude + "name" + paramLongitude)
            }
        } else {
            // locationTrack.showSettingsAlert()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }
}
