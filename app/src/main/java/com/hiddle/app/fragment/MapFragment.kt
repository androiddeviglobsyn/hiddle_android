package com.hiddle.app.fragment

import android.Manifest
import android.accounts.AuthenticatorException
import android.accounts.NetworkErrorException
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.ContentValues.TAG
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.graphics.*
import android.graphics.drawable.Drawable
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.*
import android.provider.Settings
import android.renderscript.Allocation
import android.renderscript.Element
import android.renderscript.RenderScript
import android.renderscript.ScriptIntrinsicBlur
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.facebook.FacebookSdk.getApplicationContext

import com.google.android.gms.maps.*

import com.google.android.gms.maps.model.*
import com.hiddle.app.R
import com.hiddle.app.`interface`.FragmentClick
import com.hiddle.app.activity.HomeActivity
import com.hiddle.app.activity.UserProfileActivity
import com.hiddle.app.model.UsermapModel.UsermapDataResponse
import com.hiddle.app.model.UsermapModel.UsermapResponse
import com.hiddle.app.retrofit.RetrofitRestClient
import com.hiddle.app.service.LocationTrack
import com.hiddle.app.util.*

import de.hdodenhof.circleimageview.CircleImageView
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions
import pub.devrel.easypermissions.PermissionRequest
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*
import java.net.MalformedURLException
import java.net.SocketTimeoutException
import java.net.URL
import java.util.*
import kotlin.collections.ArrayList

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

public class MapFragment : BaseFragment(), OnMapReadyCallback, View.OnClickListener, LocationListener/*, GoogleMap.OnMapLoadedCallback*//*, LocationListener*/ {

    private var param1: String? = null
    private var param2: String? = null
    private var mapUserList: ArrayList<UsermapDataResponse> = ArrayList()
    private var userIdList: ArrayList<UsermapDataResponse> = ArrayList()
    var user_count: String = "0"
    var mapuser_count: String = "0"
    var param_current_latitude: Double = 0.0
    var param_current_longitude: Double = 0.0
    var mapFragment: SupportMapFragment? = null
    private var mMap: GoogleMap? = null
    var mapView: View? = null
    var marker: Marker? = null
    var locationTrack: LocationTrack? = null
    var rlMap: RelativeLayout? = null
    var ivGpsMap: AppCompatImageView? = null
    var mapRipple: MapRipple? = null
    var mCurrLocationMarker: Marker? = null
    var is_first_location: Boolean = false
    private var pbMap: ProgressBar? = null

    //handler
    private lateinit var timer: Timer
    private val noDelay = 0L
    private val everyFiveSeconds = 18000L  //16 second  //10000  10second
    var rlMapLoad: RelativeLayout? = null
    var isClickMap: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    @SuppressLint("ObsoleteSdkInt")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        container!!.removeAllViewsInLayout()
        val view: View = inflater.inflate(R.layout.fragment_map, container, false)

        if (android.os.Build.VERSION.SDK_INT >= 9) {
            val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
            StrictMode.setThreadPolicy(policy)
        }

        //initialize view
        rlMap = view.findViewById(R.id.rlMap) as RelativeLayout
        ivGpsMap = view.findViewById(R.id.ivGpsMap) as AppCompatImageView
        pbMap = view.findViewById(R.id.pbMap) as ProgressBar
        rlMapLoad = view.findViewById(R.id.rlMapLoad) as RelativeLayout


        getPermissionforMapAndInitialization()  // permission for location and initialize map
        startRippleAnimationForCurrentUser()

        //show map load view layout
        if (AppUtils.isConnectedToInternet(activity)) {
            rlMapLoad!!.visibility = View.VISIBLE
            AppUtils.showProgressDialogTouchRemove(activity)
        } else {
            AppUtils.hideProgressDialog()
            hideMapLoadToast()
        }

        callFilterClickEvent() // call click filter using interface
        return view
    }

    private fun callFilterClickEvent() {
        (activity as HomeActivity).updateApi(object : FragmentClick {
            override fun clickonFragment(isFrom: String) {
                if (isFrom == "MapFilter") {
                    AppUtils.showProgressDialogTouchRemove(activity)
                    callAPIUserList("normal")
                    //call here api after filter click
                }
                if (isFrom == "MapBlurBitmap") {
                    if (mMap != null) {
                        snapShot()
                    }
                }

            }
        })
    }

    private fun hideMapLoadToast() {
        Handler(Looper.getMainLooper()).postDelayed({
            rlMapLoad!!.visibility = View.GONE
            //Do something after 100ms
        }, 900) //5 second
    }

    private fun getPermissionforMapAndInitialization() {
        callFineLocation()
    }


    @AfterPermissionGranted(Companion.RC_FINE_LOCATION)
    fun callFineLocation() {
        if (EasyPermissions.hasPermissions(activity!!, Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Have permission, do the thing!
            callCorseLocation()
        } else {
            // Request one permission
            EasyPermissions.requestPermissions(
                    PermissionRequest.Builder(this, Companion.RC_FINE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
                            .setRationale(R.string.rationale_call)
                            .setPositiveButtonText(getString(R.string.ok))
                            .setNegativeButtonText(getString(R.string.cancel))
                            .build())
        }
    }

    //permisson read storage
    @AfterPermissionGranted(Companion.RC_ACCESSS_LOCATION)
    fun callCorseLocation() {
        if (EasyPermissions.hasPermissions(activity!!, Manifest.permission.ACCESS_COARSE_LOCATION)) {
            // Have permission, do the thing!
            getMapInit() //for initialize map
        } else {
            // Request one permission
            EasyPermissions.requestPermissions(
                    PermissionRequest.Builder(this, Companion.RC_ACCESSS_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
                            .setRationale(R.string.rationale_call)
                            .setPositiveButtonText(getString(R.string.ok))
                            .setNegativeButtonText(getString(R.string.cancel))
                            .build())
        }
    }


    private fun getMapInit() {
        MapsInitializer.initialize(activity);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = this.childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        //mapView = mapFragment!!.view;
        // Objects.requireNonNull(mapFragment)!!.getMapAsync(this)
        mapFragment?.getMapAsync(this)
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onMapReady(googleMap: GoogleMap?) {

        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            val success = googleMap!!.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            activity, R.raw.style_json_map))
            if (!success) {
                Log.e(TAG, "Style parsing failed.")
            }
        } catch (e: Resources.NotFoundException) {
            Log.e(TAG, "Can't find style. Error: ", e)
        }


        mMap = googleMap
        //mMap!!.mapType = GoogleMap.MAP_TYPE_NORMAL;
        mMap!!.isMyLocationEnabled = false
        mMap!!.uiSettings.isMyLocationButtonEnabled = false
        mMap!!.setMinZoomPreference(4f)

        ivGpsMap!!.visibility = View.VISIBLE
        allClickListener()
        //make GPS at left top position setGPSLOcationLeftSide()

        setCurrentUserLocationMarker("current")
        callAPIUserList("normal")   //API for get user list
        /*  mMap!!.setOnMapLoadedCallback(this);*/
        mMap!!.setOnMapLoadedCallback(GoogleMap.OnMapLoadedCallback {
            snapShot()
        })
        val myLocationChangeListener = GoogleMap.OnMyLocationChangeListener { location ->
            val loc = LatLng(location.latitude, location.longitude)
            param_current_latitude = location.latitude//for get user current laitude and  longitude
            param_current_longitude = location.longitude
        }
        mMap!!.setOnMyLocationChangeListener(myLocationChangeListener)


    }

    private fun allClickListener() {
        ivGpsMap!!.setOnClickListener(this)
    }


    fun snapShot() {
        val callback = object : GoogleMap.SnapshotReadyCallback {
            internal lateinit var bitmap: Bitmap
            override fun onSnapshotReady(snapshot: Bitmap) {
                bitmap = snapshot
                final_Bitmap_map = bitmap
                final_Bitmap_map = BlurImage(bitmap);
                try {
                } catch (e: Exception) {
                    e.printStackTrace()
                    // Toast.makeText(this@PastValuations, "Not Capture", Toast.LENGTH_SHORT).show()
                }
            }
        }
        mMap!!.snapshot(callback)
    }

    private fun setCurrentUserLocationMarker(isFrom: String) {
        try {
            locationTrack = LocationTrack(activity)
            if (locationTrack!!.canGetLocation()) {
                if (locationTrack!!.longitude.toString() != "0.0") {
                    param_current_latitude = locationTrack!!.latitude
                    param_current_longitude = locationTrack!!.longitude

                    var lat = LatLng(param_current_latitude, param_current_longitude)

                    Log.e("lat_current", param_current_latitude.toString())
                    setCurrentUserMarkerAndRippleEffect(lat)
                    // mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(lat, 13f))
                    if (isFrom == "current") {
                        mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(lat, 14f))
                    }

                }
            } else {
                // locationTrack.showSettingsAlert()
            }
        } catch (e: Exception) {

        }

    }

    private fun setCurrentUserMarkerAndRippleEffect(lat: LatLng) {
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker!!.position = lat;
            try {
                mapRipple!!.withLatLng(lat)
                mapRipple!!.startRippleMapAnimation()
                hideMapLoadToast()
            } catch (e: Exception) {
            }
            mCurrLocationMarker!!.hideInfoWindow()

        } else {
            mCurrLocationMarker = mMap!!.addMarker(MarkerOptions().title("User").position(lat).anchor(0.5f, 0.5f).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_user)))
            try {
                mapRipple = MapRipple(mMap, lat, activity)
                mapRipple!!.withRippleDuration(12000)
                mapRipple!!.withTransparency(0.5f)
                mapRipple!!.withNumberOfRipples(2)
                mapRipple!!.withFillColor(resources.getColor(R.color.light_blue))
                mapRipple!!.withStrokeColor(resources.getColor(R.color.light_blue))
                mapRipple!!.withStrokewidth(8)
                mapRipple!!.startRippleMapAnimation()

                if (!mapRipple!!.isAnimationRunning) {
                    mapRipple!!.startRippleMapAnimation()
                    Log.e(TAG, "map_load")
                }
            } catch (e: Exception) {
            }
        }

        mMap!!.setOnMarkerClickListener { marker ->
            Log.e("click", "click" + marker.title)

            if (marker.title == "User") {
                mCurrLocationMarker!!.hideInfoWindow()
            } else {
                if (isClickMap == false) {
                    isClickMap = true
                    val myactivity = Intent(activity, UserProfileActivity::class.java)
                    myactivity.putExtra("userId", marker.title)
                    myactivity.putExtra("userImage", "")
                    myactivity.putExtra("isFrom", "other")
                    startActivity(myactivity)
                    AppUtils.overridePendingTransitionEnter(context)
                }

            }
            true
        }
    }


    private fun callAPIUserList(isFrom: String) {
        if (AppUtils.isConnectedToInternet(activity)) {
            if (!is_first_location) {
                //pbMap!!.visibility = View.VISIBLE
                // AppUtils.showProgressDialogTouchRemove(activity)
            }

            val call: Call<UsermapResponse>
            call = RetrofitRestClient.instance!!.callALLMapUserAPI(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
            Log.e("token", SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
            call.enqueue(object : Callback<UsermapResponse> {
                @SuppressLint("SetTextI18n")
                override fun onResponse(call: Call<UsermapResponse>?, response: Response<UsermapResponse>?) {
                    // pbMap!!.visibility = View.GONE

                    val basicModel: UsermapResponse
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {

                            is_first_location = true

                            userIdList.clear()
                            userIdList = (basicModel.data as ArrayList<UsermapDataResponse>?)!!
                            user_count = userIdList.size.toString()

                            if (isFrom == "normal") {
                                setMapUserListAndAddMarker(basicModel.data as ArrayList<UsermapDataResponse>?)
                                Log.e("call", "call")
                            }
                            if (isFrom == "handler") {
                                if (user_count != mapuser_count) {
                                    setMapUserListAndAddMarker(basicModel.data as ArrayList<UsermapDataResponse>?)
                                    Log.e("handelr", "handler_change")
                                } else {
                                    Log.e("handler", "handler_change_no_change")
                                }

                            }
                            if (userIdList.size == 0) {
                                AppUtils.hideProgressDialog()
                            }


                        } else if (Objects.requireNonNull(basicModel).code == "-1") {
                            AppUtils.hideProgressDialog()
                            callLogoutFunction()
                        } else {
                            AppUtils.hideProgressDialog()
                            //showSnackBar(activity, basicModel.message)
                        }
                    } else {
                        AppUtils.hideProgressDialog()
                        //showSnackBar(activity, response.message())
                    }
                }

                override fun onFailure(call: Call<UsermapResponse>?, t: Throwable?) {
                    //pbMap!!.visibility = View.GONE
                    AppUtils.hideProgressDialog()
                    try {
                        Log.e("map_erro", t!!.message.toString())
                        callAPIUserList("normal")
                    } catch (e: Exception) {

                    }

                }
            })
        } else {
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }
    }

    private fun setMapUserListAndAddMarker(arrayList: ArrayList<UsermapDataResponse>?) {

        mapUserList.clear()
        mapUserList = (arrayList)!!
        mapuser_count = mapUserList.size.toString()

        removeMarkerBeforeAddOtherMarker()
        setSomePoint()

    }

    private fun removeMarkerBeforeAddOtherMarker() {
        mMap!!.clear() // for clear all marker in android
        try {
            if (mapRipple != null) {
                if (mapRipple!!.isAnimationRunning) {
                    mapRipple!!.stopRippleMapAnimation()
                }
            }
        } catch (e: Exception) {

        }


        ////add custom marker for show current user location
        locationTrack = LocationTrack(activity)

        if (locationTrack!!.canGetLocation()) {
            if (locationTrack!!.longitude.toString() != "0.0") {
                param_current_latitude = locationTrack!!.latitude
                param_current_longitude = locationTrack!!.longitude

                var lat = LatLng(param_current_latitude, param_current_longitude)
                Log.e("lat_current", param_current_latitude.toString())

                mCurrLocationMarker = mMap!!.addMarker(MarkerOptions().title("User").position(lat).anchor(0.5f, 0.5f).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_user)))

            }
        }
        setCurrentUserLocationMarker("location_change")
    }


    private fun setSomePoint() {

        val builder = LatLngBounds.Builder()
        //When Map Loads Successfully
        /*mMap!!.setOnMapLoadedCallback {*/
        for (i in mapUserList.indices) {

            Log.e("Name1", mapUserList[i].name!!)
            if (mapUserList[i].latitude != null) {
                if (mapUserList[i].latitude != null) {
                    if (mapUserList[i].latitude != "null") {
                        if (mapUserList[i].longitude != "") {
                            Log.e("Name", i.toString() + mapUserList[i].name)

                            addMarkerMethod(i, mapUserList[i].latitude!!.toDouble(), mapUserList[i].longitude!!.toDouble(), mapUserList[i].baseUrl + mapUserList[i].profile, mapUserList[i].userId, mapUserList[i].isLike!!, mapUserList[i].isSuperlike!!)

                            //this is set for info window invisible
                            val customInfoWindow: CustomInfoWindowGoogleMapCustom = CustomInfoWindowGoogleMapCustom(activity)
                            mMap!!.setInfoWindowAdapter(customInfoWindow)

                            //LatLngBound will cover all your marker on Google Maps
                            //for show bound marker near to location
                            builder.include(LatLng(mapUserList[i].latitude!!.toDouble(), mapUserList[i].longitude!!.toDouble())) //Taking Point A (First LatLng)
                        }
                    }
                }
            }

            if (mapUserList.size > 0) {
                val bounds = builder.build()
                val cu: CameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, 200)
            }

        }
        AppUtils.hideProgressDialog()

    }

    private fun addMarkerMethod(pos: Int, lat: Double, lang: Double, img: String, username: String?, like: String, superlike: String) {
        try {
            if (img != null) {
                try {
                    /*Thread(Runnable {*/
                    Glide.with(activity!!).load(img).into(object : SimpleTarget<Drawable>() {
                        override fun onResourceReady(resource: Drawable, transition: Transition<in Drawable>?) {
                            try {
                                marker = mMap!!.addMarker(MarkerOptions().position(LatLng(lat, lang)).title(username)
                                        .icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(pos, resource, username!!, like, superlike))))

                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }

                        override fun onLoadFailed(errorDrawable: Drawable?) {
                            super.onLoadFailed(errorDrawable)
                            try {
                                marker = mMap!!.addMarker(MarkerOptions().position(LatLng(lat, lang)).title(username)
                                        .icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(pos, resources.getDrawable(R.drawable.ic_placeholder_icn), username!!, like, superlike))))
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }

                    })

                    /* }).start()*/
                } catch (e: Exception) {

                }

            }
        } catch (e: Exception) {
            Log.e("map_error", "null_exception")
        }

    }


    private fun getMarkerBitmapFromView(pos: Int, url: Drawable, name: String, like: String, superlike: String): Bitmap {
        val view: View = (Objects.requireNonNull(activity)!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(R.layout.custom_map_circle_marker, null)

        val user_dp = view.findViewById<View>(R.id.user_dp) as CircleImageView
        var ivAction = view.findViewById<View>(R.id.ivAction) as AppCompatImageView

        /* val tvName = view.findViewById<View>(R.id.tvName) as AppCompatTextView
         tvName.setText(name)*/
        user_dp.setImageDrawable(url)

        if (like == "1") {
            ivAction.setImageResource(R.drawable.ic_white_tick_green_circle)
            ivAction.visibility = View.VISIBLE
        }
        if (superlike == "1") {
            ivAction.setImageResource(R.drawable.ic_superlike)
            ivAction.visibility = View.VISIBLE
        }
        if (like == "0" && superlike == "0") {
            ivAction.visibility = View.GONE
        }

        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight())
        view.buildDrawingCache()
        val returnedBitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888)
        val canvas = Canvas(returnedBitmap)
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN)
        val drawable = view.getBackground()
        if (drawable != null)
            drawable.draw(canvas)
        view.draw(canvas)
        return returnedBitmap
    }


    private fun setGPSLOcationLeftSide() {
        val locationButton = (mapView!!.findViewById<View>("1".toInt()).parent as View).findViewById<View>("2".toInt())
        val rlp = locationButton.layoutParams as RelativeLayout.LayoutParams
        // position on left top
        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, 0)
        rlp.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE)
        rlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, 0)
        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE)
        rlp.addRule(RelativeLayout.ALIGN_PARENT_END, 0)
        rlp.addRule(RelativeLayout.ALIGN_END, 0)
        rlp.addRule(RelativeLayout.ALIGN_PARENT_LEFT)
        rlp.setMargins(70, 50, 70, 40)
        rlp.marginStart = 15
    }

    companion object {
        @JvmStatic
        var final_Bitmap_map: Bitmap? = null

        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                MapFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }

        const val RC_ACCESSS_LOCATION: Int = 126
        const val RC_FINE_LOCATION: Int = 127
    }

    @SuppressLint("NewApi")
    fun BlurImage(input: Bitmap): Bitmap {
        try {
            val rsScript = RenderScript.create(getApplicationContext())
            val alloc = Allocation.createFromBitmap(rsScript, input)
            val blur = ScriptIntrinsicBlur.create(rsScript, Element.U8_4(rsScript))
            blur.setRadius(21F)
            blur.setInput(alloc)
            val result = Bitmap.createBitmap(input.getWidth(), input.getHeight(), Bitmap.Config.ARGB_8888)
            val outAlloc = Allocation.createFromBitmap(rsScript, result)
            blur.forEach(outAlloc)
            outAlloc.copyTo(result)
            rsScript.destroy()
            return result
        } catch (e: Exception) {
            // TODO: handle exception
            return input
        }
    }

    override fun onStop() {
        super.onStop()
        try {
            if (mapRipple != null) {
                if (mapRipple!!.isAnimationRunning) {
                    mapRipple!!.stopRippleMapAnimation()
                }
            }
        } catch (e: Exception) {
        }
    }

    override fun onPause() {
        super.onPause()
        timer.cancel() //for stop handelr every 15 second
        timer.purge()
    }

    override fun onResume() {
        super.onResume()
        isClickMap = false

        startRippleAnimationForCurrentUser()
        callAPIIfNewUserCrossed()
    }

    private fun startRippleAnimationForCurrentUser() {
        try {
            if (mapRipple != null) {  //start ripple animation if it stop for current location
                if (param_current_latitude.toString() != "0.0") {
                    mapRipple!!.startRippleMapAnimation()
                }
            }
        } catch (e: Exception) {
        }
    }

    private fun callAPIIfNewUserCrossed() {
        val timerTask = object : TimerTask() { //handler for call API every 15 second if new user crossed
            override fun run() {
                if (is_first_location == true) {
                    Log.e("handler", "APIcallUsinghandler")
                    callAPIUserList("handler")  //api for map user list
                }
            }
        }
        timer = Timer()
        timer.schedule(timerTask, noDelay, everyFiveSeconds)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.ivGpsMap -> {
                val manager = activity!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager
                if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    buildAlertMessageNoGps()
                } else {
                    setCurrentUserLocationMarker("current")
                }
                setCurrentUserLocationMarker("current")
            }
        }
    }

    private fun buildAlertMessageNoGps() {
        val alertDialog = AlertDialog.Builder(activity)
        alertDialog.setMessage(getString(R.string.gps_not_enable))
        alertDialog.setPositiveButton(getString(R.string.settings)) { dialog, which ->
            dialog.dismiss()
            val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            activity!!.startActivity(intent)
        }
        alertDialog.show()
    }


    override fun onLocationChanged(location: Location) {
        if (mMap != null) {
            if (location != null) {
                var lat = LatLng(location.latitude, location.longitude)

                if (location.latitude.toString() != "0.0") {
                    setCurrentUserMarkerAndRippleEffect(lat)
                }
                Log.e("lat_current_change", location!!.latitude.toString())
            }
        }
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
    }
}


