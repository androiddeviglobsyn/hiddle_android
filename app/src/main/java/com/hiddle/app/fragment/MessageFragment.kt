package com.hiddle.app.fragment

import android.accounts.AuthenticatorException
import android.accounts.NetworkErrorException
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.ContextThemeWrapper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.facebook.FacebookSdk.getApplicationContext

import com.hiddle.app.R
import com.hiddle.app.`interface`.MessageItemClick
import com.hiddle.app.activity.ChatActivity
import com.hiddle.app.activity.UserProfileActivity
import com.hiddle.app.adapter.MessageAdapter
import com.hiddle.app.dialog.MessageDialog
import com.hiddle.app.dialog.OtherMessageDialog
import com.hiddle.app.dialog.ReportDialog
import com.hiddle.app.model.ChatMessageModel.ChatDataResponse
import com.hiddle.app.model.ChatMessageModel.ChatMessageResponse
import com.hiddle.app.model.GeneralModel
import com.hiddle.app.retrofit.RetrofitRestClient
import com.hiddle.app.sqllitedatabase.DatabaseChatHandler
import com.hiddle.app.sqllitedatabase.MessageClass
import com.hiddle.app.util.AppUtils
import com.hiddle.app.util.AppUtils.isSameDay
import com.hiddle.app.util.LinearLayoutManagerWrapper
import com.hiddle.app.util.SharedPreferenceManager
import kotlinx.android.synthetic.main.custom_dialog_message.*
import kotlinx.android.synthetic.main.custom_dialog_message.tvRemove
import kotlinx.android.synthetic.main.custom_dialog_other.*
import kotlinx.android.synthetic.main.custom_dialog_report.*
import kotlinx.android.synthetic.main.custom_dialog_report.tvOther
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketTimeoutException
import java.util.*
import kotlin.collections.ArrayList

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

@Suppress("DEPRECATION")
class MessageFragment : BaseFragment(), MessageItemClick {
    private var param1: String? = null
    private var param2: String? = null
    private var isfirst: Boolean = false
    var messageAdapter: MessageAdapter? = null
    var messagelist: ArrayList<ChatDataResponse> = ArrayList()
    var messageUpdatelist: ArrayList<ChatDataResponse> = ArrayList()

    //swipeview
    private var rvMessage: RecyclerView? = null
    var pbMessage: ProgressBar? = null
    var tvNoData: AppCompatTextView? = null
    var messageDialog: MessageDialog? = null
    var reportDialog: ReportDialog? = null
    var databaseHandler: DatabaseChatHandler? = null
    var otherMessageDialog: OtherMessageDialog? = null
    var chatDataResponse: ChatDataResponse? = null
    var isClickMessage: Boolean = false
    var swiperefresh: SwipeRefreshLayout? = null
    private lateinit var timer: Timer
    private val noDelay = 0L
    private val everyFiveSeconds = 7000L  //7 second  //10000  10second
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_message, container, false)
        rvMessage = view.findViewById(R.id.rvMessage) as RecyclerView
        tvNoData = view.findViewById(R.id.tvNoData) as AppCompatTextView
        pbMessage = view.findViewById(R.id.pbMessage) as ProgressBar
        swiperefresh = view.findViewById(R.id.swiperefresh) as SwipeRefreshLayout

        allClickListener()

        return view
    }

    private fun allClickListener() {
        swiperefresh!!.setColorSchemeResources(R.color.green_light)
        swiperefresh!!.setOnRefreshListener(SwipeRefreshLayout.OnRefreshListener {
            if (AppUtils.isConnectedToInternet(activity as Context?)) {
                callAPIMessageList("home")
            } else {
            }
            swiperefresh!!.isRefreshing = false
        })

        callTimerToRefreshData()  //for refresh data if new message recieve at every 7 second
    }

    private fun callTimerToRefreshData() {
        val timerTask = object : TimerTask() { //handler for call API every 10 second if new user crossed
            override fun run() {
                if (AppUtils.isConnectedToInternet(activity as Context?)) {
                    callAPIMessageList("handler")
                    Log.e("timer", "APIcallUsinghandler")
                } else {
                }
            }
        }
        timer = Timer()
        timer.schedule(timerTask, noDelay, everyFiveSeconds)
    }

    override fun onPause() {
        super.onPause()
        timer.cancel() //for stop handelr every 15 second
        timer.purge()
    }

    private fun callAPIMessageList(isfrom: String) {
        if (AppUtils.isConnectedToInternet(activity as Context?)) {
            try {
                if (!isfirst) {
                    pbMessage!!.visibility = View.VISIBLE
                    //AppUtils.showProgressDialogTouchRemove(activity)
                }
            } catch (e: Exception) {

            }

            val params = HashMap<String, String?>()
            params["user_id"] = SharedPreferenceManager.getMySharedPreferences()!!.getUserId()

            Log.e("paramProfile", params.toString())
            val call: Call<ChatMessageResponse>
            call = RetrofitRestClient.instance!!.messageListAPI(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, params)
            Log.e("tokenStrike", SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
            call.enqueue(object : Callback<ChatMessageResponse> {
                @SuppressLint("SetTextI18n")
                override fun onResponse(call: Call<ChatMessageResponse>?, response: Response<ChatMessageResponse>?) {
                    pbMessage!!.visibility = View.GONE
                    //AppUtils.hideProgressDialog()
                    val basicModel: ChatMessageResponse
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {

                            if (basicModel.data != null) {
                                if (isfrom == "home")
                                {
                                    messagelist.clear()
                                    messagelist = basicModel.data as ArrayList<ChatDataResponse>
                                    isfirst = true
                                    callNoData()
                                    setAdapter()
                                    //save and delete record using database
                                    addDatainSqllite()
                                    //getListOfMessageOffline() //for get all data without filter id
                                } else {
                                    messageUpdatelist.clear()
                                    messageUpdatelist = basicModel.data as ArrayList<ChatDataResponse>
                                    if (messagelist.size > 0) {
                                        if (messagelist.size == messageUpdatelist.size) {
                                            for (i in messagelist.indices) {
                                                if (messagelist[i].userId == messageUpdatelist[i].userId) {
                                                    if (messagelist[i].msg != messageUpdatelist[i].msg) {
                                                        messagelist[i].count = messageUpdatelist[i].count
                                                        messagelist[i].msg = messageUpdatelist[i].msg
                                                        messagelist[i].time = messageUpdatelist[i].time
                                                        messageAdapter!!.notifyItemChanged(i)
                                                    }
                                                }

                                            }
                                        }
                                    }
                                }
                            } else {
                                if (isfrom == "home") {
                                    callNoData()
                                }
                            }
                        } else if (Objects.requireNonNull(basicModel).code == "-1") {
                            callLogoutFunction()
                        } else {
                            if (isfrom == "home") {
                                callNoData()
                                //showSnackBar(activity, basicModel.message)
                            }
                        }
                    } else {
                        if (isfrom == "home") {
                            callNoData()
                            //showSnackBar(activity, response.message())
                        }
                    }
                }

                override fun onFailure(call: Call<ChatMessageResponse>?, t: Throwable?) {
                    if (isfrom == "home") {
                        pbMessage!!.visibility = View.GONE
                        callNoData()
                        AppUtils.hideProgressDialog()
                        try {
                            if (t is SocketTimeoutException) {
                                showSnackBar(activity, getString(R.string.connection_timeout))
                            } else if (t is NetworkErrorException) {
                                showSnackBar(activity, getString(R.string.network_error))
                            } else if (t is AuthenticatorException) {
                                showSnackBar(activity, getString(R.string.authentiation_error))
                            } else {
                                showSnackBar(activity, getString(R.string.something_went_wrong))
                            }
                        } catch (e: Exception) {
                        }
                    }
                }
            })
        } else {
            getListOfMessageById()
            setAdapter()
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }
    }

    private fun getListOfMessageById() {
        messagelist.clear()
        //creating the instance of DatabaseHandler class
        //calling the viewEmployee method of DatabaseHandler class to read the records
        val emp: List<MessageClass> = databaseHandler!!.viewMessageId(SharedPreferenceManager.getMySharedPreferences()!!.getUserId()!!)

        var index = 0
        for (e in emp) {
            Log.e("user_id11", e.user_id!!)
            Log.e("usee_name11", e.name!!)

            chatDataResponse = ChatDataResponse()
            chatDataResponse!!.time = e.time
            chatDataResponse!!.name = e.name
            chatDataResponse!!.profile!!.image = e.profile
            chatDataResponse!!.userId = e.user_id
            chatDataResponse!!.baseUrl = e.base_url
            chatDataResponse!!.msg = e.msg
            chatDataResponse!!.status = e.status
            chatDataResponse!!.isReadCount = "0"
            messagelist.add(chatDataResponse!!)

            index++
        }
        setAdapter()
    }

    private fun addDatainSqllite() {
        //  creating the instance of DatabaseHandler class
        if (databaseHandler!!.checkRecordExitMessage(SharedPreferenceManager.getMySharedPreferences()!!.getUserId()!!)) {
            //first delete all Record
            databaseHandler!!.deleteAllReecordMessage() //for delete all recoed
        }

        //insert all recoed
        for (i in 0..messagelist.size - 1) {
            val status = databaseHandler!!.addEmployee(MessageClass(SharedPreferenceManager.getMySharedPreferences()!!.getUserId(), messagelist[i].userId,
                    messagelist[i].name, messagelist[i].profile!!.image.toString(), messagelist[i].time, messagelist[i].baseUrl, messagelist[i].msg, messagelist[i].status, SharedPreferenceManager.getMySharedPreferences()!!.getUserId()))

            if (status > -1) {
                Log.e("message", "record_save_message")
                //Toast.makeText(getApplicationContext(), "record save", Toast.LENGTH_LONG).show()
            }
        }

    }


    private fun callNoData() {
        if (messagelist.size > 0) {
            rvMessage!!.visibility = View.VISIBLE
            tvNoData!!.visibility = View.GONE
        } else {
            tvNoData!!.visibility = View.VISIBLE
            rvMessage!!.visibility = View.GONE
        }
    }


    private fun setAdapter() {
        rvMessage!!.layoutManager = LinearLayoutManagerWrapper(activity, LinearLayoutManager.VERTICAL, false) //set adapter
        messageAdapter = MessageAdapter(activity, messagelist, this)
        rvMessage!!.adapter = messageAdapter
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment MessageFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                MessageFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }


    override fun itemClick(position: Int, to_user_id: String) {
        messageDialog = MessageDialog(activity)
        messageDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        messageDialog!!.show()

        messageDialog!!.tvRemove.setOnClickListener(View.OnClickListener {
            messageDialog!!.dismiss()
            callDialogRemoveUserAndAPI(to_user_id, position)

        })

        messageDialog!!.tvReport.setOnClickListener(View.OnClickListener {
            callReportDialog(to_user_id, position)
            messageDialog!!.dismiss()
        })

        messageDialog!!.tvBlock.setOnClickListener(View.OnClickListener {
            callDialogBlockUserAndAPI(to_user_id, position)
            messageDialog!!.dismiss()
        })

    }

    override fun itemChatClick(position: Int) {
        if (isClickMessage == false) {
            isClickMessage = true
            val myactivity = Intent(activity, ChatActivity::class.java)
            myactivity.putExtra("to_user_name", messagelist[position].name)
            myactivity.putExtra("user_image", messagelist[position].baseUrl + messagelist[position].profile!!.image)
            myactivity.putExtra("to_user_id", messagelist[position].userId)
            myactivity.putExtra("isFrom", "")
            myactivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context!!.applicationContext.startActivity(myactivity)
            AppUtils.overridePendingTransitionEnter(activity)
        }

    }

    override fun itemUserProfileClick(position: Int) {
        if (isClickMessage == false) {
            isClickMessage = true
            val myactivity = Intent(activity, UserProfileActivity::class.java)
            myactivity.putExtra("userId", messagelist[position].userId)
            myactivity.putExtra("userImage", messagelist[position].baseUrl + messagelist[position].profile!!.image)
            myactivity.putExtra("isFrom", "other")
            myactivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(myactivity)
            AppUtils.overridePendingTransitionEnter(activity)
        }
    }

    private fun callDialogRemoveUserAndAPI(toUserId: String, position: Int) {
        val builder1 = AlertDialog.Builder(ContextThemeWrapper(activity, R.style.AlertDialogCustom))
        builder1.setMessage(getString(R.string.are_you_sure_want_to_remove))
        builder1.setCancelable(true)

        builder1.setPositiveButton(getString(R.string.yes)) { dialog, id ->
            callRemoveeFromMessageListAPI(toUserId, position)  //call block user API
            if (messageDialog!!.isShowing) {
                messageDialog!!.dismiss()
            }
            dialog.cancel()
        }

        builder1.setNegativeButton(getString(R.string.no)) { dialog, id -> dialog.cancel() }
        val alert11 = builder1.create()
        alert11.show()
    }

    private fun callRemoveeFromMessageListAPI(toUserId: String, position: Int) {
        if (AppUtils.isConnectedToInternet(activity)) {
            AppUtils.showProgressDialog(activity)
            val params = HashMap<String, String?>()

            params["user_id"] = toUserId
            Log.e("param", params.toString())

            val call: Call<GeneralModel>
            call = RetrofitRestClient.instance!!.removeMessageListAPI(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, params)
            Log.e("token", SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
            call.enqueue(object : Callback<GeneralModel> {
                @SuppressLint("SetTextI18n", "ShowToast")
                override fun onResponse(call: Call<GeneralModel>?, response: Response<GeneralModel>?) {
                    val basicModel: GeneralModel
                    AppUtils.hideProgressDialog()
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {
                            AppUtils.hideProgressDialog()
                            messagelist.removeAt(position)
                            setAdapter()
                            Toast.makeText(activity, basicModel.message, Toast.LENGTH_SHORT).show()
                            callNoData()
                        } else if (Objects.requireNonNull(basicModel).code == "-1") {
                            callLogoutFunction()
                        } else {
                            AppUtils.hideProgressDialog()
                            showSnackBar(activity, basicModel.message)
                        }
                    } else {
                        AppUtils.hideProgressDialog()
                        showSnackBar(activity, response.message())
                    }
                    AppUtils.hideProgressDialog()
                }

                override fun onFailure(call: Call<GeneralModel>?, t: Throwable?) {
                    AppUtils.hideProgressDialog()
                    Log.e("message3", "fail")
                    if (t is SocketTimeoutException) {
                        showSnackBar(activity, getString(R.string.connection_timeout))
                    } else if (t is NetworkErrorException) {
                        showSnackBar(activity, getString(R.string.network_error))
                    } else if (t is AuthenticatorException) {
                        showSnackBar(activity, getString(R.string.authentiation_error))
                    } else {
                        showSnackBar(activity, getString(R.string.something_went_wrong))
                    }
                }
            })
        } else {
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }
    }


    private fun callDialogBlockUserAndAPI(userId: String?, position: Int) {
        val builder1 = AlertDialog.Builder(ContextThemeWrapper(activity, R.style.AlertDialogCustom))
        builder1.setMessage(getString(R.string.are_you_sure_want_to_block))
        builder1.setCancelable(true)

        builder1.setPositiveButton(getString(R.string.yes)) { dialog, id ->
            callBlockUserAPI(userId, position)  //call block user API
            if (messageDialog!!.isShowing) {
                messageDialog!!.dismiss()
            }
            dialog.cancel()
        }

        builder1.setNegativeButton(getString(R.string.no)) { dialog, id -> dialog.cancel() }
        val alert11 = builder1.create()
        alert11.show()
    }


    private fun callReportDialog(userId: String?, position: Int) {
        reportDialog = ReportDialog(activity)
        reportDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        reportDialog!!.show()

        reportDialog!!.tvfake.setOnClickListener(View.OnClickListener {
            reportDialog!!.dismiss()
            callReportUserAPI(getString(R.string.fake_profile), "", userId)
        })
        reportDialog!!.tvSpam.setOnClickListener(View.OnClickListener {
            reportDialog!!.dismiss()
            callReportUserAPI(getString(R.string.spam), "", userId)
        })
        reportDialog!!.tvInappropriateProifle.setOnClickListener(View.OnClickListener {
            reportDialog!!.dismiss()
            callReportUserAPI(getString(R.string.inappropriate_profile), "", userId)
        })

        reportDialog!!.tvOther.setOnClickListener(View.OnClickListener {
            reportDialog!!.dismiss()
            callOtherMessageDialog(userId)

        })
    }

    private fun callOtherMessageDialog(userId: String?) {
        otherMessageDialog = OtherMessageDialog(activity)
        otherMessageDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        otherMessageDialog!!.show()

        otherMessageDialog!!.tvSubmit.setOnClickListener(View.OnClickListener {
            if (otherMessageDialog!!.etProblem.text.toString().isEmpty()) {
                Toast.makeText(activity, getString(R.string.please_enter_reason), Toast.LENGTH_SHORT).show()
            } else {
                callReportUserAPI(getString(R.string.other), otherMessageDialog!!.etProblem.text.toString(), userId)
            }
        })
        otherMessageDialog!!.tvCancel.setOnClickListener(View.OnClickListener {
            otherMessageDialog!!.dismiss()
        })
    }


    private fun callBlockUserAPI(userId: String?, position: Int) {
        if (AppUtils.isConnectedToInternet(activity)) {
            AppUtils.showProgressDialog(activity)
            val params = HashMap<String, String?>()

            params["to_id"] = userId
            Log.e("param", params.toString())

            val call: Call<GeneralModel>
            call = RetrofitRestClient.instance!!.callBlockUser(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, params)
            Log.e("token", SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
            call.enqueue(object : Callback<GeneralModel> {
                @SuppressLint("SetTextI18n", "ShowToast")
                override fun onResponse(call: Call<GeneralModel>?, response: Response<GeneralModel>?) {
                    AppUtils.hideProgressDialog()
                    val basicModel: GeneralModel
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {
                            Toast.makeText(activity, basicModel.message, Toast.LENGTH_SHORT).show()
                            messagelist.removeAt(position)
                            setAdapter()
                            callNoData()
                        } else {
                            showSnackBar(activity, basicModel.message)
                        }
                    } else {
                        showSnackBar(activity, response.message())
                    }
                }

                override fun onFailure(call: Call<GeneralModel>?, t: Throwable?) {
                    AppUtils.hideProgressDialog()
                    if (t is SocketTimeoutException) {
                        showSnackBar(activity, getString(R.string.connection_timeout))
                    } else if (t is NetworkErrorException) {
                        showSnackBar(activity, getString(R.string.network_error))
                    } else if (t is AuthenticatorException) {
                        showSnackBar(activity, getString(R.string.authentiation_error))
                    } else {
                        showSnackBar(activity, getString(R.string.something_went_wrong))
                    }
                }
            })
        } else {
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }
    }

    private fun callReportUserAPI(isFrom: String, reason: String, userId: String?) {
        val builder1 = AlertDialog.Builder(ContextThemeWrapper(activity, R.style.AlertDialogCustom))
        builder1.setMessage(getString(R.string.are_you_sure_want_to_report))
        builder1.setCancelable(true)

        builder1.setPositiveButton(getString(R.string.yes)) { dialog, id ->
            dialog.cancel()
            reportUserAPI(isFrom, reason, userId)  //call block user API
        }
        builder1.setNegativeButton(getString(R.string.no)) { dialog, id -> dialog.cancel() }
        val alert11 = builder1.create()
        alert11.show()
    }

    override fun onResume() {
        super.onResume()
        isfirst = false
        isClickMessage = false
        try {
            databaseHandler = DatabaseChatHandler(getApplicationContext())
        } catch (e: Exception) {

        }

        if (AppUtils.isConnectedToInternet(activity as Context?)) {
            callAPIMessageList("home")
        } else {
        }

    }

    private fun reportUserAPI(from: String, reason: String, userId: String?) {
        if (AppUtils.isConnectedToInternet(activity)) {
            AppUtils.showProgressDialog(activity)
            val params = HashMap<String, String?>()

            params["to_id"] = userId
            params["report_screen"] = from
            params["report"] = reason
            Log.e("param", params.toString())

            val call: Call<GeneralModel>
            call = RetrofitRestClient.instance!!.callReportUser(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!, params)
            Log.e("token", SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
            call.enqueue(object : Callback<GeneralModel> {
                @SuppressLint("SetTextI18n", "ShowToast")
                override fun onResponse(call: Call<GeneralModel>?, response: Response<GeneralModel>?) {
                    AppUtils.hideProgressDialog()
                    val basicModel: GeneralModel
                    if (response!!.isSuccessful) {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {
                            if (from == getString(R.string.other)) {
                                otherMessageDialog!!.dismiss()
                            }
                            Toast.makeText(activity, basicModel.message, Toast.LENGTH_SHORT).show()
                        } else {
                            showSnackBar(activity, basicModel.message)
                        }
                    } else {
                        showSnackBar(activity, response.message())
                    }
                }

                override fun onFailure(call: Call<GeneralModel>?, t: Throwable?) {
                    AppUtils.hideProgressDialog()
                    if (t is SocketTimeoutException) {
                        showSnackBar(activity, getString(R.string.connection_timeout))
                    } else if (t is NetworkErrorException) {
                        showSnackBar(activity, getString(R.string.network_error))
                    } else if (t is AuthenticatorException) {
                        showSnackBar(activity, getString(R.string.authentiation_error))
                    } else {
                        showSnackBar(activity, getString(R.string.something_went_wrong))
                    }
                }
            })
        } else {
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }
    }
}
