package com.hiddle.app.fragment

import android.accounts.AuthenticatorException
import android.accounts.NetworkErrorException
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.renderscript.Allocation
import android.renderscript.Element
import android.renderscript.RenderScript
import android.renderscript.ScriptIntrinsicBlur
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.androidquery.util.Progress
import com.hiddle.app.R
import com.hiddle.app.activity.EditProfileActivity
import com.hiddle.app.activity.SettingActivity
import com.hiddle.app.adapter.InstaAdapter
import com.hiddle.app.adapter.ProfileOriginAdapter
import com.hiddle.app.adapter.SlidingImage_Adapter
import com.hiddle.app.model.InstaModel
import com.hiddle.app.model.ProfileModel.ProfileResponse
import com.hiddle.app.model.UserModel.UserImageModel
import com.hiddle.app.retrofit.RetrofitRestClient
import com.hiddle.app.util.AppUtils
import com.hiddle.app.util.SharedPreferenceManager
import com.hiddle.app.util.VerticalViewPager
import com.hiddle.app.verticaldotindicator.DotsIndicatorVertical
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_edit_profile.*
import kotlinx.android.synthetic.main.activity_profile_image.*
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.layout_social_media.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketTimeoutException
import java.util.*
import kotlin.collections.ArrayList

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ProfileFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ProfileFragment : BaseFragment(), View.OnClickListener {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    var rvOriginProfile: RecyclerView? = null
    var ivEdit: AppCompatImageView? = null
    var ivSetting: RelativeLayout? = null
    var llFb: LinearLayout? = null
    var vpProfile: VerticalViewPager? = null
    var dots_indicatorVertical: DotsIndicatorVertical? = null
    var tvProfession: AppCompatTextView? = null
    var tvNoData: AppCompatTextView? = null
    var llProfile: LinearLayout? = null
    var tvAbout: AppCompatTextView? = null
    var cicleOverlay: CircleImageView? = null
    var tvName: AppCompatTextView? = null
    var pbProfile: ProgressBar? = null
    var imageProfilelist: ArrayList<UserImageModel> = ArrayList()
    var originProfilelist: ArrayList<String> = ArrayList()
    var profileOriginAdapter: ProfileOriginAdapter? = null
    var address: String = ""
    var paramOrigin: String = ""
    var paramLatitude: String = ""
    var paramLongitude: String = ""
    var paramName: String = ""
    var paramProfessioon: String = ""
    var paramAge: String = ""
    var paramHeight: String = ""
    var paramGender: String = ""
    var baseUrl: String = ""
    var isFirstTime: Boolean = false
    var geocoder: Geocoder? = null
    var addresses: List<Address> = ArrayList()
    var paramFbConnect: String = "0"
    var paramInstaConnect: String = "0"

    var instaMedialist: ArrayList<String> = ArrayList()
    var instalist: ArrayList<InstaModel> = ArrayList()

    var fbMedialist: ArrayList<String> = ArrayList()
    var fblist: ArrayList<InstaModel> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view: View = inflater.inflate(R.layout.fragment_profile, container, false)

        rvOriginProfile = view.findViewById(R.id.rvOriginProfile) as RecyclerView
        vpProfile = view.findViewById(R.id.vpProfile) as VerticalViewPager
        dots_indicatorVertical = view.findViewById(R.id.dots_indicator) as DotsIndicatorVertical
        ivEdit = view.findViewById(R.id.ivEdit) as AppCompatImageView
        ivSetting = view.findViewById(R.id.ivSetting) as RelativeLayout
        llFb = view.findViewById(R.id.llFb) as LinearLayout

        tvProfession = view.findViewById(R.id.tvProfession) as AppCompatTextView
        tvAbout = view.findViewById(R.id.tvAbout) as AppCompatTextView
        tvNoData = view.findViewById(R.id.tvNoData) as AppCompatTextView
        llProfile = view.findViewById(R.id.llProfile) as LinearLayout
        tvName = view.findViewById(R.id.tvName) as AppCompatTextView
        cicleOverlay = view.findViewById(R.id.cicleOverlay) as CircleImageView
        pbProfile = view.findViewById(R.id.pbProfile) as ProgressBar

        allClicklistener()
        setBlurBackground()

        return view
    }

    private fun setBlurBackground() {
        var final_Bitmap: Bitmap? = null
        final_Bitmap = BitmapFactory.decodeResource(activity!!.resources, R.drawable.ic_placeholder_icn);
        final_Bitmap = BlurImage(final_Bitmap);
        cicleOverlay!!.setImageBitmap(final_Bitmap)
    }

    private fun allClicklistener() {
        ivEdit!!.setOnClickListener(this)
        ivSetting!!.setOnClickListener(this)

    }

    @SuppressLint("NewApi")
    fun BlurImage(input: Bitmap): Bitmap {
        try {
            val rsScript = RenderScript.create(context)
            val alloc = Allocation.createFromBitmap(rsScript, input)
            val blur = ScriptIntrinsicBlur.create(rsScript, Element.U8_4(rsScript))
            blur.setRadius(21F)
            blur.setInput(alloc)
            val result = Bitmap.createBitmap(input.width, input.height, Bitmap.Config.ARGB_8888)
            val outAlloc = Allocation.createFromBitmap(rsScript, result)
            blur.forEach(outAlloc)
            outAlloc.copyTo(result)
            rsScript.destroy()
            return result
        } catch (e: Exception) {
            // TODO: handle exception
            return input
        }
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.ivEdit -> {
                ivEdit!!.isEnabled = false
                val i = Intent(activity, EditProfileActivity::class.java)
                i.putExtra("paramProfileName", paramName)
                i.putExtra("paramProfileHeight", paramHeight)
                i.putExtra("paramProfileAge", paramAge)
                i.putExtra("paramProfileGender", paramGender)
                i.putExtra("paramProfileLatitude", paramLatitude)
                i.putExtra("paramProfileLongitude", paramLongitude)
                i.putExtra("paramProfileLocation", tvLocation.text.toString())
                i.putExtra("paramProfileProfession", paramProfessioon)
                i.putExtra("imageProfilelist", imageProfilelist)
                i.putExtra("paramBaseUrl", baseUrl)
                i.putStringArrayListExtra("originProfilelist", originProfilelist)
                i.putExtra("paramInstaCoonect", paramInstaConnect)
                i.putExtra("paramFbConnect", paramFbConnect)
                i.putStringArrayListExtra("fblist", fbMedialist)
                i.putStringArrayListExtra("instalist", instaMedialist)
                startActivity(i)
                AppUtils.overridePendingTransitionEnter(activity)
            }
            R.id.ivSetting -> {
                ivSetting!!.isEnabled = false
                val i = Intent(activity, SettingActivity::class.java)
                startActivity(i)
                AppUtils.overridePendingTransitionEnter(activity)
            }
        }
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ProfileFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                ProfileFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }


    override fun setUserVisibleHint(visible: Boolean) {
        super.setUserVisibleHint(visible)
        if (visible && isResumed()) { //Only manually call onResume if fragment is already visible
            //Otherwise allow natural fragment lifecycle to call onResume
            onResume()
        }
    }

    override fun onResume() {
        super.onResume()
        Log.e("param", param1.toString())
        if (userVisibleHint)//when user see this fragment only when API call
        {
            ivEdit!!.isEnabled = true
            ivSetting!!.isEnabled = true
            callProfileAPI()  //call profile API
            return
        }
    }


    private fun callProfileAPI() {
        if (AppUtils.isConnectedToInternet(activity)) {
            ivEdit!!.isEnabled = false
            pbProfile!!.visibility = View.VISIBLE

            val call: Call<ProfileResponse>
            call = RetrofitRestClient.instance!!.getProfile(SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
            Log.e("token_123", SharedPreferenceManager.getMySharedPreferences()!!.getAuthorizationToken()!!)
            call.enqueue(object : Callback<ProfileResponse> {
                @SuppressLint("SetTextI18n")
                override fun onResponse(call: Call<ProfileResponse>?, response: Response<ProfileResponse>?) {
                    pbProfile!!.visibility = View.GONE
                    //AppUtils.hideProgressDialog()
                    val basicModel: ProfileResponse
                    if (response!!.isSuccessful)
                    {
                        basicModel = response.body()!!
                        if (Objects.requireNonNull(basicModel).code == "1") {
                            AppUtils.hideProgressDialog()

                            isFirstTime = true
                            llProfile!!.visibility = View.VISIBLE
                            tvNoData!!.visibility = View.GONE

                            paramName = basicModel.data!!.username!!
                            SharedPreferenceManager.getMySharedPreferences()!!.setUserName(paramName)
                            SharedPreferenceManager.getMySharedPreferences()!!.setCountryCode(basicModel.data!!.cc!!)
                            try {
                                if (basicModel.data.education!! != null) {
                                    paramProfessioon = basicModel.data.education!!
                                }
                                paramAge = basicModel.data.age!!
                                paramHeight = basicModel.data.height!!
                                paramGender = basicModel.data.gender!!
                                if (paramGender == "Woman") {
                                    paramGender = getString(R.string.woman)
                                } else {
                                    paramGender = getString(R.string.man)
                                }
                            } catch (e: Exception) {
                            }



                            baseUrl = basicModel.data.baseUrl!!

                            if (basicModel.data.images!!.size > 0) {
                                imageProfilelist = basicModel.data.images!! as ArrayList<UserImageModel>
                            }

                            if (paramName != null) {
                                tvName!!.text = paramName
                            }

                            if (paramProfessioon != null) {
                                if (paramProfessioon == "null" || paramProfessioon.equals("")) {
                                    tvProfession!!.visibility = View.GONE
                                } else {
                                    tvProfession!!.text = paramProfessioon
                                    tvProfession!!.visibility = View.VISIBLE
                                }
                            } else {
                                tvProfession!!.visibility = View.GONE
                            }

                            try {
                                if(basicModel.data.location!!.size != 0)
                                if (basicModel.data.location!![0].toString() != null) {
                                    paramLatitude = basicModel.data.location!![0].toString()
                                    paramLongitude = basicModel.data.location!![1].toString()
                                }

                                tvAbout!!.text = paramAge + " " + context!!.getString(R.string.years_old) + " " + paramHeight + " " + context!!.getString(R.string.meter) + " , " + paramGender

                                if (basicModel.data.address != "") {
                                    tvLocation.text = getAddress(context, paramLatitude.toDouble(), paramLongitude.toDouble())
                                    tvLocation.text = getAddress(context, paramLatitude.toDouble(), paramLongitude.toDouble())
                                //  tvLocation.text = basicModel.data.address
                                } else {
                                    tvLocation.text = basicModel.data.address
                                }
                            } catch (e: Exception) {

                            }


                            if (imageProfilelist.size > 0) {
                                SharedPreferenceManager.getMySharedPreferences()!!.setProfileImage(baseUrl + imageProfilelist[0].image)
                            }

                            try {
                                if (imageProfilelist.size > 0) {
                                    vpProfile!!.adapter = SlidingImage_Adapter(activity!!, imageProfilelist, baseUrl)
                                    dots_indicatorVertical!!.setViewPager(vpProfile!!)
                                    dots_indicatorVertical!!.setViewPager(vpProfile!!)
                                }
                            } catch (e: Exception) {
                            }
                           /* paramOrigin = basicModel.data.origins!!
                            Log.e("origin", paramOrigin)
                            if (paramOrigin != null) {
                                if (paramOrigin != "null") {

                                    val lstValues: List<String> = paramOrigin.split(",").map { it -> it.trim() }
                                    lstValues.forEach { it ->
                                        Log.i("Values", "value=$it")
                                        //Do Something
                                    }
                                    originProfilelist = lstValues as ArrayList<String>
                                    rvOriginProfile!!.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false) //set adapter
                                    profileOriginAdapter = ProfileOriginAdapter(activity, originProfilelist)
                                    rvOriginProfile!!.adapter = profileOriginAdapter
                                }
                            }*/
                            originProfilelist.clear()
                            originProfilelist.addAll(basicModel.data.origins!!)
                            rvOriginProfile!!.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false) //set adapter
                            profileOriginAdapter = ProfileOriginAdapter(activity, originProfilelist)
                            rvOriginProfile!!.adapter = profileOriginAdapter

                            if (basicModel.data.isFbConnect != null) {
                                paramFbConnect = basicModel.data.isFbConnect!!
                                paramInstaConnect = basicModel.data.isInstaConnect!!

                                fbMedialist.clear()
                                instaMedialist.clear()

                                fbMedialist = basicModel.data.fbJsonArray as ArrayList<String>
                                instaMedialist = basicModel.data.instaJsonArray as ArrayList<String>
                            }

                            setDataForSocialMedia()


                            llProfile!!.visibility = View.VISIBLE
                            pbProfile!!.visibility = View.GONE
                            ivEdit!!.isEnabled = true
                            //AppUtils.hideProgressDialog()
                        } else if (Objects.requireNonNull(basicModel).code == "-1") {
                            ivEdit!!.isEnabled = true
                            getNoDataLayout()
                            AppUtils.hideProgressDialog()
                            callLogoutFunction()
                        } else {
                            ivEdit!!.isEnabled = true
                            pbProfile!!.visibility = View.GONE
                            // AppUtils.hideProgressDialog()
                            getNoDataLayout()
                            showSnackBar(activity, basicModel.message)
                        }
                    } else {
                        ivEdit!!.isEnabled = true
                        pbProfile!!.visibility = View.GONE
                        //AppUtils.hideProgressDialog()
                        getNoDataLayout()
                        showSnackBar(activity, response.message())
                    }
                }

                override fun onFailure(call: Call<ProfileResponse>?, t: Throwable?) {
                    ivEdit!!.isEnabled = true
                    getNoDataLayout()
                    pbProfile!!.visibility = View.GONE
                    //AppUtils.hideProgressDialog()
                    try {
                        getNoDataLayout()
                        if (t is SocketTimeoutException) {
                            showSnackBar(activity, getString(R.string.connection_timeout))
                        } else if (t is NetworkErrorException) {
                            showSnackBar(activity, getString(R.string.network_error))
                        } else if (t is AuthenticatorException) {
                            showSnackBar(activity, getString(R.string.authentiation_error))
                        } else {
                            showSnackBar(activity, getString(R.string.something_went_wrong))
                        }
                    } catch (e: Exception) {
                    }

                }
            })
        } else {
            ivEdit!!.isEnabled = true
            pbProfile!!.visibility = View.GONE
            //AppUtils.hideProgressDialog()
            getNoDataLayout()
            AppUtils.showToast(activity, getString(R.string.no_internet_connection))
        }
    }

    private fun setDataForSocialMedia() {
        if (paramFbConnect == "1") {
            showAllFbMedia()
        } else {
            llFb!!.visibility = View.GONE
        }

        if (paramInstaConnect == "1") {
            showAllInstaMedia()

        } else {
            llInsta.visibility = View.GONE
        }

    }

    private fun getNoDataLayout() {
        tvNoData!!.visibility = View.VISIBLE
        llProfile!!.visibility = View.GONE
    }

    fun getAddress(context: Context?, LATITUDE: Double, LONGITUDE: Double): String? {
        try {
            geocoder = Geocoder(activity, Locale.ENGLISH)
            addresses = geocoder!!.getFromLocation(LATITUDE, LONGITUDE, 1) // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            Log.e("Address", addresses[0].toString())

            if (addresses.isNotEmpty()) {

                if (addresses[0].locality != null) {
                    if (addresses[0].locality == addresses[0].adminArea) {
                        address = addresses[0].adminArea + ", " + addresses[0].countryName
                    } else {
                        address = addresses[0].locality + ", " + addresses[0].adminArea + ", " + addresses[0].countryName
                    }
                } else {
                    address = addresses[0].adminArea + ", " + addresses[0].countryName
                }
                /* if (addresses[0].locality != null) {
                     address = addresses[0].locality
                 }
                 if (addresses[0].adminArea != null) {
                     address = address + "," + addresses[0].adminArea
                 }
                 if (addresses[0].countryName != null) {
                     address = address + "," + addresses[0].countryName
                 }*/
                Log.e("Name", address)
            }

            /* if (addresses.isNotEmpty()) {

                 if (addresses[0].locality != null) {
                     address = addresses[0].locality
                 }
                 if (addresses[0].adminArea != null) {
                     address = address + "," + addresses[0].adminArea
                 }
                 if (addresses[0].countryName != null) {
                     address = address + "," + addresses[0].countryName
                 }
                 Log.e("Name", address)

             }*/

            if (address == "") {
                if (addresses[0].getAddressLine(0) != null) {
                    address = addresses[0].getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                }
            }

        } catch (e: Exception) {
        }
        return address

    }

    private fun showAllFbMedia() {
        fblist.clear()
        val G: Int = 6;
        val NG: Int = (fbMedialist.size + G - 1) / G;
        val chunks: List<List<String>> = fbMedialist.chunked(6)
        Log.e("chunk", chunks.toString())
        Log.e("chunk", chunks.size.toString())

        for (i in chunks.indices) {
            var instaModel: InstaModel = InstaModel()
            instaModel.path = ""
            instaModel.list = chunks[i]
            fblist.add(instaModel)
            Log.e("main_size", fblist[i].list.toString())
            AppUtils.hideProgressDialog()
        }

        vpFb!!.adapter = InstaAdapter(activity!!, fblist)
        dots_indicator_fb.setViewPager(vpFb)

        if (fblist.size > 0) {
            llFb!!.visibility = View.VISIBLE
        } else {
            llFb!!.visibility = View.GONE
        }


    }

    private fun showAllInstaMedia() {
        instalist.clear()

        val G: Int = 6;
        val NG: Int = (instaMedialist.size + G - 1) / G;
        val chunks: List<List<String>> = instaMedialist.chunked(6)
        Log.e("chunk", chunks.toString())
        Log.e("chunk", chunks.size.toString())

        for (i in chunks.indices) {
            var instaModel: InstaModel = InstaModel()
            instaModel.path = ""
            instaModel.list = chunks[i]
            instalist.add(instaModel)
            Log.e("main_size", instalist[i].list.toString())
            AppUtils.hideProgressDialog()
        }

        if (instalist.size > 0) {
            llInsta.visibility = View.VISIBLE
        } else {
            llInsta.visibility = View.GONE
        }


        vpInsta!!.adapter = InstaAdapter(activity!!, instalist)
        dots_indicator_insta.setViewPager(vpInsta)
        AppUtils.hideProgressDialog()
    }
}
