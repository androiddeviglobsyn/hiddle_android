package com.hiddle.app.model.FilterModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import com.hiddle.app.model.PersonModel.MapFilter
import com.hiddle.app.model.UserModel.UserImageModel


class
FilterDataResponse {

    @SerializedName("location")
    @Expose
    val location: List<Double>? = null
   /* @SerializedName("location")
    @Expose
    val location: HomeLocation? = null*/
    @SerializedName("notification")
    @Expose
    val notification: HomeNotification? = null
    @SerializedName("preferences")
    @Expose
    val preferences: HomePreference? = null
    @SerializedName("filter")
    @Expose
    val filter: HomeFilter? = null
    @SerializedName("map_filter")
    @Expose
    val mapFilter: MapFilter? = null
    @SerializedName("social_id")
    @Expose
    val socialId: String? = null
    @SerializedName("insta_id")
    @Expose
    val instaId: String? = null
    @SerializedName("apple_id")
    @Expose
    val appleId: String? = null
    @SerializedName("device_type")
    @Expose
    val deviceType: String? = null
    @SerializedName("device_token")
    @Expose
    val deviceToken: String? = null
    @SerializedName("email")
    @Expose
    val email: String? = null
    @SerializedName("images")
    @Expose
    val images: List<UserImageModel>? = null
    @SerializedName("base_url")
    @Expose
    val baseUrl: String? = null

    @SerializedName("origins")
    @Expose
    val origins: List<String>? = null
   /* @SerializedName("origins")
    @Expose
    val origins: String? = null*/
    @SerializedName("education")
    @Expose
    val education: String? = null
    @SerializedName("address")
    @Expose
    val address: String? = null
    @SerializedName("is_verify")
    @Expose
    val isVerify: String? = null
    @SerializedName("is_online")
    @Expose
    val isOnline: String? = null
    @SerializedName("_id")
    @Expose
    val id: String? = null
    @SerializedName("cc")
    @Expose
    val cc: String? = null
    @SerializedName("number")
    @Expose
    val number: String? = null
    @SerializedName("tokens")
    @Expose
    val tokens: List<HomeToken>? = null
    @SerializedName("createdAt")
    @Expose
    val createdAt: String? = null
    @SerializedName("updatedAt")
    @Expose
    val updatedAt: String? = null
    @SerializedName("__v")
    @Expose
    val v: String? = null
    @SerializedName("age")
    @Expose
    val age: String? = null
    @SerializedName("gender")
    @Expose
    val gender: String? = null
    @SerializedName("height")
    @Expose
    val height: String? = null
    @SerializedName("username")
    @Expose
    val username: String? = null
    @SerializedName("distance")
    @Expose
    val distance: String? = null
    @SerializedName("is_cross")
    @Expose
    val isCross: String? = null
    @SerializedName("c_latitude")
    @Expose
    val cLatitude: String? = null
    @SerializedName("c_longitude")
    @Expose
    val cLongitude: String? = null
}




