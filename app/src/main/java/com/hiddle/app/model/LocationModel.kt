package com.hiddle.app.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LocationModel() {
    @SerializedName("id")
    @Expose
    var id: String? = null
    @SerializedName("name")
    @Expose
    var name: String? = null

    @SerializedName("lat")
    @Expose
    var lat: String? = null
    @SerializedName("lang")
    @Expose
    var lang: String? = null

    @SerializedName("is_selected")
    @Expose
    var isSelected: Boolean? = null
}