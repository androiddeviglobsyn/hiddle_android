package com.hiddle.app.model.ProfileModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class ProfileNotification {
    @SerializedName("new_strike")
    @Expose
    public val newStrike: String? = null

    @SerializedName("new_message")
    @Expose
    public val newMessage: String? = null

    @SerializedName("likes")
    @Expose
    public val likes: String? = null

    @SerializedName("sounds")
    @Expose
    public val sounds: String? = null

    @SerializedName("vibration")
    @Expose
    public val vibration: String? = null
}