package com.hiddle.app.model.VerifyCodeModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class VerifyToken {
    @SerializedName("_id")
    @Expose
    val id: String? = null

    @SerializedName("token")
    @Expose
    val token: String? = null
}