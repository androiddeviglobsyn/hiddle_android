package com.hiddle.app.model.PersonModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class PersonToken {
    @SerializedName("_id")
    @Expose
     val id: String? = null
    @SerializedName("token")
    @Expose
     val token: String? = null

}