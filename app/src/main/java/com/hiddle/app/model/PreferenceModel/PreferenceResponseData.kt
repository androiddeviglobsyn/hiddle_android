package com.hiddle.app.model.PreferenceModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class PreferenceResponseData {
    @SerializedName("language")
    @Expose
     val language: String? = null
    @SerializedName("distance_in")
    @Expose
     val distanceIn: String? = null
    @SerializedName("show_age")
    @Expose
     val showAge: String? = null
    @SerializedName("share_location")
    @Expose
     val shareLocation: String? = null
}