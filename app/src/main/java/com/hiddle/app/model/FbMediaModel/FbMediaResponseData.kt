package com.hiddle.app.model.FbMediaModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class FbMediaResponseData {
    @SerializedName("images")
    @Expose
     val images: List<FbMediaImage>? = null

    @SerializedName("id")
    @Expose
     val id: String? = null
}