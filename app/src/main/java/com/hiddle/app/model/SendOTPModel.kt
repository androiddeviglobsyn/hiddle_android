package com.hiddle.app.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class SendOTPModel {
    @SerializedName("code")
    @Expose
     val code: String? = null
    @SerializedName("message")
    @Expose
     val message: String? = null
    @SerializedName("data")
    @Expose
     val data: Data? = null


}

class Data {

    @SerializedName("otp")
    @Expose
     val otp: String? = null
    @SerializedName("otp_limit")
    @Expose
     val otpLimit: String? = null


}
