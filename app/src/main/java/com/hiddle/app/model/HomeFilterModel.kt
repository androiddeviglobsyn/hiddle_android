package com.hiddle.app.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

import com.hiddle.app.model.FilterModel.FLocation




class HomeFilterModel {
    @SerializedName("f_location")
    @Expose
    var fLocation: List<FLocation>? = null
    @SerializedName("user_id")
    @Expose
    var userId: String? = null
    @SerializedName("max_age")
    @Expose
    var maxAge: String? = null
    @SerializedName("min_height")
    @Expose
    var minHeight: String? = null
    @SerializedName("f_origin")
    @Expose
    var fOrigin: String? = null
    @SerializedName("f_distance")
    @Expose
    var fDistance: String? = null
    @SerializedName("max_height")
    @Expose
    var maxHeight: String? = null
    @SerializedName("min_age")
    @Expose
    var minAge: String? = null
    @SerializedName("f_show_me")
    @Expose
    var fShowMe: String? = null
}