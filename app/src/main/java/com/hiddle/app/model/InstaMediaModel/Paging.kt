package com.hiddle.app.model.InstaMediaModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class Paging {
    @SerializedName("cursors")
    @Expose
     val cursors: Cursors? = null

    @SerializedName("previous")
    @Expose
     val previous: String? = null
}