package com.hiddle.app.model.PlaceSearchModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Result {

    @SerializedName("formatted_address")
    @Expose
    val formattedAddress: String? = null

    @SerializedName("geometry")
    @Expose
    public val geometry: Geometry? = null

    @SerializedName("icon")
    @Expose
    public val icon: String? = null

    @SerializedName("id")
    @Expose
    public val id: String? = null

    @SerializedName("name")
    @Expose
    val name: String? = null

    @SerializedName("opening_hours")
    @Expose
    public val openingHours: OpeningHours? = null

    @SerializedName("photos")
    @Expose
    public val photos: List<Photo>? = null

    @SerializedName("place_id")
    @Expose
    public val placeId: String? = null

    @SerializedName("plus_code")
    @Expose
    public val plusCode: PlusCode? = null

    @SerializedName("rating")
    @Expose
    public val rating: Double? = null

    @SerializedName("reference")
    @Expose
    public val reference: String? = null

    @SerializedName("types")
    @Expose
    public val types: List<String>? = null

    @SerializedName("user_ratings_total")
    @Expose
    public val userRatingsTotal: Int? = null



}