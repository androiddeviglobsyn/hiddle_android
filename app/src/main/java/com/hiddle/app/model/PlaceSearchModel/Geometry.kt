package com.hiddle.app.model.PlaceSearchModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Geometry {

    @SerializedName("location")
    @Expose
    public var location: Location? = null

    @SerializedName("viewport")
    @Expose
    public var viewport: Viewport? = null



}
