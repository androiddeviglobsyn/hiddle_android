package com.hiddle.app.model.UserModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import com.hiddle.app.model.PersonModel.MapFilter


public class UserDataResponse {
    @SerializedName("images")
    @Expose
    val images: List<UserImageModel>? = null

    @SerializedName("base_url")
    @Expose
    val baseUrl: String? = null

    @SerializedName("_id")
    @Expose
    val id: String? = null

    @SerializedName("username")
    @Expose
    val username: String? = null


    @SerializedName("distance")
    @Expose
    val distance: String= "0.0"

    @SerializedName("_isLike")
    @Expose
    var isLike: String = ""

    /* @SerializedName("location")
     @Expose
      val location: Location? = null

     @SerializedName("notification")
     @Expose
      val notification: Notification? = null

     @SerializedName("preferences")
     @Expose
      val preferences: Preferences? = null

     @SerializedName("filter")
     @Expose
      val filter: Filter? = null*/
/*

    @SerializedName("map_filter")
    @Expose
     val mapFilter: MapFilter? = null

    @SerializedName("social_id")
    @Expose
     val socialId: String? = null

    @SerializedName("insta_id")
    @Expose
     val instaId: String? = null

    @SerializedName("apple_id")
    @Expose
     val appleId: String? = null

    @SerializedName("device_type")
    @Expose
     val deviceType: String? = null

    @SerializedName("device_token")
    @Expose
     val deviceToken: String? = null

    @SerializedName("email")
    @Expose
     val email: String? = null
*/



    /*@SerializedName("origins")
    @Expose
     val origins: String? = null

    @SerializedName("education")
    @Expose
     val education: String? = null

    @SerializedName("address")
    @Expose
     val address: String? = null

    @SerializedName("is_fb_connect")
    @Expose
     val isFbConnect: Int? = null

    @SerializedName("is_insta_connect")
    @Expose
     val isInstaConnect: Int? = null*/

    /*@SerializedName("insta_json_array")
    @Expose
     val instaJsonArray: List<Any>? = null

    @SerializedName("fb_json_array")
    @Expose
     val fbJsonArray: List<Any>? = null*/

    /*  @SerializedName("is_verify")
      @Expose
       val isVerify: Int? = null

      @SerializedName("is_delete")
      @Expose
       val isDelete: Int? = null

      @SerializedName("is_ban")
      @Expose
       val isBan: Boolean? = null

      @SerializedName("report_count")
      @Expose
       val reportCount: Int? = null*/

    /* @SerializedName("skipped_user_array")
     @Expose
      val skippedUserArray: List<Any>? = null*/

    /*  @SerializedName("is_online")
      @Expose
       val isOnline: Int? = null*/



    /* @SerializedName("cc")
     @Expose
      val cc: String? = null

     @SerializedName("number")
     @Expose
      val number: Int? = null*/

    /* @SerializedName("tokens")
     @Expose
      val tokens: List<Token>? = null*/

    /*@SerializedName("createdAt")
    @Expose
     val createdAt: String? = null

    @SerializedName("updatedAt")
    @Expose
     val updatedAt: String? = null

    @SerializedName("__v")
    @Expose
     val v: Int? = null

    @SerializedName("age")
    @Expose
     val age: Int? = null

    @SerializedName("gender")
    @Expose
     val gender: String? = null

    @SerializedName("height")
    @Expose
     val height: Double? = null*/



   /* @SerializedName("distance")
    @Expose
    val distance: String = "0.0"*/

    /* @SerializedName("fb_insta_array")
     @Expose
      val fbInstaArray: List<Any>? = null*/

    /* @SerializedName("location")
     @Expose
     val location: UserLocation? = null

     @SerializedName("notification")
     @Expose
     val notification: UserNotification? = null

     @SerializedName("preferences")
     @Expose
     val preferences: UserPreferences? = null

     @SerializedName("filter")
     @Expose
     val filter: UserFilter? = null*/

    /*@SerializedName("images")
    @Expose
    val images: List<String>? = null*/

    /*  @SerializedName("origins")
      @Expose
      val origins: String? = null

      @SerializedName("is_verify")
      @Expose
      val isVerify: String? = null*/

    /*@SerializedName("base_url")
    @Expose
    val baseUrl: String? = null

    @SerializedName("_id")
    @Expose
    val id: String? = null

    @SerializedName("distance")
    @Expose
    val distance: String= "0.0"*/



    /* @SerializedName("number")
     @Expose
     val number: String? = null

     @SerializedName("cc")
     @Expose
     val cc: String? = null

     @SerializedName("tokens")
     @Expose
     val tokens: List<UserToken>? = null*/
/*
    @SerializedName("createdAt")
    @Expose
    val createdAt: String? = null

    @SerializedName("updatedAt")
    @Expose
    val updatedAt: String? = null

    @SerializedName("__v")
    @Expose
    val v: String? = null*/
/*
    @SerializedName("address")
    @Expose
    val address: String? = null

    @SerializedName("age")
    @Expose
    val age: String? = null

    @SerializedName("education")
    @Expose
    val education: String? = null

    @SerializedName("email")
    @Expose
    val email: String? = null*/

    /*@SerializedName("gender")
    @Expose
    val gender: String? = null

    @SerializedName("height")
    @Expose
    val height: String? = null

    @SerializedName("insta_id")
    @Expose
    val instaId: String? = null

    @SerializedName("social_id")
    @Expose
    val socialId: String? = null*/

    /* @SerializedName("username")
     @Expose
     val username: String? = null*/


}