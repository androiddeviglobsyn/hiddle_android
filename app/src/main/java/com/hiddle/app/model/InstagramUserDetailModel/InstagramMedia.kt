package com.hiddle.app.model.InstagramUserDetailModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class InstagramMedia {
    @SerializedName("data")
    @Expose
    private val data: List<MediaDatum>? = null

    @SerializedName("paging")
    @Expose
    private val paging: InstagramPaging? = null
}