package com.hiddle.app.model.VerifyCodeModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class VerifyModelResponse {

    @SerializedName("code")
    @Expose
     val code: String? = null

    @SerializedName("message")
    @Expose
    val message: String? = null

    @SerializedName("data")
    @Expose
    val data: VerifyResponseData? = null



















}