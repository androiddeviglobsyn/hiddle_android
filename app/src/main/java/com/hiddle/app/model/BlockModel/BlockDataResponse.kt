package com.hiddle.app.model.BlockModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.hiddle.app.model.UserModel.UserImageModel

class BlockDataResponse {
    @SerializedName("_id")
    @Expose
     val _id: String? = null

    @SerializedName("from_id")
    @Expose
    val from_id: String? = null

    @SerializedName("toId")
    @Expose
    val toId: String? = null

    @SerializedName("name")
    @Expose
     val name: String? = null



    @SerializedName("profile")
    @Expose
    val profile: List<UserImageModel>? = null



    @SerializedName("baseUrl")
    @Expose
     val baseUrl: String? = null

    @SerializedName("status")
    @Expose
    val status: String? = null
}