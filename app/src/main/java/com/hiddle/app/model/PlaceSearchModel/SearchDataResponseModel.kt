package com.hiddle.app.model.PlaceSearchModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SearchDataResponseModel {
    @SerializedName("html_attributions")
    @Expose
    public val htmlAttributions: List<Any>? = null

    @SerializedName("results")
    @Expose
    public val results: List<Result>? = null

    @SerializedName("status")
    @Expose
   public val status: String? = null
}