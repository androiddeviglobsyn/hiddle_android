package com.hiddle.app.model.UserModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class UserLocation {
    @SerializedName("latitude")
    @Expose
     val latitude: String? = null

    @SerializedName("longitude")
    @Expose
     val longitude: String? = null
}