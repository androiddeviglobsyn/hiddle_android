package com.hiddle.app.model.VerifyCodeModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class VerifyImageModel {
    @SerializedName("_id")
    @Expose
     val id: String? = null

    @SerializedName("image")
    @Expose
     val image: String? = null

    @SerializedName("serial_number")
    @Expose
     val serialNumber: Int? = null

    @SerializedName("is_selected")
    @Expose
     val isSelected: Boolean? = null
}