package com.hiddle.app.model.UsermapModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class UsermapDataResponse {
    @SerializedName("user_id")
    @Expose
     val userId: String? = null

    @SerializedName("name")
    @Expose
    val name: String? = null

    @SerializedName("profile")
    @Expose
     val profile: String? = null

    @SerializedName("is_like")
    @Expose
     val isLike: String? = null

    @SerializedName("is_superlike")
    @Expose
     val isSuperlike: String? = null

    @SerializedName("base_url")
    @Expose
     val baseUrl: String? = null

    @SerializedName("distance")
    @Expose
     val distance: String? = null

    @SerializedName("latitude")
    @Expose
     val latitude: String? = null

    @SerializedName("longitude")
    @Expose
     val longitude: String? = null
}