package com.hiddle.app.model.ProfileModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ProfilePreference {

    @SerializedName("language")
    @Expose
    public val language: String? = null

    @SerializedName("distance_in")
    @Expose
    public val distance_in: String? = null


    @SerializedName("show_age")
    @Expose
    public val show_age: String? = null

    @SerializedName("share_location")
    @Expose
    public val share_location: String? = null
    
   
}