package com.hiddle.app.model.PreferenceModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class PreferenceResponse {
    @SerializedName("code")
    @Expose
     val code: String? = null
    @SerializedName("message")
    @Expose
     val message: String? = null
    @SerializedName("data")
    @Expose
     val data: PreferenceResponseData? = null
}