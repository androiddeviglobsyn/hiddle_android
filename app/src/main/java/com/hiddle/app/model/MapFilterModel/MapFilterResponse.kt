package com.hiddle.app.model.MapFilterModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class MapFilterResponse {
    @SerializedName("code")
    @Expose
     val code: String? = null
    @SerializedName("message")
    @Expose
     val message: String? = null
    @SerializedName("data")
    @Expose
     val data: MapFilterDataResponse? = null
}