package com.hiddle.app.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LocationJsonModel {

    @SerializedName("latitude")
    @Expose
    var latitude: String? = null

    @SerializedName("longitude")
    @Expose
    var longitude: String? = null

    @SerializedName("address")
    @Expose
    var address: String? = null

    @SerializedName("is_selected")
    @Expose
    var is_selected: Boolean? = null

}