package com.hiddle.app.model.ProfileModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ProfileResponse {
    @SerializedName("code")
    @Expose
    public val code: String? = null

    @SerializedName("message")
    @Expose
    public val message: String? = null



    @SerializedName("data")
    @Expose
    public val data: ProfileResponseData? = null


}