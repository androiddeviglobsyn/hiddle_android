package com.hiddle.app.model.UserModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class UserResponse {
    @SerializedName("code")
    @Expose
     val code: String? = null

    @SerializedName("message")
    @Expose
     val message: String? = null

    @SerializedName("data")
    @Expose
     val data: List<UserDataResponse>? = null
}