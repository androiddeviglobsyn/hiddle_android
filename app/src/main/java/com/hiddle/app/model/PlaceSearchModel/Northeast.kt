package com.hiddle.app.model.PlaceSearchModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Northeast {
    @SerializedName("lat")
    @Expose
    private val lat: Double? = null

    @SerializedName("lng")
    @Expose
    private val lng: Double? = null

}