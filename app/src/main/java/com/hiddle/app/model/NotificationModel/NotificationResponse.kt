package com.hiddle.app.model.NotificationModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class NotificationResponse {
    @SerializedName("code")
    @Expose
     val code: String? = null
    @SerializedName("message")
    @Expose
     val message: String? = null
    @SerializedName("data")
    @Expose
     val data: NotificationResponseData? = null
}