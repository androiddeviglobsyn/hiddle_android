package com.hiddle.app.model.ChatMessageModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class ChatDataResponse {
    @SerializedName("count")
    @Expose
    var count: String? = null
    @SerializedName("user_id")
    @Expose
    var userId: String? = null
    @SerializedName("name")
    @Expose
    var name: String? = null

    @SerializedName("profile")
    @Expose
    val profile: ChatProfile? = null



    @SerializedName("base_url")
    @Expose
    var baseUrl: String? = null
    @SerializedName("msg")
    @Expose
    var msg: String? = null
    @SerializedName("status")
    @Expose
    var status: String? = null
    @SerializedName("time")
    @Expose
    var time: String? = null
    @SerializedName("_id")
    @Expose
     val id: String? = null
    @SerializedName("message")
    @Expose
     val message: String? = null
    @SerializedName("is_read")
    @Expose
     val isRead: String? = null
    @SerializedName("is_read_count")
    @Expose
    var isReadCount: String? = null
    @SerializedName("from_id")
    @Expose
     val fromId: String? = null
    @SerializedName("to_id")
    @Expose
     val toId: String? = null
    @SerializedName("createdAt")
    @Expose
     val createdAt: String? = null
    @SerializedName("updatedAt")
    @Expose
     val updatedAt: String? = null
    @SerializedName("from_user_name")
    @Expose
     val fromUserName: String? = null
    @SerializedName("from_user_image")
    @Expose
     val fromUserImage: List<String>? = null
    @SerializedName("from_user_base_url")
    @Expose
     val fromUserBaseUrl: String? = null
    @SerializedName("from_user_is_online")
    @Expose
     val fromUserIsOnline: String? = null
    @SerializedName("to_user_name")
    @Expose
     val toUserName: String? = null
    @SerializedName("to_user_image")
    @Expose
     val toUserImage: List<String>? = null
    @SerializedName("to_user_base_url")
    @Expose
     val toUserBaseUrl: String? = null
    @SerializedName("to_user_is_online")
    @Expose
     val toUserIsOnline: String? = null

}