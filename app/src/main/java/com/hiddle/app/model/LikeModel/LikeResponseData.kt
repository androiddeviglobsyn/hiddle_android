package com.hiddle.app.model.LikeModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class LikeResponseData {

    @SerializedName("from_name")
    @Expose
    val fromName: String? = null
    @SerializedName("from_profile")
    @Expose
    val fromProfile: String? = null
    @SerializedName("from_address")
    @Expose
    val fromAddress: String? = null
    @SerializedName("from_education")
    @Expose
    val fromEducation: String? = null
    @SerializedName("to_name")
    @Expose
    val toName: String? = null
    @SerializedName("to_profile")
    @Expose
    val toProfile: String? = null
    @SerializedName("to_address")
    @Expose
    val toAddress: String? = null
    @SerializedName("to_education")
    @Expose
    val toEducation: String? = null
    @SerializedName("base_url")
    @Expose
    val baseUrl: String? = null
    @SerializedName("match")
    @Expose
    val match: String? = null
    @SerializedName("_id")
    @Expose
    val id: String? = null
    @SerializedName("to_id")
    @Expose
    val toId: String? = null
    @SerializedName("from_id")
    @Expose
    val fromId: String? = null
    @SerializedName("from_action")
    @Expose
    val fromAction: String? = null
    @SerializedName("from_age")
    @Expose
    val fromAge: String? = null
    @SerializedName("to_age")
    @Expose
    val toAge: String? = null
    @SerializedName("createdAt")
    @Expose
    val createdAt: String? = null
    @SerializedName("updatedAt")
    @Expose
    val updatedAt: String? = null
    @SerializedName("__v")
    @Expose
    val v: String? = null

    @SerializedName("last_premium_like_time")
    @Expose
    val last_premium_like_time: String? = null
}