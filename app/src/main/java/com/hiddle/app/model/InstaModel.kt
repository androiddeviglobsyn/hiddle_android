package com.hiddle.app.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class InstaModel {
    @SerializedName("path")
    @Expose
    var path: String? = null

    @SerializedName("list")
    @Expose
    var list: List<String>? = null

}