package com.hiddle.app.model.FilterModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class HomeToken {
    @SerializedName("_id")
    @Expose
     val id: String? = null
    @SerializedName("token")
    @Expose
     val token: String? = null
}