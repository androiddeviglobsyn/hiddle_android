package com.hiddle.app.model.InstaMediaModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class InstaMediaResponse {
    @SerializedName("data")
    @Expose
     val data: List<InstaMediaResponseData>? = null

    @SerializedName("paging")
    @Expose
     val paging: Paging? = null
}