package com.hiddle.app.model.LoginModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class LoginResponse {

    @SerializedName("code")
    @Expose
    val code: String? = null

    @SerializedName("message")
    @Expose
    val message: String? = null

    @SerializedName("is_verify")
    @Expose
     val isVerify: String? = null
}