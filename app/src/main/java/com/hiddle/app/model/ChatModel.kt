package com.hiddle.app.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ChatModel {
    @SerializedName("media_type")
    @Expose
    var media_type: String = ""


    @SerializedName("from_user_id")
    @Expose
    var from_user_id: String = ""


    @SerializedName("to_user_id")
    @Expose
    var to_user_id: String = ""

    @SerializedName("id")
    @Expose
    var id: String = ""

    @SerializedName("data")
    @Expose
    var data: String = ""
}