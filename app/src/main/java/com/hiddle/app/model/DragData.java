package com.hiddle.app.model;


public class DragData {
    public final ImageModel item;
    public final int width;
    public final int height;

    public DragData(ImageModel item, int width, int height) {
        this.item= item;
        this.width = width;
        this.height = height;

    }
}