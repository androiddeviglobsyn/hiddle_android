package com.hiddle.app.model.FilterModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class FilterResponse {

    @SerializedName("code")
    @Expose
     val code: String? = null
    @SerializedName("message")
    @Expose
     val message: String? = null
    @SerializedName("data")
    @Expose
     val data: FilterDataResponse? = null
}