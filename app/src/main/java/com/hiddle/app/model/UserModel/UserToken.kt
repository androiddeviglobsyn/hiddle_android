package com.hiddle.app.model.UserModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class UserToken {
    @SerializedName("_id")
    @Expose
     val id: String? = null

    @SerializedName("token")
    @Expose
     val token: String? = null
}