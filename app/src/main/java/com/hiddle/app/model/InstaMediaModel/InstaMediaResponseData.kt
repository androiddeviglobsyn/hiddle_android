package com.hiddle.app.model.InstaMediaModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class InstaMediaResponseData {
    @SerializedName("id")
    @Expose
    val id: String? = null

    @SerializedName("media_type")
    @Expose
    val mediaType: String? = null

    @SerializedName("media_url")
    @Expose
    val mediaUrl: String? = null
}