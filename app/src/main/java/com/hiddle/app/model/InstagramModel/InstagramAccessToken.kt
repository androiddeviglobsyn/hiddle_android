package com.hiddle.app.model.InstagramModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class InstagramAccessToken {

   @SerializedName("user_id")
    @Expose
    var user_id: String =""
    @SerializedName("access_token")
    @Expose
    var access_token: String = ""



}