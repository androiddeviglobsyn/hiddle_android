package com.hiddle.app.model

class InfoWindowData {
    private val image: String? = null
    private val hotel: String? = null
    private val food: String? = null
    private val transport: String? = null
}