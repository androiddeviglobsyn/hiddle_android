package com.hiddle.app.model.PersonModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class MapFilter {
    @SerializedName("min_age")
    @Expose
     val minAge: String? = null
    @SerializedName("max_age")
    @Expose
     val maxAge: String? = null
    @SerializedName("m_show_me")
    @Expose
     val mShowMe: String? = null
}