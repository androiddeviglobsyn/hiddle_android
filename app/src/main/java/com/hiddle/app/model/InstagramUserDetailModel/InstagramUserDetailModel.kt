package com.hiddle.app.model.InstagramUserDetailModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class InstagramUserDetailModel {

    @SerializedName("id")
    @Expose
    public val id: String? = null

    @SerializedName("media_count")
    @Expose
    public val mediaCount: String? = null

    @SerializedName("media")
    @Expose
    public val media: InstagramMedia? = null

    @SerializedName("username")
    @Expose
    val username: String? = null

    @SerializedName("account_type")
    @Expose
    public val accountType: String? = null
}