package com.hiddle.app.model.VerifyCodeModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class VerifyNotification {
    @SerializedName("new_strike")
    @Expose
    val newStrike: String? = null

    @SerializedName("new_message")
    @Expose
    val newMessage: String? = null

    @SerializedName("likes")
    @Expose
    val likes: String? = null

    @SerializedName("sounds")
    @Expose
    val sounds: String? = null

    @SerializedName("vibration")
    @Expose
    val vibration: String? = null
}