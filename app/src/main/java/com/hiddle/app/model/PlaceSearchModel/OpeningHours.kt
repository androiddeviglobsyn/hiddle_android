package com.hiddle.app.model.PlaceSearchModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class OpeningHours {

    @SerializedName("open_now")
    @Expose
    private var openNow: Boolean? = null

    fun getOpenNow(): Boolean? {
        return openNow
    }

    fun setOpenNow(openNow: Boolean?) {
        this.openNow = openNow
    }

}