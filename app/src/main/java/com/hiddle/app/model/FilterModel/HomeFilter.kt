package com.hiddle.app.model.FilterModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class HomeFilter {
    @SerializedName("f_location")
    @Expose
     val fLocation: List<FLocation>? = null
    @SerializedName("f_distance")
    @Expose
     val fDistance: String? = null
    @SerializedName("f_show_me")
    @Expose
     val fShowMe: String? = null
    @SerializedName("min_age")
    @Expose
     val minAge: String? = null
    @SerializedName("max_age")
    @Expose
     val maxAge: String? = null
    @SerializedName("min_height")
    @Expose
     val minHeight: String? = null
    @SerializedName("max_height")
    @Expose
     val maxHeight: String? = null
    @SerializedName("f_origin")
    @Expose
     val fOrigin: String? = null
}