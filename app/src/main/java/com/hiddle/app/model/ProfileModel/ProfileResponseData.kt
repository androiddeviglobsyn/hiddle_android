package com.hiddle.app.model.ProfileModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import com.hiddle.app.model.UserModel.UserImageModel


class ProfileResponseData {
   /* @SerializedName("location")
    @Expose
    public val location: ProfileLocatin? = null*/

    @SerializedName("location")
    @Expose
    public val location: List<Double>? = null


    @SerializedName("notification")
    @Expose
    public val notification: ProfileNotification? = null

    @SerializedName("preferences")
    @Expose
    public val preferences: ProfilePreference? = null

    @SerializedName("images")
    @Expose
    public val images: List<UserImageModel>? = null

   /* @SerializedName("origins")
    @Expose
    public val origins: String? = null*/

    @SerializedName("origins")
    @Expose
    val origins: List<String>? = null

    @SerializedName("base_url")
    @Expose
    public val baseUrl: String? = null

    @SerializedName("_id")
    @Expose
    public val id: String? = null

    @SerializedName("cc")
    @Expose
    public val cc: String? = null

    @SerializedName("number")
    @Expose
    public val number: String? = null

    @SerializedName("social_id")
    @Expose
    public val socialId: String? = null

    @SerializedName("tokens")
    @Expose
    public val tokens: List<ProfileToken>? = null

    @SerializedName("createdAt")
    @Expose
    public val createdAt: String? = null

    @SerializedName("updatedAt")
    @Expose
    public val updatedAt: String? = null

    @SerializedName("__v")
    @Expose
    public val v: String? = null

    @SerializedName("address")
    @Expose
    public val address: String? = null

    @SerializedName("age")
    @Expose
    public val age: String? = null

    @SerializedName("education")
    @Expose
    public val education: String = ""

    @SerializedName("email")
    @Expose
    public val email: String? = null

    @SerializedName("gender")
    @Expose
    public val gender: String? = null

    @SerializedName("height")
    @Expose
    public val height: String? = null

    @SerializedName("insta_id")
    @Expose
    public val instaId: String? = null

    @SerializedName("username")
    @Expose
    public val username: String? = null

    @SerializedName("is_fb_connect")
    @Expose
    val isFbConnect: String? = null

    @SerializedName("is_insta_connect")
    @Expose
    val isInstaConnect: String? = null

    @SerializedName("insta_json_array")
    @Expose
    val instaJsonArray: List<String>? = null

    @SerializedName("fb_json_array")
    @Expose
    val fbJsonArray: List<String>? = null
}