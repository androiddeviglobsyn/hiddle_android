package com.hiddle.app.model.ProfileModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class ProfileToken {

    @SerializedName("_id")
    @Expose
    public val id: String? = null

    @SerializedName("token")
    @Expose
    public val token: String? = null

}
