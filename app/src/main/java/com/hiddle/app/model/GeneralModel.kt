package com.hiddle.app.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class GeneralModel {
    @SerializedName("code")
    @Expose
    public val code: String? = null

    @SerializedName("message")
    @Expose
    public val message: String? = null
}