package com.hiddle.app.model.InstagramUserDetailModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class MediaDatum {
    @SerializedName("id")
    @Expose
    private var id: String? = null

    fun getId(): String? {
        return id
    }

    fun setId(id: String?) {
        this.id = id
    }
}