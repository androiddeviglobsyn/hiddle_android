package com.hiddle.app.model.StrikeModelClass

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class StrikeResponse {
    @SerializedName("code")
    @Expose
    val code: String? = null
    @SerializedName("message")
    @Expose
    val message: String? = null
    @SerializedName("data")
    @Expose
    val data: List<StrikeResponseData>? = null

}