package com.hiddle.app.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class OriginModel/*(size: String, countryName: String, countryCode: String,flag:Int)*/ {

    @SerializedName("id")
    @Expose
    var id: String? = null
    @SerializedName("countryName")
    @Expose
    var countryName: String? = null
    @SerializedName("countryCode")
    @Expose
    var countryCode: String? = null

  /*  @SerializedName("flag")
    @Expose
    var flag: Int? = null*/
}