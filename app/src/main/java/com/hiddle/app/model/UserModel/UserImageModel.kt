package com.hiddle.app.model.UserModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class UserImageModel : Serializable {
    @SerializedName("_id")
    @Expose
    val id: String? = null

    @SerializedName("image")
    @Expose
    var image: String = ""

    @SerializedName("order_id")
    @Expose
    val orderId: String? = null

    @SerializedName("is_selected")
    @Expose
    val isSelected: Boolean? = null
}