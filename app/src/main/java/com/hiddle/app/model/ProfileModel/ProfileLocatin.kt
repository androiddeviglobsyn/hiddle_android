package com.hiddle.app.model.ProfileModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ProfileLocatin {
    @SerializedName("latitude")
    @Expose
    public var latitude: String? = null

    @SerializedName("longitude")
    @Expose
    public var longitude: String? = null


}