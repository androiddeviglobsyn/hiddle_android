package com.hiddle.app.model.VerifyCodeModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class VerifyPreference {

    @SerializedName("language")
    @Expose
    val language: String? = null

    @SerializedName("distance_in")
    @Expose
    val distance_in: String? = null

    @SerializedName("show_age")
    @Expose
    val show_age: String? = null

    @SerializedName("share_location")
    @Expose
    val share_location: String? = null


}