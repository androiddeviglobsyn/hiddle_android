package com.hiddle.app.model.StrikeModelClass

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class StrikeResponseData {


    @SerializedName("user_id")
    @Expose
     val userId: String? = null
    @SerializedName("name")
    @Expose
     val name: String? = null
    @SerializedName("profile")
    @Expose
     val profile: String? = null
    @SerializedName("age")
    @Expose
     val age: String? = null
    @SerializedName("address")
    @Expose
     val address: String? = null
    @SerializedName("education")
    @Expose
     val education: String? = null
    @SerializedName("base_url")
    @Expose
     val baseUrl: String? = null
    @SerializedName("distance")
    @Expose
     val distance: String? = null
    @SerializedName("is_online")
    @Expose
    val is_online: String? = null


}