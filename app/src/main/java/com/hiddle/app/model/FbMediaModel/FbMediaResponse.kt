package com.hiddle.app.model.FbMediaModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.hiddle.app.model.InstaMediaModel.Paging




class FbMediaResponse {

    @SerializedName("data")
    @Expose
    val data: List<FbMediaResponseData>? = null

    @SerializedName("paging")
    @Expose
     val paging: FbMediaPaging? = null

}