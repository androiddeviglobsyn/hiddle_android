package com.hiddle.app.model.FbMediaModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class FbMediaCursor {
    @SerializedName("before")
    @Expose
    val before: String? = null

    @SerializedName("after")
    @Expose
    val after: String? = null

}