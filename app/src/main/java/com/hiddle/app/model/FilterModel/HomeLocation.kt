package com.hiddle.app.model.FilterModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class HomeLocation {
    @SerializedName("latitude")
    @Expose
     val latitude: String? = null
    @SerializedName("longitude")
    @Expose
     val longitude: String? = null

}