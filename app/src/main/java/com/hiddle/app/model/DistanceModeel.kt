package com.hiddle.app.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DistanceModeel {

    @SerializedName("distance_language")
    @Expose
    var distance_language: String = ""

    @SerializedName("distance_parameter")
    @Expose
    var distance_parameter: String = ""
}