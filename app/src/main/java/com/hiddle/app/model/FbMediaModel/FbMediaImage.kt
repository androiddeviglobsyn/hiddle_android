package com.hiddle.app.model.FbMediaModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class FbMediaImage {
    @SerializedName("height")
    @Expose
     val height: Int? = null

    @SerializedName("source")
    @Expose
     val source: String? = null

    @SerializedName("width")
    @Expose
     val width: Int? = null
}