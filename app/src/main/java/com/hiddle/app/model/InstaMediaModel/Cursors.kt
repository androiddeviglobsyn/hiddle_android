package com.hiddle.app.model.InstaMediaModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class Cursors {
    @SerializedName("before")
    @Expose
     val before: String? = null

    @SerializedName("after")
    @Expose
     val after: String? = null
}