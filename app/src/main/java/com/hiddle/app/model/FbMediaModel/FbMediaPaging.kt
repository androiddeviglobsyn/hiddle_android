package com.hiddle.app.model.FbMediaModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.hiddle.app.model.InstaMediaModel.Cursors


class FbMediaPaging {
    @SerializedName("cursors")
    @Expose
     val cursors: FbMediaCursor? = null
}