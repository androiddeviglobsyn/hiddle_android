package com.hiddle.app.model;

public class DragDataPicture {

    public final String item;
    public final int width;
    public final int height;

    public DragDataPicture(String item, int width, int height) {
        this.item= item;
        this.width = width;
        this.height = height;

    }
}
