package com.hiddle.app.model.BlockModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class BlockResponse {
    @SerializedName("code")
    @Expose
     val code: String? = null
    @SerializedName("message")
    @Expose
     val message: String? = null
    @SerializedName("data")
    @Expose
     val data: List<BlockDataResponse>? = null
}