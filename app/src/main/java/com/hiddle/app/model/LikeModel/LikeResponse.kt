package com.hiddle.app.model.LikeModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class LikeResponse {

    @SerializedName("code")
    @Expose
   val code: String? = null
    @SerializedName("message")
    @Expose
   val message: String? = null
    @SerializedName("data")
    @Expose
   val data: LikeResponseData? = null
}