package com.hiddle.app.model.UserModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class UserPreferences {
    @SerializedName("language")
    @Expose
     val language: String? = null

    @SerializedName("distance_in")
    @Expose
     val distanceIn: String? = null

    @SerializedName("show_age")
    @Expose
     val showAge: String? = null

    @SerializedName("share_location")
    @Expose
     val shareLocation: String? = null
}