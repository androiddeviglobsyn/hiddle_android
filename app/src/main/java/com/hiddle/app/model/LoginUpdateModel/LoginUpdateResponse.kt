package com.hiddle.app.model.LoginUpdateModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LoginUpdateResponse {
    @SerializedName("code")
    @Expose
    val code: String? = null

    @SerializedName("message")
    @Expose
    val message: String? = null
}