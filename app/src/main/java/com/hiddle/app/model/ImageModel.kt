package com.hiddle.app.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ImageModel{

    @SerializedName("is_new")
    @Expose
    var is_new: String = ""


    @SerializedName("is_selected")
    @Expose
    var is_selected: Boolean = false

    @SerializedName("order_id")
    @Expose
    var order_id: String = ""



    @SerializedName("_id")
    @Expose
    var _id: String = ""

  /*  @SerializedName("old_image")
    @Expose
    var old_image: String = ""*/


    @SerializedName("new_image")
    @Expose
    var new_image: String = ""


}