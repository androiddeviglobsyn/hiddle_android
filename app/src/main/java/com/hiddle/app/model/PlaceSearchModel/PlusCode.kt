package com.hiddle.app.model.PlaceSearchModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class PlusCode {
    @SerializedName("compound_code")
    @Expose
    private var compoundCode: String? = null

    @SerializedName("global_code")
    @Expose
    private var globalCode: String? = null

    fun getCompoundCode(): String? {
        return compoundCode
    }

    fun setCompoundCode(compoundCode: String?) {
        this.compoundCode = compoundCode
    }

    fun getGlobalCode(): String? {
        return globalCode
    }

    fun setGlobalCode(globalCode: String?) {
        this.globalCode = globalCode
    }

}