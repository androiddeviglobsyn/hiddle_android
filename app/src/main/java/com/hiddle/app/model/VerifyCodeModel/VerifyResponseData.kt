package com.hiddle.app.model.VerifyCodeModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class VerifyResponseData {

    @SerializedName("notification")
    @Expose
    val notification: VerifyNotification? = null

    @SerializedName("preferences")
    @Expose
    val preferences: VerifyPreference? = null

    @SerializedName("images")
    @Expose
     val images: List<VerifyImageModel>? = null

    /*@SerializedName("images")
    @Expose
    val images: List<String>? = null*/

   /* @SerializedName("origins")
    @Expose
    val origins: String? = null*/

    @SerializedName("origins")
    @Expose
    private val origins: List<String>? = null

    @SerializedName("base_url")
    @Expose
    val base_url: String? = null

    @SerializedName("_id")
    @Expose
    val id: String? = null

    @SerializedName("social_id")
    @Expose
    val socialId: String? = null

    @SerializedName("insta_id")
    @Expose
    val instaId: String? = null

    @SerializedName("is_verify")
    @Expose
    val is_verify: String? = null

    @SerializedName("cc")
    @Expose
    val cc: String? = null

    @SerializedName("number")
    @Expose
    val number: String? = null

    @SerializedName("tokens")
    @Expose
    val tokens: List<VerifyToken>? = null

    @SerializedName("createdAt")
    @Expose
    val createdAt: String? = null

    @SerializedName("updatedAt")
    @Expose
    val updatedAt: String? = null

    @SerializedName("__v")
    @Expose
    val v: String? = null

    @SerializedName("email")
    @Expose
    val email: String? = null


    @SerializedName("age")
    @Expose
    val age: String? = null

    @SerializedName("gender")
    @Expose
    val gender: String? = null

    @SerializedName("username")
    @Expose
    val username: String? = null

    @SerializedName("height")
    @Expose
    val height: String? = null






}