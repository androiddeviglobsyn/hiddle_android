package com.hiddle.app.model.ChatMessageModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class ChatProfile {
    @SerializedName("_id")
    @Expose
     val id: String? = null

    @SerializedName("image")
    @Expose
    var image: String? = null

    @SerializedName("is_selected")
    @Expose
     val isSelected: Boolean? = null

    @SerializedName("order_id")
    @Expose
     val orderId: String? = null
}