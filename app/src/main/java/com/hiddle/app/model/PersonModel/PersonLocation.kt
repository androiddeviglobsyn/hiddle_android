package com.hiddle.app.model.PersonModel

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class PersonLocation {
    @SerializedName("latitude")
    @Expose
     val latitude: String? = null

    @SerializedName("longitude")
    @Expose
     val longitude: String? = null
}