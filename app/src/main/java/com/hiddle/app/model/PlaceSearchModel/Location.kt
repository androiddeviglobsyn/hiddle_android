package com.hiddle.app.model.PlaceSearchModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Location {
    @SerializedName("lat")
    @Expose
    public val lat: Double? = null

    @SerializedName("lng")
    @Expose
    public val lng: Double? = null
}